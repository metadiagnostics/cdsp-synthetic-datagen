/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.serializer;

import java.io.OutputStream;
import java.util.Map;

import org.socraticgrid.clinical.document.DocumentFormat;

/**
 * Interface contract for the serialization of a clinical model or
 * datatype. Implement this interface to extend the framework with
 * new serializers. 
 * 
 * @author Claude Nanjo
 *
 * @param <T>
 * @param <C>
 */
public interface Serializer<T, C extends SerializationContext> {
	
	/**
	 * Serializes model using passed in document format and
	 * output stream. This method supports a serialization context argument
	 * for cross model dependencies typical in graph or hierarchical structures.
	 * This method will throw a SerializationException if the format is not 
	 * supported by the serializer.
	 * 
	 * @param model
	 * @param format
	 * @param outputStream
	 */
	public void serialize(T model, DocumentFormat format, OutputStream outputStream, C serializationContext);
	
	/**
	 * Serializes model using passed in document format and
	 * output stream. This method will throw a SerializationException
	 * if the format is not supported by the serializer.
	 * 
	 * @param model
	 * @param format
	 * @param outputStream
	 */
	public void serialize(T model, DocumentFormat format, OutputStream outputStream);
	
	/**
	 * Initializes/resets serializer state
	 */
	public void initialize();
	
	/**
	 * Initializes/resets serializer state
	 */
	public void initialize(Map<String, Object> parameters);
	
	/**
	 * Frees resources tied by this serializer
	 * 
	 * @param parameters
	 */
	public void tearDown();
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/