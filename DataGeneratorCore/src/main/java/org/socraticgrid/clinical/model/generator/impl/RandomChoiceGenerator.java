/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator.impl;

import java.util.Random;

public class RandomChoiceGenerator {
	
	private Random random;
	private int itemCount;
	
	public RandomChoiceGenerator() {
		random = new Random(System.currentTimeMillis());
	}
	
	/**
	 * Zero based index. Returns any number between
	 * 0 and itemCount - 1
	 * 
	 */
	public int getChoiceNumber() {
		return random.nextInt(itemCount);
	}
	
	public void setItemCount(int count) {
		this.itemCount = count;
	}
	
	public int getItemCount() {
		return itemCount;
	}
	
	public <T extends Enum<T>> T getEnumerationItem(Class<T> enumType) {
		itemCount = enumType.getEnumConstants().length;
		return enumType.getEnumConstants()[getChoiceNumber()];
	}
	
}