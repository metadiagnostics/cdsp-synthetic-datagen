/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator.impl;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.GenderGenerator;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class DiscreteGenderDistributionGenerator implements GenderGenerator {
	
	private DiscreteProbabilityDistribution<String> distribution;

	public DiscreteGenderDistributionGenerator() {
	}
	
	public DiscreteGenderDistributionGenerator(double percentMale) {
		DiscreteItemProbabilityPair<String> male = new DiscreteItemProbabilityPair<String>("MALE", percentMale);
		DiscreteItemProbabilityPair<String> female = new DiscreteItemProbabilityPair<String>("FEMALE", 1-percentMale);
		List<DiscreteItemProbabilityPair<String>> genders = new ArrayList<DiscreteItemProbabilityPair<String>>();
		genders.add(male);
		genders.add(female);
		distribution = new DiscreteProbabilityDistribution<String>(genders);
	}

	public AdministrativeGender getNextGender() {
		return AdministrativeGender.valueOf(distribution.getSample().toUpperCase());
	}

	public DiscreteProbabilityDistribution<String> getDistribution() {
		return distribution;
	}

	public void setDistribution(DiscreteProbabilityDistribution<String> distribution) {
		this.distribution = distribution;
	}
}