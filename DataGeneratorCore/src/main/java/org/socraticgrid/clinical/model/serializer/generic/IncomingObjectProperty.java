/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.serializer.generic;

/**
 * Class represents a partial RDF statement whose subject and predicate
 * are known but whose object IRI is yet to be specified. The 'incoming'
 * edge is generally grouped by the RDF statement's object type. This is 
 * represented by the statement group label which generally is the full 
 * class name of the RDF statement's object.
 * 
 * @author Claude Nanjo
 *
 * @param <T>
 */
public class IncomingObjectProperty {
	
	private String subjectIri;
	private String predicateIri;
	private String statementGroupLabel;
	
	public IncomingObjectProperty(String subjectIri, String predicateIri, String statementGroupLabel) {
		this.subjectIri = subjectIri;
		this.predicateIri = predicateIri;
		this.statementGroupLabel = statementGroupLabel;
	}
	
	/**
	 * Returns the subject of the RDF statement as
	 * a string representation of the subject IRI
	 * 
	 * Must be a valid IRI
	 * 
	 * @return
	 */
	public String getSubject() {
		return subjectIri;
	}
	
	/**
	 * Sets the subject of this RDF statement as a string
	 * representation of the subject IRI.
	 * 
	 * Must be a valid IRI
	 * 
	 * @param subjectIri
	 */
	public void setSubject(String subjectIri) {
		this.subjectIri = subjectIri;
	}
	
	/**
	 * Returns the predicate of this RDF statement as a
	 * string representation of the predicate IRI.
	 * 
	 * Must be a valid IRI
	 * 
	 * @return
	 */
	public String getPredicate() {
		return predicateIri;
	}
	
	/**
	 * Sets the predicate of this RDF statement as a string
	 * representation of the predicate IRI.
	 * 
	 * Must be a valid IRI
	 * 
	 * @param subjectIri
	 */
	public void setPredicate(String predicateIri) {
		this.predicateIri = predicateIri;
	}
	
	/**
	 * Returns the statement group label associated with this
	 * RDF statement. A statement group label allows the grouping
	 * of RDF statements.
	 * 
	 * Generally, this is the full class name of the statement's object
	 * 
	 * @param subjectIri
	 */
	public String getStatementGroupLabel() {
		return statementGroupLabel;
	}
	
	/**
	 * Associates this RDF statement to a group represented by
	 * the statement group label. A statement group label allows the grouping
	 * of RDF statements.
	 * 
	 * Generally, this is the full class name of the statement's object
	 * 
	 * @param subjectIri
	 */
	public void setStatementGroupLabel(String statementGroupLabel) {
		this.statementGroupLabel = statementGroupLabel;
	}
	
	/**
	 * Convenience method that returns a statement group label that is derived from
	 * the canonical class name of the object of the statement.
	 * 
	 * @param object
	 * @return
	 */
	public static String buildStatementGroupLabelFromClass(Object object) {
		return object.getClass().getCanonicalName();
	}
	
	public String toString() {
		return "[" + subjectIri + "," + predicateIri + "," + statementGroupLabel + "]";
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/