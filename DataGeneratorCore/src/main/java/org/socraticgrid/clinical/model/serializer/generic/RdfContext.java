/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.serializer.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.socraticgrid.clinical.model.serializer.SerializationContext;

/**
 * Class represents a serialization context that can be used to 
 * convey information from a parent concept to a child concept in
 * a depth-first graph serialization approach where each node is responsible
 * for its own serialization.
 * <p>
 * For instance, the serializer may generate all outbound datatype predicates
 * for the given node. Outbound object predicates in a depth-first approach, 
 * on the other hand, cannot be generated as the identity of the object has not yet been 
 * established. Through the context, the upstream node passes a downstream node 
 * a partial 'incoming' predicate (an IncomingObjectProperty) that includes a 
 * reference to itself (the statement's subject) as well as the predicate IRI. 
 * The child node completes the predicate once its identity has been established.
 * It becomes the object of the incoming statement contained in the context.
 * 
 * @author Claude Nanjo
 *
 */
public class RdfContext implements SerializationContext {
	
	private Map<String,List<IncomingObjectProperty>> incomingObjectProperty;
	
	public RdfContext() {
		incomingObjectProperty = new HashMap<String, List<IncomingObjectProperty>>();
	}
	
	/**
	 * Returns the incoming properties defined in this context grouped by
	 * statement group label as a map.
	 * 
	 * @return
	 */
	public Map<String, List<IncomingObjectProperty>> getIncomingObjectProperties() {
		return incomingObjectProperty;
	}
	
	/**
	 * Sets a map of grouped IncomingObjectProperties by statement label
	 * 
	 * @param incomingObjectProperties
	 */
	public void setIncomingObjectProperties(Map<String, List<IncomingObjectProperty>> incomingObjectProperties) {
		this.incomingObjectProperty = incomingObjectProperties;
	}
	
	/**
	 * Adds an IncomingObjectProperty to this context.
	 * 
	 * @param property
	 */
	public void addIncomingProperty(IncomingObjectProperty property) {
		if(incomingObjectProperty.containsKey(property.getStatementGroupLabel())) {
			List<IncomingObjectProperty> list = incomingObjectProperty.get(property.getStatementGroupLabel());
			list.add(property);
		} else {
			List<IncomingObjectProperty> list = new ArrayList<IncomingObjectProperty>();
			list.add(property);
			incomingObjectProperty.put(property.getStatementGroupLabel(), list);
		}
	}
	
	/**
	 * Returns a list of incoming object properties that share the same statement group label.
	 * 
	 * @param statementGroupLabel
	 * @return
	 */
	public List<IncomingObjectProperty> getIncomingPropertiesForClass(String statementGroupLabel) {
		return (List<IncomingObjectProperty>) incomingObjectProperty.get(statementGroupLabel);
	}
	
	public String toString() {
		return incomingObjectProperty.toString();
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/