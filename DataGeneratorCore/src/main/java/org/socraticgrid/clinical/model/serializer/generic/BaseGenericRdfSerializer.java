/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.serializer.generic;

import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.Serializer;
import org.socraticgrid.common.jena.RdfModelBuilder;

public abstract class BaseGenericRdfSerializer<T> implements Serializer<T,RdfContext> {

	public static final String BASE_ONTOLOGY_IRI = "http://www.socraticgrid.org/owl/ontology";
	public static final String BASE_INSTANCE_IRI = "http://www.socraticgrid.org/rdf/instance/";
	
	private RdfModelBuilder model;
	
	/**
	 * Constructor creates a new Jena model for this serializer.
	 * Use this constructor if you wish to create a new Jena model for
	 * this serialization.
	 * 
	 */
	public BaseGenericRdfSerializer() {
		this.model = new RdfModelBuilder();
	}
	
	/**
	 * Constructor uses an existing Jena model for this serializer.
	 * Use this constructor if you wish to append statements to an existing
	 * Jena model.
	 * 
	 * @param model
	 */
	public BaseGenericRdfSerializer(RdfModelBuilder model) {
		this.model = model;
	}
	
	public void initialize(Map<String,Object> parameters) {
		initialize();
	}
	
	public void initialize() {
		this.model = new RdfModelBuilder();
	}
	
	public void tearDown() {
		this.model = null;
	}
	
	protected void buildDatatypeStatement(RdfModelBuilder model, String subjectIri, String predicate, Object value) {
		if(value != null) {
			model.addDataProperty(subjectIri, predicate, value.toString());
		}
	}
	
	protected void buildObjectStatement(RdfModelBuilder model, String subjectIri, String predicate, String objectIri){
		if(objectIri != null) {
			model.addObjectProperty(subjectIri, predicate, objectIri);
		}
	}
	
	public void serialize(T model, DocumentFormat format, OutputStream outputStream, RdfContext serializationContext) {
		
	}
	
	public void serialize(T model, DocumentFormat format, OutputStream outputStream) {
		serialize(model, format, outputStream, new RdfContext());
	}
	
	protected abstract String buildInstanceIri(String instanceId);
	
	protected abstract String buildInstanceIdentifier(T clinicalModel);
	
	protected abstract void buildRdfGraph(T model, DocumentFormat format, OutputStream outputStream, RdfContext context);
	
	protected void buildIncomingEdgesFromContext(T statementObject, RdfContext context, String instanceId) {
		List<IncomingObjectProperty> incomingPredicates = context.getIncomingPropertiesForClass(statementObject.getClass().getCanonicalName());
		if(incomingPredicates== null) {
			return;
		}
		for(IncomingObjectProperty property:incomingPredicates) {
			buildObjectStatement(model, property.getSubject(), property.getPredicate(), buildInstanceIri(instanceId));
		}
	}

	public RdfModelBuilder getModel() {
		return model;
	}

	public void setModel(RdfModelBuilder model) {
		this.model = model;
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/