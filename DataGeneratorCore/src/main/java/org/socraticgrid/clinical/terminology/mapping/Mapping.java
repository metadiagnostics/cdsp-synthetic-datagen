/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.terminology.mapping;


public class Mapping<T extends BaseProperty> {
	
	private T sourceModelProperty;
	private AtomicProperty targetModelProperty;
	private String mapper;
	
	public T getSourceModelProperty() {
		return sourceModelProperty;
	}
	public void setSourceModelProperty(T sourceModelProperty) {
		this.sourceModelProperty = sourceModelProperty;
	}
	public String getSourceModelPropertyLabel() {
		return sourceModelProperty.getPropertyLabel();
	}
	public String getSourceModelPropertyValue(Object subject) {
		return sourceModelProperty.getTransformedPropertyValue(subject);
	}
	public AtomicProperty getTargetModelProperty() {
		return targetModelProperty;
	}
	public void setTargetModelProperty(AtomicProperty targetModelProperty) {
		this.targetModelProperty = targetModelProperty;
	}
	public String getTargetModelPropertyLabel() {
		return targetModelProperty.getPropertyLabel();
	}
	public String getMapper() {
		return mapper;
	}
	public void setMapper(String mapper) {
		this.mapper = mapper;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/