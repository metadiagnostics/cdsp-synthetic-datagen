/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.terminology.mapping;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.commons.beanutils.PropertyUtils;
import org.socraticgrid.exception.SocraticGridRuntimeException;

public class AtomicProperty extends BaseProperty {
	
	private Class transformationClass;
	private String transformationMethod;
	
	public AtomicProperty() {
		super();
	}
	
	public AtomicProperty(String label) {
		setPropertyLabel(label);
	}

	public Class getTransformationClass() {
		return transformationClass;
	}

	public void setTransformationClass(Class transformationClass) {
		this.transformationClass = transformationClass;
	}

	public String getTransformationMethod() {
		return transformationMethod;
	}

	public void setTransformationMethod(String transformationMethod) {
		this.transformationMethod = transformationMethod;
	}
	
	public String getTransformedPropertyValue(Object subject) {
		try {
			Method noOp = transformationClass.getMethod(transformationMethod, String.class);
			Object aligner = transformationClass.newInstance();
			return (String)noOp.invoke(aligner, PropertyUtils.getProperty(subject, getPropertyLabel()));
		}catch(NoSuchMethodException nsme) {
			throw new SocraticGridRuntimeException("Could not find specified method " + transformationMethod, nsme);
		}catch(SecurityException se) {
			throw new SocraticGridRuntimeException("Method could not be accessed " + transformationMethod, se);
		}catch(InstantiationException ie) {
			throw new SocraticGridRuntimeException("Could not instantiate specified class " + transformationClass, ie);
		}catch(IllegalAccessException iae) {
			throw new SocraticGridRuntimeException("Could not access method " + transformationMethod, iae);
		}catch(InvocationTargetException ite) {
			throw new SocraticGridRuntimeException("Could not invoke method " + transformationMethod, ite);
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/