/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.terminology.mapping;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.socraticgrid.common.xml.XmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;

public class InMemoryLookup extends BaseAligner {
	
	public static final String ELEMENT_NAME_MAPPING = "mapping";
	public static final String ELEMENT_NAME_SOURCE = "source";
	public static final String ELEMENT_NAME_TARGET = "target";
	
	private String configurationFilePath;
	private Map<String,String> lookupMap = new HashMap<String,String>();
	
	public String lookup(String key) {
		return lookupMap.get(key);
	}
	
	public void addMapping(String key, String value) {
		lookupMap.put(key, value);
	}

	public String getConfigurationFilePath() {
		return configurationFilePath;
	}

	public void setConfigurationFilePath(String configurationFilePath) {
		this.configurationFilePath = configurationFilePath;
	}
	
	public void configure() {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(configurationFilePath);
		Document configuration = XmlUtils.loadDocumentFromStream(is);
		NodeList mappings = configuration.getElementsByTagName(ELEMENT_NAME_MAPPING);
		for(int i = 0; i < mappings.getLength(); i++) {
			Node mapping = mappings.item(i);
			NodeList children = mapping.getChildNodes();
			String source = null;
			String target = null;
			for(int j = 0; j < children.getLength(); j++) {
				Node child = children.item(j);
				if(child.getNodeName().equalsIgnoreCase(ELEMENT_NAME_SOURCE)) {
					source = child.getTextContent();
				} else if(child.getNodeName().equalsIgnoreCase(ELEMENT_NAME_TARGET)){
					target = child.getTextContent();
				}
				if(source != null && target != null) {
					addMapping(source, target);
				}
			}
		}
	}
}

/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/

