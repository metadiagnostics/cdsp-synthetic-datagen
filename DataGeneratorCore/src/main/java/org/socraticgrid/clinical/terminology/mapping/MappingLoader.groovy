/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.terminology.mapping

import org.socraticgrid.exception.SocraticGridRuntimeException

class MappingLoader {
	
	def mappingType
	
	List<Graph> graphs = new ArrayList<Graph>()
	
	def load(mappingConfiguration) {
		def mappingTemplate = new XmlSlurper().parse(mappingConfiguration);
		mappingType = mappingTemplate.mappingType.text()
		loadGraphs(mappingTemplate.graphs.graph)
		return mappingTemplate	
	}
	
	def loadGraphs(graphList) {
		graphList.each {
			graphItem -> Graph graph = new Graph();
			loadNodes(graph, graphItem.nodes.node)
			graphs.add(graph);
		}
	}
	
	def loadNodes(graph, nodeList) {
		nodeList.each {
			nodeItem -> Node node = new Node();
			node.setConcept(nodeItem.concept.text())
			node.setConceptClass(nodeItem.conceptClass.text())
			loadMappings(node, nodeItem.mappings.mapping)
			graph.addNode(node)
		}
	}
	
	def loadMappings(node, mappingList) {
		for(mapping in mappingList) {
			if(mapping.compositeSourceModelProperty.isEmpty()) {
				Mapping<AtomicProperty> mappingItem = new Mapping<AtomicProperty>();
				AtomicProperty sourceProperty = new AtomicProperty(mapping.sourceModelProperty.text())
				sourceProperty.setTransformationClass(Class.forName(mapping.sourceModelProperty.@mapper.text()))
				sourceProperty.setTransformationMethod(mapping.sourceModelProperty.@method.text())
				mappingItem.setSourceModelProperty(sourceProperty)
				mappingItem.setTargetModelProperty(new AtomicProperty(mapping.targetModelProperty.text()))
				node.addMapping(mappingItem)
			} else if(!mapping.compositeSourceModelProperty.isEmpty()){
				Mapping<CompositeProperty> mappingItem = new Mapping<CompositeProperty>()
				CompositeProperty compositeSourceProperty = new CompositeProperty()
				compositeSourceProperty.setTransformationClass(Class.forName(mapping.compositeSourceModelProperty.composition.@transformationClass.text()))
				compositeSourceProperty.setTransformationMethod(mapping.compositeSourceModelProperty.composition.@method.text())
				mapping.compositeSourceModelProperty.sourceModelProperty.each{ atomicProperty ->
					AtomicProperty sourceProperty = new AtomicProperty(atomicProperty.text())
					sourceProperty.setTransformationClass(Class.forName(atomicProperty.@mapper.text()))
					sourceProperty.setTransformationMethod(atomicProperty.@method.text())
					compositeSourceProperty.addAtomicProperty(sourceProperty)
				}
				mappingItem.setSourceModelProperty(compositeSourceProperty);
				mappingItem.setTargetModelProperty(new AtomicProperty(mapping.targetModelProperty.text()))
				node.addMapping(mappingItem)
			}
		}
	}
	
}
/* **********************************************************************************
 Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
	 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 * *********************************************************************************/
