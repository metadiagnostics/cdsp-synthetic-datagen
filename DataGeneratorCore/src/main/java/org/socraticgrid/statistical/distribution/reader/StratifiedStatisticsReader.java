/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.common.reader.CsvListReader;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;
import org.socraticgrid.statistical.distribution.NormalDistribution;

/**
 * Class representing a tabular representation of stratified probabilities. For instance,
 * it may represent the gender and race distribution of a military population stratified
 * by military service and rank.
 * <p>
 * Precondition: The first row of the table is assumed to be the header row.
 * 
 * @author Claude Nanjo
 *
 */
public class StratifiedStatisticsReader {
	
	private int[] stratumColumnIndices;
	private int stratumProbabilityColumnIndex;
	private Map<Class<?>, Object> generators;
	private CsvListReader csvReader;

	public StratifiedStatisticsReader(int stratumProbabilityColIndex, int[] stratumColIndices) {
		this.stratumProbabilityColumnIndex = stratumProbabilityColIndex;
		this.stratumColumnIndices = stratumColIndices;
	}
	
	/**
	 * Loads stratified statistics table definition and delimiter used between fields.
	 * 
	 * @param source
	 * @param delimiter
	 * @return
	 */
	public List<String[]> loadStratifiedStatistics(Object source, char delimiter) {
		csvReader = BaseCsvReader.readCsvFile(source, delimiter);
		return csvReader.getLines();
	}
	
	/**
	 * Method returns a discrete probability distribution for a given stratum. For instance,
	 * If the gender distribution for Army O1 ranks is 80% male and 20% female, a distribution 
	 * representing this gender breakdown will be returned.
	 * 
	 * @param categoryIndexColumns The columns representing the distribution variable values of interest
	 * @param row The stratum in question
	 * @param isPercent If true, will divide the number by 100.
	 * @return
	 */
	public DiscreteProbabilityDistribution<String> loadDiscreteGeneratorForRow(int[] categoryIndexColumns, int row, boolean isPercent) {
		List<DiscreteItemProbabilityPair<String>> categories = new ArrayList<DiscreteItemProbabilityPair<String>>();
		for(int index = 0; index < categoryIndexColumns.length; index++) {
			int column = categoryIndexColumns[index];
			String item = csvReader.getHeaderColumnName(column);
			Double value = null;
			if(isPercent) {
				value = csvReader.getPercentAsDouble(row, column);
			} else {
				value = csvReader.getDoubleValue(row, column);
			}
			DiscreteItemProbabilityPair<String> pair = new DiscreteItemProbabilityPair<String>(item, value);
			categories.add(pair);
		}
		return new DiscreteProbabilityDistribution<String>(categories);
	}
	
	/**
	 * Method returns a discrete probability distribution for a given stratum with the remaining percentage
	 * allocated to the 'other' column passed in. For instance,
	 * If the marital status for Army O1 ranks is 55% married, 45% will be assigned to the 'other' column of
	 * 'unmarried'.
	 * 
	 * @param categoryIndexColumns The columns representing the distribution variable values of interest
	 * @param row The stratum in question
	 * @param isPercent If true, will divide the number by 100.
	 * @return
	 */
	public DiscreteProbabilityDistribution<String> loadDiscreteGeneratorForRow(int[] categoryIndexColumns, String otherColumnName, int row, boolean isPercent) {
		List<DiscreteItemProbabilityPair<String>> categories = new ArrayList<DiscreteItemProbabilityPair<String>>();
		double otherProbability = 1.0;
		for(int index = 0; index < categoryIndexColumns.length; index++) {
			int column = categoryIndexColumns[index];
			String item = csvReader.getHeaderColumnName(column);
			Double value = null;
			if(isPercent) {
				value = csvReader.getPercentAsDouble(row, column);
			} else {
				value = csvReader.getDoubleValue(row, column);
			}
			otherProbability -= value;
			DiscreteItemProbabilityPair<String> pair = new DiscreteItemProbabilityPair<String>(item, value);
			categories.add(pair);
		}
		DiscreteItemProbabilityPair<String> pair = new DiscreteItemProbabilityPair<String>(otherColumnName, otherProbability);
		categories.add(pair);
		return new DiscreteProbabilityDistribution<String>(categories);
	}
	
	/**
	 * Method iterates through all rows of the spreadsheet and sums the probabilities
	 * associated with each stratum. Method then returns the overall strata probability distribution.
	 * 
	 * @param categoryIndexColumns
	 * @param row
	 * @param isPercent
	 * @return
	 */
	public DiscreteProbabilityDistribution<Stratum> loadDiscreteGeneratorForColumn(int[] categoryIndexColumns, boolean isPercent) {
		double sum = sumStrataProbabilities();
		List<DiscreteItemProbabilityPair<Stratum>> categories = new ArrayList<DiscreteItemProbabilityPair<Stratum>>();
		for(int index = 1; index < getLines().size(); index++) {
			String[] line = getLines().get(index);
			Stratum stratum = new Stratum(csvReader.getHeaderColumnNames(categoryIndexColumns), getStratumValues(line));
			Double value = csvReader.getDoubleValue(index, stratumProbabilityColumnIndex)/sum; // TODO May not be wise. handle gracefully or fail hard?
			DiscreteItemProbabilityPair<Stratum> pair = new DiscreteItemProbabilityPair<Stratum>(stratum, value);
			categories.add(pair);
		}
		return new DiscreteProbabilityDistribution<Stratum>(categories);
	}
	
	/**
	 * Method returns the probability distribution for the given stratum as a normal
	 * distribution. For instance, if the Army O1 rank stratum specifies an average age
	 * of 23 and a standard deviation of 0.5, then the corresponding normal distribution
	 * is returned.
	 * 
	 * @param meanColumnIndex The mean for the variable (e.g., mean age)
	 * @param stdColumnIndex The standard deviation for the variable (age standard dev)
	 * @param row The stratum in question
	 * @return
	 */
	public NormalDistribution loadNormalDistribution(int meanColumnIndex, int stdColumnIndex, int row) {
		double mean = csvReader.getDoubleValue(row, meanColumnIndex);
		double std = csvReader.getDoubleValue(row, stdColumnIndex);
		return new NormalDistribution(mean, std);
	}
	
	/**
	 * Method returns true if the values passed in represent
	 * a stratum key that exists in the spreadsheet.
	 * 
	 * @param stratum
	 * @return
	 */
	public boolean validStratum(String[] stratum) {
		int index = getStratumIndex(stratum);
		return index >= 0;
	}
	
	/**
	 * Method sums all values in a column.
	 * <p>
	 * @param columnIndex
	 * @return
	 */
	public double sumValues(int columnIndex) {
		double sum = 0;
		for(int index = 1; index < getLines().size(); index++) {
			sum += csvReader.getDoubleValue(index, columnIndex);
		}
		return sum;
	}
	
	public double sumStrataProbabilities() {
		return sumValues(this.stratumProbabilityColumnIndex);
	}
	
	/**
	 * Returns the index of the line of the CSV file that contains the stratum.
	 * If the stratum is not found, method returns an index of -1.
	 * 
	 * @param stratum
	 * @return
	 */
	public int getStratumIndex(String[] stratum) {
		int foundRowIndex = -1;
		int index = 0;
		for(String[] line : getLines()) {
			String[] stratumValues = getStratumValues(line);
			if(arrayCaseInsensitiveStringValuesEqual(stratum, stratumValues)) {
				foundRowIndex = index;
				break;
			}
			index ++;
		}
		return foundRowIndex;
	}
	
	/**
	 * Returns the stratum values for a given line. For instance,
	 * this may return the Military Service and Rank for the given
	 * line if these attributes represent the stratification key.
	 * 
	 * @param line
	 * @return
	 */
	public String[] getStratumValues(String[] line) {
		String[] values = new String[stratumColumnIndices.length];
		for(int index = 0; index < stratumColumnIndices.length; index++) {
			values[index] = line[stratumColumnIndices[index]];
		}
		return values;
	}
	
	/**
	 * Method returns true if the values in the arrays are equal.
	 * Method performs a case insensitive string comparison.
	 * 
	 * @param source
	 * @param destination
	 * @return
	 */
	public boolean arrayCaseInsensitiveStringValuesEqual(String[] source, String[] destination) {
		boolean same = true;
		for(int index = 0; index < source.length; index++) {
			same = same && (source[index].equalsIgnoreCase(destination[index]));
		}
		return same;
	}
	
	/**
	 * Method returns an array that represents the column indices
	 * for the stratum keys. A stratum definition may consist of
	 * one or more columns in a spreadsheet.
	 * 
	 * @return
	 */
	public int[] getStratumColumnIndices() {
		return stratumColumnIndices;
	}

	/**
	 * Method returns the index in the spreadsheet that represents the
	 * distribution of each stratum value.
	 * 
	 * @return
	 */
	public int getStratumProbabilityColumnIndex() {
		return stratumProbabilityColumnIndex;
	}
	
	/**
	 * Method returns each line in the CSV file as a list of string arrays.
	 * @return
	 */
	public List<String[]> getLines() {
		return csvReader.getLines();
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/