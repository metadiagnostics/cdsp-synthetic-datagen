/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Class represents a named set. If set A is subsumed by set B
 * the we say B is a 'parent' of A and A is a child of B. The area
 * of set A divided by the area of the universe represents the probability
 * of landing in set A.
 * 
 * In the context of the synthetic data generator, a set represents a collection
 * of individual that share a common clinical characteristic.
 * 
 * @author Claude Nanjo
 *
 */
public class VennSet {
	
	private Set<VennSet> parentList;
	private Set<VennSet> children;
	private Map<String, VennSet> parentMap;
	private String name;
	private double probability;
	private Generator generator;
	
	/**
	 * No-arg constructor
	 * 
	 * @param name
	 * @param probability
	 */
	protected VennSet() {
		parentList = new TreeSet<VennSet>(comparator);
		children = new TreeSet<VennSet>(comparator);
		parentMap = new HashMap<String, VennSet>();
	}
	
	/**
	 * Constructor creates a set with the given name and
	 * probability.
	 * 
	 * @param name
	 * @param probability
	 */
	public VennSet(String name, double probability) {
		this.name = name;
		this.probability = probability;
		parentList = new TreeSet<VennSet>(comparator);
		children = new TreeSet<VennSet>(comparator);
		parentMap = new HashMap<String, VennSet>();
	}
	
	/**
	 * Constructor creates a set with the given name and
	 * probability.
	 * 
	 * @param name
	 * @param probability
	 */
	public VennSet(String name, double probability, VennSet ...sets) {
		this(name, probability);
		for(VennSet set : sets) {
			addParent(set);
		}
	}
	
	/**
	 * Method returns the name for this set.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Setter method for the name of the set
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Method returns the probability of being a member of
	 * this set. It is the cardinality of this set divided
	 * by the cardinality of the universe.
	 * 
	 * @return
	 */
	public double getProbability() {
		return probability;
	}
	
	/**
	 * Method sets the probability associated with this set.
	 * 
	 * @param probability
	 */
	public void setProbability(double probability) {
		this.probability = probability;
	}
	
	/**
	 * Method indicates that this set is a strict subset of the
	 * parent set.
	 * 
	 * @param parent
	 */
	public void addParent(VennSet parent) {
		parentList.add(parent);
		parentMap.put(parent.name, parent);
		parent.addChild(this);
	}
	
	/**
	 * Method indicates that this set is parent of the 
	 * child set. Conversely, the child set is a strict subset 
	 * of the parent set.
	 * 
	 * @param child
	 */
	protected void addChild(VennSet child) {
		if(!children.contains(child)) {
			children.add(child);
		}
	}
	
	/**
	 * Method returns all sets that subsume the current set.
	 * For instance, if AB is the intersection of A and B, then
	 * getParents() will return a list containing both sets A and
	 * B.
	 * 
	 * @return
	 */
	public Set<VennSet> getParents() {
		return parentList;
	}
	
	/**
	 * Method returns all sets that this set subsumes. That is, if
	 * A subsumes both AB and AC, then both sets AB and AC will be returned.
	 * 
	 * @return
	 */
	public Set<VennSet> getChildren() {
		return children;
	}
	
	
	
	/**
	 * Method returns true if this set is subsumed by more than
	 * one set (i.e., this set has more than one 'parent' set).
	 * 
	 * @return
	 */
	public boolean isIntersection() {
		return parentList.size() > 1;
	}
	
	/**
	 * Method returns true if this set has the superset argument
	 * as a parent. Method does not move up the containment hierarchy
	 * at this time.
	 * 
	 * @param superSet
	 * @return
	 */
	public boolean isSubsumedBy(VennSet superSet) {
		boolean subsumed = false;
		if(parentList.contains(superSet)) {
			subsumed = true;
		} else if (!superSet.isTopLevelSet() && parentList.containsAll(getTopLevelParents(superSet))) {
			subsumed = true;
		}
		return subsumed;
	}
	
	/**
	 * Returns true is subset is a proper subset of this set.
	 * 
	 * @param subset
	 * @return
	 */
	public boolean subsumes(VennSet subset) {
		return subset.isSubsumedBy(this);
	}
	
	/**
	 * Method searches for a parent with name argument.
	 * Returns null if no match is found.
	 * 
	 * @param parentName
	 * @return
	 */
	public VennSet getParent(String parentName) {
		return parentMap.get(parentName);
	}
	
	/**
	 * Method returns the number of sets that subsumes this set.
	 * 
	 * @return
	 */
	public int parentCount() {
		return parentList.size();
	}
	
	public void sortChildren() {
		for(VennSet child:children) {
			child.sortChildren();
		}
		Set<VennSet> temp = new TreeSet<VennSet>(comparator);
		for(VennSet item : children) {
			temp.add(item);
		}
		children = temp;
	}
	
	/**
	 * Method moves up the containment hierarchy and returns all top level sets
	 * (That is sets that are not subsumed by any other set).
	 * 
	 * @param set
	 * @return
	 */
	public static Set<VennSet> getTopLevelParents(VennSet set) {
		Set<VennSet> parents = new TreeSet<VennSet>(comparator);
		if(!set.isTopLevelSet()) {
			getParents(set, parents);
		}
		return parents;
	}
	
	/**
	 *  Method moves up the containment hierarchy and returns all top level sets
	 * (That is sets that are not subsumed by any other set).
	 * 
	 * @param set
	 * @param list
	 */
	public static void getParents(VennSet set, Set<VennSet> list) {
		Set<VennSet> parents = set.getParents();
		for(VennSet parent:parents) {
			if(parent.isTopLevelSet()) {
				list.add(parent);
			} else {
				getParents(parent, list);
			}
		}
	}
	
	/**
	 * Method associates a generator algorithm to this set.
	 * 
	 * @param generator
	 */
	public void setGenerator(Generator generator) {
		this.generator = generator;
		this.generator.setVennSet(this);
	}
	
	/**
	 * Method returns the generator algorithm associated with this set.
	 * 
	 * @return
	 */
	public Generator getGenerator() {
		return generator;
	}
	
	/**
	 * Top level sets are direct children of 'Universe'
	 * 
	 * @return
	 */
	public boolean isTopLevelSet() {
		return (getParents().size() == 1 && getParents().iterator().next().getName().equals("Universe")) || getParents().size() == 0;
	}
	
	/**
	 * Method pretty-prints this set.
	 */
	public String toString() {
		return name + "(" + probability + ", " + parentList.size() + ", " + children.size() + ")";
	}
	
	public static List<Set<VennSet>> generatePowerSet(List<VennSet> set) {
		int setCardinality = set.size();
		int powerSetCardinality = 1 << setCardinality; // 2^|set|
		List<Set<VennSet>> powerSet = new ArrayList<Set<VennSet>>();
		for ( int i = 0; i < powerSetCardinality; i++ )
		{
		    Set<VennSet> subset = new TreeSet<VennSet>(VennSet.comparator);
			for ( int j = 0; j < setCardinality; j++) {
//				System.out.println("i=" + i +",j=" + j + ", (i >> j) & 1) " + ((i >> j) & 1));
		        if ( ((i >> j) & 1) == 1 ) {
		        	subset.add(set.get(j));
		        }
		    }
			powerSet.add(subset);
		}
		return powerSet;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VennSet other = (VennSet) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name)) {
			return false;
		} else if(other.parentCount() != this.parentCount()) {
			return false;
		}
	
		return true;
	}

	public static Comparator<VennSet> comparator = new Comparator<VennSet>() {
		/**
		 * Method used to rank-order sets. Compare algorithm works as follows:
		 * 1. Compare the cardinalities of the sets.
		 * 2. If the cardinalities, order sets lexicographically by name.
		 */
		public int compare(VennSet first, VennSet second) {
			int diff = 0;
			if(first != second) {
				diff =  first.parentCount() - second.parentCount();
				if(diff == 0) {
					diff = first.getName().compareTo(second.getName());
				}
			}
			//System.out.println("Comparing " + first + " to " + second +". Result " + diff);
			return diff;
		}
	};
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/