/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class AutomatonRunner {
	
	private Vertex start;
	
	public AutomatonRunner(Vertex start) {
		this.start = start;
	}
	
	public List generate() {
		return generate(null);
	}

	public List generate(Map<String,Object> configurationParameters) {
		List clinicalStatements = new ArrayList();
		try {
			depthsFirstGeneration(start, clinicalStatements,configurationParameters);
		} catch(Exception e) {
			throw new SocraticGridRuntimeException("Error running the generating automaton", e);
		}
		return clinicalStatements;
	}
	
	protected void depthsFirstGeneration(Vertex parent, List clinicalStatements, Map<String,Object> configurationParameters) {
		DiscreteProbabilityDistribution<String> discreteDistribution = parent.getDiscreteDistribution();
		Edge selectedEdge = parent.getOutgoingEdgeWithSignature(discreteDistribution.getSample());
//		System.out.println(selectedEdge);
		Vertex child = selectedEdge.getDestination();
		if(child.getName().equals(End.END) || child.getName().equals(Start.START)){
			// Do nothings
		} else {
			if(!child.isLeaf()) {
				depthsFirstGeneration(child, clinicalStatements, configurationParameters);
			} 
			clinicalStatements.addAll(child.generate(configurationParameters));
		}
	}
}