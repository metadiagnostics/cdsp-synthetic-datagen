/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

/**
 * Class represents a node in a directed graph. The vertex class tracks
 * incoming and outgoing edges that link it to other nodes.
 * 
 * @author Claude Nanjo
 *
 */
public class Vertex {
	
	private List<Edge> incomingEdges;
	private List<Edge> outgoingEdges;
	private Double probability;
	private String name;
	private Generator generator;
	private DiscreteProbabilityDistribution<String> discreteDistribution;
	
	/**
	 * Creates a new node with the given node name
	 * 
	 * @param name The name of the node
	 */
	public Vertex(String name) {
		this(name, null);
	}
	
	public Vertex(String name, Generator generator) {
		incomingEdges = new ArrayList<Edge>();
		outgoingEdges = new ArrayList<Edge>();
		this.name = name;
		this.generator = generator;
	}
	
	/**
	 * Return the name of the node
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Sets the name for the node to name.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Return the probability associated
	 * with this vertex.
	 * @return
	 */
	public Double getProbability() {
		return probability;
	}
	
	/**
	 * Sets the probability associated with
	 * this vertex
	 * 
	 * @param probability
	 */
	public void setProbability(Double probability) {
		this.probability = probability;
	}

	/**
	 * Adds a new incoming edge to this vertex.
	 * 
	 * @param incomingEdge
	 * @return
	 */
	public Vertex addIncomingEdge(Edge incomingEdge) {
		incomingEdges.add(incomingEdge);
		return this;
	}
	
	/**
	 * Adds a new outgoing edge to this vertex.
	 * 
	 * @param outgoingEdge
	 * @return
	 */
	public Vertex addOutgoingEdge(Edge outgoingEdge) {
		outgoingEdges.add(outgoingEdge);
		return this;
	}
	
	/**
	 * Returns all incoming edges to this vertex.
	 * 
	 * @return
	 */
	public List<Edge> getIncomingEdges() {
		return incomingEdges;
	}
	
	/**
	 * Returns all outgoing edges from the vertex.
	 * 
	 * @return
	 */
	public List<Edge> getOutgoingEdges() {
		return outgoingEdges;
	}
	
	/**
	 * Returns the number of incoming edges.
	 * 
	 * @return
	 */
	public int getIncomingEdgeCount() {
		return incomingEdges.size();
	}
	
	/**
	 * Returns the number of outgoing edges.
	 * 
	 * @return
	 */
	public int getOutgoingEdgeCount() {
		return outgoingEdges.size();
	}
	
	public void setGenerator(Generator generator) {
		this.generator = generator;
	}
	
	public Generator getGenerator() {
		return generator;
	}
	
	/**
	 * Runs the generator
	 */
	public List generate(Map<String,Object> configurationParameters) {
		if(generator == null) {
			//generator = new NoOpGenerator(this);
			throw new IllegalStateException("No generator configured for this vertex. Generator is null.");
		}
		return generator.generate(configurationParameters);
	}
	
	public void generateDiscreteProbabilityFromEdges() {
		if(getOutgoingEdgeCount() == 0) {
			return;
		}
		List<DiscreteItemProbabilityPair<String>> masterItemList = new ArrayList<DiscreteItemProbabilityPair<String>>();
		for(Edge edge : getOutgoingEdges()) {
			masterItemList.add(new DiscreteItemProbabilityPair<String>(edge.toString(), edge.getProbability()));
		}
		discreteDistribution = new DiscreteProbabilityDistribution<String>(masterItemList);
	}
	
	public void setDiscreteDistribution(DiscreteProbabilityDistribution<String> distribution) {
		this.discreteDistribution = distribution;
	}
	
	public DiscreteProbabilityDistribution<String> getDiscreteDistribution() {
		return this.discreteDistribution;
	}
	
	public Edge getOutgoingEdgeWithSignature(String signature) {
		Edge found = null;
		for(Edge edge : getOutgoingEdges()) {
			if(edge.toString().equals(signature)) {
				found = edge;
				break;
			}
		}
		return found;
	}
	
	public String toString() {
		return name;
	}
	
	/**
	 * True if the vertex has no outgoing edges
	 * 
	 * @return
	 */
	public boolean isLeaf() {
		return getOutgoingEdgeCount() == 0;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/