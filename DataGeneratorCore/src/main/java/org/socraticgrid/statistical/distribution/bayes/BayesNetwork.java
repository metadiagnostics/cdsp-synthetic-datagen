/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//import org.eclipse.recommenders.jayes.BayesNet;
//import org.eclipse.recommenders.jayes.BayesNode;
//import org.eclipse.recommenders.jayes.inference.IBayesInferer;
//import org.eclipse.recommenders.jayes.inference.junctionTree.JunctionTreeAlgorithm;

public class BayesNetwork {

	public BayesNetwork() {
		// TODO Auto-generated constructor stub
	}
	
//	public double[] doSomething() {
//		BayesNet net = new BayesNet();
//		BayesNode temp = net.createNode("Temp");
//		temp.addOutcomes("Low", "Medium", "High");
//		temp.setProbabilities(0.2, 0.6, 0.2);
//		
//		BayesNode sweater = net.createNode("Sweater");
//		sweater.addOutcomes("True", "False");
//		sweater.setParents(Arrays.asList(temp));
//		
//		sweater.setProbabilities(
//			0.9, 0.1, // temp = 'Low'
//			0.7, 0.3, // temp = 'Medium'
//			0.2, 0.8  // temp = 'High'
//		);
//		
//		IBayesInferer inferer = new JunctionTreeAlgorithm();
//		inferer.setNetwork(net);
//
//		Map<BayesNode,String> evidence = new HashMap<BayesNode,String>();
//		evidence.put(temp, "Low");
//		inferer.setEvidence(evidence);
//
//		return inferer.getBeliefs(sweater);
//	}
//	
//	public void example() {
//		
//		BayesNet net = new BayesNet();
//		BayesNode a = net.createNode("a");
//		a.addOutcomes("true", "false");
//		a.setProbabilities(0.2, 0.8);
//
//		BayesNode b = net.createNode("b");
//		b.addOutcomes("one", "two", "three");
//		b.setParents(Arrays.asList(a));
//
//		b.setProbabilities(
//		  0.1, 0.4, 0.5, // a == true
//		  0.3, 0.4, 0.3 // a == false
//		);
//
//		BayesNode c = net.createNode("c");
//		c.addOutcomes("true", "false");
//		c.setParents(Arrays.asList(a, b));
//		c.setProbabilities(
//		  // a == true
//		  0.1, 0.9, // b == one
//		  0.0, 1.0, // b == two
//		  0.5, 0.5, // b == three
//		  // a == false
//		  0.2, 0.8, // b == one
//		  0.0, 1.0, // b == two
//		  0.7, 0.3 // b == three
//		);
//		
//		IBayesInferer inferer = new JunctionTreeAlgorithm();
//		inferer.setNetwork(net);
//
//		Map<BayesNode,String> evidence = new HashMap<BayesNode,String>();
//		evidence.put(a, "false");
//		evidence.put(b, "three");
//		inferer.setEvidence(evidence);
//
//		double[] beliefsC = inferer.getBeliefs(c);
//		//This gives us the probability distribution P(c | a = ���false���, b =���three���). 
//	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/