/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import java.io.Reader;
import java.util.List;

import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;

public interface CsvReader {
	public boolean hasHeader();
	public void setHeaderFlag(boolean fileHasHeader);
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvFile(String filePath, char delimiter);
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvResourceFolder(String resourcePath, char delimiter);
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvInputSteam(String inputStream, char delimiter);
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvReader(Reader reader, char delimiter);
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/