/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Represents the universe of all sets.
 * 
 * @author Claude Nanjo
 *
 */
public class Universe extends VennSet{
	
	public static final String UNIVERSE = "Universe";
	
	private Map<Set<VennSet>,VennSet> setMap; 
	
	/**
	 * Constructor defines a new universe.
	 */
	public Universe() {
		super();
		setMap = new HashMap<Set<VennSet>, VennSet>();
		setName(UNIVERSE);
		setProbability(1.0);
	}
	
	/**
	 * Method adds a set to the universe.
	 * 
	 * @param set
	 */
	public void addSet(VennSet set) {
		addChild(set);
	}
	
	/**
	 * Method creates a new set with name and probability.
	 * Adds this set to the universe.
	 * 
	 * @param name
	 * @param probability
	 * @return
	 */
	public VennSet createSet(String name, double probability) {
		VennSet set = new VennSet(name, probability);
		addChild(set);
		return set;
	}
	
	public VennSet createTopLevelSet(String name, double probability) {
		VennSet set = new VennSet(name, probability);
		set.addParent(this);
		return set;
	}
	
	/**
	 * Method creates a new set with name, probability, and parents
	 * and adds set to the universe.
	 * 
	 * @param name
	 * @param probability
	 * @param parents
	 * @return
	 */
	public VennSet createSet(String name, double probability, VennSet ... parents) {
		VennSet set = new VennSet(name, probability);
		for(VennSet parent : parents) {
			set.addParent(parent);// TODO Add an addAll method to set.
		}
		addChild(set);
		Set<VennSet> parentSet = new TreeSet<VennSet>(comparator);
		parentSet.addAll(Arrays.asList(parents));
		setMap.put(getTopLevelParents(parentSet), set);
		return set;
	}
	
//	/**
//	 * Returns the set at the given index of the list of sets contained by the universe.
//	 * Index must be a valid index and cannot exceed the size of the list of all sets for
//	 * this universe.
//	 * 
//	 * @param index
//	 * @return
//	 */
//	public VennSet get(int index) {
//		return getChildren().get(index);
//	}
	
	/**
	 * Method returns all the top level sets contained within this universe.
	 * A top-level set is a set that has no parents apart from the Universe.
	 * 
	 * @return
	 */
	public List<VennSet> getTopLevelSets() {
		List<VennSet> topLevel = new ArrayList<VennSet>();
		for(VennSet set : getChildren()) {
			if(set.isTopLevelSet()) {
				topLevel.add(set);
			}
		}
		return topLevel;
	}
	
	/**
	 * Returns all sets that have parents as their parents.
	 * 
	 * @param parents
	 * @return
	 */
	public VennSet findIntersection(Set<VennSet> parents) {
		//Collections.sort(parents);
		return setMap.get(parents);
	}
	
	/**
	 * Returns the collection of top level parents without duplications (direct children of
	 * universe) for the sets passed as arguments.
	 * @param parents
	 * @return
	 */
	public static Set<VennSet> getTopLevelParents(Set<VennSet> parents) {
		Set<VennSet> topLevelParents = new TreeSet<VennSet>(comparator);
		getTopLevelParents(parents, topLevelParents);
		return topLevelParents;
	}
	
	public static void getTopLevelParents(Set<VennSet> parents, Set<VennSet> topLevelParents) {
		for(VennSet parent : parents) {
			if(parent.isTopLevelSet()) {
				if(!topLevelParents.contains(parent)) {
					topLevelParents.add(parent);
				}
			} else {
				getTopLevelParents(parent.getParents(), topLevelParents);
			}
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/