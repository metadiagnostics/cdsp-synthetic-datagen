/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class UniformDistribution<T> extends BaseListBasedDistribution<T> {
	
	private Random random;
	
	public UniformDistribution() {
		random = new Random();
	}
	
	public List<T> getRandomSubset(List<T> items, int subsetSize) {
		return getSubsetByIndexArray(getRandomSetOfSize(subsetSize, items.size()), items);
	}
	
	public List<T> getNonRepeatingRandomSubset(int subsetSize, List<T> items) {
		List<T> clonedItemsList = new ArrayList<T>();
		List<T> subset = new ArrayList<T>();
		clonedItemsList.addAll(items);
		while(clonedItemsList.size() > getNumberRemaining(items, subsetSize)){
		     int i = random.nextInt(clonedItemsList.size());
		     subset.add(clonedItemsList.get(i));
		     clonedItemsList.remove(i);
		}
		return subset;
	}
	
	public boolean isWithinProbability(double probability) {
		double value = random.nextDouble();
		if(value < probability) {
			return true;
		} else {
			return false;
		}
	}
	
	protected int[] getRandomSetOfSize(int size, int upperBound) {
		int[] indices = new int[size];
		for(int i = 0; i < size; i++) {
			indices[i] = random.nextInt(upperBound);
		}
		return indices;
	}
	
	protected int getNumberRemaining(List<T> items, int subsetSize) {
		validateSize(items.size(), subsetSize);
		return items.size() - subsetSize;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/