/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import java.util.List;

import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;

/**
 * Class reads from CSV files, the first names, middle names, and last names as
 * separate collections.
 * 
 * 
 * @author Claude Nanjo
 * 
 */
public class PersonNameCsvReader extends BaseCsvReader implements PersonNameReader {

	private String givenNamePath;
	private String middleNamePath;
	private String surnamePath;

	private char givenNameDelimiter;
	private char middleNameDelimiter;
	private char surnameDelimiter;

	private boolean isGivenNameFromFile = true;
	private boolean isMiddleNameFromFile = true;
	private boolean isSurnameFromFile = true;

	/**
	 * Default constructor. Use accessor methods to set paths, delimiters, and
	 * source.
	 */
	public PersonNameCsvReader() {
	}

	/**
	 * This constructor fully configures the reader and gives full flexibility
	 * in terms of source specification. Each name component source may come
	 * from different sources, use different delimiters, and have different
	 * paths.
	 * 
	 * @param givenNamePath
	 *            - The path to the given name source.
	 * @param givenNameDelimiter
	 *            - The delimiter of the given name source.
	 * @param isGivenNameFromFile
	 *            - 'true' if the source is a file. 'false' indicates that the
	 *            source is on the class path.
	 * @param middleNamePath
	 *            - The path to the given name source.
	 * @param middleNameDelimiter
	 *            - The delimiter of the middle name source.
	 * @param isMiddleNameFromFile
	 *            - 'true' if the source is a file. 'false' indicates that the
	 *            source is on the class path.
	 * @param surnamePath
	 *            - The path to the surname source.
	 * @param surnameDelimiter
	 *            - The delimiter of the surname source.
	 * @param isSurnameFromFile
	 *            - 'true' if the source is a file. 'false' indicates that the
	 *            source is on the class path.
	 */
	public PersonNameCsvReader(String givenNamePath, char givenNameDelimiter,
			boolean isGivenNameFromFile, String middleNamePath,
			char middleNameDelimiter, boolean isMiddleNameFromFile,
			String surnamePath, char surnameDelimiter, boolean isSurnameFromFile) {

		this.givenNamePath = givenNamePath;
		this.middleNamePath = middleNamePath;
		this.surnamePath = surnamePath;

		this.givenNameDelimiter = givenNameDelimiter;
		this.middleNameDelimiter = middleNameDelimiter;
		this.surnameDelimiter = surnameDelimiter;

		this.isGivenNameFromFile = isGivenNameFromFile;
		this.isMiddleNameFromFile = isMiddleNameFromFile;
		this.isSurnameFromFile = isSurnameFromFile;
	}

	/**
	 * Use this constructor if all CSV files share the same delimiter and source
	 * type.
	 * 
	 * @param givenNamePath
	 *            - The path to the given name source.
	 * @param middleNamePath
	 *            - The delimiter of the middle name source.
	 * @param surnamePath
	 *            - The path to the surname source.
	 * @param defaultDelimiter
	 *            - delimiter if shared by all three sources
	 * @param isFileSource
	 *            - 'true' if the source is a file. 'false' indicates that the
	 *            source is on the class path.
	 */
	public PersonNameCsvReader(String givenNamePath, String middleNamePath,
			String surnamePath, char defaultDelimiter, boolean isFileSource) {

		this.givenNamePath = givenNamePath;
		this.middleNamePath = middleNamePath;
		this.surnamePath = surnamePath;

		this.givenNameDelimiter = defaultDelimiter;
		this.middleNameDelimiter = defaultDelimiter;
		this.surnameDelimiter = defaultDelimiter;

		this.isGivenNameFromFile = isFileSource;
		this.isMiddleNameFromFile = isFileSource;
		this.isSurnameFromFile = isFileSource;
	}

	/**
	 * @return a list of given names
	 */
	public List<DiscreteItemProbabilityPair<String>> loadGivenName() {
		if (isGivenNameFromFile) {
			return loadItemsFromCsvFile(givenNamePath, givenNameDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(givenNamePath,
					givenNameDelimiter);
		}
	}

	/**
	 * 
	 * @return a list of middle names
	 */
	public List<DiscreteItemProbabilityPair<String>> loadMiddleName() {
		if (isMiddleNameFromFile) {
			return loadItemsFromCsvFile(middleNamePath, middleNameDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(middleNamePath,
					middleNameDelimiter);
		}
	}

	/**
	 * 
	 * @return a list of surnames
	 */
	public List<DiscreteItemProbabilityPair<String>> loadSurname() {
		if (isSurnameFromFile) {
			return loadItemsFromCsvFile(surnamePath, surnameDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(surnamePath, surnameDelimiter);
		}
	}

	public String getGivenNamePath() {
		return givenNamePath;
	}

	public void setGivenNamePath(String givenNamePath) {
		this.givenNamePath = givenNamePath;
	}

	public String getMiddleNamePath() {
		return middleNamePath;
	}

	public void setMiddleNamePath(String middleNamePath) {
		this.middleNamePath = middleNamePath;
	}

	public String getSurnamePath() {
		return surnamePath;
	}

	public void setSurnamePath(String surnamePath) {
		this.surnamePath = surnamePath;
	}

	public char getGivenNameDelimiter() {
		return givenNameDelimiter;
	}

	public void setGivenNameDelimiter(char givenNameDelimiter) {
		this.givenNameDelimiter = givenNameDelimiter;
	}

	public char getMiddleNameDelimiter() {
		return middleNameDelimiter;
	}

	public void setMiddleNameDelimiter(char middleNameDelimiter) {
		this.middleNameDelimiter = middleNameDelimiter;
	}

	public char getSurnameDelimiter() {
		return surnameDelimiter;
	}

	public void setSurnameDelimiter(char surnameDelimiter) {
		this.surnameDelimiter = surnameDelimiter;
	}

	public boolean isGivenNameFromFile() {
		return isGivenNameFromFile;
	}

	public void setGivenNameFromFile(boolean isGivenNameFromFile) {
		this.isGivenNameFromFile = isGivenNameFromFile;
	}

	public boolean isMiddleNameFromFile() {
		return isMiddleNameFromFile;
	}

	public void setMiddleNameFromFile(boolean isMiddleNameFromFile) {
		this.isMiddleNameFromFile = isMiddleNameFromFile;
	}

	public boolean isSurnameFromFile() {
		return isSurnameFromFile;
	}

	public void setSurnameFromFile(boolean isSurnameFromFile) {
		this.isSurnameFromFile = isSurnameFromFile;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/