/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import java.io.FileReader;
import java.io.InputStream;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.common.reader.CsvListReader;
import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;

public abstract class BaseCsvReader {
	
	private boolean hasHeader = false;
	
	public boolean hasHeader() {
		return hasHeader;
	}

	public void setHeaderFlag(boolean hasHeader) {
		this.hasHeader = hasHeader;
	}
	
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvFile(String filePath, char delimiter) {
		try {
			FileReader csvFileReader = new FileReader(filePath);
			return loadCsvItemsFromReaderOrInputStream(csvFileReader, delimiter);
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error loading CSV file with specified file path: " + filePath, cause);
		}
	}
	
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvResourceFolder(String resourcePath, char delimiter) {
		try {
			InputStream is = ReaderUtilities.loadResourceFromStream(resourcePath);
			return loadCsvItemsFromReaderOrInputStream(is, delimiter);
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error loading CSV file from resource folder: " + resourcePath, cause);
		}
	}
	
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvInputSteam(String inputStream, char delimiter) {
		try {
			return loadCsvItemsFromReaderOrInputStream(inputStream, delimiter);
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error loading from input stream", cause);
		}
	}
	
	public List<DiscreteItemProbabilityPair<String>> loadItemsFromCsvReader(Reader reader, char delimiter) {
		try {
			return loadCsvItemsFromReaderOrInputStream(reader, delimiter);
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error loading from reader", cause);
		}
	}
	
	/**
	 * Loads a file with data delimited using 'delimiter' and having the token in the first column
	 * and the frequency in the second column.
	 * 
	 * @param source
	 * @param delimiter
	 * @return
	 */
	protected List<DiscreteItemProbabilityPair<String>> loadCsvItemsFromReaderOrInputStream(Object source, char delimiter) {
		CsvListReader csvReader = BaseCsvReader.readCsvFile(source, delimiter);
		List<String[]> lines = csvReader.getLines();
		List<DiscreteItemProbabilityPair<String>> items = new ArrayList<DiscreteItemProbabilityPair<String>>();
		int count = 0;
		for(String[] line : lines) {
			if(hasHeader && count == 0) {
				count++;
				continue;
			}
			items.add(new DiscreteItemProbabilityPair<String>(line[0], Double.parseDouble(line[1])));
		}
		return items;
	}

	public static CsvListReader readCsvFile(Object source, char delimiter) {
		CsvListReader csvReader = new CsvListReader();
		if(source == null) {
			throw new SocraticGridRuntimeException("Source stream is null. Is file in classpath?");
		}
		if(source instanceof Reader) {
			csvReader.initialize((Reader)source, delimiter);
		} else if(source instanceof InputStream) {
			csvReader.initialize((InputStream)source, delimiter);
		} else {
			throw new SocraticGridRuntimeException("Unknown source object. Source may be of type Reader or InputStream");
		}
		csvReader.loadContentInMemory();
		return csvReader;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/