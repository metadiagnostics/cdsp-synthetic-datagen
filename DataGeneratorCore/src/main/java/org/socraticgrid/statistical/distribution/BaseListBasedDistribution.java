/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseListBasedDistribution<T> {
	
	protected List<T> getSubsetByIndexArray(int[] indexArray, List<T> items) {
		validateSize(items.size(), indexArray.length);
		List<T> subset = new ArrayList<T>();
		for(int i = 0; i < indexArray.length; i++) {
			subset.add(items.get(indexArray[i]));
		}
		return subset;
	}
	
	protected void validateSize(int parentSet, int subset) {
		if(parentSet < subset) {
			throw new IllegalArgumentException("A subset cannot be larger than the superset.");
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/