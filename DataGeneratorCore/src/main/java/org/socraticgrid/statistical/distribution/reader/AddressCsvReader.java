/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import java.util.List;

import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;

/**
 * TODO This class is of yet still incomplete
 * 
 * @author Claude Nanjo
 *
 */
public class AddressCsvReader extends BaseCsvReader implements AddressReader {

	private String streetAddressListPath;
	private String cityListPath;
	private String stateListPath;
	private String zipListPath;

	private char streetAddressDelimiter;
	private char cityDelimiter;
	private char stateDelimiter;
	private char zipDelimiter;

	private boolean isStreetAddressFromFile = true;
	private boolean isCityFromFile = true;
	private boolean isStateFromFile = true;
	private boolean isZipFromFile = true;

	/**
	 * Default constructor. Use accessor methods to set paths, delimiters, and
	 * source.
	 */
	public AddressCsvReader() {}
	
	public AddressCsvReader(String streetAddressListPath,
							char streetAddressDelimiter,
							boolean isStreetAddressFromFile,
							String cityListPath,
							char cityDelimiter,
							boolean isCityFromFile,
							String stateListPath,
							char stateDelimiter,
							boolean isStateFromFile,
							String zipListPath,
							char zipDelimiter,
							boolean isZipFromFile) {
		
		this.streetAddressListPath = streetAddressListPath;
		this.cityListPath = cityListPath;
		this.stateListPath = stateListPath;
		this.zipListPath = zipListPath;
		
		this.streetAddressDelimiter = streetAddressDelimiter;
		this.cityDelimiter = cityDelimiter;
		this.stateDelimiter = stateDelimiter;
		this.zipDelimiter = zipDelimiter;
		
		this.isStreetAddressFromFile = isStreetAddressFromFile;
		this.isCityFromFile = isCityFromFile;
		this.isStateFromFile = isStateFromFile;
		this.isZipFromFile = isZipFromFile;
	}

	/**
	 * @return a list of street addresses (without street number but with street suffixes)
	 */
	public List<DiscreteItemProbabilityPair<String>> loadStreetAddresses() {
		if (isStreetAddressFromFile) {
			return loadItemsFromCsvFile(streetAddressListPath, streetAddressDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(streetAddressListPath, streetAddressDelimiter);
		}
	}
	
	/**
	 * @return a list of Cities
	 */
	public List<DiscreteItemProbabilityPair<String>> loadCities() {
		if (isCityFromFile) {
			return loadItemsFromCsvFile(cityListPath, cityDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(cityListPath, cityDelimiter);
		}
	}
	
	/**
	 * @return a list of States (may or may not be abbreviated)
	 */
	public List<DiscreteItemProbabilityPair<String>> loadStates() {
		if (isStateFromFile) {
			return loadItemsFromCsvFile(stateListPath, stateDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(stateListPath, stateDelimiter);
		}
	}
	
	/**
	 * @return a list of Zip Codes
	 */
	public List<DiscreteItemProbabilityPair<String>> loadZipCodes() {
		if (isZipFromFile) {
			return loadItemsFromCsvFile(zipListPath, zipDelimiter);
		} else {
			return loadItemsFromCsvResourceFolder(zipListPath, zipDelimiter);
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/