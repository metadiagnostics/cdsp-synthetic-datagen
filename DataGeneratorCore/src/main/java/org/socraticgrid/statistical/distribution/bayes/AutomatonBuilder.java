/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.socraticgrid.exception.SocraticGridRuntimeException;

/**
 * 
 * @author Claude Nanjo
 *
 */
public class AutomatonBuilder {
	
	private Universe universe;
	private Vertex start;

	public AutomatonBuilder(Universe universe) {
		this.universe = universe;
	}
	
	public void buildAutomaton() {
		start = new Start();
		List<VennSet> consumed = new ArrayList<VennSet>();
		handleChildren(start, universe, consumed, null);
		start.generateDiscreteProbabilityFromEdges();
	}
	
	public void handleChildren(Vertex top, VennSet topSet, List<VennSet> consumed, List<VennSet> seen) {
		double sum = 0.0;
		List<VennSet> visited = new ArrayList<VennSet>();
		if(seen != null) {
			visited.addAll(seen);
		}
		for(VennSet subset : topSet.getChildren()){
			if(consumed.contains(subset) || subsumeAndEqualToSubset(consumed, subset)) {
				continue;
			} else {
				consumed.add(subset);
			}
			Vertex child = new Vertex(subset.getName());
			child.setGenerator(subset.getGenerator());
			double edgeProbability = calculateProbability1(subset, visited, topSet, top.getProbability());
			Edge edgeChild = new Edge(top, child, edgeProbability);
			child.setProbability(top.getProbability() * edgeProbability);
			sum += edgeProbability;
			if(subset.getChildren().size() > 0) {
				handleChildren(child, subset, consumed, visited);
			} else {
				Edge end = new Edge(child, new End(), 1);
			}
			if(subset.isTopLevelSet()) {
				visited.add(subset);
			} else {
				visited.addAll(VennSet.getTopLevelParents(subset));
			}
			child.generateDiscreteProbabilityFromEdges();
		}
		if(sum < 1) {
			Edge other = new Edge(top, new End(), 1 - sum);
		} else if (sum == 1) {
			// Do nothing
		} else {
			throw new SocraticGridRuntimeException("Error: Outgoing probability cannot exceed 1: " + topSet.getName());
		}
	}
	
	public double calculateProbability1(VennSet currentSet, List<VennSet> visited, VennSet topSet, double parentProbability) {
		double probability = 0;
		List<Set<VennSet>> powerSet = VennSet.generatePowerSet(visited);
		List<VennSet> covered = new ArrayList<VennSet>();
		covered.add(currentSet);
		for(Set<VennSet> member : powerSet) {
			if(member.size() == 0) {
				probability += currentSet.getProbability();
			} else {
				Set<VennSet> intersection = new TreeSet<VennSet>(VennSet.comparator);
				if(currentSet.isTopLevelSet()) {
					intersection.add(currentSet);
				} else {
					intersection.addAll(VennSet.getTopLevelParents(currentSet));
				}
				intersection.addAll(member);
				VennSet intersect = universe.findIntersection(intersection);
				if(intersect != null) {
					if(!covered.contains(intersect)) {
						probability += + Math.pow(-1,member.size()) * intersect.getProbability();
					} else {
//						System.out.println(intersect + " is covered already");
					}
					covered.add(intersect);
				} else {
//					System.out.println("Intersection for " + intersection + " is null");
				}
			}
		}
		return probability/parentProbability;
	}
	
	public Vertex getStartingPoint() {
		return start;
	}
	
	public boolean subsumeAndEqualToSubset(List<VennSet> sets, VennSet interestedSet) {// TODO Fix such that if A sub B && B sub A return true
		boolean isSubsumedAndEquals = false;
		for(VennSet set : sets) {
			if(set.isSubsumedBy(interestedSet) && (set.getProbability() == interestedSet.getProbability())) {
				isSubsumedAndEquals = true;
				break;
			}
		}
		return isSubsumedAndEquals;
	}
}