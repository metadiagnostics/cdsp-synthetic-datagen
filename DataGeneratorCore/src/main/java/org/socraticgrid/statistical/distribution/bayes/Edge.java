/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

public class Edge {
	
	private Vertex source;
	private Vertex destination;
	private double probability;
	
	/**
	 * Represents a weighted edge in a directed graph.
	 * 
	 * @param source
	 * @param destination
	 * @param probability
	 */
	public Edge(Vertex source, Vertex destination, double probability) {
		this.source = source;
		this.destination = destination;
		this.probability = probability;
		source.addOutgoingEdge(this);
		destination.addIncomingEdge(this);
	}
	
	/**
	 * Returns the edge's source vertex
	 * @return
	 */
	public Vertex getSource() {
		return source;
	}
	
	/**
	 * Returns the edge's destination vertex.
	 * 
	 * @return
	 */
	public Vertex getDestination() {
		return destination;
	}
	
	/**
	 * Returns the weight associated with this age in the
	 * form of a probability.
	 * 
	 * @return
	 */
	public double getProbability() {
		return probability;
	}
	
	/**
	 * Returns a string representing this edge.
	 * 
	 */
	public String toString() {
		return source.getName() + "-->" + destination.getName() + "(" + probability + ")";
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/