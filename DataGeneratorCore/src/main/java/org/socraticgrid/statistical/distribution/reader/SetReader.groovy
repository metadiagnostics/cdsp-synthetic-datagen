/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader

import groovy.util.slurpersupport.GPathResult

import org.socraticgrid.common.reader.ReaderUtilities
import org.socraticgrid.exception.SocraticGridRuntimeException
import org.socraticgrid.statistical.distribution.bayes.Generator
import org.socraticgrid.statistical.distribution.bayes.Universe
import org.socraticgrid.statistical.distribution.bayes.VennSet

/** 
 * Not thread-safe
 * 
 * @author Claude Nanjo
 *
 */
class SetReader<T> {
	
	Map<String, VennSet> processedSets;
	def configuration

	public SetReader() {
		processedSets = new HashMap<String, VennSet>();
	}
	
	public GPathResult loadConfiguration(String filePath) {
		InputStream is = ReaderUtilities.loadResourceFromStream(filePath)
		configuration = new XmlSlurper().parse(is)
	}
	
	public List<Generator<T>> loadConditionalProbabilityGenerators() {
		List<Generator<T>> condProbList = new ArrayList<Generator<T>>()
		def conditionalProbs = configuration.conditionalProbabilityGenerators.conditionalProbabilityGenerator
		conditionalProbs.each { conditionalProb ->
			String generatorName = conditionalProb.generatorClass;
			Generator<T> generator = (Generator<T>)Class.forName(generatorName.trim()).newInstance();
			condProbList.add(generator)
		}
		return condProbList
	}
	
	public List<Universe> loadUniverses() {
		List<Universe> setDefinitions = new ArrayList<Universe>()
		def universes = configuration.universe
		universes.each { universe ->
			setDefinitions.add(processUniverse(universe))
		}
		return setDefinitions
	}
	
	public Universe processUniverse(def universeDef) {
		Universe universe = new Universe()
		def sets = universeDef.set
		sets.each { setNode ->
			processSet(setNode, universe)
		}
		universe.sortChildren();
		return universe
	}
	
	public void processSet(def setNode, Universe universe) {
		List<VennSet> parents = processParents(setNode.parentSets)
		String setName = setNode.name
		String setId = setNode.@id
		double probability = setNode.probability.toDouble()
		String className = setNode.generatorClass
		VennSet set = null;
		if(parents.size() <= 0) {
			set = universe.createTopLevelSet(setName, probability)
		} else {
			set = universe.createSet(setName, probability, getAsArray(parents))
		}
		set.setGenerator(Class.forName(className.trim()).newInstance());
		processedSets.put(setId, set)
	}
	
	public List<VennSet> processParents(def parentSets) {
		List<VennSet> parents = new ArrayList<VennSet>();
		parentSets.parentSet.each { parent ->
			String idref = parent.@idref
			VennSet item = processedSets.get(idref);
			if(item != null) {
				parents.add(item)
			} else {
				throw new SocraticGridRuntimeException("Invalid parent reference: " + idref + ". Parent definition must appear prior to its reference");
			}
		}
		return parents;
	}
	
	protected VennSet[] getAsArray(List<VennSet> sets) {
		VennSet[] setArray = new Set[sets.size()]
		return sets.toArray(setArray)
	}
}