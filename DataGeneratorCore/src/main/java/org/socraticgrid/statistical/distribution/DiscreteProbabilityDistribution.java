/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.EnumeratedIntegerDistribution;

public class DiscreteProbabilityDistribution<T> extends BaseListBasedDistribution<T> {
	
	private List<DiscreteItemProbabilityPair<T>> itemSet;
	private int[] indexArray;
	private double[] probabilityArray;
	private EnumeratedIntegerDistribution distribution;
	
	public DiscreteProbabilityDistribution(List<DiscreteItemProbabilityPair<T>> masterItemList) {
		this.itemSet = masterItemList;
		if(itemSet == null) {
			itemSet = new ArrayList<DiscreteItemProbabilityPair<T>>();
		}
		indexArray = new int[itemSet.size()];
		probabilityArray = new double[itemSet.size()];
		for(int i = 0; i < itemSet.size(); i++) {
			indexArray[i] = i;
			probabilityArray[i] = itemSet.get(i).getProbability();
		}
		distribution = new EnumeratedIntegerDistribution(indexArray, probabilityArray);
	}
	
	public List<T> getSampleItems(int size) {
		List<T> selectedItems = new ArrayList<T>();
		int[] selectedIndices = distribution.sample(size);
		for(int i = 0; i < size; i++) {
			selectedItems.add(itemSet.get(selectedIndices[i]).getItem());
		}
		return selectedItems;
	}
	
	public T getSample() {
		return itemSet.get(distribution.sample()).getItem();
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/