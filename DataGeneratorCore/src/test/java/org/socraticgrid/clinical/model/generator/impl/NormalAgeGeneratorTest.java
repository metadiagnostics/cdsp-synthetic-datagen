/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator.impl;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.statistical.distribution.NormalDistribution;

public class NormalAgeGeneratorTest {
	
	public static final int ITERATION_COUNT = 1000000;
	public static final double ERROR_DELTA = 0.01;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetNextAge() {
		NormalDistribution ageDistribution = new NormalDistribution(19, 1.5);
		NormalAgeGenerator ageGenerator = new NormalAgeGenerator(ageDistribution);
		double sum = 0;
		for(int i=0; i<ITERATION_COUNT;i++) {
			sum += ageGenerator.getNextAge();
		}
		double averageAge = sum/ITERATION_COUNT;
		assertEquals(19, averageAge, ERROR_DELTA);
		System.out.println(averageAge);
	}
	
	@Test
	public void testGenerateDateOfBirth() {
		NormalAgeGenerator ageGenerator = new NormalAgeGenerator(59, 16);
		for(int i = 0; i < 100; i++) {
			System.out.println(ageGenerator.generateDateOfBirth());
		}
	}

}
