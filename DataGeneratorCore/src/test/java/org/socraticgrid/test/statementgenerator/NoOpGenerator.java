/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.test.statementgenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.statistical.distribution.bayes.Generator;
import org.socraticgrid.statistical.distribution.bayes.VennSet;

/**
 * Simple generator that prints generation timestamped statements.
 * Useful for testing purposes.
 * 
 * @author Claude Nanjo
 *
 */
public class NoOpGenerator implements Generator<String> {
	
	private VennSet owner;

	public NoOpGenerator() {
	}
	
	public List<String> generate() {
		return generate(null);
	}
	
	public List<String> generate(Map<String,Object> configurationParameters) {
		List<String> collection = new ArrayList<String>();
		collection.add(owner.getName());
		return collection;
	}

	public void setVennSet(VennSet set) {
		owner = set;
		
	}

	public VennSet getVennSet() {
		return owner;
	}
}