/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AutomatonBuilderTest2 {
	
	private Universe universe1;
	private AutomatonBuilder automaton;

	@Before
	public void setUp() throws Exception {
		universe1 = buildUniverse1();
		automaton = new AutomatonBuilder(universe1);
		
	}
	
	@Test
	public void testBuildAutomaton() {
		automaton.buildAutomaton();
		
		Vertex start = automaton.getStartingPoint();
		assertNotNull(start);
		
		//Validate we have a starting point
		assertEquals(0, start.getIncomingEdgeCount());
		assertEquals(5, start.getOutgoingEdgeCount()); // Includes End vertex
		
		for(Edge edge : start.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("A")) {
				assertEquals(0.2, edge.getProbability(), 0.001);
				validateNodeA(destination);
			} else if(name.equals("B")) {
				assertEquals(0.1, edge.getProbability(), 0.001);
				validateNodeB(destination);
			} else if(name.equals("C")) {
				assertEquals(0.1, edge.getProbability(), 0.001);
				validateNodeC(destination);
			} else if(name.equals("D")) {
				assertEquals(0.05, edge.getProbability(), 0.001);
				validateNodeD(destination);
			} else if(name.equals(End.END)) {
				assertEquals(.55, edge.getProbability(), 0.001);
			} else {
				fail();
			}
		}
	}
	
	public void validateNodeA(Vertex A) {
		for(Edge edge : A.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("AB")) {
				assertEquals(0.5, edge.getProbability(), 0.001);
				validateNodeAB(destination);
			} else if(name.equals("AC")) {
				assertEquals(0.25, edge.getProbability(), 0.001);
				validateNodeAC(destination);
			} else if(name.equals("AD")) {
				assertEquals(0.0, edge.getProbability(), 0.001);
				validateNodeAC(destination);
			} else if(name.equals(End.END)) {
				assertEquals(.25, edge.getProbability(), 0.001);
			} else {
				fail("Invalid node name '" + name + "' for node " + A);
			}
		}
	}
	
	public void validateNodeB(Vertex B) {
		for(Edge edge : B.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("BC")) {
				assertEquals(0.0, edge.getProbability(), 0.001);// No overlap between (B-A) and C
				validateNodeBC(destination);
			} else if(name.equals("BD")) {
				assertEquals(0.5, edge.getProbability(), 0.001);// No overlap between (B-A) and C
				validateNodeBD(destination);
			} else if(name.equals(End.END)) {
				assertEquals(0.5, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeC(Vertex C) {
		for(Edge edge : C.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("CD")) {
				assertEquals(0.5, edge.getProbability(), 0.001);
			} else if(name.equals(End.END)) {
				assertEquals(0.5, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeD(Vertex D) {
		for(Edge edge : D.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeAB(Vertex AB) {
		for(Edge edge : AB.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("ABC")) {
				assertEquals(0.5, edge.getProbability(), 0.001);
				validateNodeABC(destination);
			} else if(name.equals("ABD")) {
				assertEquals(0.0, edge.getProbability(), 0.001);
				//validateNodeABD(destination);
			} else if(name.equals(End.END)) {
				assertEquals(0.5, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeAC(Vertex AC) {
		for(Edge edge : AC.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeAD(Vertex AD) {
		for(Edge edge : AD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeBC(Vertex BC) {
		for(Edge edge : BC.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeBD(Vertex BD) {
		for(Edge edge : BD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeCD(Vertex CD) {
		for(Edge edge : CD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeABC(Vertex ABC) {
		for(Edge edge : ABC.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("ABCD")) {
				assertEquals(1.0, edge.getProbability(), 0.001);
				validateNodeABCD(destination);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeABD(Vertex ABD) {
		for(Edge edge : ABD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeBCD(Vertex BCD) {
		for(Edge edge : BCD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeABCD(Vertex ABCD) {
		for(Edge edge : ABCD.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}

	@After
	public void tearDown() throws Exception {
	}
	
	protected Universe buildUniverse1() throws Exception {
		Universe universe = new Universe();
		VennSet A = universe.createTopLevelSet("A", 0.2);
		VennSet B = universe.createTopLevelSet("B", 0.2);
		VennSet C = universe.createTopLevelSet("C", 0.2);
		VennSet D = universe.createTopLevelSet("D", 0.2);
		VennSet AB = universe.createSet("AB", 0.1, A, B);
		VennSet AC = universe.createSet("AC", 0.1, A, C);
		VennSet AD = universe.createSet("AD", 0.05, A, D);
		VennSet BC = universe.createSet("BC", 0.05, B, C);
		VennSet BD = universe.createSet("BD", 0.1, B, D);
		VennSet CD = universe.createSet("CD", 0.1, C, D);
		VennSet ABC = universe.createSet("ABC", 0.05, AB, BC, AC);
		VennSet ABD = universe.createSet("ABD", 0.05, AB, BD, AD);
		VennSet BCD = universe.createSet("BCD", 0.05, BC, BD, CD);
		VennSet ACD = universe.createSet("ACD", 0.05, AC, AD, CD);
		VennSet ABCD = universe.createSet("ABCD", 0.05, ABC, ABD, BCD, ACD);
		universe.sortChildren();
		return universe;
	}

}
