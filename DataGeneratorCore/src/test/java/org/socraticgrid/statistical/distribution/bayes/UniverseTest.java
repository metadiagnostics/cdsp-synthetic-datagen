/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UniverseTest {
	
	private Universe universe;

	@Before
	public void setUp() throws Exception {
		universe = new Universe();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddSet() {
		VennSet set = new VennSet("A", 0.2);
		universe.addSet(set);
		assertEquals(1, universe.getChildren().size());
	}

	@Test
	public void testSort() {
		VennSet A = new VennSet("A", 0.2);
		VennSet B = new VennSet("B", 0.2);
		VennSet AB = new VennSet("AB", 0.1, A, B);
		universe.addSet(AB);
		universe.addSet(B);
		universe.addSet(A);
//		universe.sort();
//		assertEquals(A, universe.get(0));
//		assertEquals(B, universe.get(1));
//		assertEquals(AB, universe.get(2));
	}

	@Test
	public void testCreateSet() {
		try {
			VennSet A = universe.createTopLevelSet("A", 0.2);
			VennSet B = universe.createTopLevelSet("B", 0.2);
			VennSet C = universe.createTopLevelSet("C", 0.2);
			VennSet AB = universe.createSet("AB", 1, A, B);
			VennSet ABC = universe.createSet("ABC", 0.05, A, B, C);
			assertEquals(3, universe.getTopLevelSets().size());
		} catch(Exception e) {
			fail();
		}
			
	}

//	@Test
//	public void testGet() {
//		VennSet A = universe.createSet("A", 0.2);
//		VennSet B = universe.createSet("B", 0.2);
//		assertEquals(B, universe.get(1));
//	}

	@Test
	public void testGetTopLevelSets() {
		VennSet A = universe.createTopLevelSet("A", 0.2);
		VennSet B = universe.createTopLevelSet("B", 0.2);
		VennSet C = universe.createTopLevelSet("C", 0.2);
		VennSet AB = universe.createSet("AB", 1, A, B);
		VennSet ABC = universe.createSet("ABC", 0.05, A, B, C);
		assertEquals(3, universe.getTopLevelSets().size());
	}

	@Test
	public void testFindIntersection1() {
		VennSet A = universe.createSet("A", 0.2);
		VennSet B = universe.createSet("B", 0.2);
		VennSet C = universe.createSet("C", 0.2);
		VennSet AB = universe.createSet("AB", 1, A, B);
		VennSet ABC = universe.createSet("ABC", 0.05, AB, C);
		Set<VennSet> intersectionSets = new TreeSet<VennSet>(VennSet.comparator);
		intersectionSets.add(A);
		intersectionSets.add(B);
		intersectionSets.add(C);
		VennSet set = universe.findIntersection(intersectionSets);
		assertEquals(ABC, set);
	}
	
//	@Test
//	public void testFindIntersection2() {
//		Set A = universe.createSet("A", 0.2);
//		Set B = universe.createSet("B", 0.2);
//		Set C = universe.createSet("C", 0.2);
//		Set AB = universe.createSet("AB", 1, A, B);
//		Set ABC = universe.createSet("ABC", 0.05, AB, C);
//		List<Set> intersectionSets = new ArrayList<Set>();
//		intersectionSets.add(A);
//		intersectionSets.add(AB);
//		intersectionSets.add(C);
//		Set set = universe.findIntersection(intersectionSets);
//		assertEquals(ABC, set);
//	}

	@Test
	public void testGetTopLevelParentsListOfSet() {
		VennSet A = universe.createSet("A", 0.2);
		VennSet B = universe.createSet("B", 0.2);
		VennSet C = universe.createSet("C", 0.2);
		VennSet AB = universe.createSet("AB", 1, A, B);
		VennSet ABC = universe.createSet("ABC", 0.05, AB, C);
		Set<VennSet> intersectionSets = new TreeSet<VennSet>(VennSet.comparator);
		intersectionSets.add(A);
		intersectionSets.add(AB);
		intersectionSets.add(C);
		VennSet set = universe.findIntersection(Universe.getTopLevelParents(intersectionSets));
		assertEquals(ABC, set);
	}

}
