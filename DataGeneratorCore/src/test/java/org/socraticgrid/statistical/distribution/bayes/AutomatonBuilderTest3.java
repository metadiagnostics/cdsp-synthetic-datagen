/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AutomatonBuilderTest3 {
	
	private Universe universe1;
	private AutomatonBuilder automaton;

	@Before
	public void setUp() throws Exception {
		universe1 = buildUniverse1();
		automaton = new AutomatonBuilder(universe1);
		
	}
	
	@Test
	public void testBuildAutomaton() {
		automaton.buildAutomaton();
		
		Vertex start = automaton.getStartingPoint();
		assertNotNull(start);
		
		//Validate we have a starting point
		assertEquals(0, start.getIncomingEdgeCount());
		assertEquals(4, start.getOutgoingEdgeCount()); // Includes End vertex
		
		for(Edge edge : start.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("A")) {
				assertEquals(0.2, edge.getProbability(), 0.001);
				validateNodeA(destination);
			} else if(name.equals("B")) {
				assertEquals(0.05, edge.getProbability(), 0.001);
				validateNodeB(destination);
			} else if(name.equals("C")) {
				assertEquals(0.11, edge.getProbability(), 0.001);
				validateNodeC(destination);
			} else if(name.equals(End.END)) {
				assertEquals(.64, edge.getProbability(), 0.001);
			} else {
				fail();
			}
		}
	}
	
	public void validateNodeA(Vertex A) {
		for(Edge edge : A.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("AB")) {
				assertEquals(0.25, edge.getProbability(), 0.001);
				validateNodeAB(destination);
			} else if(name.equals("AC")) {
				assertEquals(0.1, edge.getProbability(), 0.001);
				validateNodeAC(destination);
			} else if(name.equals(End.END)) {
				assertEquals(.65, edge.getProbability(), 0.001);
			} else {
				fail();
			}
		}
	}
	
	public void validateNodeB(Vertex B) {
		for(Edge edge : B.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeC(Vertex C) {
		for(Edge edge : C.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeAB(Vertex AB) {
		for(Edge edge : AB.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("ABC")) {
				assertEquals(0.4, edge.getProbability(), 0.001);
				validateNodeABC(destination);
			} else if(name.equals(End.END)) {
				assertEquals(0.6, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeAC(Vertex AC) {
		for(Edge edge : AC.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateNodeABC(Vertex ABC) {
		for(Edge edge : ABC.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}

	@After
	public void tearDown() throws Exception {
	}
	
	protected Universe buildUniverse1() throws Exception {
		Universe universe = new Universe();
		VennSet A = universe.createTopLevelSet("A", 0.2);
		VennSet B = universe.createTopLevelSet("B", 0.1);
		VennSet C = universe.createTopLevelSet("C", 0.15);
		VennSet AB = universe.createSet("AB", 0.05, A, B);
		VennSet BC = universe.createSet("BC", 0.02, B, C);
		VennSet AC = universe.createSet("AC", 0.04, A, C);
		universe.createSet("ABC", 0.02, AB, AC, BC);
		universe.sortChildren();
		return universe;
	}

}
