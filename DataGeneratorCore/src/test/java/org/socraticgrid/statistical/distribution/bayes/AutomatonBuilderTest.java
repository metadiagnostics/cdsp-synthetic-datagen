/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.statistical.distribution.reader.SetReader;

public class AutomatonBuilderTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		SetReader reader = new SetReader();
		reader.loadConfiguration("conf/DiseaseProfile/TestUniverses.xml");
		List<Universe> universes = reader.loadUniverses();
		AutomatonBuilder automaton = new AutomatonBuilder(universes.get(1));
		automaton.buildAutomaton();
		Vertex start = automaton.getStartingPoint();
		int edgeCount = start.getOutgoingEdgeCount();
		assertEquals(3, edgeCount);
		List<Edge> startEdges = start.getOutgoingEdges();
		for(Edge edge : startEdges) {
			if(edge.getDestination().getName().equals("1")) {
				assertEquals(0.2, edge.getProbability(), 0.0001);
				Vertex one = edge.getDestination();
				List<Edge> oneEdges = one.getOutgoingEdges();
				assertEquals(2, oneEdges.size());
				for(Edge oneEdge : oneEdges) {
					if(oneEdge.getDestination().getName().equals("1 and 2")) {
						assertEquals(0.5, oneEdge.getProbability(), 0.0001);
					} else if(oneEdge.getDestination().getName().equals("End")) {
						assertEquals(0.5, oneEdge.getProbability(), 0.0001);
					} else {
						fail();
					}
				}
			} else if(edge.getDestination().getName().equals("2")) {
				assertEquals(0.1, edge.getProbability(), 0.0001);
			}else if(edge.getDestination().getName().equals("End")) {
				assertEquals(0.7, edge.getProbability(), 0.0001);
			} else {
				fail();
			}
		}
		AutomatonRunner runner = new AutomatonRunner(start);
		int count = 1000000;
		int countEmpty = 0;
		int countOneAndTwo = 0;
		int countOneOrTwo = 0;
		for(int i = 0; i < count; i++) {
			List statements = runner.generate();
			if(statements.size() == 0) {
				countEmpty++;
			} else if(statements.size() == 2) {
				countOneAndTwo++;
			} else if(statements.size() == 1) {
				countOneOrTwo++;
			}
		}
		assertEquals(0.7, ((float)countEmpty)/count, 0.01);
		assertEquals(0.1, ((float)countOneAndTwo)/count, 0.001);
		assertEquals(0.2, ((float)countOneOrTwo)/count, 0.001);
	}

}
