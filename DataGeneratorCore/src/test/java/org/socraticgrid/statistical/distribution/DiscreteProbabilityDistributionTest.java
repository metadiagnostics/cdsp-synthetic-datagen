/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.common.reader.CsvListReader;

public class DiscreteProbabilityDistributionTest {
	
	private DiscreteProbabilityDistribution<String> distribution;
	private List<DiscreteItemProbabilityPair<String>> masterItemList;

	@Before
	public void setUp() throws Exception {
		masterItemList = new ArrayList<DiscreteItemProbabilityPair<String>>();
		masterItemList.add(new DiscreteItemProbabilityPair<String>("Item 1", 0.60));
		masterItemList.add(new DiscreteItemProbabilityPair<String>("Item 2", 0.30));
		masterItemList.add(new DiscreteItemProbabilityPair<String>("Item 3", 0.10));
		distribution = new DiscreteProbabilityDistribution<String>(masterItemList);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		try {
			List<String> sampleItems = distribution.getSampleItems(10000);
			Map<String, Integer> itemCount = new HashMap<String, Integer>();
			for(String item : sampleItems) {
				if(itemCount.containsKey(item)) {
					itemCount.put(item, itemCount.get(item) + 1);
				} else {
					itemCount.put(item, 1);
				}
			}
			assertEquals(10000, sampleItems.size());
			assertEquals(0.6,itemCount.get("Item 1")/10000.0, 0.05);
		} catch(Exception e) {
			fail();
		}
	}
	
	@Test
	public void processNamesFromFile() {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream("distribution/commonFemaleNames.txt");
		CsvListReader reader = new CsvListReader();
		reader.initialize(is,'\t');
		reader.loadContentInMemory();
		assertEquals("BARBARA", reader.getItemAt(4, 0));
		assertEquals("0.011972097",reader.getItemAt(4, 1));
	}
}
