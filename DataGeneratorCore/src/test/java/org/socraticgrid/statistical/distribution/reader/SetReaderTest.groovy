/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.socraticgrid.statistical.distribution.bayes.Universe
import org.socraticgrid.statistical.distribution.bayes.VennSet

class SetReaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadConfiguration() {
		SetReader reader = new SetReader();
		def xml = reader.loadConfiguration("conf/DiseaseProfile/TestUniverses.xml");
		assertNotNull(xml)
		List<Universe> universes = reader.loadUniverses()
		assertEquals(2, universes.size());
		assertEquals(1, universes.get(0).topLevelSets.size());
		assertEquals(2, universes.get(1).topLevelSets.size());
		VennSet one = universes.get(1).topLevelSets.get(0);
		VennSet two = universes.get(1).topLevelSets.get(1);
		assertEquals(1,one.getChildren().size());
		assertEquals(1,two.getChildren().size());
		assertEquals(one.getChildren().toArray()[0], two.getChildren().toArray()[0]);
	}

}
