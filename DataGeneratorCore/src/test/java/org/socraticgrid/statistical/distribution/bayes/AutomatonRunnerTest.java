/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.test.statementgenerator.NoOpGenerator;

public class AutomatonRunnerTest {

	private Universe universe1;
	private AutomatonBuilder automaton;
	private AutomatonRunner runner;

	@Before
	public void setUp() throws Exception {
		universe1 = buildUniverse();
		automaton = new AutomatonBuilder(universe1);
		automaton.buildAutomaton();
		runner = new AutomatonRunner(automaton.getStartingPoint());
		
		
	}
	
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerate() {
		runner.generate();
	}
	
	protected Universe buildUniverse() throws Exception {
		Universe universe = new Universe();
		VennSet A = universe.createSet("A", 0.2);
		VennSet B = universe.createSet("B", 0.2);
		VennSet C = universe.createSet("C", 0.2);
		VennSet AB = universe.createSet("AB", 0.1, A, B);
		VennSet AC = universe.createSet("AC", 0.1, A, C);
		VennSet BC = universe.createSet("BC", 0.1, B, C);
		VennSet ABC = universe.createSet("ABC", 0.05, AB, BC, AC);
		
		A.setGenerator(new NoOpGenerator());
		B.setGenerator(new NoOpGenerator());
		C.setGenerator(new NoOpGenerator());
		AB.setGenerator(new NoOpGenerator());
		AC.setGenerator(new NoOpGenerator());
		BC.setGenerator(new NoOpGenerator());
		ABC.setGenerator(new NoOpGenerator());
		
		return universe;
	}

}
