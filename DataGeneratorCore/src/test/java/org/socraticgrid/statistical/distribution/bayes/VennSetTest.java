/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.bayes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VennSetTest {
	
	private VennSet A;
	private VennSet B;
	private VennSet C;
	private VennSet AB;
	private VennSet AC;
	private VennSet BC;
	private VennSet ABC;
	
	private Universe universe;
	
	@Before
	public void setUp() throws Exception {
		universe = new Universe();
		A = universe.createTopLevelSet("A", 0.2);
		B = universe.createTopLevelSet("B", 0.2);
		C = universe.createTopLevelSet("C", 0.2);
		AB = universe.createSet("AB", 0.1, A, B);
		AC = universe.createSet("AC", 0.1, A, C);
		BC = universe.createSet("BC", 0.1, B, C);
		ABC = universe.createSet("ABC", 0.5);
		ABC.addParent(A);
		ABC.addParent(B);
		ABC.addParent(C);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsIntersection() {
		assertFalse(A.isIntersection());
		assertFalse(B.isIntersection());
		assertFalse(C.isIntersection());
		assertTrue(AB.isIntersection());
		assertTrue(AC.isIntersection());
		assertTrue(BC.isIntersection());
		assertTrue(ABC.isIntersection());
	}

	@Test
	public void testIsSubsumedBy() {
		assertTrue(AB.isSubsumedBy(A));
		assertFalse(A.isSubsumedBy(AB));
		assertTrue(ABC.isSubsumedBy(AB));
		assertTrue(ABC.isSubsumedBy(A));
	}

	@Test
	public void testSubsumes() {
		assertFalse(A.subsumes(B));
		assertTrue(A.subsumes(AB));
		assertTrue(AB.subsumes(ABC));
	}

	@Test
	public void testGetParent() {
		assertTrue(BC.getParent("B") == B);
	}

	@Test
	public void testParentCount() {
		assertEquals(3, ABC.parentCount());
	}

	@Test
	public void testCompareTo() {
		assertEquals(-1, VennSet.comparator.compare(AB,ABC));
		assertEquals(-1, VennSet.comparator.compare(AB,BC));
		assertEquals(1, VennSet.comparator.compare(AB,A));
		assertEquals(0, VennSet.comparator.compare(A,A));
		assertTrue(A.equals(A));
		assertFalse(A.equals(B));
		//universe.sort();
		//assertTrue(universe.get(6).parentCount() == 3);
	}
	
	@Test
	public void testTopLevelGetter() {
		assertEquals(3, universe.getTopLevelSets().size());
	}
	
	@Test
	public void testGetTopLevelParents() {
		VennSet A = new VennSet("A", 0.2);
		VennSet B = new VennSet ("B", 0.2);
		VennSet C = new VennSet ("C", 0.2);
		VennSet D = new VennSet ("D", 0.2);
		VennSet AB = new VennSet("AB", 0.1);
		AB.addParent(A);
		AB.addParent(B);
		VennSet CD = new VennSet("CD", 0.1);
		CD.addParent(C);
		CD.addParent(D);
		VennSet ABCD = new VennSet("ABCD", 0.05);
		ABCD.addParent(AB);
		ABCD.addParent(CD);
		Set<VennSet> parents = VennSet.getTopLevelParents(ABCD);
		assertEquals(4,VennSet.getTopLevelParents(ABCD).size());
		assertTrue(parents.contains(A));
		assertTrue(parents.contains(B));
		assertTrue(parents.contains(C));
		assertTrue(parents.contains(D));
	}
	
	@Test
	public void algorithmTest() {
		String current = "D";
		List<String> visited = new ArrayList<String>();
		visited.add("A");
		visited.add("B");
		visited.add("C");
		System.out.println("P(D)");
		for(int i = 0; i < visited.size(); i++) {
			int factor = -1;
			int mult = factor;
			List<String> intersection = new ArrayList<String>();
			intersection.add(current);
			intersection.add(visited.get(i));
			Collections.sort(intersection);
			System.out.println(mult + intersection.toString());
			for(int j = i+1; j < visited.size(); j++) {
				mult*=factor;
				intersection.add(visited.get(j));
				Collections.sort(intersection);
				System.out.println(mult + intersection.toString());
			}
		}
	}
	
	@Test
	public void powersetAlgorithmTest() {
		List<VennSet> set = new ArrayList<VennSet>();
		VennSet A = new VennSet("A", 0.2);
		VennSet B = new VennSet("B", 0.2);
		VennSet C = new VennSet("C", 0.2);
		set.add(A);
		set.add(B);
		set.add(C);
		Collections.sort(set, VennSet.comparator);
		List<Set<VennSet>> powerSet = VennSet.generatePowerSet(set);
		assertEquals(8, powerSet.size());
		for(Set<VennSet> member : powerSet) {
			System.out.print(member);
			System.out.println(" factor: " + Math.pow(-1, member.size()));
		}
	}

}
