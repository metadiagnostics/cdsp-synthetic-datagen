/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class UniformDistributionTest {

	private UniformDistribution<String> uniformDistribution;
	private List<String> items;

	@Before
	public void setUp() throws Exception {
		uniformDistribution = new UniformDistribution<String>();
		items = new ArrayList<String>();
		items.add("Item 1");
		items.add("Item 2");
		items.add("Item 3");
		items.add("Item 4");
		items.add("Item 5");
		items.add("Item 6");
		items.add("Item 7");
		items.add("Item 8");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetRandomSubset() {
		List<String> subset = uniformDistribution.getRandomSubset(items, 5);
		assertEquals(5, subset.size());
		System.out.println(subset);
	}
	
	@Test
	public void testGetRandomLargerSubset() {
		try {
			uniformDistribution.getRandomSubset(items, 10);
			fail();
		} catch(IllegalArgumentException iae) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testIsWithinProbability() {
		try {
			int count = 0;
			for(int  index = 0; index < 10000; index++) {
				if(uniformDistribution.isWithinProbability(0.65)) {
					count++;
				}
			}
			assertEquals(0.65, ((double)count/10000), 0.05);
		} catch(IllegalArgumentException iae) {
			assertTrue(true);
		}
	}
	
	@Test
	public void testNonRepeatingRandomTrueSubset() {
		List<String> subset = uniformDistribution.getNonRepeatingRandomSubset(5, items);
		assertEquals(5, subset.size());
		System.out.println(subset);
	}
	
	@Test
	public void testNonRepeatingRandomEqualSubset() {
		List<String> subset = uniformDistribution.getNonRepeatingRandomSubset(8, items);
		assertEquals(8, subset.size());
		System.out.println(subset);
	}
	
	@Test
	public void testNonRepeatingRandomSubset() {
		try {
			uniformDistribution.getNonRepeatingRandomSubset(10, items);
			fail();
		} catch(IllegalArgumentException iae) {
			assertTrue(true);
		}
	}
}
