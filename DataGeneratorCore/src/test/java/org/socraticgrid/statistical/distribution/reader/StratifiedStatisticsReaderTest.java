/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.statistical.distribution.reader;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class StratifiedStatisticsReaderTest {
	
	public static final String STRATIFIED_PROB_FILE = "distribution/StratifiedDemographicDataMilitary.txt";
	
	private StratifiedStatisticsReader reader;
	private List<String[]> lines;

	@Before
	public void setUp() throws Exception {
		reader = new StratifiedStatisticsReader(0, new int[]{1,2});
		InputStream resourceStream = reader.getClass().getClassLoader().getResourceAsStream(STRATIFIED_PROB_FILE);
		assertNotNull(resourceStream);
		lines = reader.loadStratifiedStatistics(resourceStream, '\t');
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadStratifiedStatistics() {
		try {
			assertEquals(92, lines.size());
			assertEquals(22, lines.get(0).length);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void testValidStratrum() {
		String[] stratum = {"Army","E5"};
		assertTrue(reader.validStratum(stratum));
	}
	
	@Test
	public void testValidStratrumUnhappyPath() {
		String[] stratum = {"NotInThere","WWW"};
		assertFalse(reader.validStratum(stratum));
	}
	
	@Test
	public void testGetStratumIndex() {
		String[] stratum = {"Army","E5"};
		int index = reader.getStratumIndex(stratum);
		assertEquals(20, index);
	}
	
	@Test
	public void testGetStratumIndexUnhappyPath() {
		String[] stratum = {"Basketweaving","101"};
		int index = reader.getStratumIndex(stratum);
		assertEquals(-1, index);
	}
	
	@Test
	public void testGetSum() {
		double sum = reader.sumStrataProbabilities();
		assertEquals(97.026, sum, 0.001);
	}
	
	@Test
	public void testGetLine() {
		String[] line27 = lines.get(26);
		assertEquals("0.462", line27[0]);
	}

}
