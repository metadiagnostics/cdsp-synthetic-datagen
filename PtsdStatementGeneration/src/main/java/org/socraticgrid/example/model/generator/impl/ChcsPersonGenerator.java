/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.socraticgrid.clinical.model.generator.impl.DiscreteGenderDistributionGenerator;
import org.socraticgrid.clinical.model.generator.impl.NormalAgeGenerator;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.participation.Military;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;
import org.socraticgrid.statistical.distribution.NormalDistribution;
import org.socraticgrid.statistical.distribution.reader.StratifiedStatisticsReader;
import org.socraticgrid.statistical.distribution.reader.Stratum;

public class ChcsPersonGenerator implements PersonGenerator<ChcsPerson> {
	
	private StratifiedStatisticsReader reader;
	private DefaultPersonGenerator personGenerator;

	public ChcsPersonGenerator() {
		// TODO Auto-generated constructor stub
	}

	public void initialize() {
		personGenerator = new DefaultPersonGenerator();
		personGenerator.initialize();
	}
	
	public void initialize(StratifiedStatisticsReader reader) {
		initialize();
		this.reader = reader;
	}

	public ChcsPerson generatePerson() {
		// TODO Create a person
		ChcsPerson person = new ChcsPerson();
		person.setMilitaryInfo(generateMilitaryRole());
		generatePersonGender(person);
		personGenerator.generateRandomIdentifier(person);
		personGenerator.generatePreferredName(person);
		personGenerator.generateAddress(person);
		generatePersonAge(person);
		generatePersonEducation(person);
		generateEthnicity(person);
		generateMaritalStatus(person);
		return person;
	}

	public List<ChcsPerson> generatePeople(int count) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Returns a random military role according to a specific distribution specified by the underlying stratum distribution
	 * @return
	 */
	public Military generateMilitaryRole() {
		DiscreteProbabilityDistribution<Stratum> militaryDistribution = reader.loadDiscreteGeneratorForColumn(new int[]{1,2}, true);
		Stratum stratum = militaryDistribution.getSample();
		Military militaryRole = new Military(stratum.getAsString("Service"), stratum.getAsString("Rank"));
		return militaryRole;
	}
	
	public void generatePersonAge(ChcsPerson person) {
		int row = reader.getStratumIndex(person.getMilitaryInfo().getAsArray());
		NormalDistribution distribution = reader.loadNormalDistribution(6, 7, row);// TODO Save in a map rather than create each time
		NormalAgeGenerator ageGenerator = new NormalAgeGenerator(distribution);
		double ageInYears = ageGenerator.getNextAge();
		Date birthDate = getBirthdateBackThen(ageInYears);
		person.setDateOfBirth(birthDate);
	}
	
	public Date getBirthdateBackThen(double ageInYears) {
		if(ageInYears < 18) {//Valid military age is 18 <= x <= 65
			ageInYears = 18;
		} if(ageInYears > 65) {
			ageInYears = 65;
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.add(Calendar.HOUR, -1 * (int)(ageInYears*365*24));
		return cal.getTime();
	}
	
	public void generatePersonGender(ChcsPerson person) {
		int row = reader.getStratumIndex(person.getMilitaryInfo().getAsArray());
		DiscreteProbabilityDistribution<String> distribution = reader.loadDiscreteGeneratorForRow(new int[]{3,4}, row, true);// TODO Save in a map rather than create each time
		DiscreteGenderDistributionGenerator generator = new DiscreteGenderDistributionGenerator();
		generator.setDistribution(distribution);
		person.setAdministrativeGender(generator.getNextGender());
	}
	
	public void generatePersonEducation(ChcsPerson person) {
		int row = reader.getStratumIndex(person.getMilitaryInfo().getAsArray());
		DiscreteProbabilityDistribution<String> distribution = reader.loadDiscreteGeneratorForRow(new int[]{17,18,19,20,21}, row, true);// TODO Save in a map rather than create each time
		DiscreteEducationLevelGenerator generator = new DiscreteEducationLevelGenerator();
		generator.setDistribution(distribution);
		person.setEducationLevel(generator.getNextEducationLevel());
	}
	
	public void generateEthnicity(ChcsPerson person) {
		int row = reader.getStratumIndex(person.getMilitaryInfo().getAsArray());
		DiscreteProbabilityDistribution<String> distribution = reader.loadDiscreteGeneratorForRow(new int[]{9,10,11,12,13,14,15,16}, row, true);// TODO Save in a map rather than create each time
		DiscreteEthnicityGenerator generator = new DiscreteEthnicityGenerator();
		generator.setDistribution(distribution);
		person.setEthnicity(generator.getNextEthnicity());
	}
	
	public void generateMaritalStatus(ChcsPerson person) {
		int row = reader.getStratumIndex(person.getMilitaryInfo().getAsArray());
		DiscreteProbabilityDistribution<String> distribution = reader.loadDiscreteGeneratorForRow(new int[]{5}, "Unmarried", row, true);// TODO Save in a map rather than create each time
		DefaultMaritalStatusGenerator generator = new DefaultMaritalStatusGenerator();
		generator.setDistribution(distribution);
		person.setMaritalStatus(generator.getNextMaritalStatus());
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/