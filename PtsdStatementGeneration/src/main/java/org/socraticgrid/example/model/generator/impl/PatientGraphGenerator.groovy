/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl

import org.socraticgrid.clinical.model.clinicalstatement.generator.ClinicalStatementGenerator
import org.socraticgrid.clinical.terminology.mapping.Graph
import org.socraticgrid.clinical.terminology.mapping.Mapping
import org.socraticgrid.clinical.terminology.mapping.MappingLoader
import org.socraticgrid.clinical.terminology.mapping.Node
import org.socraticgrid.example.model.model.AbstractClinicalModel;


//TODO Rename this class
class PatientGraphGenerator {
	
	private ClinicalStatementGenerator clinicalStatementGenerator;
	
	def MappingLoader loadMappings(String configurationFile) {
		MappingLoader loader = new MappingLoader()
		InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(configurationFile)
		loader.load(stream);
		return loader;
	}
	
	def void generateMappings(AbstractClinicalModel source, String configurationFile) {
		List<Graph> graphs = loadMappings(configurationFile).graphs;
		graphs.each{ graph ->
			processGraph(source, graph)
		}
	}
	
	def void processGraph(AbstractClinicalModel source, Graph graph) {
		println "Processing a graph"
		graph.nodes.each{ node ->
			processNode(source, graph, node)
		}
	}
	
	def void processNode(AbstractClinicalModel source, Graph graph, Node node) {
		println "Process a node"
		node.mappings.each{ mapping ->
			processMapping(source, graph, node, mapping)
		}
	}
	
	def void processMapping(AbstractClinicalModel source, Graph graph, Node node, Mapping mapping) {
		println "Process a mapping"
		def labelMapping = mapping.getSourceModelPropertyValue(source)
		println serializer.serialize(source.getPatientIdentifiers().get(0).toString(), mapping.targetModelPropertyLabel, labelMapping)
	}
	
	def void registerClinicalStatementGenerator(ClinicalStatementGenerator clinicalStatementGenerator) {
		this.clinicalStatementGenerator = clinicalStatementGenerator
	}
}
/* **********************************************************************************
 Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
	 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 * *********************************************************************************/