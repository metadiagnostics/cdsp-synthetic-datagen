/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.GenderGenerator;
import org.socraticgrid.clinical.model.generator.impl.BinaryChoiceGenerator;

/**
 * Class assigning M/F gender in a 50%-50% likelihood.
 * 
 * @author Claude Nanjo
 *
 */
public class DefaultGenderGenerator extends BinaryChoiceGenerator implements GenderGenerator {
	
	/**
	 * Method returns either Male or Female randomly. The probability of each gender
	 * is 50%.
	 */
	public AdministrativeGender getNextGender() {
		if(isTrue()) {
			return AdministrativeGender.MALE;
		} else {
			return AdministrativeGender.FEMALE;
		}
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/