/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.generator.ClinicalStatementGenerator;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.statistical.distribution.bayes.AutomatonBuilder;
import org.socraticgrid.statistical.distribution.bayes.AutomatonRunner;
import org.socraticgrid.statistical.distribution.bayes.Universe;
import org.socraticgrid.statistical.distribution.reader.SetReader;

/**
 * TODO Define configuration file format and load from file.
 * TODO Make generic at a later time
 * 
 * @author Claude Nanjo
 *
 */
public class PatientRecordGeneratorConfigurator {
	
	private String configurationFilePath;
	private PatientRecordGenerator<ChcsPerson> recordGenerator;

	public PatientRecordGeneratorConfigurator(String configurationFilePath) {
		this.configurationFilePath = configurationFilePath;
	}
	
	/**
	 * TODO In the interest of time, all will be hard-coded. Shortly after we produce
	 * data, it will be configured from a file.
	 * 
	 * @param patientRecordGenerator
	 */
	public void configure(PatientRecordGenerator<ChcsPerson> patientRecordGenerator) {
		SetReader reader = new SetReader();
		reader.loadConfiguration(configurationFilePath);
		
		List<AutomatonRunner> runners = buildAutomatons(reader);
		recordGenerator = patientRecordGenerator;
		recordGenerator.addAutomatonRunners(runners);
		
		recordGenerator.addConditionalProbabilities(reader.loadConditionalProbabilityGenerators());
	}
	
	/**
	 * TODO Build from configuration file
	 */
	protected List<AutomatonRunner> buildAutomatons(SetReader reader) {
		
		List<Universe> universes = reader.loadUniverses();
		
		List<AutomatonRunner> runners = new ArrayList<AutomatonRunner>();
		
		for(Universe universe : universes) {
			AutomatonBuilder builder = new AutomatonBuilder(universe);
			builder.buildAutomaton();
			AutomatonRunner runner = new AutomatonRunner(builder.getStartingPoint());
			runners.add(runner);
		}
		
		return runners;
	}

}