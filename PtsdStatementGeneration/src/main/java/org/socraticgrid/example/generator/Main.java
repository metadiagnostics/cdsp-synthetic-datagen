/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.generator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.common.jena.RdfModelBuilder;
import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.generator.PatientGenerator;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.generator.impl.ChcsPersonGenerator;
import org.socraticgrid.example.model.generator.impl.DefaultPatientGenerator;
import org.socraticgrid.example.model.generator.impl.DefaultPatientRecordGenerator;
import org.socraticgrid.example.model.generator.impl.PatientRecordGeneratorConfigurator;
import org.socraticgrid.example.model.generator.impl.PatientRecordSynthesizer;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.example.model.serializer.rdf.GenericRdfPatientRecordSerializer;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.SimpleDiabetesMellitusGenerator;
import org.socraticgrid.statistical.distribution.reader.StratifiedStatisticsReader;



public class Main {
	
public static final String STRATIFIED_PROB_FILE = "distribution/StratifiedDemographicDataMilitary.txt";
	
	private PatientRecordSynthesizer<ChcsPerson> patientRecordSynthesizer;
	private PatientRecordGenerator<ChcsPerson> patientRecordGenerator;
	private RdfModelBuilder rdfModelBuilder;
	
	public Main() {
		// TODO Auto-generated constructor stub
	}
	
	public void setUp() {
		StratifiedStatisticsReader reader = new StratifiedStatisticsReader(0, new int[]{1,2});
		InputStream resourceStream = ReaderUtilities.loadResourceFromStream(STRATIFIED_PROB_FILE);
		reader.loadStratifiedStatistics(resourceStream, '\t');
		PersonGenerator<ChcsPerson> personGenerator = new ChcsPersonGenerator();
		((ChcsPersonGenerator)personGenerator).initialize(reader);
		patientRecordGenerator = new DefaultPatientRecordGenerator<ChcsPerson>(personGenerator);
		PatientRecordGeneratorConfigurator configurator = new PatientRecordGeneratorConfigurator("conf/DiseaseProfile/ptsd.xml");
		configurator.configure(patientRecordGenerator);
		PatientGenerator<ChcsPerson> patientGenerator = new DefaultPatientGenerator<ChcsPerson>();
		patientGenerator.registerPersonGenerator(personGenerator);
		patientRecordGenerator.registerPatientGenerator(patientGenerator);
		patientRecordGenerator.registerClinicalStatementGenerator(new SimpleDiabetesMellitusGenerator());
		patientRecordGenerator.initialize();
		rdfModelBuilder = new RdfModelBuilder();
		//patientRecordSynthesizer = new PatientRecordSynthesizer<PatientRecordGenerator<PatientRecord<ChcsPerson>,ChcsPerson, Patient<ChcsPerson>,ClinicalContext>>(patientRecordGenerator);
		patientRecordSynthesizer = new PatientRecordSynthesizer<ChcsPerson>((DefaultPatientRecordGenerator) patientRecordGenerator);
		GenericRdfPatientRecordSerializer patientRecordSerializer = new GenericRdfPatientRecordSerializer();
		patientRecordSynthesizer.registerSerializer(patientRecordSerializer);
	
	}
	
	public void runGenerator(int count, String dirPath, String baseFileName) {
		try {
			for(int index = 0; index < count; index++) {
				File file = new File(dirPath, baseFileName + index + ".xml");
				OutputStream os = new FileOutputStream(file);
				patientRecordSynthesizer.synthesizePatientRecord(DocumentFormat.GENERIC_RDF, os);
				os.close();
			}
		} catch(FileNotFoundException fnfe) {
			System.out.println("Please verify the path: " + fnfe.getMessage());
		}catch(Exception cause) {
			throw new RuntimeException("An error has occurred", cause);
		}
	}
	
	public static void main(String[] args) {
		if(args.length != 3) {
			usage();
		} else {
			int count = Integer.parseInt(args[0]);
			String dirPath = args[1];
			String baseFileName = args[2];
			File dir = new File(dirPath);
			if(!dir.isDirectory()) {
				usage();
			}
			Main main = new Main();
			main.setUp();
			main.runGenerator(count, dirPath, baseFileName);
		}
	}
	
	public static void usage() {
		System.out.println("Main requires three parameters: an integer count and an output dir path and a base file name without an extension");
		System.out.println("\"Main 5 /users/me/temp/work output\" will generate five patient records and output results in /users/me/temp/work. Each file will have an index appended (e.g., output1.xml)");
	}

}