/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class InsomniaStatementGenerator extends BaseClinicalStatementGenerator {

	 
	private DiscreteProbabilityDistribution<String> ptsdInsomniaDistribution;
	private DiscreteProbabilityDistribution<String> noPtsdInsomniaDistribution;

	public InsomniaStatementGenerator() {
		List<DiscreteItemProbabilityPair<String>> pstd = new ArrayList<DiscreteItemProbabilityPair<String>>();
		pstd.add(new DiscreteItemProbabilityPair<String>("INSOMNIA", 0.8));
		pstd.add(new DiscreteItemProbabilityPair<String>("NO_INSOMNIA", 0.20));
		ptsdInsomniaDistribution=new DiscreteProbabilityDistribution<String>(pstd);
		List<DiscreteItemProbabilityPair<String>> noptsd = new ArrayList<DiscreteItemProbabilityPair<String>>();
		noptsd.add(new DiscreteItemProbabilityPair<String>("INSOMNIA", 0.236));
		noptsd.add(new DiscreteItemProbabilityPair<String>("NO_INSOMNIA", 0.764));
		noPtsdInsomniaDistribution=new DiscreteProbabilityDistribution<String>(noptsd);
	}


	
	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		

		if(getPatientRecord(configurationParameters).getIndex().exists(PtsdStatementGenerator.getPtsdIndexKey())) {
			String key = ptsdInsomniaDistribution.getSample();
			if(key.equalsIgnoreCase("INSOMNIA")) {
				Condition condition = new Condition();
				Coding coding = new Coding();
				coding.setCode("193462001");
				coding.setCodeSystem("http://snomed.info/sct");
				coding.setCodeDisplayName("Insomnia");
				condition.setConditionCode(coding);
				Period period = new Period();
				TimeCalculation timeCalculation = new TimeCalculation();
				period.setStart(timeCalculation.getTimeStamp());
				period.setEnd(timeCalculation.getCurrentTimeStamp());
				condition.setTimeOfOnset(period);
				statements.add(condition);
			}
		} else {
			String key = noPtsdInsomniaDistribution.getSample();
			if(key.equalsIgnoreCase("INSOMNIA")) {
				Condition condition = new Condition();
				Coding coding = new Coding();
				coding.setCode("193462001");
				coding.setCodeSystem("http://snomed.info/sct");
				coding.setCodeDisplayName("Insomnia");
				condition.setConditionCode(coding);
				Period period = new Period();
				TimeCalculation timeCalculation = new TimeCalculation();
				period.setStart(timeCalculation.getTimeStamp());
				period.setEnd(timeCalculation.getCurrentTimeStamp());
				condition.setTimeOfOnset(period);
				statements.add(condition);
			}
		}
		
				
		
		return statements;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/