/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.IntegerRange;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.enumeration.Ethnicity;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;

public class HypertensionStatementGenerator extends BaseClinicalStatementGenerator {

	private List<Ethnicity> ethnicityList;
	private List<IntegerRange> cohortList;
	private double[][] confusionMatrix;
	private Random generator;
	
	public HypertensionStatementGenerator() {
		ethnicityList = Arrays.asList(Ethnicity.values());
		cohortList = createAgeCohortList();
		confusionMatrix = createConfusionMatrix();
		generator = new Random(System.currentTimeMillis());
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		double patientAge = getPatientRecord(configurationParameters).getPatient().getEvaluatedPerson()
				.getAgeInYears();
		Ethnicity patientEthnicity = getPatientRecord(configurationParameters).getPatient().getEvaluatedPerson()
				.getEthnicity();

		int indexAgeCohort = getCohortIndex((int)patientAge);
		int indexEthnicity = ethnicityList.indexOf(patientEthnicity);
		
		double probability = confusionMatrix[indexAgeCohort][indexEthnicity];
		
		double guess = generator.nextInt(10000)/10000.0;
		
		if(guess < probability) {
			statements.add(generateCondition());
		}
		
		return statements;
	}

	public Condition generateCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("38341003");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Hypertensive disorder");
		condition.setConditionCode(coding);

		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);

//		System.out.println("Adding hypertension Condition.");

		return condition;

	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}
	
	public List<IntegerRange> createAgeCohortList() {
		List<IntegerRange> cohortList = new ArrayList<IntegerRange>();
		cohortList.add(new IntegerRange(21,25));
		cohortList.add(new IntegerRange(26,30));
		cohortList.add(new IntegerRange(31,35));
		cohortList.add(new IntegerRange(36,40));
		cohortList.add(new IntegerRange(41,null));
		return cohortList;
		
	}
	
	public int getCohortIndex(int age) {
		int index = -1;
		for(IntegerRange range : cohortList) {
			++index;
			if(range.isInRange(age)) {
				break;
			}
		}
		return index;
	}
	
	public double[][] createConfusionMatrix() {
//		NON_HISPANIC_WHITE,
//		HISPANIC,
//		NON_HISPANIC_BLACK,
//		ASIAN,
//		HAWAIIAN,
//		NATIVE,
//		NON_HISPANIC_MULTIRACIAL,
//		NON_HISPANIC_OTHER;
		
		double[][] matrix = new double[5][8];
		
		//*** First Row
		matrix[0][0] = 0.0098;
		matrix[0][1] = 0.0122;
		matrix[0][2] = 0.0086;
		matrix[0][3] = 0.0187;
		matrix[0][4] = 0.008;
		matrix[0][5] = 0.0086;
		
		//*** Second Row
		matrix[1][0] = 0.0224;
		matrix[1][1] = 0.0267;
		matrix[1][2] = 0.0203;
		matrix[1][3] = 0.0484;
		matrix[1][4] = 0.0198;
		matrix[1][5] = 0.0203;
		
		// *** Third Row
		matrix[2][0] = 0.0422;
		matrix[2][1] = 0.0542;
		matrix[2][2] = 0.0551;
		matrix[2][3] = 0.0953;
		matrix[2][4] = 0.0417;
		matrix[2][5] = 0.0551;
		
		// *** Fourth Row
		matrix[3][0] = 0.0815;
		matrix[3][1] = 0.1002;
		matrix[3][2] = 0.1096;
		matrix[3][3] = 0.1761;
		matrix[3][4] = 0.0814;		
		matrix[3][5] = 0.1096;
		
		//*** Fifth row
		matrix[4][0] = 0.1292;
		matrix[4][1] = 0.1475;
		matrix[4][2] = 0.183;
		matrix[4][3] = 0.276;
		matrix[4][4] = 0.1431;	
		matrix[4][5] = 0.183;	
		
		return matrix;
	}
}