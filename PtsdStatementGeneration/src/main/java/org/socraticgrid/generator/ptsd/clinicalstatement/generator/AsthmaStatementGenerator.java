/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.IntegerRange;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.enumeration.Ethnicity;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;

public class AsthmaStatementGenerator extends BaseClinicalStatementGenerator {

	private List<Ethnicity> ethnicityList;
	private List<IntegerRange> cohortList;
	private double[][] confusionMatrix;
	private Random generator;

	public AsthmaStatementGenerator() {
		ethnicityList = Arrays.asList(Ethnicity.values());
		cohortList = createAgeCohortList();
		confusionMatrix = createConfusionMatrix();
		generator = new Random(System.currentTimeMillis());
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();

		double patientAge = getPatientRecord(configurationParameters).getPatient().getEvaluatedPerson()
				.getAgeInYears();
		Ethnicity patientEthnicity = getPatientRecord(configurationParameters).getPatient().getEvaluatedPerson()
				.getEthnicity();

		int indexAgeCohort = getCohortIndex((int) patientAge);
		int indexEthnicity = ethnicityList.indexOf(patientEthnicity);

		double probability = confusionMatrix[indexAgeCohort][indexEthnicity];

		double guess = generator.nextInt(20000) / 20000.0;

		if (guess < probability) {
			statements.add(generateCondition());
		}
		return statements;
	}

	public int getCohortIndex(int age) {
		int index = -1;
		for (IntegerRange range : cohortList) {
			++index;
			if (range.isInRange(age)) {
				break;
			}
		}
		return index;
	}

	public List<IntegerRange> createAgeCohortList() {
		List<IntegerRange> cohortList = new ArrayList<IntegerRange>();
		cohortList.add(new IntegerRange(21, 25));
		cohortList.add(new IntegerRange(26, 30));
		cohortList.add(new IntegerRange(31, 35));
		cohortList.add(new IntegerRange(36, 40));
		cohortList.add(new IntegerRange(41, null));
		return cohortList;

	}

	public Condition generateCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("195967001");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Asthma");
		condition.setConditionCode(coding);
		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);

		return condition;
	}

	public double[][] createConfusionMatrix() {
		// NON_HISPANIC_WHITE,
		// HISPANIC,
		// NON_HISPANIC_BLACK,
		// ASIAN,
		// HAWAIIAN,
		// NATIVE,
		// NON_HISPANIC_MULTIRACIAL,
		// NON_HISPANIC_OTHER;

		double[][] matrix = new double[5][8];

		// *** First Row
		matrix[0][0] = 0.0041;
		matrix[0][1] = 0.0042;
		matrix[0][2] = 0.0044;
		matrix[0][3] = 0.0063;
		matrix[0][4] = 0.0055;
		matrix[0][5] = 0.0044;

		// *** Second Row
		matrix[1][0] = 0.0061;
		matrix[1][1] = 0.0065;
		matrix[1][2] = 0.0066;
		matrix[1][3] = 0.0102;
		matrix[1][4] = 0.0081;
		matrix[1][5] = 0.0066;

		// *** third Row
		matrix[2][0] = 0.0096;
		matrix[2][1] = 0.0105;
		matrix[2][2] = 0.0102;
		matrix[2][3] = 0.0155;
		matrix[2][4] = 0.0122;
		matrix[2][5] = 0.0102;

		// *** Fourth Row
		matrix[3][0] = 0.0122;
		matrix[3][1] = 0.0127;
		matrix[3][2] = 0.0131;
		matrix[3][3] = 0.0196;
		matrix[3][4] = 0.0153;
		matrix[3][5] = 0.0131;

		// *** Fifth row
		matrix[4][0] = 0.0123;
		matrix[4][1] = 0.0126;
		matrix[4][2] = 0.0128;
		matrix[4][3] = 0.0187;
		matrix[4][4] = 0.0146;
		matrix[4][5] = 0.0128;

		return matrix;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}
