/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class SmokingStatementGenerator extends BaseClinicalStatementGenerator {

	private DiscreteProbabilityDistribution<String> maleSmokingDistribution;
	private DiscreteProbabilityDistribution<String> femaleSmokingDistribution;

	public SmokingStatementGenerator() {
		List<DiscreteItemProbabilityPair<String>> maleSmoking = new ArrayList<DiscreteItemProbabilityPair<String>>();
		maleSmoking.add(new DiscreteItemProbabilityPair<String>("Smoking",
				0.205));
		maleSmoking.add(new DiscreteItemProbabilityPair<String>("noSmoking",
				0.795));
		maleSmokingDistribution = new DiscreteProbabilityDistribution<String>(
				maleSmoking);
		List<DiscreteItemProbabilityPair<String>> femaleSmoking = new ArrayList<DiscreteItemProbabilityPair<String>>();
		femaleSmoking.add(new DiscreteItemProbabilityPair<String>("Smoking",
				0.158));
		femaleSmoking.add(new DiscreteItemProbabilityPair<String>("noSmoking",
				0.842));
		femaleSmokingDistribution = new DiscreteProbabilityDistribution<String>(
				femaleSmoking);
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();

		AdministrativeGender patientGender = getPatientRecord(configurationParameters).getPatient()
				.getEvaluatedPerson().getAdministrativeGender();
		String malesmokingDistribution = maleSmokingDistribution.getSample();
		String femalesmokingDistribution = femaleSmokingDistribution
				.getSample();
		if (patientGender.name().compareTo("MALE") == 0) {
			if (malesmokingDistribution.compareToIgnoreCase("Smoking") == 0) {
				Condition condition = generateCondition();
				statements.add(condition);
				condition.indexStatement(getPatientRecord(configurationParameters).getIndex());
				ObservationResult<Double> smokingObservation = generateObservation(0.205);
				statements.add(smokingObservation);
				smokingObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());
			}
		} else if (patientGender.name().compareTo("FEMALE") == 0) {
			if (femalesmokingDistribution.compareToIgnoreCase("Smoking") == 0) {
				Condition condition = generateCondition();
				statements.add(condition);
				condition.indexStatement(getPatientRecord(configurationParameters).getIndex());
				ObservationResult<Double> smokingObservation = generateObservation(0.158);
				statements.add(smokingObservation);
				smokingObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());
			}
		}

		return statements;
	}

	public Condition generateCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("365980008");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Tobacco use and exposure");
		condition.setConditionCode(coding);
		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);
		return condition;
	}

	public ObservationResult<Double> generateObservation(double d) {
		ObservationResult<Double> smokingObservation = new ObservationResult<Double>();
		smokingObservation.setObservationValue(d);
		Period periodObservation = new Period();
		TimeCalculation ObservationtimeCalculation = new TimeCalculation();
		periodObservation.setStart(ObservationtimeCalculation.getTimeStamp());
		periodObservation.setEnd(ObservationtimeCalculation
				.getCurrentTimeStamp());
		smokingObservation.setObservationTime(periodObservation);

		smokingObservation.setObservationFocus(new Coding("365980008",
				"Smoking ", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));
		
		return smokingObservation;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}