/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementKey;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.complex.ReferenceRange;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.NormalDistribution;

public class DepressionStatementGenerator extends BaseClinicalStatementGenerator {

	private NormalDistribution normalDistribution;

	public DepressionStatementGenerator() {
		this.normalDistribution = new NormalDistribution(39, 15.334);
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		Condition depressionCondition = generateDepressionCondition();
		statements.add(depressionCondition);
		System.out.println(getPatientRecord(configurationParameters));
		depressionCondition.indexStatement(getPatientRecord(configurationParameters).getIndex());
		ObservationResult<Integer> depressionObservation = generateDepressionObservation();
		statements.add(depressionObservation);
		depressionObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());

		return statements;
	}

	public Condition generateDepressionCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("35489007");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Depressive disorder");
		condition.setConditionCode(coding);
		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);
		return condition;
	}

	public ObservationResult<Integer> generateDepressionObservation() {
		ObservationResult<Integer> depressionObservation = new ObservationResult<Integer>();
		Integer depressiondistributionVal = (int) normalDistribution
				.getNormalValue();

		depressionObservation.setObservationFocus(new Coding("23459",
				"PHQ9 Score", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));
		ReferenceRange<Integer> referenceRange = new ReferenceRange<Integer>();
		depressionObservation.setReferenceRange(referenceRange);
		Integer low = 0;
		referenceRange.setLow(low);
		Integer high = 61;
		referenceRange.setHigh(high);

		depressionObservation.setObservationValue(setInboundRandomValue(low,
				high, depressiondistributionVal));
		
		return depressionObservation;
	}

	public Integer setInboundRandomValue(Integer low, Integer high,
			Integer normalDistributionVal) {
		Integer inboundnormalDistributionVal;
		//
		// System.out.println("This is the low   " + low + "\t this is high \t "
		// + high + "\t this is normaldistributiion Value"
		// + normalDistributionVal);

		if (normalDistributionVal < low)
			inboundnormalDistributionVal = low;
		if (normalDistributionVal > high)
			inboundnormalDistributionVal = high;
		else
			inboundnormalDistributionVal = normalDistributionVal;

		// System.out.println("This is the inbound val  "
		// + inboundnormalDistributionVal);

		return inboundnormalDistributionVal;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}
	public static ClinicalStatementKey getAnxietyKey() {
		Coding coding = new Coding();
		coding.setCode("35489007");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Depressive disorder");
		return new ClinicalStatementKey(Condition.class.getCanonicalName(),
				"conditionCode", coding);
	}

}