/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Quantity;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.statistical.distribution.NormalDistribution;

public class SimpleDiabetesMellitusGenerator extends BaseClinicalStatementGenerator {
	
	private NormalDistribution normalDistribution;
	
	public SimpleDiabetesMellitusGenerator() {
		normalDistribution = new NormalDistribution(1.20, 0.4);
	}
	
	
	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		List<ClinicalStatement> diabetesClinicalProfile = new ArrayList<ClinicalStatement>();
		ObservationResult<Quantity> result = new ObservationResult<Quantity>();
		result.setObservationFocus(new Coding("123", "Hemoglobin A1C", "0.00.0.0000.0", "SNOMED-CT"));
		result.setObservationValue(new Quantity(normalDistribution.getNormalValue(),"mg/dL"));
		diabetesClinicalProfile.add(result);
		return diabetesClinicalProfile;
	}
	
	public ClinicalContext getClinicalContext(){return null;};
	
	public void setClinicalContext(ClinicalContext clinicalContext){}

}
