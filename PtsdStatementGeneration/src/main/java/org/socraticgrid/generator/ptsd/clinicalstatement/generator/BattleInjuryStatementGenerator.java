/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementKey;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class BattleInjuryStatementGenerator extends BaseClinicalStatementGenerator {

	private DiscreteProbabilityDistribution<String> ptsdBattleInjuryDistribution;
	private DiscreteProbabilityDistribution<String> noPtsdBattleInjuryDistribution;

	public BattleInjuryStatementGenerator() {
		List<DiscreteItemProbabilityPair<String>> pstd = new ArrayList<DiscreteItemProbabilityPair<String>>();
		pstd.add(new DiscreteItemProbabilityPair<String>("BATTLEINJURY", 0.252));
		pstd.add(new DiscreteItemProbabilityPair<String>("NO_BATTLEINJURY", 0.748));
		ptsdBattleInjuryDistribution = new DiscreteProbabilityDistribution<String>(
				pstd);
		List<DiscreteItemProbabilityPair<String>> noptsd = new ArrayList<DiscreteItemProbabilityPair<String>>();
		noptsd.add(new DiscreteItemProbabilityPair<String>("BATTLEINJURY", 0.30));
		noptsd.add(new DiscreteItemProbabilityPair<String>("NO_BATTLEINJURY", 0.70));
		noPtsdBattleInjuryDistribution = new DiscreteProbabilityDistribution<String>(
				noptsd);
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();

		if (getPatientRecord(configurationParameters).getIndex().exists(PtsdStatementGenerator.getPtsdIndexKey())) {
			String key = ptsdBattleInjuryDistribution.getSample();
			if (key.equalsIgnoreCase("BATTLEINJURY")) {
				Condition condition = new Condition();
				Coding coding = new Coding();
				coding.setCode("274933008");
				coding.setCodeSystem("http://snomed.info/sct");
				coding.setCodeDisplayName("Injury resulting from military weapons");
				condition.setConditionCode(coding);

				Period period = new Period();
				TimeCalculation timeCalculation = new TimeCalculation();
				period.setStart(timeCalculation.getTimeStamp());
				period.setEnd(timeCalculation.getCurrentTimeStamp());
				condition.setTimeOfOnset(period);

				statements.add(condition);
				condition.indexStatement(getPatientRecord(configurationParameters).getIndex());
			}
		} else {
			String key = noPtsdBattleInjuryDistribution.getSample();
			if (key.equalsIgnoreCase("BATTLEINJURY")) {
				Condition condition = new Condition();
				Coding coding = new Coding();
				coding.setCode("274933008");
				coding.setCodeSystem("http://snomed.info/sct");
				coding.setCodeDisplayName("Injury resulting from military weapons");
				condition.setConditionCode(coding);

				Period period = new Period();
				TimeCalculation timeCalculation = new TimeCalculation();
				period.setStart(timeCalculation.getTimeStamp());
				period.setEnd(timeCalculation.getCurrentTimeStamp());
				condition.setTimeOfOnset(period);

				statements.add(condition);
				condition.indexStatement(getPatientRecord(configurationParameters).getIndex());
			}
		}
		return statements;
	}

	public static ClinicalStatementKey getBattleInjuryIndexKey() {
		Coding coding = new Coding();
		coding.setCode("274933008");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Injury resulting from military weapons");
		return new ClinicalStatementKey(Condition.class.getCanonicalName(), "conditionCode", coding);
	}


	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}