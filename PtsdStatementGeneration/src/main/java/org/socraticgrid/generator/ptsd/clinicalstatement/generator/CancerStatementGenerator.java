/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class CancerStatementGenerator extends BaseClinicalStatementGenerator {

	private DiscreteProbabilityDistribution<String> maleCancerDistribution;
	private DiscreteProbabilityDistribution<String> femaleCancerDistribution;

	public CancerStatementGenerator() {
		List<DiscreteItemProbabilityPair<String>> maleCancer = new ArrayList<DiscreteItemProbabilityPair<String>>();
		maleCancer.add(new DiscreteItemProbabilityPair<String>("hasCancer",
				0.073));
		maleCancer.add(new DiscreteItemProbabilityPair<String>("noCancer",
				0.927));
		maleCancerDistribution = new DiscreteProbabilityDistribution<String>(
				maleCancer);
		List<DiscreteItemProbabilityPair<String>> femaleCancer = new ArrayList<DiscreteItemProbabilityPair<String>>();
		femaleCancer.add(new DiscreteItemProbabilityPair<String>("hasCancer",
				0.075));
		femaleCancer.add(new DiscreteItemProbabilityPair<String>("noCancer",
				0.923));
		femaleCancerDistribution = new DiscreteProbabilityDistribution<String>(
				femaleCancer);
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();

		AdministrativeGender patientGender = getPatientRecord(configurationParameters).getPatient()
				.getEvaluatedPerson().getAdministrativeGender();
		String malecancerDistribution = maleCancerDistribution.getSample();
		String femalecancerDistribution = femaleCancerDistribution.getSample();
		if (patientGender.name().compareTo("MALE") == 0) {
			if (malecancerDistribution.compareToIgnoreCase("hasCancer") == 0) {
				Condition condition = generateCondition();
				statements.add(condition);
			}
		} else if (patientGender.name().compareTo("FEMALE") == 0) {
			if (femalecancerDistribution.compareToIgnoreCase("hasCancer") == 0) {
				Condition condition = generateCondition();
				statements.add(condition);
			}
		}

		return statements;
	}

	public Condition generateCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("189536001");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Neoplasm of unspecified nature of other genitourinary organs");
		condition.setConditionCode(coding);

		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);

		return condition;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}