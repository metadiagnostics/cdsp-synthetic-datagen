/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementKey;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;

public class AlcoholAbuseStatementGenerator extends BaseClinicalStatementGenerator {

	public AlcoholAbuseStatementGenerator() {

	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();

		Condition condition = getCondition();
		statements.add(condition);
		condition.indexStatement(getPatientRecord(configurationParameters).getIndex());

		return statements;
	}

	public Condition getCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("191882002");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Nondependent alcohol abuse, continuous");
		condition.setConditionCode(coding);

		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);
		return condition;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

	public static ClinicalStatementKey getAlcoholKey() {
		Coding coding = new Coding();
		coding.setCode("191882002");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Nondependent alcohol abuse, continuous");
		return new ClinicalStatementKey(Condition.class.getCanonicalName(),
				"conditionCode", coding);
	}

}