/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.complex.ReferenceRange;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.DoubleRange;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class BodyMassIndexStatementGenerator extends BaseClinicalStatementGenerator {

	private List<DoubleRange> bmiList;
	private DiscreteProbabilityDistribution<String> bmiDistribution;

	public BodyMassIndexStatementGenerator() {
		List<DiscreteItemProbabilityPair<String>> bmi = new ArrayList<DiscreteItemProbabilityPair<String>>();
		bmi.add(new DiscreteItemProbabilityPair<String>("Low BMI", 0.0043));
		bmi.add(new DiscreteItemProbabilityPair<String>("Normal BMI", 0.3989));
		bmi.add(new DiscreteItemProbabilityPair<String>("Overweight", 0.4609));
		bmi.add(new DiscreteItemProbabilityPair<String>("Obesity", 0.1359));
		bmiDistribution = new DiscreteProbabilityDistribution<String>(bmi);
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		ObservationResult<Double> bmiScore = null;

		String bmi = bmiDistribution.getSample();
		if (bmi.equalsIgnoreCase("Low BMI")) {
			bmiScore = generateObservation("Low BMI", (double) 0, 18.5);
		} else if (bmi.equalsIgnoreCase("Normal BMI")) {
			bmiScore = generateObservation("Normal BMI", 18.5, 24.9);
		} else if (bmi.equalsIgnoreCase("Overweight")) {
			bmiScore = generateObservation("Overweight", 24.9, 29.9);
		} else if (bmi.equalsIgnoreCase("Obesity")) {
			bmiScore = generateObservation("Obesity", (double) 30, null);
		}
		statements.add(bmiScore);
		return statements;

	}

	public int getBMIIndex(double value) {
		int index = -1;
		for (DoubleRange range : bmiList) {
			++index;
			if (range.isInRange(value)) {
				break;
			}
		}
		return index;
	}


	public String getSnomedCode(String bmiLevel) {
		HashMap<String, String> bmiSnomedCodes = new HashMap<String, String>();
		bmiSnomedCodes.put("Low BMI", "310252000");
		bmiSnomedCodes.put("Normal BMI", "412768003");
		bmiSnomedCodes.put("Overweight", "162863004");
		bmiSnomedCodes.put("Obesity", "162864005");

		return bmiSnomedCodes.get(bmiLevel);
	}

	public ObservationResult<Double> generateObservation(String bmiCategory,
			Double low, Double high) {

		ObservationResult<Double> obesityObservation = new ObservationResult<Double>();

		obesityObservation.setObservationFocus(new Coding(
				getSnomedCode(bmiCategory), "BMI Score",
				"http://snomed.info/sct", "SNOMED"));

		ReferenceRange<Double> referenceRange = new ReferenceRange<Double>();
		obesityObservation.setReferenceRange(referenceRange);
		referenceRange.setLow(low);
		referenceRange.setHigh(high);
		Period severityPeriod = new Period();
		TimeCalculation severitytimeCalculation = new TimeCalculation();
		severityPeriod.setStart(severitytimeCalculation.getTimeStamp());
		severityPeriod.setEnd(severitytimeCalculation.getCurrentTimeStamp());
		obesityObservation.setObservationTime(severityPeriod);
		return obesityObservation;

	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}