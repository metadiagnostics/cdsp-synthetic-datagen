/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class DeploymentStatementGenerator extends BaseClinicalStatementGenerator {

	private DiscreteProbabilityDistribution<Integer> distribution;
	

	public DeploymentStatementGenerator() {
		buildDiscreteDistibution();
	}

	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		
		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		ClinicalStatement deployementObservation = generateObservation();
		statements.add(deployementObservation);
		deployementObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());
		return statements;
	}

	public ClinicalStatement generateObservation() {
		ObservationResult<Integer> deployementObservation = new ObservationResult<Integer>();
		deployementObservation.setObservationFocus(new Coding("23490",
				"Deployment", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));

		deployementObservation.setObservationValue(distribution.getSample());
		return deployementObservation;

	}

	public void buildDiscreteDistibution() {

		List<DiscreteItemProbabilityPair<Integer>> masterItemList = new ArrayList<DiscreteItemProbabilityPair<Integer>>();
		masterItemList.add(new DiscreteItemProbabilityPair<Integer>(1, 0.14));
		masterItemList.add(new DiscreteItemProbabilityPair<Integer>(2, 0.29));
		masterItemList.add(new DiscreteItemProbabilityPair<Integer>(3, 0.14));
		distribution = new DiscreteProbabilityDistribution<Integer>(
				masterItemList);
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}