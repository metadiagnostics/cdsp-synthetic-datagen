/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.ptsd.clinicalstatement.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.complex.ReferenceRange;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.NormalDistribution;

public class PtsdBaseLineStatementGenerator extends BaseClinicalStatementGenerator {

	private NormalDistribution auditDistribution;
	private NormalDistribution ptsdSeverityDistribution;
	private NormalDistribution ptsdSymptomsDistribution;

	public PtsdBaseLineStatementGenerator() {
		auditDistribution = new NormalDistribution(4.7, 5.75);
		ptsdSeverityDistribution = new NormalDistribution(30.7, 12.54);
		ptsdSymptomsDistribution = new NormalDistribution(39.52, 39.51);
	}
	
	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {

		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		ReferenceRange<Integer> referenceRange;
		Integer low, high;
		

		/**
		 * Set the Severity Range date
		 */

		ObservationResult<Integer> severityObservation = new ObservationResult<Integer>();
		Integer severitydistributionVal = (int) ptsdSeverityDistribution
				.getNormalValue();
		severityObservation.setObservationFocus(new Coding("23456",
				"PCL Score", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));
		referenceRange = new ReferenceRange<Integer>();
		severityObservation.setReferenceRange(referenceRange);
		low = 17;
		referenceRange.setLow(low);
		high = 87;
		referenceRange.setHigh(high);
		Period severityPeriod = new Period();
		TimeCalculation severitytimeCalculation = new TimeCalculation();
		severityPeriod.setStart(severitytimeCalculation.getTimeStamp());
		severityPeriod.setEnd(severitytimeCalculation.getCurrentTimeStamp());
		severityObservation.setObservationTime(severityPeriod);

		severityObservation.setObservationValue(setInboundRandomValue(
				low, high,
				severitydistributionVal));
		statements.add(severityObservation);
		System.out.println(getPatientRecord(configurationParameters));
		severityObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());

		/**
		 * Set the PTSD Range
		 */

		ObservationResult<Integer> symptomObservation = new ObservationResult<Integer>();
		Integer symptomdistributionVal = (int) ptsdSymptomsDistribution
				.getNormalValue();

		symptomObservation.setObservationFocus(new Coding("23457",
				"PTSD Symptoms", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));
		referenceRange = new ReferenceRange<Integer>();
		low = 0;
		referenceRange.setLow(low);
		high = 136;
		referenceRange.setHigh(high);
		symptomObservation.setReferenceRange(referenceRange);
		symptomObservation.setObservationValue(setInboundRandomValue(
				low, high,
				symptomdistributionVal));
		statements.add(symptomObservation);
		symptomObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());

		/**
		 * Set the Audit Score
		 * 
		 */

		ObservationResult<Integer> auditscoreObservation = new ObservationResult<Integer>();
		auditscoreObservation.setObservationFocus(new Coding("23457",
				"Audit Score", "http://www.socraticgrid.org/terminology",
				"SocraticGrid"));
		Integer auditdistributionVal = (int) auditDistribution.getNormalValue();

		referenceRange = new ReferenceRange<Integer>();
		auditscoreObservation.setReferenceRange(referenceRange);
		low = 0;
		referenceRange.setLow(low);
		high = 37;
		referenceRange.setHigh(high);
		Period auditPeriod = new Period();
		TimeCalculation audittimeCalculation = new TimeCalculation();
		auditPeriod.setStart(audittimeCalculation.getTimeStamp());
		auditPeriod.setEnd(audittimeCalculation.getCurrentTimeStamp());
		auditscoreObservation.setObservationTime(auditPeriod);
		auditscoreObservation.setObservationValue(setInboundRandomValue(
				low, high,
				auditdistributionVal));

		statements.add(auditscoreObservation);
		auditscoreObservation.indexStatement(getPatientRecord(configurationParameters).getIndex());

		return statements;
	}

	public Integer setInboundRandomValue(Integer low, Integer high,
			Integer normalDistributionVal) {
		Integer inboundnormalDistributionVal;

		if (normalDistributionVal < low)
			inboundnormalDistributionVal = low;
		else {
			if (normalDistributionVal > high)

				inboundnormalDistributionVal = high;
			else
				inboundnormalDistributionVal = normalDistributionVal;
		}

		return inboundnormalDistributionVal;
	}

	public ClinicalContext getClinicalContext() {
		// TODO Auto-generated method stub
		return null;
	}

	public void setClinicalContext(ClinicalContext clinicalContext) {
		// TODO Auto-generated method stub

	}

}

