/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.clinicalstatement.generator;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.BodyMassIndexStatementGenerator;

public class BodyMassIndexStatementGeneratorTest {

	private static final double ERROR_RATE = 0.05;
	private BodyMassIndexStatementGenerator generator;
	private int count = 1000000;
	List<ClinicalStatement> statements;

	@Before
	public void setUp() throws Exception {
		generator = new BodyMassIndexStatementGenerator();
	}

	@Test
	public void testGenerateStatements() {
		ChcsPerson person = new ChcsPerson();
		Patient<ChcsPerson> patient = new Patient<ChcsPerson>();
		patient.setEvaluatedPerson(person);
		PatientRecord<ChcsPerson> record = new PatientRecord<ChcsPerson>();
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put(PatientRecord.class.getName(), record);
		record.setPatient(patient);
		int low = 0;
		int normal = 0;
		int over = 0;
		int obese = 0;
		for (int index = 0; index < count; index++) {
			statements = generator.generate(parameters);
			ObservationResult clinicalStatement = (ObservationResult) statements
					.get(0);
			if (clinicalStatement.getObservationFocus().getCode()
					.compareTo("310252000") == 0) {
				low++;
			} else if (clinicalStatement.getObservationFocus().getCode()
					.compareTo("412768003") == 0) {
				normal++;
			} else if (clinicalStatement.getObservationFocus().getCode()
					.compareTo("162863004") == 0) {
				over++;
			} else if (clinicalStatement.getObservationFocus().getCode()
					.compareTo("162864005") == 0) {
				obese++;
			}

		}

		double probabilitylow = (double) low / (double) count;
		assertEquals(0.0043, probabilitylow, getError(0.0043));

		double probabilitynormal = (double) normal / (double) count;
		assertEquals(0.3989, probabilitynormal, getError(0.3989));

		double probabilityover = (double) over / (double) count;
		assertEquals(0.4609, probabilityover, getError(0.4609));

		double probabilityobese = (double) obese / (double) count;
		assertEquals(0.1359, probabilityobese, getError(0.1359));

	}

	public double getError(double expectedValue) {
		return ERROR_RATE * expectedValue;
	}
}
