/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.clinicalstatement.generator;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.enumeration.Ethnicity;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.AsthmaStatementGenerator;

public class AsthmaStatementGeneratorTest {

	private static final double ERROR_RATE = 0.1;
	private AsthmaStatementGenerator generator;
	private int count = 200000;
	private int observation;

	@Before
	public void setUp() throws Exception {
		generator = new AsthmaStatementGenerator();
	}

	@Test
	public void testGenerateStatements() throws ParseException {
		ChcsPerson person = new ChcsPerson();
		person.setEthnicity(Ethnicity.HISPANIC);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date birthDate = sdf.parse("29/11/1981");
		person.setDateOfBirth(birthDate);
		Patient<ChcsPerson> patient = new Patient<ChcsPerson>();
		patient.setEvaluatedPerson(person);
		PatientRecord<ChcsPerson> record = new PatientRecord<ChcsPerson>();
		record.setPatient(patient);
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put(PatientRecord.class.getName(), record);
		for (int index = 0; index < count; index++) {
			List<ClinicalStatement> statements = generator.generate(parameters);
			observation += statements.size();
		}
		double probability = (double) observation / (double) count;
		assertEquals(0.0122, probability, getError(0.0122));

	}

	@Test
	public void testGenerateStatements71() throws ParseException {
		ChcsPerson person = new ChcsPerson();
		person.setEthnicity(Ethnicity.NON_HISPANIC_BLACK);
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date birthDate = sdf.parse("29/11/1971");
		person.setDateOfBirth(birthDate);
		Patient<ChcsPerson> patient = new Patient<ChcsPerson>();
		patient.setEvaluatedPerson(person);
		PatientRecord<ChcsPerson> record = new PatientRecord<ChcsPerson>();
		record.setPatient(patient);
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put(PatientRecord.class.getName(), record);
		for (int index = 0; index < count; index++) {
			List<ClinicalStatement> statements = generator.generate(parameters);
			observation += statements.size();
		}
		double probability = (double) observation / (double) count;
		assertEquals(0.0187, probability, getError(0.0187));

	}

	public double getError(double expectedValue) {
		return ERROR_RATE * expectedValue;
	}
}
