/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.clinicalstatement.generator;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.CardioVascularStatementGenerator;

public class CardioVascularStatementGeneratorTest {

	private static final double ERROR_RATE = 0.05;
	private PatientRecord<ChcsPerson> record;
	private CardioVascularStatementGenerator generator;
	private int count = 200000;

	@Before
	public void setUp() throws Exception {
		generator = new CardioVascularStatementGenerator();
	}

	@Test
	public void testFemaleGenerateStatements() {
		ChcsPerson person = new ChcsPerson();

		person.setAdministrativeGender(AdministrativeGender.FEMALE);
		Patient<ChcsPerson> patient = new Patient<ChcsPerson>();
		patient.setEvaluatedPerson(person);
		record = new PatientRecord<ChcsPerson>();
		record.setPatient(patient);
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put(PatientRecord.class.getName(), record);
		int condition = 0;
		for (int index = 0; index < count; index++) {
			List<ClinicalStatement> statements = generator.generate(parameters);
			condition += statements.size();
		}
		double probability = (double) condition / (double) count;
		assertEquals(0.032, probability, getError(0.032));
	}

	@Test
	public void testMaleGenerateStatements() {
		ChcsPerson person = new ChcsPerson();
		person.setAdministrativeGender(AdministrativeGender.MALE);
		Patient<ChcsPerson> patient = new Patient<ChcsPerson>();
		patient.setEvaluatedPerson(person);
		record = new PatientRecord<ChcsPerson>();
		record.setPatient(patient);
		Map<String,Object> parameters = new HashMap<String,Object>();
		parameters.put(PatientRecord.class.getName(), record);
		int condition = 0;
		for (int index = 0; index < count; index++) {
			List<ClinicalStatement> statements = generator.generate(parameters);
			condition += statements.size();
		}
		double probability = (double) condition / (double) count;
		assertEquals(0.059, probability, getError(0.059));

	}
	
	public double getError(double expectedValue) {
		return ERROR_RATE * expectedValue;
	}

}
