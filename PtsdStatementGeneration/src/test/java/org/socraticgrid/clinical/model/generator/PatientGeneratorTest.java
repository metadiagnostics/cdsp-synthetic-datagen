/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.generator.impl.DefaultPatientGenerator;

public class PatientGeneratorTest {
	
	private DefaultPatientGenerator patientGenerator;

	@Before
	public void setUp() throws Exception {
		patientGenerator = new DefaultPatientGenerator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGeneratePatient() {
	}

}
