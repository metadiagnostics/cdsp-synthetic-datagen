/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.model.generator.impl.BinaryChoiceGenerator;

public class BinaryChoiceGeneratorTest {
	
	private BinaryChoiceGenerator booleanGenerator;
	private static double APPROX_TRUE_FREQUENCY = 0.5;
	private static double MARGIN_OF_ERROR = 0.05;

	@Before
	public void setUp() throws Exception {
		booleanGenerator = new BinaryChoiceGenerator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBooleanFrequency() {
		int trueCount = 0;
		for(int i=0; i < 1000; i++) {
			if(booleanGenerator.isTrue()) {
				++trueCount;
			}
		}
		assertEquals(APPROX_TRUE_FREQUENCY, trueCount/1000.0, MARGIN_OF_ERROR);
	}

}
