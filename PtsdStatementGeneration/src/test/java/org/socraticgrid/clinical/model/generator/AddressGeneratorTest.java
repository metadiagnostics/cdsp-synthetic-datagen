/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.generator.impl.AddressGenerator;
import org.socraticgrid.statistical.distribution.reader.AddressCsvReader;
import org.socraticgrid.statistical.distribution.reader.AddressReader;

public class AddressGeneratorTest {

	private AddressGenerator addressGenerator;

	@Before
	public void setUp() throws Exception {
		addressGenerator = new AddressGenerator();
		AddressReader reader = new AddressCsvReader("distribution/addressesUS.txt", '\t', false,
													"distribution/citiesUS.txt", '\t', false, 
													"distribution/StatesByPopulationUS.txt", '\t', false, 
													"distribution/ZipByPopulationUS.txt", '\t', false);
		reader.setHeaderFlag(true);
		addressGenerator.initialize(reader);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateName() {
		try {
			for(int index = 0; index < 10000; index++) {
				Address address = addressGenerator.generateAddress();
				assertNotNull(address);
				System.out.println(address);
			}
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

}
