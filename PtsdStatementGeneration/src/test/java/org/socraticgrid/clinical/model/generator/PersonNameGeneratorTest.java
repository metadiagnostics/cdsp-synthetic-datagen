/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.datatypes.complex.HumanName;
import org.socraticgrid.example.model.generator.impl.PersonNameGenerator;
import org.socraticgrid.statistical.distribution.reader.PersonNameCsvReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameReader;

public class PersonNameGeneratorTest {
	
	private PersonNameGenerator personNameGenerator;

	@Before
	public void setUp() throws Exception {
		personNameGenerator = new PersonNameGenerator();
		PersonNameReader reader = new PersonNameCsvReader(	"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonLastNames.txt", '\t', false);
		reader.setHeaderFlag(true);
		personNameGenerator.initialize(reader);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateName() {
		try {
			for(int index = 0; index < 1000; index++) {
				HumanName name = personNameGenerator.generateName();
				assertNotNull(name);
				System.out.println(name);
			}
		} catch(Exception e) {
			fail();
		}
	}

}
