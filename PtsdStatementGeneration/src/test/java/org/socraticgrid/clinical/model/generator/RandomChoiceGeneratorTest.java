/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.impl.RandomChoiceGenerator;

public class RandomChoiceGeneratorTest {
	
	private RandomChoiceGenerator randomChoiceGenerator;

	@Before
	public void setUp() throws Exception {
		randomChoiceGenerator = new RandomChoiceGenerator();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetChoiceNumber() {
		randomChoiceGenerator.setItemCount(3);
		for(int i = 0; i < 1000; i++) {
			int choice = randomChoiceGenerator.getChoiceNumber();
			if(choice > 2 || choice < 0) {
				fail("Invalid choice: " + choice);
			}
		}
		assertTrue(true);
	}

	@Test
	public void testGetEnumerationItem() {
		for(int i = 0; i < 1000; i++) {
			System.out.println(randomChoiceGenerator.getEnumerationItem(AdministrativeGender.class));
		}
	}

}
