/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.clinicalstatement.generator;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.PtsdBaseLineStatementGenerator;
import org.socraticgrid.statistical.distribution.NormalDistribution;

public class PtsdBaseLineStatementGeneratorTest {

	private PtsdBaseLineStatementGenerator ptsdBaseLineStatementGenerator;
	private NormalDistribution auditDistribution;
	private NormalDistribution ptsdSeverityDistribution;
	private NormalDistribution ptsdSymptomsDistribution;

	@Before
	public void setUp() throws Exception {
		auditDistribution = new NormalDistribution(4.7, 5.75);
		ptsdSeverityDistribution = new NormalDistribution(30.7, 12.54);
		ptsdSymptomsDistribution = new NormalDistribution(39.52, 39.51);
		ptsdBaseLineStatementGenerator = new PtsdBaseLineStatementGenerator();
	}

	@Test
	public void testPtsdSeverity() {
		Integer severitydistributionVal = (int) ptsdSeverityDistribution
				.getNormalValue();
		Integer value = ptsdBaseLineStatementGenerator.setInboundRandomValue(
				17, 87, severitydistributionVal);
		assertTrue(value >= 17);
		assertTrue(value < 87);

	}

	@Test
	public void testPtsdSymptoms() {
		Integer symptomdistributionVal = (int) ptsdSymptomsDistribution
				.getNormalValue();
		Integer value = ptsdBaseLineStatementGenerator.setInboundRandomValue(0,
				136, symptomdistributionVal);
		assertTrue(value >= 0);
		assertTrue(value < 136);

	}

	@Test
	public void testPtsdAuditScore() {
		Integer auditdistributionVal = (int) auditDistribution.getNormalValue();
		Integer value = ptsdBaseLineStatementGenerator.setInboundRandomValue(
				0, 37, auditdistributionVal);
		assertTrue(value >= 0);
		assertTrue(value < 37);

	}

}
