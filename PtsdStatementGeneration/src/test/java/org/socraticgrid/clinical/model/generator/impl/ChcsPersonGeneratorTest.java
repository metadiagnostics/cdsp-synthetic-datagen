/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.generator.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.example.model.enumeration.MilitaryRank;
import org.socraticgrid.example.model.enumeration.MilitaryService;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.generator.impl.ChcsPersonGenerator;
import org.socraticgrid.example.model.participation.Military;
import org.socraticgrid.statistical.distribution.reader.StratifiedStatisticsReader;

public class ChcsPersonGeneratorTest {
	
	public static final String STRATIFIED_PROB_FILE = "distribution/StratifiedDemographicDataMilitary.txt";
	private ChcsPersonGenerator generator;

	
	private StratifiedStatisticsReader reader;
	private List<String[]> lines;

	@Before
	public void setUp() throws Exception {
		reader = new StratifiedStatisticsReader(0, new int[]{1,2});
		InputStream resourceStream = reader.getClass().getClassLoader().getResourceAsStream(STRATIFIED_PROB_FILE);
		assertNotNull(resourceStream);
		lines = reader.loadStratifiedStatistics(resourceStream, '\t');
		generator = new ChcsPersonGenerator();
		generator.initialize(reader);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGeneratePerson() {
		ChcsPerson person = generator.generatePerson();
		System.out.println(person.getMilitaryInfo().getService() + "," + person.getMilitaryInfo().getRank());
	}
	
	@Test
	public void testGenerateGender() {
		ChcsPerson person = generator.generatePerson();
		//generator.generatePersonAge(person);
		assertNotNull(AdministrativeGender.valueOf(person.getAdministrativeGender().toString()));
	}
	
//	@Test
//	public void testGetBirthdateBackThen() {
//		long age = System.currentTimeMillis() - 27 * ChcsPersonGenerator.millisecondsPerYear;
//		assertEquals(new Date(age).getTime(), generator.getBirthdateBackThen(27).getTime());
//	}

	@Test
	public void testGenerateMilitaryRole() {
		try {
			Military militaryRole = generator.generateMilitaryRole();
			assertNotNull(MilitaryService.valueOf(militaryRole.getService().toString()));
			assertNotNull(MilitaryRank.valueOf(militaryRole.getRank().toString()));
		} catch(Exception e) {
			fail(e.getMessage());
		}
	}
	
	@Test
	public void testGenerateAge() {
		ChcsPerson person = generator.generatePerson();
		generator.generatePersonAge(person);
	}

}
