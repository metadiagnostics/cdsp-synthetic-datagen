/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.model.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.entity.Person;

public class PersonTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAgeInYears() {
		try {
			Calendar dob = new GregorianCalendar(1967, 9, 11);
			Person person = new Person();
			person.setDateOfBirth(dob.getTime());
			int age = (int) person.getAgeInYears();
			assertEquals(47,age);
		} catch(Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

}
