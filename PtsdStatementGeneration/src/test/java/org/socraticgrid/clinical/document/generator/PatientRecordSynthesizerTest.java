/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.document.generator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementKey;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;
import org.socraticgrid.example.model.generator.PatientGenerator;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.generator.impl.ChcsPersonGenerator;
import org.socraticgrid.example.model.generator.impl.DefaultPatientGenerator;
import org.socraticgrid.example.model.generator.impl.DefaultPatientRecordGenerator;
import org.socraticgrid.example.model.generator.impl.PatientRecordGeneratorConfigurator;
import org.socraticgrid.example.model.generator.impl.PatientRecordSynthesizer;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.example.model.serializer.rdf.GenericRdfPatientRecordSerializer;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.AnxietyStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.MaleSexualAbuseStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.PtsdStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.SimpleDiabetesMellitusGenerator;
import org.socraticgrid.statistical.distribution.reader.StratifiedStatisticsReader;

public class PatientRecordSynthesizerTest {

	public static final String STRATIFIED_PROB_FILE = "distribution/StratifiedDemographicDataMilitary.txt";

	public static final double ERROR_RATE = 0.1;
	public static final double PTSDRATE = 0.14;

	private PatientRecordSynthesizer<ChcsPerson> patientRecordSynthesizer;
	private PatientRecordGenerator<ChcsPerson> patientRecordGenerator;
	// private ArrayList<ClinicalStatementKey> femaleClinicalStatements = new
	// ArrayList<ClinicalStatementKey>();
	// private ArrayList<ClinicalStatementKey> maleClinicalStatements = new
	// ArrayList<ClinicalStatementKey>();

	private int femaleSexualAssualt = 0;
	private int maleSexualAssualt = 0;
	private int maleCount = 0;
	private int femaleCount = 0;
	private int ptsdGeneratedPopulation = 0;
	private int keyCount = 0;
	private int anxietyGeneratedPopulation = 0;

	@Before
	public void setUp() throws Exception {
		StratifiedStatisticsReader reader = new StratifiedStatisticsReader(0,
				new int[] { 1, 2 });
		InputStream resourceStream = reader.getClass().getClassLoader()
				.getResourceAsStream(STRATIFIED_PROB_FILE);
		assertNotNull(resourceStream);
		reader.loadStratifiedStatistics(resourceStream, '\t');
		PersonGenerator<ChcsPerson> personGenerator = new ChcsPersonGenerator();
		((ChcsPersonGenerator) personGenerator).initialize(reader);
		patientRecordGenerator = new DefaultPatientRecordGenerator(personGenerator);
		PatientRecordGeneratorConfigurator configurator = new PatientRecordGeneratorConfigurator(
				"conf/DiseaseProfile/ptsd.xml");
		configurator.configure(patientRecordGenerator);
		PatientGenerator<ChcsPerson> patientGenerator = new DefaultPatientGenerator<ChcsPerson>();
		patientGenerator.registerPersonGenerator(personGenerator);
		patientRecordGenerator.registerPatientGenerator(patientGenerator);
		patientRecordGenerator
				.registerClinicalStatementGenerator(new SimpleDiabetesMellitusGenerator());
		patientRecordGenerator.initialize();
		patientRecordSynthesizer = new PatientRecordSynthesizer<ChcsPerson>(
				(DefaultPatientRecordGenerator) patientRecordGenerator);
		GenericRdfPatientRecordSerializer patientRecordSerializer = new GenericRdfPatientRecordSerializer();
		patientRecordSynthesizer.registerSerializer(patientRecordSerializer);

		femaleSexualAssualt = 0;
		maleSexualAssualt = 0;
		maleCount = 0;
		femaleCount = 0;
		ptsdGeneratedPopulation = 0;
		keyCount = 0;
		anxietyGeneratedPopulation = 0;
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGenerateClinicalRecord() {
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			patientRecordSynthesizer.synthesizePatientRecord(
					DocumentFormat.GENERIC_RDF, baos);
			String output = baos.toString();
			assertNotNull(output);
			System.out.println(output);

		} catch (Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

	public void countGenerateClinicalRecords(PatientRecord<ChcsPerson> record,
			ClinicalStatementKey generatorName) {
		if (record.getIndex().exists(generatorName)) {
			keyCount++;
		}

	}

	/**
	 * Gender Tests
	 * 
	 * @param record
	 */

	protected void assertTopLevelSexualAssultProbabilities(int populationCount) {
		double femaleSexualAssault = (double) femaleSexualAssualt
				/ (double) femaleCount;
		double maleSexualAssault = (double) maleSexualAssualt
				/ (double) maleCount;
		System.out.println("Female Count: " + femaleCount);
		System.out.println("Male Count: " + maleCount);
		System.out.println("Female Sexual Assault: " + femaleSexualAssualt);
		System.out.println("Male Sexual Assault: " + maleSexualAssualt);
		assertEquals(0.13, femaleSexualAssault, getError(0.13));
		assertEquals(0.016, maleSexualAssault, getError(0.016));
	}

	public void testGenderGenerateClinicalRecords(
			PatientRecord<ChcsPerson> record) {

		if (record.getPatient().getEvaluatedPerson().getAdministrativeGender() == AdministrativeGender.FEMALE) {

			if (record.getIndex().exists(
					MaleSexualAbuseStatementGenerator.getSexualAssaultKey())) {
				femaleSexualAssualt++;
			}
			femaleCount++;

		} else {
			if (record.getIndex().exists(
					MaleSexualAbuseStatementGenerator.getSexualAssaultKey())) {
				maleSexualAssualt++;
			}
			maleCount++;
		}
	}

	@Test
	public void testGenderGenerateClinicalRecords() {
		try {
			Map<ClinicalStatementKey, Integer> itemCount = new HashMap<ClinicalStatementKey, Integer>();

			boolean continueFlag = true;
			int index = 20000;
			int count = 0;

			while (continueFlag && count < index) {
				PatientRecord<ChcsPerson> record = patientRecordGenerator
						.generatePatientRecord();
				testGenderGenerateClinicalRecords(record);

				List<ClinicalStatement> statements = record
						.getClinicalStatements();

				for (ClinicalStatement statement : statements) {
					Integer cnt = itemCount.get(statement.getIndexKey());
					if (cnt == null) {
						cnt = 0;
					} else {
						cnt += 1;
					}
					itemCount.put(statement.getIndexKey(), cnt);
				}
				count++;
			}
			assertTopLevelSexualAssultProbabilities(count);
		} catch (Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

	/**
	 * 
	 */

	@Test
	public void testAnx_Alc_Dep_LevelGenerateClinicalRecords() {
		try {
			Map<ClinicalStatementKey, Integer> itemCount = new HashMap<ClinicalStatementKey, Integer>();

			boolean continueFlag = true;
			int index = 20000;
			int count = 0;
			int noOfPtsd = 0;

			long current = System.currentTimeMillis();
			while (continueFlag && count < index) {
				PatientRecord<ChcsPerson> record = patientRecordGenerator
						.generatePatientRecord();
				noOfPtsd = (int) countTopLevelPTSDStatements(record);
				countTopLevelAnxietyStatements(record);

				List<ClinicalStatement> statements = record
						.getClinicalStatements();
				for (ClinicalStatement statement : statements) {
					Integer cnt = itemCount.get(statement.getIndexKey());
					if (cnt == null) {
						cnt = 0;
					} else {
						cnt += 1;
					}
					itemCount.put(statement.getIndexKey(), cnt);
				}
				count++;
			}

			assert_Anx_Alc_Dep_LevelProbabilities(itemCount, count);

		} catch (Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

	/**
	 * 
	 */
	@Test
	public void testPtsdGenerateClinicalRecords() {
		try {
			Map<ClinicalStatementKey, Integer> itemCount = new HashMap<ClinicalStatementKey, Integer>();

			boolean continueFlag = true;
			int index = 20000;
			int count = 0;
			int noOfPtsd = 0;

			while (continueFlag && count < index) {
				PatientRecord<ChcsPerson> record = patientRecordGenerator
						.generatePatientRecord();
				testGenderGenerateClinicalRecords(record);
				noOfPtsd = (int) countTopLevelPTSDStatements(record);
				countTopLevelAnxietyStatements(record);

				List<ClinicalStatement> statements = record
						.getClinicalStatements();

				for (ClinicalStatement statement : statements) {
					Integer cnt = itemCount.get(statement.getIndexKey());
					if (cnt == null) {
						cnt = 0;
					} else {
						cnt += 1;
					}
					itemCount.put(statement.getIndexKey(), cnt);
				}
				count++;
			}

			assertTopLevelPTSDProbabilities(noOfPtsd, count);

		} catch (Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

	/**
	 * 
	 */
	@Test
	public void testGenerateClinicalRecords() {
		try {
			Map<ClinicalStatementKey, Integer> itemCount = new HashMap<ClinicalStatementKey, Integer>();

			boolean continueFlag = true;
			int index = 20000;
			int count = 0;
			int noOfPtsd = 0;

			long current = System.currentTimeMillis();
			while (continueFlag && count < index) {
				PatientRecord<ChcsPerson> record = patientRecordGenerator
						.generatePatientRecord();
				testGenderGenerateClinicalRecords(record);
				noOfPtsd = (int) countTopLevelPTSDStatements(record);
				countTopLevelAnxietyStatements(record);

				List<ClinicalStatement> statements = record
						.getClinicalStatements();

				for (ClinicalStatement statement : statements) {
					Integer cnt = itemCount.get(statement.getIndexKey());
					if (cnt == null) {
						cnt = 0;
					} else {
						cnt += 1;
					}
					itemCount.put(statement.getIndexKey(), cnt);
				}
				count++;
			}

			System.out.println("TIME ELAPSED: "
					+ (System.currentTimeMillis() - (double) current) / 1000);
			//TODO Need to fix this test and uncomment line below
			//assertTopLevelProbabilities(itemCount, count);

		} catch (Exception cause) {
			cause.printStackTrace();
			fail(cause.getMessage());
		}
	}

	/**
	 * 
	 * @param record
	 * @return
	 */

	protected double countTopLevelPTSDStatements(
			PatientRecord<ChcsPerson> record) {

		if (record.getIndex().exists(PtsdStatementGenerator.getPtsdIndexKey())) {
			ptsdGeneratedPopulation++;
		}
		return ptsdGeneratedPopulation;
	}

	protected void assertTopLevelPTSDProbabilities(int ptsdStatementCount,
			int populationCount) {

		double probability = (double) ptsdStatementCount / populationCount;
		assertEquals(PTSDRATE, probability, getError(PTSDRATE));
	}

	/**
	 * assertTopLevelAnxietyProbabilities
	 * 
	 * @param populationCount
	 */

	protected double countTopLevelAnxietyStatements(
			PatientRecord<ChcsPerson> record) {

		if (record.getIndex().exists(AnxietyStatementGenerator.getAnxietyKey())) {
			anxietyGeneratedPopulation++;
		}
		return anxietyGeneratedPopulation;
	}

	/******************************************************/
	/**
     * 
     */

	protected void assert_Anx_Alc_Dep_LevelProbabilities(
			Map<ClinicalStatementKey, Integer> histogram, int populationCount) {

		for (ClinicalStatementKey key : histogram.keySet()) {
			if (key.getClinicalStatementClass()
					.equals("org.socraticgrid.clinical.model.clinicalstatement.Condition")) {
				if (((Coding) key.getAttributeValue()).getCodeDisplayName()
						.equals("Nondependent alcohol abuse, continuous")) {
					System.out.println("Total Number of Alcohol Abuse \t"
							+ histogram.get(key) + "  Probablity "
							+ getProbability(key, histogram, populationCount));
					assertEquals(0.14,
							getProbability(key, histogram, populationCount),
							getError(0.14));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals("Anxiety")) {
					System.out.println("Total Number of Anxiety \t"
							+ histogram.get(key) + "  Probablity "
							+ getProbability(key, histogram, populationCount));
					assertEquals(0.25,
							getProbability(key, histogram, populationCount),
							getError(0.25));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals("Depressive disorder")) {
					System.out.println("Total Number of Depressive Disorder \t"
							+ histogram.get(key) + "  Probablity "
							+ getProbability(key, histogram, populationCount));
					assertEquals(0.28,
							getProbability(key, histogram, populationCount),
							getError(0.28));
				}
			}
		}
	}

	/******************************************************/
	/**
     * 
     */

	protected void assertTopLevelProbabilities(
			Map<ClinicalStatementKey, Integer> histogram, int populationCount) {

		for (ClinicalStatementKey key : histogram.keySet()) {
			if (key.getClinicalStatementClass()
					.equals("org.socraticgrid.example.model.clinicalstatement.Condition")) {
				if (((Coding) key.getAttributeValue()).getCodeDisplayName()
						.equals("Traumatic brain injury")) { // PASSED
					assertEquals(0.12,
							getProbability(key, histogram, populationCount),
							getError(0.12));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals("Victim of child abuse")) { // PASSED
					assertEquals(0.07,
							getProbability(key, histogram, populationCount),
							getError(0.07));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals(
								"Injury resulting from military weapons")) { // NOT PASSED
				// assertEquals(0.252,
				// getProbability(key, histogram, populationCount),
				// getError(0.252));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals("Nightmares")) { // PASSED
					assertEquals(0.20,
							getProbability(key, histogram, populationCount),
							getError(0.20));
				} else if (((Coding) key.getAttributeValue())
						.getCodeDisplayName().equals("Smoking")) { // PASSED
					assertEquals(0.205,
							getProbability(key, histogram, populationCount),
							getError(0.205));
				} else {
					System.out.println("Unknown key: " + key);
					// fail();
				}
			} else if (key
					.getClinicalStatementClass()
					.equals("org.socraticgrid.example.model.clinicalstatement.ObservationResult")) {
//				// if (((Coding) key.getAttributeValue()).getCodeDisplayName()
//				// .equals("PHQ9 Score")) { //NOT PASSED
//				// assertEquals(0.14,
//				// getProbability(key, histogram, populationCount),
//				// getError(0.14));
//				// } else
//				if (((Coding) key.getAttributeValue()).getCodeDisplayName()
//						.equals("PCL Score")) { // NOT PASSED
//				 assertEquals(0.14,
//				 getProbability(key, histogram, populationCount),
//				 getError(0.14));
//				} else if (((Coding) key.getAttributeValue())
//						.getCodeDisplayName().equals("Audit Score")) { // NOT
//																		// PASSED
//				 assertEquals(0.14,
//				 getProbability(key, histogram, populationCount),
//				 getError(0.14));
//				} else if (((Coding) key.getAttributeValue())
//						.getCodeDisplayName().equals("PTSD Symptoms")) { // NOT
//																			// PASSED
//				 assertEquals(0.14,
//				 getProbability(key, histogram, populationCount),
//				 getError(0.14));
//				} else {
//					System.out.println("Unknown key: " + key);
//					// fail();
//				}
			} else {
				fail(key.getClinicalStatementClass());
			}
		}
	}

	protected double getProbability(ClinicalStatementKey key,
			Map<ClinicalStatementKey, Integer> histogram, int populationCount) {
		double count = histogram.get(key);
		double probability = count / populationCount;
		return probability;
	}

	public double getError(double expectedValue) {
		return ERROR_RATE * expectedValue;
	}

}
