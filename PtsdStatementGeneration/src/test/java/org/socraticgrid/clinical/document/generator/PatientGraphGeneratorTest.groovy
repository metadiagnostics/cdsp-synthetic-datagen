/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.document.generator;

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.enumeration.AddressType;
import org.socraticgrid.example.model.enumeration.StateEnumeration;
import org.socraticgrid.example.model.extension.chcs.ChcsPatient;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;

class PatientGraphGeneratorTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadMappings() {
		try {
//			PatientGraphGenerator generator = new PatientGraphGenerator();
//			generator.loadMappings("mappings/chcs/ChcsPersonMapping.xml")
		} catch(Exception e) {
			fail();
		}
	}

	@Test
	public void testGenerateMappings() {
		try {
//			PatientGraphGenerator generator = new PatientGraphGenerator();
//			generator.registerSerializer(new TurtleSerializer())
//			generator.generateMappings(buildPatient(), "mappings/chcs/ChcsPersonMapping.xml")
		}catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	private ChcsPatient buildPatient() {
		
		ChcsPatient chcsPatient = new ChcsPatient()
		chcsPatient.setLabel("CRAYTONXXX,RED W")
		chcsPatient.addPatientIdentifier(new Identifier("http://chcs.taps.com/","2_123456","CHCS Patient Identifier"));
		
		Address address = new Address();
		address.setAddressType(AddressType.CONTACT);
		address.setHouseNumber("3722")
		address.setStreetName("Internal Test Point")
		address.setStateName(StateEnumeration.CALIFORNIA)
		address.setPostalCode("60090")
		address.setCityName("WHEELING")
		chcsPatient.addAddress(address)
		
		ChcsPerson nextOfKin = new ChcsPerson()
		nextOfKin.setLabel("CRAYTONXXX, ORANGE W")
		nextOfKin.addAddress(address)
		chcsPatient.setNextOfKin(nextOfKin)
		
		ChcsPerson emergencyContact = new ChcsPerson()
		emergencyContact.setLabel("CRAYTONXXX, GREEN W")
		emergencyContact.addAddress(address)
		chcsPatient.setEmergencyContact(emergencyContact)
		
		return chcsPatient;
	}

}
