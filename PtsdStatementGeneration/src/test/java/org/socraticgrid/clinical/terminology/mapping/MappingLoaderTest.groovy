/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.terminology.mapping;

import static org.junit.Assert.*

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.enumeration.AddressType;
import org.socraticgrid.example.model.enumeration.StateEnumeration;
import org.socraticgrid.example.model.extension.chcs.ChcsPatient;
import org.socraticgrid.example.model.extension.chcs.ChcsPerson;

class MappingLoaderTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoad() {
		try {
			ChcsPatient person = new ChcsPatient();
			
			person.setLabel("CRAYTONXXX,RED W")
			
			Address address = new Address();
			address.setAddressType(AddressType.CONTACT);
			address.setHouseNumber("3722")
			address.setStreetName("Internal Test Point")
			address.setStateName(StateEnumeration.CALIFORNIA)
			address.setPostalCode("60090")
			address.setCityName("WHEELING")
			person.addAddress(address)
			
			ChcsPerson nextOfKin = new ChcsPerson();
			nextOfKin.setLabel("CRAYTONXXX, ORANGE W")
			nextOfKin.addAddress(address)
			person.setNextOfKin(nextOfKin)
			
			ChcsPerson emergencyContact = new ChcsPerson()
			emergencyContact.setLabel("CRAYTONXXX, GREEN W")
			emergencyContact.addAddress(address)
			person.setEmergencyContact(emergencyContact)
			
			MappingLoader loader = new MappingLoader();
			InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("mappings/chcs/ChcsPersonMapping.xml")
			loader.load(stream);
			
			//Ensure the graph was loaded as expected
			assertEquals(1, loader.getGraphs().size())
			assertEquals(1, loader.getGraphs()[0].getNodes().size())
			assertEquals(5, loader.getGraphs()[0].getNodes()[0].getMappings().size())
			
			//Ensure object state is properly loaded
			assertEquals("RDF", loader.mappingType)
			assertEquals("http://chcs.taps.com/2", loader.graphs[0].nodes[0].concept)
			assertEquals("org.socraticgrid.clinical.model.ChcsPatient", loader.graphs[0].nodes[0].conceptClass)
			assertEquals("label", loader.graphs[0].nodes[0].mappings[0].sourceModelPropertyLabel)
			assertEquals("rdf:label", loader.graphs[0].nodes[0].mappings[0].targetModelPropertyLabel)
			assertEquals("org.socraticgrid.clinical.terminology.mapping.ChcsAligner", loader.graphs[0].nodes[0].mappings[0].sourceModelProperty.transformationClass.getName())
			assertEquals("noOp", loader.graphs[0].nodes[0].mappings[0].sourceModelProperty.transformationMethod)
			
			//Test reflection to invoke mapper
			def labelMapping = loader.graphs[0].nodes[0].mappings[0].getSourceModelPropertyValue(person)
			assertEquals("CRAYTONXXX,RED W", labelMapping)
			assertEquals("http://chcs.taps.com/2 rdf:label CRAYTONXXX,RED W", loader.graphs[0].nodes[0].concept + " " + loader.graphs[0].nodes[0].mappings[0].targetModelPropertyLabel + " " + labelMapping)
			
			def stateMapping = loader.graphs[0].nodes[0].mappings[1].getSourceModelPropertyValue(person)
			assertEquals("http://chcs.taps.com/5-17", stateMapping)
			assertEquals("http://chcs.taps.com/2 state-2 http://chcs.taps.com/5-17", loader.graphs[0].nodes[0].concept + " " + loader.graphs[0].nodes[0].mappings[1].targetModelPropertyLabel + " " + stateMapping)
			
			def addressStreetMapping = loader.graphs[0].nodes[0].mappings[2].getSourceModelPropertyValue(person)
			assertEquals("3722 Internal Test Point WHEELING", addressStreetMapping)
			assertEquals("http://chcs.taps.com/2 street-address-2 3722 Internal Test Point WHEELING", loader.graphs[0].nodes[0].concept + " " + loader.graphs[0].nodes[0].mappings[2].targetModelPropertyLabel + " " + addressStreetMapping)
			
			def emergencyContactValue = loader.graphs[0].nodes[0].mappings[3].getSourceModelPropertyValue(person)
			assertEquals("CRAYTONXXX, GREEN W", emergencyContactValue)
			assertEquals("http://chcs.taps.com/2 chcs:emergency-contact-2 CRAYTONXXX, GREEN W", loader.graphs[0].nodes[0].concept + " " + loader.graphs[0].nodes[0].mappings[3].targetModelPropertyLabel + " " + emergencyContactValue)
			
			def nextOfKinContactValue = loader.graphs[0].nodes[0].mappings[4].getSourceModelPropertyValue(person)
			assertEquals("CRAYTONXXX, ORANGE W", nextOfKinContactValue)
			assertEquals("http://chcs.taps.com/2 chcs:nok-contact-2 CRAYTONXXX, ORANGE W", loader.graphs[0].nodes[0].concept + " " + loader.graphs[0].nodes[0].mappings[4].targetModelPropertyLabel + " " + nextOfKinContactValue)
			
		} catch(Exception e) {
			e.printStackTrace();
			fail()
		}
		
	}

}
