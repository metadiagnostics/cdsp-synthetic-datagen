/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.clinical.statistical;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.AlcoholAbuseStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.AnxietyStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.DepressionStatementGenerator;
import org.socraticgrid.generator.ptsd.clinicalstatement.generator.PtsdStatementGenerator;
import org.socraticgrid.statistical.distribution.bayes.AutomatonBuilder;
import org.socraticgrid.statistical.distribution.bayes.Edge;
import org.socraticgrid.statistical.distribution.bayes.End;
import org.socraticgrid.statistical.distribution.bayes.Generator;
import org.socraticgrid.statistical.distribution.bayes.Start;
import org.socraticgrid.statistical.distribution.bayes.Universe;
import org.socraticgrid.statistical.distribution.bayes.VennSet;
import org.socraticgrid.statistical.distribution.bayes.Vertex;

public class AutomatonBuilderTest_PTSD {
	
	private Universe universe1;
	private AutomatonBuilder automaton;

	@Before
	public void setUp() throws Exception {
		universe1 = buildUniverse1();
		automaton = new AutomatonBuilder(universe1);
		
	}
	
	@Test
	public void testBuildAutomaton() {
		automaton.buildAutomaton();
		
		Vertex start = automaton.getStartingPoint();
		assertNotNull(start);
		
		//Validate we have a starting point
		assertEquals(0, start.getIncomingEdgeCount());
		assertEquals(5, start.getOutgoingEdgeCount()); // Includes End vertex
		
		for(Edge edge : start.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Alcohol")) {
				assertEquals(0.14, edge.getProbability(), 0.001);
				validateAlcohol(destination);
			} else if(name.equals("Anxiety")) {
				assertEquals(0.22, edge.getProbability(), 0.001);
				validateAnxiety(destination);
			} else if(name.equals("Depression")) {
				assertEquals(0.07, edge.getProbability(), 0.001);
				validateDepression(destination);
			} else if(name.equals("PTSD")) {
				assertEquals(0.033, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.537, edge.getProbability(), 0.001);
			} else {
				fail();
			}
		}
		
		validateTopLevelProbabilities(start);
	}
	
	public void validateAlcohol(Vertex alcohol) {
		assertEquals(4, alcohol.getOutgoingEdgeCount());
		for(Edge edge : alcohol.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Alcohol and Anxiety")) {
				assertEquals(0.2143, edge.getProbability(), 0.001);
				validateAnxietyGivenAlcohol(destination);
			} else if(name.equals("Alcohol and Depression")) {
				assertEquals(0.4286, edge.getProbability(), 0.001);
				validateDepressionGivenAlcohol(destination);
			} else if(name.equals("Alcohol and PTSD")) {
				assertEquals(0.0857, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.2714, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateAnxietyGivenAlcohol(Vertex anxiety) {
		assertEquals(3, anxiety.getOutgoingEdgeCount());
		for(Edge edge : anxiety.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Alcohol and Anxiety and Depression")) {
				assertEquals(0.667, edge.getProbability(), 0.001);
				validateDepressionGivenAlcoholAndAnxiety(destination);
			} else if(name.equals("Alcohol and Anxiety and PTSD")) {
				assertEquals(0.3, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.033, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateDepressionGivenAlcohol(Vertex depression) {
		assertEquals(2, depression.getOutgoingEdgeCount());
		for(Edge edge : depression.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Alcohol and Depression and PTSD")) {
				assertEquals(0.1833, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.8167, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateDepressionGivenAlcoholAndAnxiety(Vertex depression) {
		assertEquals(2, depression.getOutgoingEdgeCount());
		for(Edge edge : depression.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Alcohol and Anxiety and Depression and PTSD")) {
				assertEquals(0.50, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.5, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}

	public void validateAnxiety(Vertex anxiety) {
		assertEquals(3, anxiety.getOutgoingEdgeCount());
		for(Edge edge : anxiety.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Anxiety and Depression")) {
				assertEquals(0.5909, edge.getProbability(), 0.001);
				validateAnxietyAndDepression(destination);
			} else if(name.equals("Anxiety and PTSD")) {
				assertEquals(0.2, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.2091, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateAnxietyAndDepression(Vertex anxiety) {
		assertEquals(2, anxiety.getOutgoingEdgeCount());
		for(Edge edge : anxiety.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Anxiety and Depression and PTSD")) {
				assertEquals(0.05385, edge.getProbability(), 0.001);
				assertEquals(1, destination.getOutgoingEdgeCount());
			} else if(name.equals(End.END)) {
				assertEquals(0.9462, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateDepression(Vertex anxiety) {
		assertEquals(2, anxiety.getOutgoingEdgeCount());
		for(Edge edge : anxiety.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals("Depression and PTSD")) {
				assertEquals(0.2, edge.getProbability(), 0.001);
				validateDepressionAndPTSD(destination);
			} else if(name.equals(End.END)) {
				assertEquals(0.8, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateDepressionAndPTSD(Vertex depression) {
		assertEquals(1, depression.getOutgoingEdgeCount());
		for(Edge edge : depression.getOutgoingEdges()) {
			Vertex destination = edge.getDestination();
			String name = destination.getName();
			if(name.equals(End.END)) {
				assertEquals(1.0, edge.getProbability(), 0.001);
			} else {
				fail(name);
			}
		}
	}
	
	public void validateTopLevelProbabilities(Vertex start) {
		
		Map<Class<?>,Double> counts = new HashMap<Class<?>, Double>();
		counts.put(AlcoholAbuseStatementGenerator.class, new Double(0.0));
		counts.put(AnxietyStatementGenerator.class, new Double(0.0));
		counts.put(DepressionStatementGenerator.class, new Double(0.0));
		counts.put(PtsdStatementGenerator.class, new Double(0.0));
		
		tallyProbabilities(start, counts);
		
		assertEquals(0.14, counts.get(AlcoholAbuseStatementGenerator.class), 0.01);
		assertEquals(0.25, counts.get(AnxietyStatementGenerator.class), 0.01);
		assertEquals(0.28, counts.get(DepressionStatementGenerator.class), 0.01);
		assertEquals(0.14, counts.get(PtsdStatementGenerator.class), 0.01);
	}
	
	public void tallyProbabilities(Vertex currentNode, Map<Class<?>,Double> counts) {
		if(currentNode.getOutgoingEdgeCount() <= 0) {
			return;
		} else if(!currentNode.getName().equals(Start.START)){
			Generator<ClinicalStatement> generator = (Generator<ClinicalStatement>)currentNode.getGenerator();
			Double value = counts.get(generator.getClass());
			value = new Double(value + currentNode.getProbability());
			counts.put(generator.getClass(), value);
		}
		for(Edge edge : currentNode.getOutgoingEdges()) {
			tallyProbabilities(edge.getDestination(), counts);
		}
	}

	@After
	public void tearDown() throws Exception {
	}
	
	protected Universe buildUniverse1() throws Exception {
		Universe universe = new Universe();
		
		VennSet Alcohol = universe.createTopLevelSet("Alcohol", 0.14);
		Alcohol.setGenerator(new AlcoholAbuseStatementGenerator());
		VennSet Anxiety = universe.createTopLevelSet("Anxiety", 0.25);
		Anxiety.setGenerator(new AnxietyStatementGenerator());
		VennSet Depression = universe.createTopLevelSet("Depression", 0.28);
		Depression.setGenerator(new DepressionStatementGenerator());
		VennSet PTSD = universe.createTopLevelSet("PTSD", 0.14);
		PTSD.setGenerator(new PtsdStatementGenerator());
		
		VennSet Alcohol_Anxiety = universe.createSet("Alcohol and Anxiety", 0.03, Alcohol, Anxiety);
		Alcohol_Anxiety.setGenerator(new AnxietyStatementGenerator());
		VennSet Alcohol_Depression = universe.createSet("Alcohol and Depression", 0.08, Alcohol, Depression);
		Alcohol_Depression.setGenerator(new DepressionStatementGenerator());
		VennSet Alcohol_PTSD = universe.createSet("Alcohol and PTSD", 0.042, Alcohol, PTSD);
		Alcohol_PTSD.setGenerator(new PtsdStatementGenerator());
		VennSet Anxiety_Depression = universe.createSet("Anxiety and Depression", 0.15, Anxiety, Depression);
		Anxiety_Depression.setGenerator(new DepressionStatementGenerator());
		VennSet Anxiety_PTSD = universe.createSet("Anxiety and PTSD", 0.07, Anxiety, PTSD);
		Anxiety_PTSD.setGenerator(new PtsdStatementGenerator());
		VennSet Depression_PTSD = universe.createSet("Depression and PTSD", 0.042, Depression, PTSD);
		Depression_PTSD.setGenerator(new PtsdStatementGenerator());
	
		VennSet Alcohol_Anxiety_Depression = universe.createSet("Alcohol and Anxiety and Depression", 0.02, Alcohol_Anxiety, Alcohol_Depression, Anxiety_Depression);
		Alcohol_Anxiety_Depression.setGenerator(new DepressionStatementGenerator());
		VennSet Alcohol_Anxiety_PTSD = universe.createSet("Alcohol and Anxiety and PTSD", 0.019, Alcohol_Anxiety, Alcohol_PTSD, Anxiety_PTSD);
		Alcohol_Anxiety_PTSD.setGenerator(new PtsdStatementGenerator());
		VennSet Alcohol_Depression_PTSD = universe.createSet("Alcohol and Depression and PTSD", 0.021, Alcohol_Depression, Alcohol_PTSD, Depression_PTSD);
		Alcohol_Depression_PTSD.setGenerator(new PtsdStatementGenerator());
		VennSet Anxiety_Depression_PTSD = universe.createSet("Anxiety and Depression and PTSD", 0.017, Anxiety_Depression, Anxiety_PTSD, Depression_PTSD);
		Anxiety_Depression_PTSD.setGenerator(new PtsdStatementGenerator());
		
		VennSet Alcohol_Anxiety_Depression_PTSD = universe.createSet("Alcohol and Anxiety and Depression and PTSD", 0.01, Alcohol_Anxiety_Depression, Alcohol_Anxiety_PTSD, Alcohol_Depression_PTSD, Anxiety_Depression_PTSD);
		Alcohol_Anxiety_Depression_PTSD.setGenerator(new PtsdStatementGenerator());
		
		universe.sortChildren();
		return universe;
	}

}
