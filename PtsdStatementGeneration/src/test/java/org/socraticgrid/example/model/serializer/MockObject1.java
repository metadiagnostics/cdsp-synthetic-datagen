/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.serializer.SkipSerialization;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.core.AbstractCoreDataType;

@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/MockObject1",instanceIri=AbstractCoreDataType.BASE_IRI)
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class MockObject1 extends AbstractClinicalModel{
	
	private Integer field1;
	private Double field2;
	private String field3;
	private float field4;
	private Date date1;
	@SkipSerialization
	private SimpleDateFormat format;
	private AdministrativeGender gender;

	public MockObject1() {
		field1 = 5;
		field2 = 4.8;
		field3 = "Hello";
		field4 = 2.1f;
		format = new SimpleDateFormat();
		date1 = new Date(1);
		gender = AdministrativeGender.MALE;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return AbstractClinicalModel.BASE_IRI + "/" + getCanonicalName();
	}

	@Override
	public String getCanonicalName() {
		return "MockObject1";
	}

}
