/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.example.model.AbstractClinicalDataType;
import org.socraticgrid.example.model.datatypes.core.AbstractCoreDataType;
import org.socraticgrid.example.model.datatypes.core.Period;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.SimpleSelector;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.vocabulary.RDF;

public class SemanticAnnotationBuilderTest {
	
	private MockObject1 mock1;
	private SemanticAnnotationBuilder builder;

	@Before
	public void setUp() throws Exception {
		mock1 = new MockObject1();
		try {
			builder = new SemanticAnnotationBuilder();
			builder.configureNewBuilder();
		} catch(Exception e) {
			fail(e.getMessage());
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetSemanticResourceIri() {
		String iri = builder.getSemanticResourceIri(mock1);
		assertEquals(AbstractCoreDataType.BASE_IRI + "/MockObject1", iri);
	}
	
	@Test
	public void testGetSemanticResourceInstanceIri() {
		String instanceIri = builder.getSemanticResourceInstanceIri(mock1);
		assertEquals(AbstractCoreDataType.BASE_IRI, instanceIri);
	}
	
	@Test
	public void testGetAutoAnnotateBaseIri() {
		String instanceIri = builder.getAutoAnnotateBaseIri(mock1);
		assertEquals(AbstractClinicalDataType.BASE_IRI, instanceIri);
	}
	
	@Test
	public void testGetAutoAnnotateInstanceIri() {
		String instanceIri = builder.getAutoAnnotateInstanceIri(mock1);
		assertEquals(AbstractCoreDataType.BASE_IRI, instanceIri);
	}

	@Test
	public void testCollectSemanticPredicateIris() {
		Map<Field,String> fieldMap = builder.collectSemanticPredicateIris(mock1,true, false);
		assertEquals(7, fieldMap.size());
		
		fieldMap = builder.collectSemanticPredicateIris(mock1,true, true);
		assertEquals(8, fieldMap.size());
		
		fieldMap = builder.collectSemanticPredicateIris(mock1,false, true);
		assertEquals(9, fieldMap.size());
	}

	@Test
	public void testAddDataTypeStatements() {
		Model model = builder.getModelBuilder().getModel();
		builder.addDataTypeStatements(mock1, new RdfContext());
		
		assertEquals(7, builder.getModelBuilder().getModel().size());
		
		assertRdfType(model, "http://socraticgrid.org/clinical/datatype/MockObject1");
		
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/gender", "MALE");
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/date1", "Wed Dec 31 16:00:00 PST 1969");
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/field1", "5");
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/field2", "4.8");
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/field3", "Hello");
		assertStringDatatypeProperty(model, "http://socraticgrid.org/clinical/datatype/field4", "2.1");

		builder.getModelBuilder().getModel().write(System.out);
		
	}
	
	@Test
	public void testProcessMap() {
		
	}

	@Test
	public void testProcessList() {
	}

	@Test
	public void testAddDataProperty() {
	}

	@Test
	public void testGetFieldValue() {
	}

	@Test
	public void testGetFieldPredicate() {
	}

	@Test
	public void testIsStatic() {
	}

	@Test
	public void testBuildIncomingEdgesFromContext() {
	}
	
	@Test public void testIsCoreSerializableType() {
		Map<Field,String> fieldMap = builder.collectSemanticPredicateIris(mock1,true, false);
		for(Field field : fieldMap.keySet()) {
			boolean isCore = builder.isCoreSerializableType(mock1, field);
			if(field.getType().equals(SimpleDateFormat.class)){
				assertFalse(isCore);
			} else {
				assertTrue(isCore);
			}
		}
	}
	
	@Test
	public void testSkipSerialization() {
		Period period = new Period();
		Map<Field,String> fieldMap = builder.collectSemanticPredicateIris(period,true, true);
		for(Field field : fieldMap.keySet()) {
			if(field.getType().getName().equalsIgnoreCase("java.text.SimpleDateFormat")) {
				boolean skipField = SemanticAnnotationBuilder.skipField(field);
				assertTrue(skipField);
			} else {
				boolean skipField = SemanticAnnotationBuilder.skipField(field);
				assertFalse(skipField);
			}
		}
	}
	
//	@Test
//	public void testAnnotatedClass() {
//		try {
//			Model model = ModelFactory.createDefaultModel();
//			MockAnnotatedClass mock = new MockAnnotatedClass();
//			mock.setName("I am a mock object");
//			mock.setValue(MockEnum.ENUM_VAL_1);
//			String addressIri = SemanticAnnotationBuilder.getSemanticResourceIri(mock);
//			assertEquals("http://resource.org/Resource", addressIri);
//			Map<Field,String> predicateMap = SemanticAnnotationBuilder.collectSemanticPredicateIris(mock);
//			assertTrue(predicateMap.size() == 2);
//			SemanticAnnotationBuilder.addDataTypeStatements(model, mock, "ID123");
//			assertTrue(model.size() == 2);
//			ResIterator iterator = model.listSubjects();
//			while(iterator.hasNext()) {
//				Resource resource = iterator.next();
//				assertEquals("http://resource.org/instance/ID123", resource.getURI());
//			}
//			StmtIterator stmtIterator = model.listStatements();
//			while(stmtIterator.hasNext()) {
//				Statement statement = stmtIterator.next();
//				String predicateUri = statement.getPredicate().getURI();
//				if(predicateUri.equals("http://resource.org/Resource/name")) {
//					String value = statement.getObject().asLiteral().getString();
//					assertEquals("I am a mock object", value);
//				} else if(predicateUri.equals("http://resource.org/Resource/value")) {
//					String value = statement.getObject().asLiteral().getString();
//					assertEquals(MockEnum.ENUM_VAL_1.toString(), value);
//				} else {
//					fail();
//				}
//			}
//		} catch(Exception e) {
//			e.printStackTrace();
//			fail();
//		}
//	}
//	
//	@Test
//	public void testAutoAnnotatedClass1() {
//		Model model = ModelFactory.createDefaultModel();
//		MockAutoAnnotatedClass mock = new MockAutoAnnotatedClass();
//		mock.setName("I am another mock object");
//		mock.setValue(MockEnum.ENUM_VAL_2);
//		SemanticAnnotationBuilder.addDataTypeStatements(model, mock, "ID123");
//		assertTrue(model.size() == 2);
//		ResIterator iterator = model.listSubjects();
//		while(iterator.hasNext()) {
//			Resource resource = iterator.next();
//			assertEquals("http://www.example.com/baseIri/Resource/ID123", resource.getURI());
//		}
//		StmtIterator stmtIterator = model.listStatements();
//		while(stmtIterator.hasNext()) {
//			Statement statement = stmtIterator.next();
//			String predicateUri = statement.getPredicate().getURI();
//			if(predicateUri.equals("http://www.example.com/baseIri/Resource/name")) {
//				String value = statement.getObject().asLiteral().getString();
//				assertEquals("I am another mock object", value);
//			} else if(predicateUri.equals("http://www.example.com/baseIri/Resource/value")) {
//				String value = statement.getObject().asLiteral().getString();
//				assertEquals(MockEnum.ENUM_VAL_2.toString(), value);
//			} else {
//				fail();
//			}
//		}
//	}
	
	/*************************************
	 * HELPER METHODS
	 *************************************/
	
	protected void assertStringDatatypeProperty(Model model, String propertyUri, String value) {
		Property property = model.getProperty(propertyUri);
		assertNotNull(property);
		Statement stmt = model.listStatements(new SimpleSelector(null, property, (RDFNode)null)).next();
		assertEquals(stmt.getString(), value);
	}
	
	protected void assertRdfType(Model model, String typeIRI) {
		Statement stmt = model.listStatements(new SimpleSelector(null, RDF.type, (RDFNode)null)).next();
		assertTrue(stmt.getResource().getURI().equals(typeIRI));
	}

}