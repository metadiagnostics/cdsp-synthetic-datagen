/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.fhir.core.impl;

import java.util.List;

import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.generator.fhir.core.HumanNameGenerator;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;
import org.socraticgrid.statistical.distribution.reader.PersonNameReader;

import ca.uhn.fhir.model.dstu2.composite.HumanNameDt;

/**
 * Random human name generator
 * 
 * Currently class only supports common western name formats and does not have
 * support for hyphenated names. Maiden names not supported at this time. These
 * will be added as needed.
 * 
 * @author Claude Nanjo
 * 
 */
public class DefaultHumanNameGenerator implements HumanNameGenerator {

	private PersonNameReader reader;
	private DiscreteProbabilityDistribution<String> givenNameDistribution;
	private DiscreteProbabilityDistribution<String> middleNameDistribution;
	private DiscreteProbabilityDistribution<String> surnameDistribution;

	/**
	 * Default class constructor
	 */
	public DefaultHumanNameGenerator(PersonNameReader reader) {
		this.reader = reader;
	}

	/**
	 * The person name generator loads name distributions from a
	 * PersonNameReader. The initializer initializes names for the three
	 * collections - first, middle, and last
	 * 
	 * @param reader
	 */
	public void initialize() {
		try {
			givenNameDistribution = new DiscreteProbabilityDistribution<String>(reader.loadGivenName());
			middleNameDistribution = new DiscreteProbabilityDistribution<String>(reader.loadMiddleName());
			surnameDistribution = new DiscreteProbabilityDistribution<String>(reader.loadSurname());
		} catch (Exception cause) {
			throw new SocraticGridRuntimeException("Error configurating random name generator", cause);
		}
	}

	/**
	 * The names of higher frequency occur more often than names of lower
	 * frequency.
	 * 
	 * @param middleNameCount
	 *            - The number of middle names to include. Eventually, this will
	 *            be a range rather than an absolute number.
	 * @param isMiddleNameAbbridged
	 *            - Should the middle name at times be an initial only.
	 * @param abbridgedFrequency
	 *            - Range that specifies the abbreviation frequency. 1 if the
	 *            middle name is always the first initial. 0 if the middle name
	 *            should never be an initial. TODO Implement this
	 * @return
	 */
	public HumanNameDt generateName(int middleNameCount, boolean isMiddleNameAbbridged, float abbridgedFrequency) {
		HumanNameDt name = new HumanNameDt();
		name.addGiven(givenNameDistribution.getSample());
		if (middleNameCount > 0) {
			List<String> middleNames = middleNameDistribution.getSampleItems(middleNameCount);
			for (String middleName : middleNames) {
				name.addGiven(middleName);
			}
		}
		name.addFamily(surnameDistribution.getSample());
		return name;
	}

	/**
	 * Generates given name, surname only.
	 * 
	 * @return
	 */
	public HumanNameDt generateName() {
		HumanNameDt name = new HumanNameDt();
		name.addGiven(givenNameDistribution.getSample());
		name.addFamily(surnameDistribution.getSample());
		return name;
	}
}
