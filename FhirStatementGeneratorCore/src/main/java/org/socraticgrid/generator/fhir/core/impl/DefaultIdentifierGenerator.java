/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.fhir.core.impl;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.socraticgrid.generator.fhir.core.IdentifierGenerator;

import ca.uhn.fhir.model.dstu2.composite.IdentifierDt;

public class DefaultIdentifierGenerator implements IdentifierGenerator {
	
	private Set<String> integerIds;
	private Random randomNumberGenerator;
	private final int defaultIntegerBase = 100000000;
	private String systemRoot;
	
	public DefaultIdentifierGenerator(String systemRoot, String systemDisplayLabel) {
		this.systemRoot = systemRoot;
		integerIds = new HashSet<String>();
		randomNumberGenerator = new Random();
	}
	
	public IdentifierDt getNextIdentifier() {
		String candidate = getCandidateIdentifier();
		while(integerIds.contains(candidate)) {
			candidate = getCandidateIdentifier();
		}
		return new IdentifierDt(systemRoot, "" + candidate);
	}
	
	protected String getCandidateIdentifier() {
		Integer candidate = randomNumberGenerator.nextInt(defaultIntegerBase*10);
		if(candidate < defaultIntegerBase) {
			candidate += defaultIntegerBase;
		}
		return candidate.toString();
	}
}
