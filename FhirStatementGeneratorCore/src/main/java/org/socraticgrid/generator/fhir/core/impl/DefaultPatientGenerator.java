/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.generator.fhir.core.impl;


import java.util.ArrayList;
import java.util.List;

import org.hl7.fhir.instance.model.Enumerations.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.impl.RandomChoiceGenerator;
import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.generator.fhir.core.HumanNameGenerator;
import org.socraticgrid.generator.fhir.core.IdentifierGenerator;
import org.socraticgrid.generator.fhir.core.PatientGenerator;
import org.socraticgrid.statistical.distribution.reader.AddressCsvReader;
import org.socraticgrid.statistical.distribution.reader.AddressReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameCsvReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameReader;

import ca.uhn.fhir.model.dstu2.resource.Patient;

public class DefaultPatientGenerator implements PatientGenerator {
	
	private IdentifierGenerator identifierGenerator;
	private RandomChoiceGenerator randomChoiceGenerator;
	private HumanNameGenerator maleNameGenerator;
	private HumanNameGenerator femaleNameGenerator;
	//private AddressGenerator addressGenerator;
	
	public DefaultPatientGenerator() {}
	
	public void initialize() {
		randomChoiceGenerator = new RandomChoiceGenerator();
		PersonNameReader maleNameReader = new PersonNameCsvReader(	"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonLastNames.txt", '\t', false);
		PersonNameReader femaleNameReader = new PersonNameCsvReader("distribution/commonFemaleNames.txt", '\t', false, 
															"distribution/commonFemaleNames.txt", '\t', false, 
															"distribution/commonLastNames.txt", '\t', false);
		
		maleNameReader.setHeaderFlag(true);
		femaleNameReader.setHeaderFlag(true);
		
		AddressReader addressReader = new AddressCsvReader("distribution/addressesUS.txt", '\t', false,
													"distribution/CitiesUS.txt", '\t', false, 
													"distribution/StatesByPopulationUS.txt", '\t', false, 
													"distribution/ZipByPopulationUS.txt", '\t', false);	
		addressReader.setHeaderFlag(true);
		
		HumanNameGenerator maleNameGenerator = new DefaultHumanNameGenerator(maleNameReader);
		maleNameGenerator.initialize();
		
		HumanNameGenerator femaleNameGenerator = new DefaultHumanNameGenerator(femaleNameReader);
		femaleNameGenerator.initialize();
	}
	
	public Patient generatePatient() {
		try {
			Patient patient = new Patient();
			patient.addIdentifier(identifierGenerator.getNextIdentifier());
			if(patient.getGender().equals(AdministrativeGender.MALE.toCode())) {
				patient.addName(maleNameGenerator.generateName());
			} else {
				patient.addName(maleNameGenerator.generateName());
			}
			return patient;
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error generating patient instance", cause);
		}
	}
	
	public List<Patient> generatePatients(int count) {
		List<Patient> generatedPatients = new ArrayList<Patient>(count);
		for(int i=0; i< count; i++) {
			generatedPatients.add(generatePatient());
		}
		return generatedPatients;
	}
}
