/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.serializers.json;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.socraticgrid.exception.SocraticGridRuntimeException;

import com.github.jsonldjava.core.JsonLdError;
import com.github.jsonldjava.core.JsonLdProcessor;
import com.github.jsonldjava.core.RDFDataset;
import com.github.jsonldjava.core.RDFDatasetUtils;
import com.github.jsonldjava.jena.JenaJSONLD;
import com.github.jsonldjava.utils.JSONUtils;
import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.DatasetFactory;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.RDFReader;
import com.hp.hpl.jena.rdf.model.RDFWriter;
import com.hp.hpl.jena.util.FileManager;


public class JsonLdUtils {
	
	/**
	 * Loads RDF from a file and returns a JSON LD string.
	 * @param fileName The path to the RDF file
	 * @return JSON LD string corresponding to the RDF graph input
	 */
	public static String serializeRdfFileAsJsonString(String fileName) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		serializeRdfFileAsJsonOutputStream(fileName, baos);
		byte[] jsonBytes = baos.toByteArray();
		return new String(jsonBytes);
	}
	
	/**
	 * Loads RDF from a file and returns a handle to a JSON LD output steam.
	 * @param fileName The path to the RDF file
	 * @param outputSteam The handle to the output steam for the JSON LD output
	 */
	public static void serializeRdfFileAsJsonOutputStream(String fileName, OutputStream outputSteam) {
		Model model = FileManager.get().loadModel(fileName);
		serializeRdfModelAsJsonOutputStream(model, outputSteam);
	}
	
	/**
	 * Transforms an in-memory RDF model into a JSON LD output stream.
	 * @param rdfModel An RDF Model
	 * @param outputSteam The handle to the output stream for the JSON LD output
	 */
	public static void serializeRdfModelAsJsonOutputStream(Model rdfModel, OutputStream outputSteam) {
		RDFDataMgr.write(outputSteam, rdfModel, JenaJSONLD.JSONLD);
	}
	
	/**
	 * Serializes an RDF in-memory model into a JSON LD String
	 * @param rdfModel An RDF Model
	 * @return A string containing the JSON LD representation of the model
	 */
	public static String serializeRdfModelAsJsonString(Model rdfModel) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		serializeRdfModelAsJsonOutputStream(rdfModel, baos);
		byte[] jsonBytes = baos.toByteArray();
		return new String(jsonBytes);
	}
	
	/**
	 * Converts a JSON-LD file into RDF/XML.
	 * Note: Places resources at top-level.
	 * 
	 * @param jsonldInput
	 * @param rdfOutput
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToRdf(InputStream jsonldInput, OutputStream rdfOutput) throws IOException, JsonLdError {
		transformJsonLdToSpecifiedOutput(jsonldInput, rdfOutput, "RDF/XML"); 
	}
	
	/**
	 * Converts a JSON-LD file into RDF/XML-ABBREV.
	 * Note: Inlines resources rather than putting them at the top level.
	 * 
	 * @param jsonldInput
	 * @param rdfOutput
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToRdfAbbreviated(InputStream jsonldInput, OutputStream rdfOutput) throws IOException, JsonLdError {
		transformJsonLdToSpecifiedOutput(jsonldInput, rdfOutput, Lang.RDFXML);
	}
	
	/**
	 * Converts a JSON-LD file into turtle.
	 * 
	 * @param jsonldInput
	 * @param rdfOutput
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToTurtle(InputStream jsonldInput, OutputStream rdfOutput) throws IOException, JsonLdError {
		transformJsonLdToSpecifiedOutput(jsonldInput, rdfOutput, Lang.TURTLE);
	}
	
	/**
	 * Converts a JSON-LD file into the specified output format.
	 * 
	 * @param jsonldInput
	 * @param rdfOutput
	 * @param outputFormat
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToSpecifiedOutput(InputStream jsonldInput, OutputStream rdfOutput, Lang outputFormat) throws IOException, JsonLdError {
		QualifiedModel InOutMap = loadJsonLdAsNquadDataset(jsonldInput);
	    Model model = getDatasetNamedModels(InOutMap.graph).get(0);//In our case we only have a single graph.
		RDFDataMgr.write(rdfOutput, model.getGraph(), outputFormat);
	}
	
	/**
	 * Converts a JSON-LD
	 * 
	 * @param jsonldInput
	 * @param rdfOutput
	 * @param outputFormat
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToSpecifiedOutput(InputStream jsonldInput, OutputStream rdfOutput, String outputFormat) throws IOException, JsonLdError {
		QualifiedModel qualifiedModel = loadJsonLdAsNquadDataset(jsonldInput);
	    Model model = getDatasetNamedModels(qualifiedModel.graph).get(0);//In our case we only have a single graph.
		RDFWriter rdfWriter = model.getWriter(outputFormat);
		if(qualifiedModel.baseUri != null) {
			rdfWriter.setProperty("xmlbase", qualifiedModel.baseUri);
		}
	    rdfWriter.write(model,rdfOutput, qualifiedModel.baseUri);
	}
	
	/**
	 * Converts a JSON-LD file into NQ (NQUAD) format
	 * 
	 * @param jsonldInput
	 * @param nquadOutput
	 * @throws IOException
	 * @throws JsonLdError
	 */
	public static void transformJsonLdToNquads(InputStream jsonldInput, OutputStream nquadOutput) throws IOException, JsonLdError {
		Object jsonObject = JSONUtils.fromInputStream(jsonldInput);
		String rdfNQuads = RDFDatasetUtils.toNQuads((RDFDataset) JsonLdProcessor.toRDF(jsonObject));//TODO Find a better way to do this. Not ideal for large files
		nquadOutput.write(rdfNQuads.getBytes());
	}
	
	/**
	 * Method returns a Jena model that was populated from a TURTLE input stream.
	 * 
	 * @param inputStream
	 * @param rdfOutput
	 * @return
	 */
	public static Model loadJenaModelFromTurtleFile(InputStream inputStream, OutputStream rdfOutput, String baseUri) {
		try {
		    Model model = ModelFactory.createDefaultModel();
		    RDFReader r = model.getReader("TURTLE");
		    r.read(model,inputStream, baseUri);
		    inputStream.close();
		    return model;
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("An error occurred while loading this TURTLE file", cause);
		}
	}
	
	/**
	 * This method reads a JSON-LD input stream and returns the loaded JSON as an NQUAD dataset.
	 * 
	 * @param jsonldInput
	 * @return
	 */
	public static QualifiedModel loadJsonLdAsNquadDataset(InputStream jsonldInput) {
		try {
			QualifiedModel qualifiedModel = new QualifiedModel();
			Object jsonObject = JSONUtils.fromInputStream(jsonldInput);
			qualifiedModel.baseUri = getJsonLdBaseUri(jsonObject);
			String rdfNQuads = RDFDatasetUtils.toNQuads((RDFDataset) JsonLdProcessor.toRDF(jsonObject));//TODO Find a better way to do this. Not ideal for large files
			qualifiedModel.graph = DatasetFactory.createMem();
			ByteArrayInputStream bais = new ByteArrayInputStream(rdfNQuads.getBytes());
			if(qualifiedModel.baseUri != null) {
				RDFDataMgr.read(qualifiedModel.graph, bais, qualifiedModel.baseUri, Lang.NQUADS);
			} else {
				RDFDataMgr.read(qualifiedModel.graph, bais, Lang.NQUADS);
			}
			return qualifiedModel;
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("An error occurred loading JSON-LD document", cause);
		}
	}
	
	/**
	 * Method returns all named graphs in this dataset.
	 * 
	 * @param dataset
	 * @return
	 */
	public static List<Model> getDatasetNamedModels(Dataset dataset) {
		List<Model> namedModelList = new ArrayList<Model>();
		Iterator<String> namesIter = dataset.listNames();
		while(namesIter.hasNext()) {
			String modelName = namesIter.next();
			namedModelList.add(dataset.getNamedModel(modelName));
		}
		return namedModelList;
	}
	
	/**
	 * Method returns the name of all named graphs in this dataset.
	 * 
	 * @param dataset
	 * @return
	 */
	public static List<String> getDatasetModelNames(Dataset dataset) {
		List<String> namedModelList = new ArrayList<String>();
		Iterator<String> namesIter = dataset.listNames();
		while(namesIter.hasNext()) {
			namedModelList.add(namesIter.next());
		}
		return namedModelList;
	}
	
	/**
	 * Method returns @base context attribute from JSON-LD document.
	 * 
	 * Pre-condition: A valid JSON-LD document with a context node
	 * 
	 * @param object
	 * @return The base URI for this object
	 */
	public static String getJsonLdBaseUri(Object object) {
		String baseUri = null;
		try {
			if(object instanceof Map) {
				Object context = ((Map)object).get("@context");
				if(object != null) {
					baseUri = (String)((Map)context).get("@base");
				}
			}
		} catch(Exception e) {
			System.out.println(e);//Log error
		}
		return baseUri;
	}
	
	/**
	 * Internal class to use as an in-out parameter class
	 * @author claude
	 *
	 */
	private static class QualifiedModel {
		public String baseUri;
		public Dataset graph;
	}
}

