/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.common.reader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.exception.SocraticGridRuntimeException;

import au.com.bytecode.opencsv.CSVReader;


/**
 * Reader that loads table content into memory.
 * <p>
 * File represents a rectangular array.
 * 
 * @author Claude Nanjo
 *
 */
public class CsvListReader {
	
	private CSVReader csvReader;
	private String csvFilePath;
	private List<String[]> lines;
	
	public CsvListReader() {
		lines = new ArrayList<String[]>();
	}
	
	/**
	 * Method initializes underlying CSV reader
	 * 
	 * @param reader
	 * @param separator
	 */
	public void initialize(Reader reader, char separator) {
		csvReader = new CSVReader(reader, separator);
	}
	
	/**
	 * Method initializes underlying CSV reader
	 *
	 * @param reader
	 * @param separator
	 */
	public void initialize(InputStream inputStream, char separator) {
		Reader reader = new InputStreamReader(inputStream);
		initialize(reader, separator);
	}
	
	/**
	 * Method loads each row of table in memory as a list
	 * of Strings, each string representing a single cell.
	 * 
	 * Initialize must be called before any invocation of
	 * this method.
	 * 
	 * @return
	 */
	public void loadContentInMemory() {
		try {
			lines = new ArrayList<String[]>();
			String [] nextLine;
			while ((nextLine = csvReader.readNext()) != null) {
				lines.add(nextLine);
			}
			csvReader.close();
		} catch (Exception cause) {
			throw new SocraticGridRuntimeException("Error parsing CSV file", cause);
		}
	}
	
	/**
	 * Method returns the total row count in this table.
	 * 
	 * @return
	 */
	public int getRowCount() {
		return lines.size();
	}
	
	/**
	 * Method returns the total column count for this table
	 * 
	 * @return
	 */
	public int getColumnCount() {
		int count = 0;
		if(lines.size() > 0) {
			count = lines.get(0).length;
		}
		return count;
	}
	
	/**
	 * Method returns the value of the cell specified by the
	 * given row and column indices
	 * <p>
	 * 0 based row and column indices
	 * 
	 * @param row
	 * @param col
	 * @return
	 */
	public String getItemAt(int row, int col) {
		return lines.get(row)[col].trim();
	}
	
	/**
	 * Method returns the table's rows as a list of String arrays.
	 * 
	 * @return
	 */
	public List<String[]> getLines() {
		return this.lines;
	}
	
	/**
	 * Returns the column name at that index
	 * 
	 * @param index
	 * @return
	 */
	public String getHeaderColumnName(int index) {
		return lines.get(0)[index];
	}
	
	/**
	 * Returns the column names for the indices passed in.
	 * 
	 * @param index
	 * @return
	 */
	public String[] getHeaderColumnNames(int[] indices) {
		String[] headers = new String[2];
		for(int index = 0; index < indices.length; index++) {
			headers[index] = getHeaderColumnName(indices[index]);
		}
		return headers;
	}
	
	/**
	 * Returns the value at that cell as a Double by first dividing
	 * by 100.
	 * <p>
	 * For instance, 10.4% will be returned as .104
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public double getPercentAsDouble(int row, int column) {
		return Double.parseDouble(lines.get(row)[column])/100.0;
	}
	
	/**
	 * Returns the value at that cell as a double
	 * 
	 * @param row
	 * @param column
	 * @return
	 */
	public double getDoubleValue(int row, int column) {
		return Double.parseDouble(lines.get(row)[column]);
	}
}
