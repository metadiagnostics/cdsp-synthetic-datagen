/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.common.reader;

import java.io.InputStream;

import org.socraticgrid.exception.SocraticGridRuntimeException;

public class ReaderUtilities {
	
	/** 
	 * Attempts to load the resource located in the relative resourcePath from the classpath.
	 * If the resource is not found, prepends "resources" directory to the path.
	 * An exception is null if the resource cannot be found.
	 * 
	 * @param resourcePath
	 * @return
	 */
	public static InputStream loadResourceFromStream(String resourcePath) {
		InputStream inputStream = null;
		inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(resourcePath);
		if(inputStream ==  null) {
			String fullResourcePath = (resourcePath.charAt(0) == '/')?"resources":"resources/";
			fullResourcePath += resourcePath;
			inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(fullResourcePath);
		}
		if(inputStream == null) {
			throw new SocraticGridRuntimeException("InputStream is null. Please be sure " + resourcePath + " is on classpath");
		}
		return inputStream;
	}
}
