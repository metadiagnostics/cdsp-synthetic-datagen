/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.common.jena;

import java.io.OutputStream;

import com.hp.hpl.jena.datatypes.RDFDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

/**
 * Naive Jena graph builder
 * 
 * @author Claude Nanjo
 *
 */
public class RdfModelBuilder {
	
	public static final String FORMAT_RDF_XML = "RDF/XML";
	public static final String FORMAT_RDF_XML_ABBREV = "RDF/XML-ABBREV";
	public static final String FORMAT_NTRIPLE = "N-TRIPLE";
	public static final String FORMAT_N3 = "N3";
	
	
	private Model model;
	
	public RdfModelBuilder() {
		this.model = ModelFactory.createDefaultModel();
	}
	
	public RdfModelBuilder(Model model) {
		this.model = model;
	}
	
	public Resource createNewResource(String uri) {
		return model.createResource(uri);
	}
	
	public Resource addObjectProperty(String subjectIri, String propertyIri, String objectIri) {
		Property property = model.createProperty(propertyIri);
		Resource object = createNewResource(objectIri);
		return model.createResource(subjectIri).addProperty(property, object);
	}
	
	public Resource addDataProperty(String subjectIri, String propertyIri, String value) {
		Property property = model.createProperty(propertyIri);
		return model.createResource(subjectIri).addProperty(property, value);
	}
	
	public Resource addTypedDataProperty(String subjectIri, String propertyIri, String value, RDFDatatype type) {
		Property property = model.createProperty(propertyIri);
		return model.createResource(subjectIri).addProperty(property, value, type);
	}
	
	public void writeasRdfXml(OutputStream os) {
		model.write(os, FORMAT_RDF_XML);
	}
	
	public Model getModel() {
		return model;
	}
}
