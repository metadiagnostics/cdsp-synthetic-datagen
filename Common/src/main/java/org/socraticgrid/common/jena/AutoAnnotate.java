/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.common.jena;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Flag indicating whether RDF generators may support
 * auto-annotations.
 * <p>
 * Used in conjunction with SemanticPredicate and SemanticResource
 * annotations
 * <p>
 * If value is false, auto-annotation should not occur.
 * If value is true, auto-annotation will be performed 
 * for all class attributes unless they are annotated
 * with a SemanticPredicate annotation which trumps. 
 * <p>
 * Auto-annotation proceeds as follows:
 * 1. If no baseIri is provided, the resource must have a 
 * SemanticResource annotation with a valid iri. It will be
 * used as the baseIri.
 * 2. If a baseIri is provided, the AutoAnnotate.baseIri 
 * will be used, unless the resource specifies a SemanticResource.iri.
 * 3. Each class attribute will have predicate IRI generated as 
 * follows: baseIri + separator + Field.name()
 * 
 * @author Claude Nanjo
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface AutoAnnotate {
	boolean value() default true;
	String baseIri() default "";
	char separator() default '/';
}
