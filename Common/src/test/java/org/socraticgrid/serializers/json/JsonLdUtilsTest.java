/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.serializers.json;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.github.jsonldjava.jena.JenaJSONLD;
import com.github.jsonldjava.utils.JSONUtils;

public class JsonLdUtilsTest {
	
	String testRdfData1 = "rdf/test.rdf";
	String testRdfData2 = "rdf/patient.xml";
	String testJsonData1 = "json/person.json";
	
	ClassLoader loader = null;

	@Before
	public void setUp() throws Exception {
		loader = Thread.currentThread().getContextClassLoader();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void loadResources() {
		InputStream file1 = loader.getResourceAsStream(testRdfData1);
		assertNotNull(file1);
		InputStream file2 = loader.getResourceAsStream(testRdfData2);
		assertNotNull(file2);
	}

	@Test
	public void toJsonLd() {
		String json = JsonLdUtils.serializeRdfFileAsJsonString(testRdfData2);
		System.out.println(json);
		assertNotNull(json);
	}
	
	@Test
	public void toRdfTest() {
		try {
			JenaJSONLD.init();
			InputStream jsonldInput = loader.getResourceAsStream(testJsonData1);
			ByteArrayOutputStream rdfOutput = new ByteArrayOutputStream();
			JsonLdUtils.transformJsonLdToRdf(jsonldInput, rdfOutput);
			String rdfString = rdfOutput.toString();
			assertTrue(rdfString.indexOf("<rdf:type rdf:resource=\"http://xmlns.com/foaf/0.1/Person\"/>") > 0);
		} catch(Exception e) {
			fail();
		}
	}
	
	@Test
	public void toNquadTest() {
		try {
			JenaJSONLD.init();
			InputStream jsonldInput = loader.getResourceAsStream(testJsonData1);
			ByteArrayOutputStream nquadOutput = new ByteArrayOutputStream();
			JsonLdUtils.transformJsonLdToNquads(jsonldInput, nquadOutput);
			String nquadString = nquadOutput.toString();
			System.out.println(nquadString);
			assertTrue(nquadString.indexOf("<http://manu.sporny.org/about#manu> <http://xmlns.com/foaf/0.1/knows> \"http://greggkellogg.net/foaf#me\" <http://example.org/graphs/73> .") > 0);
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
	
	@Test
	public void getBaseUri() {
		JenaJSONLD.init();
		try {
			InputStream jsonldInput = loader.getResourceAsStream(testJsonData1);
			Object jsonObject = JSONUtils.fromInputStream(jsonldInput);
			String baseUri = JsonLdUtils.getJsonLdBaseUri(jsonObject);
			assertNotNull("http://www.example.com");
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}
}
