/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.common.reader;

import static org.junit.Assert.*;

import java.io.InputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CsvListReaderTest {
	
	public static final String TSV_FILE = "reader/TabDelimitedCsv.txt";
	
	private CsvListReader reader;

	@Before
	public void setUp() throws Exception {
		try {
			reader = new CsvListReader();
			InputStream resourceStream = reader.getClass().getClassLoader().getResourceAsStream(TSV_FILE);
			assertNotNull(resourceStream);
			reader.initialize(resourceStream, '\t');
			reader.loadContentInMemory();
		} catch(Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testLoadContentInMemory() {
		assertEquals(4, reader.getLines().size());
	}
	
	@Test
	public void testLoadContentInMemoryTwice() {
		try {
			reader.loadContentInMemory();
			fail();
		} catch(Exception e) {
			assertTrue(true);
		}
	}

	@Test
	public void testGetRowCount() {
		assertEquals(4, reader.getLines().size());
	}

	@Test
	public void testGetColumnCount() {
		assertEquals(3, reader.getColumnCount());
	}

	@Test
	public void testGetItemAt() {
		assertEquals("0", reader.getItemAt(2, 1));
	}

	@Test
	public void testGetLines() {
		assertNotNull(reader.getLines());
		assertTrue(reader.getLines().size() > 0);
	}
	
	@Test
	public void getHeaderColumnNames() {
		assertEquals("Col2", reader.getHeaderColumnName(1));
	}
	
	@Test
	public void getPercentAsDouble() {
		assertEquals(0.984, reader.getPercentAsDouble(3, 1), 0.0001);
	}
	
	@Test
	public void getDoubleValue() {
		assertEquals(1.23, reader.getDoubleValue(3, 0), 0.0001);
	}
	
	@Test
	public void getDoubleValueUnhappyPath() {
		try {
			assertEquals(1.23, reader.getDoubleValue(2, 0), 0.0001);
			fail();
		} catch(Exception e) {
			assertEquals(e.getClass().getName(),"java.lang.NumberFormatException");
		}
	}
	
	@Test
	public void testGetHeaderColumnNames() {
		int[] testIndices = new int[] {0,2};
		String[] headers = reader.getHeaderColumnNames(testIndices);
		assertEquals("Col1", headers[0]);
		assertEquals("Col3", headers[1]);
	}
}
