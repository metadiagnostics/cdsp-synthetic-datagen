/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.generator.impl.DefaultPatientRecordGenerator;
import org.socraticgrid.example.model.generator.impl.PatientRecordSynthesizer;
import org.socraticgrid.example.model.serializer.rdf.GenericRdfPatientRecordSerializer;

public class DefaultRunner<P extends Person> {
		
	private PersonGenerator<P> personGenerator;
	private PatientRecordSynthesizer<P> patientRecordSynthesizer;
	private PatientRecordGenerator<P> patientRecordGenerator;
	private DocumentFormat documentFormat = DocumentFormat.GENERIC_RDF;
		
	private DefaultRunner(PersonGenerator<P> personGenerator) {
		this.personGenerator = personGenerator;
	}
	
	public static <P extends Person> DefaultRunner<P> getDefaultRunner(PersonGenerator<P> personGenerator) {
		DefaultRunner<P> runner = new DefaultRunner<P>(personGenerator);
		runner.setup();
		return runner;
	}
	
	protected void setup() {
		patientRecordGenerator = new DefaultPatientRecordGenerator<P>(personGenerator);
		patientRecordGenerator.initialize();
		patientRecordSynthesizer = new PatientRecordSynthesizer<P>(patientRecordGenerator);
		if(documentFormat == DocumentFormat.GENERIC_RDF) {
			patientRecordSynthesizer.registerSerializer(new GenericRdfPatientRecordSerializer());
		} else {
			throw new RuntimeException("Invalid document format " + documentFormat);
		}
	}
		
	public void runGenerator(int count, String dirPath, String baseFileName) {
		try {
			for(int index = 0; index < count; index++) {
				File file = new File(dirPath, baseFileName + index + ".xml");
				OutputStream os = new FileOutputStream(file);
				patientRecordSynthesizer.synthesizePatientRecord(documentFormat, os);
				os.close();
			}
		} catch(FileNotFoundException fnfe) {
			System.out.println("Please verify the path: " + fnfe.getMessage());
		}catch(Exception cause) {
			throw new RuntimeException("An error has occurred", cause);
		}
	}
		
	public void setDocumentFormat(DocumentFormat format) {
		documentFormat = format;
	}
		
	public DocumentFormat getDocumentFormat() {
		return documentFormat;
	}
	
	public PatientRecordGenerator<P> getPatientRecordGenerator() {
		return patientRecordGenerator;
	}

}
