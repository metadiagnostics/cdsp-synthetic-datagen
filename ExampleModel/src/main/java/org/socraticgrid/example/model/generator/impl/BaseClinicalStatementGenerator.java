/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.Map;

import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.syntheticdata.generator.BaseGenerator;

public abstract class BaseClinicalStatementGenerator extends BaseGenerator<ClinicalStatement>{

	public BaseClinicalStatementGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public PatientRecord getPatientRecord(Map<String,Object> configurationParameters) {
		return (PatientRecord)configurationParameters.get(PatientRecord.class.getName());
	}

}
