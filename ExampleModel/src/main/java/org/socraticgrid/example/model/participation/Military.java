/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.participation;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.enumeration.MilitaryRank;
import org.socraticgrid.example.model.enumeration.MilitaryService;

/**
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractClinicalModel.BASE_IRI + "/Military")
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class Military  extends AbstractClinicalModel implements Participation {
	
	public static final String PARTICIPATION_NAME = "Military";

	private MilitaryService service;
	private MilitaryRank rank;
	
	public Military() {
	}
	
	public Military(String service, String rank) {
		this.service = MilitaryService.valueOf(service.toUpperCase().replace(' ', '_'));
		this.rank = MilitaryRank.valueOf(rank.toUpperCase());
	}

	public MilitaryService getService() {
		return service;
	}

	public void setService(MilitaryService service) {
		this.service = service;
	}

	public MilitaryRank getRank() {
		return rank;
	}

	public void setRank(MilitaryRank rank) {
		this.rank = rank;
	}
	
	public String[] getAsArray() {
		String[] values = new String[2];
		values[0] = service.getLabel();
		values[1] = rank.name();
		return values;
	}
	
	public String toString() {
		return "Service: " + service + ", Rank: " + rank;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + getService() + "/" + getRank();
	}

	@Override
	public String getCanonicalName() {
		return PARTICIPATION_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/
