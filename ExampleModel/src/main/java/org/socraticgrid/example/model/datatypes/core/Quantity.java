/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.core;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;
import org.socraticgrid.example.model.AbstractClinicalModel;

/**
 * Represents a physical quantity such as a length,
 * an age, a span of time that can be represented as
 * a value with units.
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/Quantity")
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class Quantity extends AbstractCoreDataType {
	
	public static final String DATATYPE_NAME = "Quantity";
	
	private double value;
	private String unit;
	
	public Quantity(double value, String unit) {
		super();
		this.value = value;
		this.unit = unit;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String toString() {
		return value + " " + unit;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + value + "/" + unit;
	}

	@Override
	public String getCanonicalName() {
		return DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/