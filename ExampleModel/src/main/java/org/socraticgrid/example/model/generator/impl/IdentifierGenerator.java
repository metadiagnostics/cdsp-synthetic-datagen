/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import org.socraticgrid.example.model.datatypes.core.Identifier;

public class IdentifierGenerator {
	
	private Set<Integer> integerIds;
	private Random randomNumberGenerator;
	private final int defaultIntegerBase = 100000000;
	private String systemRoot;
	private String systemDisplayLabel;
	
	public IdentifierGenerator(String systemRoot, String systemDisplayLabel) {
		this.systemRoot = systemRoot;
		this.systemDisplayLabel = systemDisplayLabel;
		integerIds = new HashSet<Integer>();
		randomNumberGenerator = new Random();
	}
	
	public Identifier getNextIdentifier() {
		Integer candidate = getCandidateIdentifier();
		while(integerIds.contains(candidate)) {
			candidate = getCandidateIdentifier();
		}
		return new Identifier(systemRoot, candidate.toString(), systemDisplayLabel);
	}
	
	protected Integer getCandidateIdentifier() {
		Integer candidate = randomNumberGenerator.nextInt(defaultIntegerBase*10);
		if(candidate < defaultIntegerBase) {
			candidate += defaultIntegerBase;
		}
		return candidate;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/