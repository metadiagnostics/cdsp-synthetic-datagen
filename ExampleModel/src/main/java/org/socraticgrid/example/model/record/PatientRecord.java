/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.record;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.clinical.model.serializer.SkipSerialization;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticPredicate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementIndexer;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.participation.Patient;

/**
 * Represents a record of clinical statements
 * relating to a patient in a given facility
 * or institution. This record may be based
 * on a single encounter with a health system or any
 * number of encounters occurring over time.
 * <p>
 * It may include both retrospective statements as
 * well as prospective statements such as a proposal or
 * an order for a procedure.
 * <p>
 * Also note that this record is a record for a person
 * in a single institution. That is, a person may have
 * a number of these records depending on which health
 * institution they have visited in the course of their 
 * care. Each institution may have a different patient
 * identification scheme.
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractClinicalModel.BASE_IRI + "/PatientRecord")
@AutoAnnotate(value=true,baseIri=AbstractClinicalModel.BASE_IRI,separator='/')
public class PatientRecord<P extends Person> extends AbstractClinicalModel {
	
	public static final String RECORD_NAME = "PatientRecord";
	
	private Patient<P> patient;
	@SemanticPredicate(iri=AbstractClinicalModel.BASE_IRI + "/clinicalStatement")
	private List<ClinicalStatement> clinicalStatements;
	@SkipSerialization
	private ClinicalStatementIndexer index = new ClinicalStatementIndexer();
	
	public PatientRecord() {
		clinicalStatements = new ArrayList<ClinicalStatement>();
	}
	
	public PatientRecord(Patient<P> patient) {
		this();
		this.patient = patient;
	}
	
	/**
	 * Returns the patient associated with this Patient Record.
	 */
	public Patient<P> getPatient() {
		return patient;
	}
	
	/**
	 * Sets the patient that is the subject of the statements in 
	 * this Patient Record.
	 */
	public void setPatient(Patient<P> patient) {
		this.patient = patient;
	}
	
	/**
	 * Returns all clinical statements contained in this record.
	 */
	public List<ClinicalStatement> getClinicalStatements() {
		return clinicalStatements;
	}
	
	/**
	 * Sets the clinical statements of this record to be the list passed
	 * into this method call.
	 */
	public void setClinicalStatements(List<ClinicalStatement> clinicalStatements) {
		this.clinicalStatements = clinicalStatements;
	}
	
	/**
	 * Adds a clinical statement to the patient's record.
	 */
	public void addClinicalStatement(ClinicalStatement statement) {
		this.clinicalStatements.add(statement);
	}
	
	/**
	 * Adds (unions) a set of new clinical statements with the set already
	 * specified for this patient record.
	 */
	public void addClinicalStatements(List<ClinicalStatement> statements) {
		this.clinicalStatements.addAll(statements);
	}
	
	public ClinicalStatementIndexer getIndex() {
		return index;
	}

	public void setIndex(ClinicalStatementIndexer index) {
		this.index = index;
	}
	
	public void removeClinicalStatement(ClinicalStatement statement) {
		clinicalStatements.remove(statement);
		getIndex().retrieveStatementsByKey(statement.getIndexKey()).remove(statement);
	}

	/**
	 * Returns the evaluated person associated with this record. This is obtained
	 * from the Patient -> Evaluated Person relationship.
	 */
	public Person getEvaluatedPerson() {
		return patient.getEvaluatedPerson();
	}
	
	/**
	 * Prints the patient record in a human readable form.
	 */
	public String toString() {
		return patient.getIdentifier() + " has " + clinicalStatements.size() + " clinical statements";
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() +"/" + UUID.randomUUID();
	}

	@Override
	public String getCanonicalName() {
		return RECORD_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/