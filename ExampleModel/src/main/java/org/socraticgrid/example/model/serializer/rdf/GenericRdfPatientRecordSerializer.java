/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer.rdf;

import java.io.OutputStream;
import java.util.UUID;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationException;
import org.socraticgrid.clinical.model.serializer.generic.BaseGenericRdfSerializer;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.example.model.serializer.SemanticAnnotationBuilder;


public class GenericRdfPatientRecordSerializer extends BaseGenericRdfSerializer<PatientRecord> {
	
	public static final String PATIENT_RECORD_CONCEPT_IRI = "patientRecord";
	public static final String PREDICATE_PATIENT_RECORD_IDENTIFIER = BASE_ONTOLOGY_IRI + PATIENT_RECORD_CONCEPT_IRI + "/identifier";
	public static final String PREDICATE_PATIENT_RECORD_SUBJECT = BASE_ONTOLOGY_IRI  + "/subject";
	
	public GenericRdfPatientRecordSerializer() {}
	
	@Override
	public void serialize(PatientRecord patientRecord, DocumentFormat format, OutputStream outputStream) {
		try {
			if(format == DocumentFormat.GENERIC_RDF) {
				RdfContext context = new RdfContext();
				buildRdfGraph(patientRecord, format, outputStream, context);
//				System.out.println(getModel().getModel());
				getModel().writeasRdfXml(outputStream);
			} else {
				throw new SerializationException("Unknown output format: " + format);
			}
		} catch(Exception cause) {
			throw new SerializationException("Error parsing PatientRecord", cause);
		}
	}
	
	protected void buildRdfGraph(PatientRecord patientRecord, DocumentFormat format, OutputStream outputStream, RdfContext context) {
//		String instanceId = buildInstanceIdentifier(patientRecord);
//		context.addIncomingProperty(new IncomingObjectProperty(buildInstanceIri(instanceId), PREDICATE_PATIENT_RECORD_SUBJECT, Patient.class.getCanonicalName()));
//		buildDatatypeStatement(getModel(), buildInstanceIri(instanceId), PREDICATE_PATIENT_RECORD_IDENTIFIER, patientRecord.getInstanceIdentifier());
//		patientRecord.getPatient().serialize(new GenericRdfPatientSerializer(getModel()), format, outputStream, context);
		SemanticAnnotationBuilder builder = new SemanticAnnotationBuilder(getModel());
		builder.addDataTypeStatements(patientRecord, context);
	}
	
	protected String buildInstanceIri(String instanceId) {
		return BASE_INSTANCE_IRI + PATIENT_RECORD_CONCEPT_IRI + "/" + instanceId;
	}

	@Override
	protected String buildInstanceIdentifier(PatientRecord patientRecord) {
		return UUID.randomUUID().toString();
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/