/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.core;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;

@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/Range")
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class Range<T>  extends AbstractCoreDataType {

	private T low;
	private T high;
	
	public Range() {
	}
	
	public Range(T low, T high) {
		this.low = low;
		this.high = high;
	}

	public T getLow() {
		return low;
	}

	public void setLow(T low) {
		this.low = low;
	}

	public T getHigh() {
		return high;
	}

	public void setHigh(T high) {
		this.high = high;
	}
	
	@Override
	public String generateDefaultInstanceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCanonicalName() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/