/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.participation;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.core.Identifier;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Practitioner")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Practitioner extends AbstractClinicalModel implements
		Participation {

	public static final String PARTICIPATION_NAME = "Practitioner";

	private Identifier practitionerId;

	public Identifier getPractitionerId() {
		return practitionerId;
	}

	public void setPractitionerId(Identifier practitionerId) {
		this.practitionerId = practitionerId;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ getPractitionerId();
	}

	@Override
	public String getCanonicalName() {
		return PARTICIPATION_NAME;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/