/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.Date;
import java.util.List;

import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.ClinicalContext;
import org.socraticgrid.example.model.datatypes.complex.Comment;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.participation.Practitioner;

/**
 * Clinical statement base class.
 * 
 * A clinical statement represents the documentation of a
 * clinical event, observation, request, goal, etc... Such 
 * documentation is relevant for documenting patient care and
 * as a record of the clinical history of a patient for 
 * Clinical Decision Support.
 * 
 * All clinical statements derive from this class. Note
 * that we are using a variant of the HL7 Clinical Statement 
 * pattern.
 * 
 * @author Claude Nanjo
 *
 */
public abstract class AbstractClinicalStatement extends AbstractClinicalModel implements ClinicalStatement {
	
	private Identifier statementId;
	private Patient patient;
	private Person evaluatedPerson;
	private ClinicalContext clinicalContext;
	private List<Practitioner> providers;
	private Coding statementSource;
	private Date documentationTime;
	private List<Comment> comments;
	
	//Getters and Setters
	
	public Identifier getStatementId() {
		return statementId;
	}
	public void setStatementId(Identifier statementId) {
		this.statementId = statementId;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Person getEvaluatedPerson() {
		return this.evaluatedPerson;
	}
	public void setEvaluatedPerson(Person person) {
		this.evaluatedPerson = person;
	}
	public ClinicalContext getClinicalContext() {
		return clinicalContext;
	}
	public void setClinicalContext(ClinicalContext clinicalContext) {
		this.clinicalContext = clinicalContext;
	}
	public List<Practitioner> getProviders() {
		return providers;
	}
	public void setProviders(List<Practitioner> providers) {
		this.providers = providers;
	}
	public Coding getStatementSource() {
		return statementSource;
	}
	public void setStatementSource(Coding statementSource) {
		this.statementSource = statementSource;
	}
	public Date getDocumentationTime() {
		return documentationTime;
	}
	public void setDocumentationTime(Date documentationTime) {
		this.documentationTime = documentationTime;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/