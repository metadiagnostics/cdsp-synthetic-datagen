/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.complex;

import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.datatypes.core.Range;
import org.socraticgrid.example.model.enumeration.ObservationMeaning;

@SemanticResource(iri=AbstractComplexDataType.BASE_IRI + "/ReferenceRange")
@AutoAnnotate(value=true,baseIri=AbstractComplexDataType.BASE_IRI,separator='/')
public class ReferenceRange<T> extends AbstractComplexDataType {
	
	public static final String COMPLEX_DATATYPE_NAME = "ReferenceRange";
	
	private T low;
	private T high;
	private ObservationMeaning meaning;
	private Range<T> applicableAgeRange;
	

	public ReferenceRange() {
		// TODO Auto-generated constructor stub
	}


	public T getLow() {
		return low;
	}


	public void setLow(T low) {
		this.low = low;
	}


	public T getHigh() {
		return high;
	}


	public void setHigh(T high) {
		this.high = high;
	}


	public ObservationMeaning getMeaning() {
		return meaning;
	}


	public void setMeaning(ObservationMeaning meaning) {
		this.meaning = meaning;
	}


	public Range<T> getApplicableAgeRange() {
		return applicableAgeRange;
	}


	public void setApplicableAgeRange(Range<T> applicableAgeRange) {
		this.applicableAgeRange = applicableAgeRange;
	}


	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + UUID.randomUUID().toString();
	}
	
	@Override
	public String getCanonicalName() {
		return COMPLEX_DATATYPE_NAME;
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/