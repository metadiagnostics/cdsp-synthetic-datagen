/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;


import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.Serializer;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.record.PatientRecord;




/**
 * 
 * Class used to generate and serialize a patient record
 * 
 * @author Claude Nanjo
 *
 */
public class PatientRecordSynthesizer<P extends Person> {
	
	private PatientRecordGenerator<P> recordGenerator;
	private PatientRecord<P> patientRecord;
	private List<Serializer> serializers;
	
	public PatientRecordSynthesizer(PatientRecordGenerator<P> generator) {
		this.recordGenerator = generator;
		this.serializers = new ArrayList<Serializer>();
	}
	
	/**
	 * Method to initialize the class. Call before invoking other methods.
	 */
	public void initialize() {
		recordGenerator.initialize();
	}
	
	/**
	 * Main class function. Method generates a synthetic patient record and delegates the
	 * serialization of this record to any number of registered serializers.
	 */
	public void synthesizePatientRecord(DocumentFormat format, OutputStream outputStream){
		PatientRecord<P> patientRecord = recordGenerator.generatePatientRecord();
		for(Serializer serializer:serializers) {
			serializer.initialize();
			serializer.serialize(patientRecord, format, outputStream);
			serializer.tearDown();
		}
	}
	
	/**
	 * Sets the patient generator associated with this class
	 */
	public void setPatientRecordGenerator(DefaultPatientRecordGenerator<P> recordGenerator) {
		this.recordGenerator = recordGenerator;
	}
	
	/**
	 * Registers the serializers used to serialize the model
	 */
	public void registerSerializer(Serializer serializer) {
		this.serializers.add(serializer);
	}
	
	/**
	 * Removes a serializer from the list of serializers associated with an instance
	 * of this class.
	 */
	public boolean removeSerializer(Serializer serializer) {
		return this.serializers.remove(serializer);
	}
	
	/**
	 * returns the patient record that has been generated.
	 */
	public PatientRecord<P> getPatientRecord() {
		return patientRecord;
	}
	
	/**
	 * Sets the patient record to be serialized.
	 */
	public void setPatientRecord(PatientRecord<P> patientRecord) {
		this.patientRecord = patientRecord;
	}
}