/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.complex;

import java.util.Date;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticPredicate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.enumeration.AddressDirectional;
import org.socraticgrid.example.model.enumeration.AddressType;
import org.socraticgrid.example.model.enumeration.StateEnumeration;

@SemanticResource(iri=AbstractComplexDataType.BASE_IRI + "/Address")
@AutoAnnotate(value=true,baseIri=AbstractComplexDataType.BASE_IRI,separator='/')
public class Address extends AbstractComplexDataType {
	
	public static final String COMPLEX_DATATYPE_NAME = "Address";
	
	@SemanticPredicate(iri="http://www.socraticgrid.org/owl/ontology/address/use")
	private AddressType addressType;
	private String houseNumber;
	private AddressDirectional predirectional;
	private String streetName;
	private String streetAddress;
	private String streetType;
	private AddressDirectional postDirectional;
	private String unitDesignator;
	private String cityName;
	private String postalCode;
	private StateEnumeration stateName;
	private Date updatedDate;
	
	public AddressType getAddressType() {
		return addressType;
	}
	public void setAddressType(AddressType addressType) {
		this.addressType = addressType;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	public AddressDirectional getPredirectional() {
		return predirectional;
	}
	public void setPredirectional(AddressDirectional predirectional) {
		this.predirectional = predirectional;
	}
	public String getStreetName() {
		return streetName;
	}
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getStreetType() {
		return streetType;
	}
	public void setStreetType(String streetType) {
		this.streetType = streetType;
	}
	public AddressDirectional getPostDirectional() {
		return postDirectional;
	}
	public void setPostDirectional(AddressDirectional postDirectional) {
		this.postDirectional = postDirectional;
	}
	public String getUnitDesignator() {
		return unitDesignator;
	}
	public void setUnitDesignator(String unitDesignator) {
		this.unitDesignator = unitDesignator;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public StateEnumeration getStateName() {
		return stateName;
	}
	public void setStateName(StateEnumeration stateName) {
		this.stateName = stateName;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(houseNumber)
			.append(" ")
			.append(streetAddress)
			.append(", ")
			.append(cityName)
			.append(" ")
			.append(stateName)
			.append(" ")
			.append(postalCode);
		return builder.toString();
	}
	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + UUID.randomUUID().toString();
	}
	@Override
	public String getCanonicalName() {
		return COMPLEX_DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/