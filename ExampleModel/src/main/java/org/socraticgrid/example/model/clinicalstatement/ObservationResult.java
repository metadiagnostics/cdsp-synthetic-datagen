/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.BodySite;
import org.socraticgrid.example.model.datatypes.complex.ReferenceRange;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.enumeration.ObservationReliability;
import org.socraticgrid.example.model.enumeration.ObservationStatus;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.participation.Practitioner;

/**
 * Class holding the result of an observation such as
 * a blood pressure measurement or a CBC panel item.
 * 
 * @author Claude Nanjo
 *
 * @param <T>
 */
@SemanticResource(iri=AbstractClinicalModel.BASE_IRI + "/ObservationResult")
@AutoAnnotate(value=true,baseIri=AbstractClinicalModel.BASE_IRI,separator='/')
public class ObservationResult<T> extends AbstractClinicalStatement {
	
	public static final String CLINICAL_STATEMENT_NAME = "ObservationResult";
	
	private Identifier identifier;
	private Coding observationFocus;
	private T observationValue;
	private Coding interpretation;
	private String issued;
	private ObservationStatus status;
	private ObservationReliability reliability;
	private BodySite bodySite;
	private Coding method;
	private Period applies;
	//private Specimen specimen;
	private Patient subject;
	private List<Practitioner> performers;
	private ReferenceRange<T> referenceRange;
	
	public Coding getObservationFocus() {
		return observationFocus;
	}
	
	public void setObservationFocus(Coding observationFocus) {
		this.observationFocus = observationFocus;
	}
	
	public T getObservationValue() {
		return observationValue;
	}
	
	public void setObservationValue(T observationValue) {
		this.observationValue = observationValue;
	}
	
	public Period getObservationTime() {
		return applies;
	}
	
	public void setObservationTime(Period applies) {
		this.applies = applies;
	}

	public Identifier getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}

	public Coding getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(Coding interpretation) {
		this.interpretation = interpretation;
	}

	public ObservationStatus getStatus() {
		return status;
	}

	public void setStatus(ObservationStatus status) {
		this.status = status;
	}

	public ObservationReliability getReliability() {
		return reliability;
	}

	public void setReliability(ObservationReliability reliability) {
		this.reliability = reliability;
	}

	public BodySite getBodySite() {
		return bodySite;
	}

	public void setBodySite(BodySite bodySite) {
		this.bodySite = bodySite;
	}

	public Coding getMethod() {
		return method;
	}

	public void setMethod(Coding method) {
		this.method = method;
	}

	public Period getApplies() {
		return applies;
	}

	public void setApplies(Period applies) {
		this.applies = applies;
	}

	public Patient getSubject() {
		return subject;
	}

	public void setSubject(Patient subject) {
		this.subject = subject;
	}

	public List<Practitioner> getPerformer() {
		return performers;
	}

	public void setPerformer(List<Practitioner> performers) {
		this.performers = performers;
	}
	
	public String getIssued() {
		return issued;
	}

	public void setIssued(String issued) {
		this.issued = issued;
	}

	public ReferenceRange<T> getReferenceRange() {
		return referenceRange;
	}

	public void setReferenceRange(ReferenceRange<T> referenceRange) {
		this.referenceRange = referenceRange;
	}

	public static String getClinicalStatementName() {
		return CLINICAL_STATEMENT_NAME;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() +"/" + UUID.randomUUID();
	}

	@Override
	public String getCanonicalName() {
		return CLINICAL_STATEMENT_NAME;
	}
	
	public ClinicalStatementKey getIndexKey() {
		return new ClinicalStatementKey(this, "observationFocus", observationFocus);
	}
	
	public void indexStatement(ClinicalStatementIndexer indexer) {
		indexer.indexStatement(getIndexKey(), this);
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/