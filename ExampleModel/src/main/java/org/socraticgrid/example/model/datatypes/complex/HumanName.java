/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.complex;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;

@SemanticResource(iri=AbstractComplexDataType.BASE_IRI + "/HumanName")
@AutoAnnotate(value=true,baseIri=AbstractComplexDataType.BASE_IRI,separator='/')
public class HumanName extends AbstractComplexDataType {
	
	public static final String COMPLEX_DATATYPE_NAME = "HumanName";

	private String givenName;
	private String familyName;
	private String maidenName;
	private List<String> middleNames;
	
	public HumanName() {
		this.middleNames = new ArrayList<String>();
	}
	
	public HumanName(String first, String last) {
		this();
		this.givenName = first;
		this.familyName = last;
	}
	public HumanName(String first, List<String> middleNames, String last) {
		this();
		this.givenName = first;
		this.familyName = last;
		this.middleNames = middleNames;
	}
	
	public String getGivenName() {
		return givenName;
	}
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getMaidenName() {
		return maidenName;
	}
	public void setMaidenName(String maidenName) {
		this.maidenName = maidenName;
	}
	public List<String> getMiddleNames() {
		return middleNames;
	}
	public void setMiddleNames(List<String> middleNames) {
		this.middleNames = middleNames;
	}
	public void addMiddleName(String middleName) {
		this.middleNames.add(middleName);
	}
	public String toString() {
		return toStringLastFirstMiddle();
	}
	public String toStringLastFirstMiddle() {
		StringBuilder builder = new StringBuilder();
		builder.append(familyName)
			.append(",")
			.append(givenName)
			.append(" ");
		for(String name:middleNames) {
			builder.append(name).append(" ");
		}
		return builder.toString().trim();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + familyName + "/" + givenName + "/" + middleNames.toString().hashCode();
	}

	@Override
	public String getCanonicalName() {
		return COMPLEX_DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/