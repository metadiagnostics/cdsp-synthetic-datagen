/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.core;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.socraticgrid.clinical.model.serializer.SkipSerialization;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;

@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/Period")
@AutoAnnotate(value=true,baseIri=AbstractCoreDataType.BASE_IRI,separator='/')
public class Period extends AbstractCoreDataType {
	
	@SkipSerialization
	private SimpleDateFormat dateFormat = new SimpleDateFormat("dd-M-yyyy-hh-mm-ss");
	public static final String DATATYPE_NAME = "Period";

	private Date start;
	private Date end;
	
	public Date getStart() {
		return start;
	}
	public void setStart(Date start) {
		this.start = start;
	}
	public Date getEnd() {
		return end;
	}
	public void setEnd(Date end) {
		this.end = end;
	}
	@Override
	public String generateDefaultInstanceIdentifier() {
		String startDate = "unbounded";
		String endDate = "unbounded";
		if(start != null) {
			startDate = dateFormat.format(start);
		}
		if(end != null) {
			endDate = dateFormat.format(end);
		}
		return getBaseIri() + "/" + getCanonicalName() + "/" + startDate + "/" + endDate;
	}
	@Override
	public String getCanonicalName() {
		return DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/