/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.core;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;

/**
 * Class representing a coded concept such as a concept contained within a
 * standard terminology such as SNOMED-CT. The code MUST belong to either
 * a code system or a value set. Codes from value sets should generally include the
 * code system the value set was defined.
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/Coding")
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class Coding extends AbstractCoreDataType {
	
	public static final String DATATYPE_NAME = "Coding";
	
	private String code;
	private String codeDisplayName;
	private String codeSystem;
	private String codeSystemDisplayName;
	private String valueSetCodeSystem;
	private String valueSetName;
	
	public Coding() {
		
	}
	
	public Coding(String code,
					String codeDisplayName,
					String codeSystem,
					String codeSystemDisplayName) {
		this();
		this.code = code;
		this.codeDisplayName = codeDisplayName;
		this.codeSystem = codeSystem;
		this.codeSystemDisplayName = codeSystemDisplayName;
	}
	
	public Coding(String code,
					String codeDisplayName,
					String codeSystem,
					String codeSystemDisplayName,
					String valueSetCodeSystem,
					String valueSetName) {
		this(code, codeDisplayName, codeSystem, codeSystemDisplayName);
		this.valueSetCodeSystem = valueSetCodeSystem;
		this.valueSetName = valueSetName;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getCodeDisplayName() {
		return codeDisplayName;
	}
	
	public void setCodeDisplayName(String codeDisplayName) {
		this.codeDisplayName = codeDisplayName;
	}
	
	/**
	 * Must be a valid URI
	 * 
	 * @return
	 */
	public String getCodeSystem() {
		return codeSystem;
	}
	
	public void setCodeSystem(String codeSystem) {
		this.codeSystem = codeSystem;
	}
	
	public String getCodeSystemDisplayName() {
		return codeSystemDisplayName;
	}
	
	public void setCodeSystemDisplayName(String codeSystemDisplayName) {
		this.codeSystemDisplayName = codeSystemDisplayName;
	}
	
	public String getValueSetCodeSystem() {
		return valueSetCodeSystem;
	}
	
	public void setValueSetCodeSystem(String valueSetCodeSystem) {
		this.valueSetCodeSystem = valueSetCodeSystem;
	}
	
	public String getValueSetName() {
		return valueSetName;
	}
	
	public void setValueSetName(String valueSetName) {
		this.valueSetName = valueSetName;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + codeSystem + "/" + code;
	}

	@Override
	public String getCanonicalName() {
		return DATATYPE_NAME;
	}
	
	

	@Override
	public String toString() {
		return "Coding [codeDisplayName=" + codeDisplayName
				+ ", codeSystemDisplayName=" + codeSystemDisplayName + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result
				+ ((codeDisplayName == null) ? 0 : codeDisplayName.hashCode());
		result = prime * result
				+ ((codeSystem == null) ? 0 : codeSystem.hashCode());
		result = prime
				* result
				+ ((codeSystemDisplayName == null) ? 0 : codeSystemDisplayName
						.hashCode());
		result = prime
				* result
				+ ((valueSetCodeSystem == null) ? 0 : valueSetCodeSystem
						.hashCode());
		result = prime * result
				+ ((valueSetName == null) ? 0 : valueSetName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coding other = (Coding) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (codeDisplayName == null) {
			if (other.codeDisplayName != null)
				return false;
		} else if (!codeDisplayName.equals(other.codeDisplayName))
			return false;
		if (codeSystem == null) {
			if (other.codeSystem != null)
				return false;
		} else if (!codeSystem.equals(other.codeSystem))
			return false;
		if (codeSystemDisplayName == null) {
			if (other.codeSystemDisplayName != null)
				return false;
		} else if (!codeSystemDisplayName.equals(other.codeSystemDisplayName))
			return false;
		if (valueSetCodeSystem == null) {
			if (other.valueSetCodeSystem != null)
				return false;
		} else if (!valueSetCodeSystem.equals(other.valueSetCodeSystem))
			return false;
		if (valueSetName == null) {
			if (other.valueSetName != null)
				return false;
		} else if (!valueSetName.equals(other.valueSetName))
			return false;
		return true;
	}
	
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/