/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PatientGenerator;
import org.socraticgrid.example.model.generator.PatientRecordGenerator;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.statistical.distribution.bayes.AutomatonRunner;
import org.socraticgrid.statistical.distribution.bayes.Generator;

/**
 * The PatientRecordGenerator generates a synthetic patient record.
 * This class makes use of ClinicalStatementGenerators to generate
 * clinical statements for this patient. The behavior of these 
 * generators can be influenced by both patient attributes (e.g.,
 * gender) and the clinical context (inpatient, outpatient) if 
 * relevant.
 * 
 * See the ClinicalRecord class for more information on the structure
 * of this class and its semantics.
 * 
 * @author Claude Nanjo
 *
 * @param <T>
 */
public class DefaultPatientRecordGenerator<P extends Person> implements PatientRecordGenerator<P> {
	
	private PatientGenerator<P> patientGenerator;
	private List<Generator<ClinicalStatement>> clinicalStatementGenerators;
	private List<AutomatonRunner> runners;
	private List<BaseClinicalStatementGenerator> conditionalProbabilityGenerators;
	
	public DefaultPatientRecordGenerator(PersonGenerator<P> personGenerator) {
		clinicalStatementGenerators = new ArrayList<Generator<ClinicalStatement>>();
		patientGenerator = new DefaultPatientGenerator<P>();
		patientGenerator.registerPersonGenerator(personGenerator);
		runners = new ArrayList<AutomatonRunner>();
		conditionalProbabilityGenerators = new ArrayList<BaseClinicalStatementGenerator>();
	}
	
	public void initialize() {
		patientGenerator.initialize();
	}
	
	public PatientRecord<P> generatePatientRecord() {
		Patient<P> patient = patientGenerator.generatePatient();
		PatientRecord<P> record = new PatientRecord<P>(patient);
		Map<String,Object> generationConfig = new HashMap<String,Object>();
		generationConfig.put(PatientRecord.class.getName(), record);
		record.setInstanceIdentifier(new Identifier(Identifier.DEFAULT_IDENTIFIER_ROOT ,UUID.randomUUID().toString(), "Socratic Grid Identifier"));
		for(Generator<ClinicalStatement> generator : clinicalStatementGenerators) {
			record.addClinicalStatements(generator.generate(generationConfig));
		}
		for(AutomatonRunner runner : runners) {
			record.addClinicalStatements(runner.generate(generationConfig)); //TODO Add clinical context to this generator
		}
		for(BaseClinicalStatementGenerator generator : conditionalProbabilityGenerators) {
			record.addClinicalStatements(generator.generate(generationConfig));
		}
		return record;
	}
	
	public void registerClinicalStatementGenerator(Generator<ClinicalStatement> generator) {
		clinicalStatementGenerators.add(generator);
	}
	
	public void removeClinicalStatementGenerator(Generator<ClinicalStatement> generator) {
		clinicalStatementGenerators.remove(generator);
	}
	
	public void registerPatientGenerator(PatientGenerator<P> patientGenerator) {
		this.patientGenerator = patientGenerator;
	}
	
	public void addAutomatonRunners(List<AutomatonRunner> automatonRunners) {
		this.runners.addAll(automatonRunners);
	}
	
	public List<BaseClinicalStatementGenerator> getConditionalProbabilityGenerators() {
		return conditionalProbabilityGenerators;
	}
	
	public void addConditionalProbability(BaseClinicalStatementGenerator generator) {
		conditionalProbabilityGenerators.add(generator);
	}
	
	public void addConditionalProbabilities(List<BaseClinicalStatementGenerator> generators) {
		conditionalProbabilityGenerators.addAll(generators);
	}
}