/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.Random;

import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.enumeration.AddressType;
import org.socraticgrid.example.model.enumeration.StateEnumeration;
import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;
import org.socraticgrid.statistical.distribution.reader.AddressReader;

/**
 * NOTE: The address generator does not map City to state to zip at this time.
 * Allocation of each value per address is random.
 * 
 * @author claude
 *
 */
public class AddressGenerator {
	
	private DiscreteProbabilityDistribution<String> streetDistribution;
	private DiscreteProbabilityDistribution<String> cityDistribution;
	private DiscreteProbabilityDistribution<String> stateDistribution;
	private DiscreteProbabilityDistribution<String> zipDistribution;
	
	private Random random;
	
	public AddressGenerator() {
		random = new Random();
	}
	
	public void initialize(AddressReader reader) {
		try {
			streetDistribution = new DiscreteProbabilityDistribution<String>(
					reader.loadStreetAddresses());
			cityDistribution = new DiscreteProbabilityDistribution<String>(
					reader.loadCities());
			stateDistribution = new DiscreteProbabilityDistribution<String>(
					reader.loadStates());
			zipDistribution = new DiscreteProbabilityDistribution<String>(
					reader.loadZipCodes());
		} catch (Exception cause) {
			throw new SocraticGridRuntimeException(
					"Error configurating random address generator: " + cause.getMessage(), cause);
		}
	}
	
	public Address generateAddress() {
		Address address = new Address();
		address.setHouseNumber("" + (random.nextInt(10000) + 1));
		address.setStreetAddress(streetDistribution.getSample());
		address.setCityName(cityDistribution.getSample());
		address.setStateName(StateEnumeration.getStateEnumerationForState(stateDistribution.getSample()));
		address.setPostalCode(zipDistribution.getSample());
		address.setAddressType(AddressType.CONTACT);//A default. Can be overridden
		return address;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/