/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator;

import java.util.List;

import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.record.PatientRecord;
import org.socraticgrid.statistical.distribution.bayes.AutomatonRunner;
import org.socraticgrid.statistical.distribution.bayes.Generator;
import org.socraticgrid.syntheticdata.generator.Initialize;

public interface PatientRecordGenerator<P extends Person>  extends Initialize {
	
	public PatientRecord<P> generatePatientRecord();
	public void registerClinicalStatementGenerator(Generator<ClinicalStatement> generator);
	public void removeClinicalStatementGenerator(Generator<ClinicalStatement> generator);
	public void registerPatientGenerator(PatientGenerator<P> patientGenerator);
	public void addAutomatonRunners(List<AutomatonRunner> runners);
	public void addConditionalProbabilities(List<BaseClinicalStatementGenerator> generators);
}