/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.participation;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;

/**
 * In this model, the 'Patient' concept is represented as a role. The
 * motivation for this approach is that a person may be a patient in 
 * more than a single institution and have records in each institution.
 * Thus, an evaluated person may have more than one patient ID and patient records.
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractClinicalModel.BASE_IRI + "/Patient")
@AutoAnnotate(value=true,baseIri=AbstractClinicalModel.BASE_IRI,separator='/')
public class Patient<T extends Person> extends AbstractClinicalModel implements Participation {
	
	public static final String PARTICIPATION_NAME = "Patient";
	
	private Identifier identifier;
	private T evaluatedPerson;
	
	/**
	 * Returns the patient identifier
	 */
	public Identifier getIdentifier() {
		return identifier;
	}
	
	/**
	 * Sets the patient identifier
	 */
	public void setIdentifier(Identifier identifier) {
		this.identifier = identifier;
	}
	
	/**
	 * Returns the evaluated person associated with this patient role
	 */
	public T getEvaluatedPerson() {
		return evaluatedPerson;
	}

	/**
	 * Sets the evaluated person functioning in the patient role
	 */
	public void setEvaluatedPerson(T evaluatedPerson) {
		this.evaluatedPerson = evaluatedPerson;
	}
	
	public String toString() {
		return evaluatedPerson + "(Patient ID: " + evaluatedPerson + ")";
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() +"/" + getIdentifier().generateDefaultInstanceIdentifier();
	}

	@Override
	public String getCanonicalName() {
		return PARTICIPATION_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/