/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ClinicalStatementIndexer {
	
	private Map<ClinicalStatementKey, List<ClinicalStatement>> index;

	public ClinicalStatementIndexer() {
		index = new HashMap<ClinicalStatementKey, List<ClinicalStatement>>();
	}
	
	public void indexStatement(ClinicalStatementKey key, ClinicalStatement statement) {
		List<ClinicalStatement> statements = index.get(key);
		if(statements == null) {
			statements = new ArrayList<ClinicalStatement>();
			index.put(key,  statements);
		}
		statements.add(statement);
	}
	
	public List<ClinicalStatement> retrieveStatementsByKey(ClinicalStatementKey key) {
		return index.get(key);
	}
	
	public boolean exists(ClinicalStatementKey key) {
		List<ClinicalStatement> indexedStatements = retrieveStatementsByKey(key);
		return (indexedStatements != null && indexedStatements.size() > 0);
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/