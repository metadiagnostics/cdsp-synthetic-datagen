/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;


public class TimeCalculation {
	private Random random;
	private final int millisInDay = 24 * 60 * 60 * 1000;

	public TimeCalculation() {
		super();
		this.random = new Random();
	}

	public Date getTimeStamp(double ageInYears) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(Math.abs(System.currentTimeMillis()
				- random.nextInt(millisInDay))));
		cal.add(Calendar.HOUR, -1 * (int) (ageInYears * 365 * 24));
		return cal.getTime();
	}

	public Date getTimeStamp() {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(Math.abs(System.currentTimeMillis()
				- random.nextInt(millisInDay))));
		int ageInYears = (int) (Math.random() * ((2013 - 2010) + 1));
		cal.add(Calendar.HOUR, -1 * (int) (ageInYears * 365 * 24));
		return cal.getTime();
	}

	public double getRandomInt(int min, int max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}
	
	private Integer getTemplateStartMinutes() {
		return (int) (getRandomInt(1, 4) * 10);
	}
	
	private Integer getTemplateWorkingDay() {
		return (int) (getRandomInt(2, 6));
	}
	

	
	public Calendar getTimeStampInFuture() {
		long offset = Timestamp.valueOf("2014-08-01 00:00:00").getTime();
		long end = Timestamp.valueOf("2015-08-01 00:00:00").getTime();
		long diff = end - offset + 1;
		Calendar cal = Calendar.getInstance();
		long diffInMilliSec = (long) (Math.random() * diff);
		cal.setTimeInMillis(offset + diffInMilliSec);
//		System.out.println("After initializing, time is: "+cal.getTime());
		cal.set(java.util.Calendar.HOUR_OF_DAY, (int) getRandomInt(8,16));
		cal.set(java.util.Calendar.DAY_OF_WEEK, getTemplateWorkingDay());
		cal.set(java.util.Calendar.MINUTE, getTemplateStartMinutes());
//		System.out.println("After setting, time is: "+cal.getTime());
		return cal;

	}

	public Date getCurrentTimeStamp() {
		java.util.Date now = new java.util.Date(); // Basically the same thing
		return now;
	}

	public Date getRandomTimeStamp() {

		Date date = new Date((long) random.nextInt(millisInDay));
		// Date date1 = new Date(System.currentTimeMillis() % 1000);
		return date;
	}
}