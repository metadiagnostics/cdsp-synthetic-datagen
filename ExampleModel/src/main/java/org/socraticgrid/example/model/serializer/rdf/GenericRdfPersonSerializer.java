/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer.rdf;

import java.io.OutputStream;
import java.util.UUID;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationException;
import org.socraticgrid.clinical.model.serializer.generic.BaseGenericRdfSerializer;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.common.jena.RdfModelBuilder;
import org.socraticgrid.example.model.entity.Person;

public class GenericRdfPersonSerializer extends BaseGenericRdfSerializer<Person> {
	
	public static final String PERSON_CONCEPT_IRI = "person";
	public static final String PREDICATE_PERSON_NAME = BASE_ONTOLOGY_IRI + PERSON_CONCEPT_IRI + "/" + "name";
	public static final String PREDICATE_PERSON_ADDRESS = BASE_ONTOLOGY_IRI + PERSON_CONCEPT_IRI + "/" + "address";
	
	public GenericRdfPersonSerializer(RdfModelBuilder model) {}

	public void serialize(Person person, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		try {
			if(format == DocumentFormat.GENERIC_RDF) {
				buildRdfGraph(person, format, outputStream, context);
			} else {
				throw new SerializationException("Unknown output format: " + format);
			}
		} catch(Exception cause) {
			throw new SerializationException("Error parsing PatientRecord", cause);
		}		
	}
	
	protected void buildRdfGraph(Person person, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		//context.addIncomingProperty(new IncomingObjectProperty(buildInstanceIri(instanceId), PREDICATE_PERSON_NAME, HumanName.class.getCanonicalName()));
		//context.addIncomingProperty(new IncomingObjectProperty(buildInstanceIri(instanceId), PREDICATE_PERSON_ADDRESS, Address.class.getCanonicalName()));
		//buildIncomingEdgesFromContext(person, context,instanceId);
		//person.getAddresses(AddressType.CONTACT).get(0).serialize(new GenericRdfAddressSerializer(getModel()), format, outputStream, context);
		//person.getPreferredName().serialize(new GenericRdfHumanNameSerializer(getModel()), format, outputStream, context);
		//SemanticAnnotationBuilder builder = new SemanticAnnotationBuilder(getModel());
		//builder.addDataTypeStatements(person, context);
	}
	
	protected String buildInstanceIri(String instanceId) {
		return BASE_INSTANCE_IRI + PERSON_CONCEPT_IRI + "/" + instanceId;
	}

	@Override
	protected String buildInstanceIdentifier(Person clinicalModel) {
		return UUID.randomUUID().toString();
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/