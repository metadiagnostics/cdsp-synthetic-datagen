/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PatientGenerator;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.participation.Patient;
import org.socraticgrid.exception.SocraticGridRuntimeException;

public class DefaultPatientGenerator<P extends Person> implements PatientGenerator<P> {
	
	private PersonGenerator<P> personGenerator;
	private IdentifierGenerator identifierGenerator;
	
	//TODO Need a configure method - ideally from a file
	public DefaultPatientGenerator() {
		identifierGenerator = new IdentifierGenerator(Identifier.DEFAULT_IDENTIFIER_ROOT, "Default Socratic Grid Identifier");
	}
	
	public DefaultPatientGenerator(PersonGenerator<P> personGenerator) {
		this();
		this.personGenerator = personGenerator;
	}
	
	public void initialize() {
		personGenerator.initialize();
	}
	
	public Patient<P> generatePatient() {
		try {
			Patient<P> patient = new Patient<P>();
			patient.setIdentifier(identifierGenerator.getNextIdentifier());
			P person = personGenerator.generatePerson();
			patient.setEvaluatedPerson(person);
			return patient;
		} catch(Exception cause) {
			throw new SocraticGridRuntimeException("Error generating patient instance", cause);
		}
	}
	
	public List<Patient<P>> generatePatients(int count) {
		List<Patient<P>> generatedPatients = new ArrayList<Patient<P>>(count);
		for(int i=0; i< count; i++) {
			generatedPatients.add(generatePatient());
		}
		return generatedPatients;
	}
	
	public void registerPersonGenerator(PersonGenerator<P> personGenerator) {
		this.personGenerator = personGenerator;
	}
}