/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.List;

import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.datatypes.core.Quantity;
import org.socraticgrid.example.model.entity.Organization;
import org.socraticgrid.example.model.enumeration.EncounterClass;
import org.socraticgrid.example.model.enumeration.EncounterStatus;
import org.socraticgrid.example.model.participation.Patient;

public class Encounter {
	
	private List<Identifier> identifiers;
	private EncounterStatus status;
	private EncounterClass encounterClass;
	private Coding type;
	private Patient subject;
	private Period encounterPeriod;
	private Quantity length;
	private Coding reason;
	private Object indication;
	private Coding priority;
	private Organization serviceProvider;
	private Encounter partOf;
	
	public Encounter() {
		// TODO Auto-generated constructor stub
	}

	public List<Identifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<Identifier> identifiers) {
		this.identifiers = identifiers;
	}

	public EncounterStatus getStatus() {
		return status;
	}

	public void setStatus(EncounterStatus status) {
		this.status = status;
	}

	public EncounterClass getEncounterClass() {
		return encounterClass;
	}

	public void setEncounterClass(EncounterClass encounterClass) {
		this.encounterClass = encounterClass;
	}

	public Coding getType() {
		return type;
	}

	public void setType(Coding type) {
		this.type = type;
	}

	public Patient getSubject() {
		return subject;
	}

	public void setSubject(Patient subject) {
		this.subject = subject;
	}

	public Period getEncounterPeriod() {
		return encounterPeriod;
	}

	public void setEncounterPeriod(Period encounterPeriod) {
		this.encounterPeriod = encounterPeriod;
	}

	public Quantity getLength() {
		return length;
	}

	public void setLength(Quantity length) {
		this.length = length;
	}

	public Coding getReason() {
		return reason;
	}

	public void setReason(Coding reason) {
		this.reason = reason;
	}

	public Object getIndication() {
		return indication;
	}

	public void setIndication(Object indication) {
		this.indication = indication;
	}

	public Coding getPriority() {
		return priority;
	}

	public void setPriority(Coding priority) {
		this.priority = priority;
	}

	public Organization getServiceProvider() {
		return serviceProvider;
	}

	public void setServiceProvider(Organization serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

	public Encounter getPartOf() {
		return partOf;
	}

	public void setPartOf(Encounter partOf) {
		this.partOf = partOf;
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/