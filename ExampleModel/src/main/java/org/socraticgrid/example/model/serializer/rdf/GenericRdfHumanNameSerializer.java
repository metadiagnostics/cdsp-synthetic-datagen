/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer.rdf;

import java.io.OutputStream;
import java.util.UUID;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationException;
import org.socraticgrid.clinical.model.serializer.generic.BaseGenericRdfSerializer;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.example.model.datatypes.complex.HumanName;

public class GenericRdfHumanNameSerializer extends BaseGenericRdfSerializer<HumanName> {

	public static final String NAME_CONCEPT_IRI = "name";
	
	public GenericRdfHumanNameSerializer() {}

	public void serialize(HumanName name, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		try {
			if(format == DocumentFormat.GENERIC_RDF) {
				buildRdfGraph(name, format, outputStream, context);
			} else {
				throw new SerializationException("Unknown output format: " + format);
			}
		} catch(Exception cause) {
			throw new SerializationException("Error parsing PatientRecord", cause);
		}
		
	}
		
	protected void buildRdfGraph(HumanName name, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		String instanceId = buildInstanceIdentifier(name);
		//SemanticAnnotationBuilder builder = new SemanticAnnotationBuilder(getModel());
		//builder.addDataTypeStatements(name, context);
		buildIncomingEdgesFromContext(name, context, instanceId);
		
	}
	
	protected String buildInstanceIri(String instanceId) {
		return BASE_INSTANCE_IRI + NAME_CONCEPT_IRI + "/" + instanceId;
	}

	@Override
	protected String buildInstanceIdentifier(HumanName humanName) {
		return UUID.randomUUID().toString();
	}

}

/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/