/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.impl.RandomChoiceGenerator;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.statistical.distribution.reader.AddressCsvReader;
import org.socraticgrid.statistical.distribution.reader.AddressReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameCsvReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameReader;

public class DefaultPersonGenerator implements PersonGenerator<Person> {
	
	private RandomChoiceGenerator randomChoiceGenerator;
	private PersonNameGenerator maleNameGenerator;
	private PersonNameGenerator femaleNameGenerator;
	private AddressGenerator addressGenerator;
	private IdentifierGenerator identifierGenerator;
	
	public DefaultPersonGenerator() {
	}
	
	public void initialize() {
		randomChoiceGenerator = new RandomChoiceGenerator();
		PersonNameReader maleNameReader = new PersonNameCsvReader(	"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonMaleNames.txt", '\t', false, 
															"distribution/commonLastNames.txt", '\t', false);
		PersonNameReader femaleNameReader = new PersonNameCsvReader("distribution/commonFemaleNames.txt", '\t', false, 
															"distribution/commonFemaleNames.txt", '\t', false, 
															"distribution/commonLastNames.txt", '\t', false);
		
		maleNameReader.setHeaderFlag(true);
		femaleNameReader.setHeaderFlag(true);
		
		AddressReader addressReader = new AddressCsvReader("distribution/addressesUS.txt", '\t', false,
													"distribution/CitiesUS.txt", '\t', false, 
													"distribution/StatesByPopulationUS.txt", '\t', false, 
													"distribution/ZipByPopulationUS.txt", '\t', false);	
		addressReader.setHeaderFlag(true);
		maleNameGenerator = new PersonNameGenerator();
		maleNameGenerator.initialize(maleNameReader);
		femaleNameGenerator = new PersonNameGenerator();
		femaleNameGenerator.initialize(femaleNameReader);
		addressGenerator = new AddressGenerator();
		addressGenerator.initialize(addressReader);
		identifierGenerator = new IdentifierGenerator(Identifier.DEFAULT_IDENTIFIER_ROOT, "Socratic Grid Default Integer Identifier");
	}
	
	public Person generatePerson() {
		Person person = new Person();
		generateAdministrativeGender(person);
		generatePreferredName(person);
		generateAddress(person);
		generateRandomIdentifier(person);
		return person;
	}
	
	public List<Person> generatePeople(int count) {
		return new ArrayList<Person>();
	}
	
	public void generateRandomIdentifier(Person person) {
		person.addIdentifier(identifierGenerator.getNextIdentifier());
	}
	
	public void generateAdministrativeGender(Person person) {
		person.setAdministrativeGender(randomChoiceGenerator.getEnumerationItem(AdministrativeGender.class));
	}
	
	public void generatePreferredName(Person person) {
		if(person.getAdministrativeGender() == AdministrativeGender.MALE) {
			person.setPreferredName(maleNameGenerator.generateName());
		} else {
			person.setPreferredName(femaleNameGenerator.generateName());
		}
	}
	
	public void generateAddress(Person person) {
		person.addAddress(addressGenerator.generateAddress());
	}
}