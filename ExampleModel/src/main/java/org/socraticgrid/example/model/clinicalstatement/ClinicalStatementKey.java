/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

public class ClinicalStatementKey {
	
	private String clinicalStatementClass;
	private String attributeName;
	private Object attributeValue;

	public ClinicalStatementKey(ClinicalStatement statement, String attributeName, Object attributeValue) {
		this(statement.getClass().getCanonicalName(), attributeName, attributeValue);
	}
	
	public ClinicalStatementKey(String statementClass, String attributeName, Object attributeValue) {
		clinicalStatementClass = statementClass;
		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
	}

	public String getClinicalStatementClass() {
		return clinicalStatementClass;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public Object getAttributeValue() {
		return attributeValue;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attributeName == null) ? 0 : attributeName.hashCode());
		result = prime * result
				+ ((attributeValue == null) ? 0 : attributeValue.hashCode());
		result = prime
				* result
				+ ((clinicalStatementClass == null) ? 0
						: clinicalStatementClass.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClinicalStatementKey other = (ClinicalStatementKey) obj;
		if (attributeName == null) {
			if (other.attributeName != null)
				return false;
		} else if (!attributeName.equals(other.attributeName))
			return false;
		if (attributeValue == null) {
			if (other.attributeValue != null)
				return false;
		} else if (!attributeValue.equals(other.attributeValue))
			return false;
		if (clinicalStatementClass == null) {
			if (other.clinicalStatementClass != null)
				return false;
		} else if (!clinicalStatementClass.equals(other.clinicalStatementClass))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClinicalStatementKey [clinicalStatementClass="
				+ clinicalStatementClass + ", attributeName=" + attributeName
				+ ", attributeValue=" + attributeValue + "]";
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/