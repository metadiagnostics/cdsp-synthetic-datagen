/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.complex;

import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.enumeration.Type;

public class Relation<R extends Person, T extends Type> extends AbstractComplexDataType {
	
	public static final String COMPLEX_DATATYPE_NAME = "Relation";
	
	private String name;
	private T type;
	private R relation;
	
	public Relation() {
		
	}
	
	public Relation(R target, T relationshipType) {
		this.relation = target;
		this.type = relationshipType;
	}
	
	public R getRelation(){
		return relation;
	}
	
	public void setRelation(R entity) {
		this.relation = entity;
	}

	public T getType() {
		return type;
	}

	public void setType(T type) {
		this.type = type;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getCanonicalName() {
		return COMPLEX_DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/