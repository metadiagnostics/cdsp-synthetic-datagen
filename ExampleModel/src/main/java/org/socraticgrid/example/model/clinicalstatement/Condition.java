/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.datatypes.core.Quantity;
import org.socraticgrid.example.model.enumeration.ConditionStatus;
import org.socraticgrid.example.model.participation.Patient;

@SemanticResource(iri=AbstractClinicalModel.BASE_IRI + "/Condition")
@AutoAnnotate(value=true,baseIri=AbstractClinicalModel.BASE_IRI,separator='/')
public class Condition extends AbstractClinicalStatement{
	
	public static final String CLINICAL_STATEMENT_NAME = "Condition";
	
	private List<Identifier> identifiers;
	private Patient patient;
	private Encounter encounter;
	//add asserter
	private Date dateAsserted;
	private Coding conditionCode;
	private Coding conditionCategory;
	private ConditionStatus conditionStatus;
	private Coding certainty;
	private Coding severity;
	private Boolean hasAbated;
	private Integer abatementAge;
	private Date abatementDate;
	private Quantity ageAtOnset;
	private Period timeOfOnset;
	
	public List<Identifier> getIdentifiers() {
		return identifiers;
	}
	public void setIdentifiers(List<Identifier> identifiers) {
		this.identifiers = identifiers;
	}
	public Patient getPatient() {
		return patient;
	}
	public void setPatient(Patient patient) {
		this.patient = patient;
	}
	public Encounter getEncounter() {
		return encounter;
	}
	public void setEncounter(Encounter encounter) {
		this.encounter = encounter;
	}
	public Date getDateAsserted() {
		return dateAsserted;
	}
	public void setDateAsserted(Date dateAsserted) {
		this.dateAsserted = dateAsserted;
	}
	public Coding getConditionCode() {
		return conditionCode;
	}
	public void setConditionCode(Coding conditionCode) {
		this.conditionCode = conditionCode;
	}
	public Coding getConditionCategory() {
		return conditionCategory;
	}
	public void setConditionCategory(Coding conditionCategory) {
		this.conditionCategory = conditionCategory;
	}
	public ConditionStatus getConditionStatus() {
		return conditionStatus;
	}
	public void setConditionStatus(ConditionStatus conditionStatus) {
		this.conditionStatus = conditionStatus;
	}
	public Coding getCertainty() {
		return certainty;
	}
	public void setCertainty(Coding certainty) {
		this.certainty = certainty;
	}
	public Coding getSeverity() {
		return severity;
	}
	public void setSeverity(Coding severity) {
		this.severity = severity;
	}
	public boolean isHasAbated() {
		return hasAbated;
	}
	public void setHasAbated(boolean hasAbated) {
		this.hasAbated = hasAbated;
	}
	public Integer getAbatementAge() {
		return abatementAge;
	}
	public void setAbatementAge(Integer abatementAge) {
		this.abatementAge = abatementAge;
	}
	public Date getAbatementDate() {
		return abatementDate;
	}
	public void setAbatementDate(Date abatementDate) {
		this.abatementDate = abatementDate;
	}
	public Quantity getAgeAtOnset() {
		return ageAtOnset;
	}
	public void setAgeAtOnset(Quantity ageAtOnset) {
		this.ageAtOnset = ageAtOnset;
	}
	public Period getTimeOfOnset() {
		return timeOfOnset;
	}
	public void setTimeOfOnset(Period timeOfOnset) {
		this.timeOfOnset = timeOfOnset;
	}
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() +"/" + UUID.randomUUID();
	}
	@Override
	public String getCanonicalName() {
		return CLINICAL_STATEMENT_NAME;
	}
	
	public ClinicalStatementKey getIndexKey() {
		return new ClinicalStatementKey(this, "conditionCode", conditionCode);
	}
	public void indexStatement(ClinicalStatementIndexer indexer) {
		indexer.indexStatement(getIndexKey(), this);
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/