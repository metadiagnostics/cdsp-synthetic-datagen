/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.core;

import java.util.Date;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalDataType;
import org.socraticgrid.example.model.entity.Organization;
import org.socraticgrid.example.model.entity.Person;

/**
 * An generic identifier class
 * 
 * @author Claude Nanjo
 *
 */
@SemanticResource(iri=AbstractCoreDataType.BASE_IRI + "/Identifier")
@AutoAnnotate(value=true,baseIri=AbstractClinicalDataType.BASE_IRI,separator='/')
public class Identifier extends AbstractCoreDataType {
	
	public static final String DATATYPE_NAME = "Identifier";
	public static final String DEFAULT_IDENTIFIER_ROOT = "http://socraticgrid.org/identifier";
	
	private String root;
	private String extension;
	private String identifierName;
	private Date createdDate;
	private Period validityPeriod;
	private Organization issuingOrganization;
	private Person issuedBy;
	
	public Identifier(String root, String extension, String identifierName) {
		this.root = root;
		this.extension = extension;
		this.identifierName = identifierName;
	}
	
	/**
	 * Must be a valid URI that represents an identification scheme
	 * 
	 * @return
	 */
	public String getIdentifierRoot() {
		return root;
	}
	
	public void setIdentifierRoot(String root) {
		this.root = root;
	}
	
	public String getIdentifierName() {
		return identifierName;
	}
	
	public void setIdentifierName(String systemName) {
		this.identifierName = systemName;
	}
	
	public String getIdentifierExtension() {
		return extension;
	}
	
	public void setIdentierExtension(String identifier) {
		this.extension = identifier;
	}
	
	public String getFullIdentifier() {
		String identifier = root;
		if(extension != null) {
			identifier += extension;
		}
		return identifier;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Period getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(Period validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	public Organization getIssuingOrganization() {
		return issuingOrganization;
	}

	public void setIssuingOrganization(Organization issuingOrganization) {
		this.issuingOrganization = issuingOrganization;
	}

	public Person getIssuedBy() {
		return issuedBy;
	}

	public void setIssuedBy(Person issuedBy) {
		this.issuedBy = issuedBy;
	}
	
	public String toString() {
		return getFullIdentifier();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return urlEncode(root + "/" + extension);
	}

	@Override
	public String getCanonicalName() {
		return DATATYPE_NAME;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/
