/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.datatypes.core.Quantity;
import org.socraticgrid.example.model.enumeration.ConditionStatus;
import org.socraticgrid.example.model.participation.Patient;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI
		+ "/MedicationPrescription")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class MedicationPrescription extends AbstractClinicalStatement {

	public static final String CLINICAL_STATEMENT_NAME = "MedicationPrescription";
	private List<Identifier> identifiers;
	private Date dateWritten;
	private Patient patient;
	private Encounter encounter;
	
//*******	
	private Coding conditionCode;
	private Coding conditionCategory;
	private ConditionStatus conditionStatus;
	private Coding certainty;
	private Coding severity;
	private Boolean hasAbated;
	private Integer abatementAge;
	private Date abatementDate;
	private Quantity ageAtOnset;
	private Period timeOfOnset;
	
//	private Coding MedicationPrescription;

	public ClinicalStatementKey getIndexKey() {
		// return new ClinicalStatementKey(this, "medicationPrescription",
		// MedicationPrescription);
		return null;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + UUID.randomUUID();
	}

	@Override
	public String getCanonicalName() {
		return CLINICAL_STATEMENT_NAME;
	}

	public void indexStatement(ClinicalStatementIndexer indexer) {
		indexer.indexStatement(getIndexKey(), this);
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/