/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.generator.impl;

import org.socraticgrid.example.model.enumeration.MaritalStatus;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class DefaultMaritalStatusGenerator {
		
	private DiscreteProbabilityDistribution<String> distribution;

	public DefaultMaritalStatusGenerator() {
	}

	public MaritalStatus getNextMaritalStatus() {
		return MaritalStatus.valueOf(distribution.getSample().toUpperCase());
	}

	public DiscreteProbabilityDistribution<String> getDistribution() {
		return distribution;
	}

	public void setDistribution(DiscreteProbabilityDistribution<String> distribution) {
		this.distribution = distribution;
	}

}
