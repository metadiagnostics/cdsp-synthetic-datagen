/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.datatypes.complex.Contact;
import org.socraticgrid.example.model.datatypes.complex.HumanName;
import org.socraticgrid.example.model.datatypes.complex.Relation;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.datatypes.core.Quantity;
import org.socraticgrid.example.model.enumeration.AddressType;
import org.socraticgrid.example.model.enumeration.Education;
import org.socraticgrid.example.model.enumeration.Ethnicity;
import org.socraticgrid.example.model.enumeration.MaritalStatus;
import org.socraticgrid.example.model.enumeration.PersonRelation;
import org.socraticgrid.example.model.enumeration.Race;
import org.socraticgrid.example.model.enumeration.Religion;
import org.socraticgrid.exception.SocraticGridRuntimeException;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Person")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Person extends Entity {

	public static final String ENTITY_NAME = "Person";

	private List<Identifier> identifiers;
	private HumanName preferredName;
	private List<HumanName> alternateNames;
	private Map<String, List<Relation<Person, PersonRelation>>> relations = new HashMap<String, List<Relation<Person, PersonRelation>>>();
	private Map<String, List<Address>> addresses = new HashMap<String, List<Address>>();
	private List<Contact> contactInformation = new ArrayList<Contact>();
	private Date dateOfBirth;
	private Date deceasedDate;
	private boolean isDeceased;
	private Quantity ageAtDeath;
	private Ethnicity ethnicity;
	private Race race;
	private Religion religion;
	private MaritalStatus maritalStatus;
	private boolean isOrganDonor;
	private Date organDonorDate;
	private AdministrativeGender administrativeGender;
	private Education educationLevel;

	public Person() {
		identifiers = new ArrayList<Identifier>();
	}

	public List<Identifier> getIdentifiers() {
		return identifiers;
	}

	public void setIdentifiers(List<Identifier> identifiers) {
		this.identifiers = identifiers;
	}

	public void addIdentifier(Identifier identifier) {
		this.identifiers.add(identifier);
	}

	public List<Address> getAddresses(AddressType type) {
		return addresses.get(type.getLabel());
	}

	public List<Address> getAddresses(String type) {
		return addresses.get(type);
	}

	public void addAddress(Address address) {
		try {
			List<Address> addressesOfType = addresses.get(address
					.getAddressType().getLabel());
			if (addressesOfType == null) {
				addressesOfType = new ArrayList<Address>();
				addresses.put(address.getAddressType().getLabel(),
						addressesOfType);
			}
			addressesOfType.add(address);
		} catch (Exception e) {
			throw new SocraticGridRuntimeException("Address must have a type",
					e);
		}
	}

	public Person getEmergencyContact() {
		return getTopRelationship(PersonRelation.EMERGENCY);
	}

	public void setEmergencyContact(Person emergencyContact) {
		getRelationsOfType(PersonRelation.EMERGENCY).add(
				new Relation<Person, PersonRelation>(emergencyContact,
						PersonRelation.EMERGENCY));
	}

	public Person getNextOfKin() {
		return getTopRelationship(PersonRelation.NEXT_OF_KIN);
	}

	public void setNextOfKin(Person nextOfKin) {
		getRelationsOfType(PersonRelation.NEXT_OF_KIN).add(
				new Relation<Person, PersonRelation>(nextOfKin,
						PersonRelation.NEXT_OF_KIN));
	}

	public Person getTopRelationship(PersonRelation relationType) {
		List<Relation<Person, PersonRelation>> relationsOfType = getRelationsOfType(relationType);
		if (relationsOfType.size() > 0) {
			return relationsOfType.get(0).getRelation();
		} else {
			return null;
		}
	}

	public List<Relation<Person, PersonRelation>> getRelationsOfType(
			PersonRelation relationshipType) {
		List<Relation<Person, PersonRelation>> relationshipsOfType = relations
				.get(relationshipType.getLabel());
		if (relationshipsOfType == null) {
			relationshipsOfType = new ArrayList<Relation<Person, PersonRelation>>();
			relations.put(relationshipType.getLabel(), relationshipsOfType);
		}
		return relationshipsOfType;
	}

	public HumanName getPreferredName() {
		return preferredName;
	}

	public void setPreferredName(HumanName preferredName) {
		this.preferredName = preferredName;
	}

	public List<HumanName> getAlternateNames() {
		return alternateNames;
	}

	public void setAlternateNames(List<HumanName> alternateNames) {
		this.alternateNames = alternateNames;
	}

	public Map<String, List<Address>> getAddresses() {
		return addresses;
	}

	public void setAddresses(Map<String, List<Address>> addresses) {
		this.addresses = addresses;
	}

	public List<Contact> getContactInformation() {
		return contactInformation;
	}

	public void setContactInformation(List<Contact> contactInformation) {
		this.contactInformation = contactInformation;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * Method returns the age in years if defined or -1 if the date of birth is
	 * null.
	 * 
	 * @return
	 */
	public double getAgeInYears() {
		double age = -1.0;
		if (dateOfBirth != null) {
			LocalDate birthdate = new LocalDate (dateOfBirth);
			LocalDate now = new LocalDate();
			Period period = new Period(birthdate, now, PeriodType.yearMonthDay());
			age = period.getYears();
		}
		return age;
	}

	public Ethnicity getEthnicity() {
		return ethnicity;
	}

	public void setEthnicity(Ethnicity ethnicity) {
		this.ethnicity = ethnicity;
	}

	public Race getRace() {
		return race;
	}

	public void setRace(Race race) {
		this.race = race;
	}

	public Religion getReligion() {
		return religion;
	}

	public void setReligion(Religion religion) {
		this.religion = religion;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public boolean isOrganDonor() {
		return isOrganDonor;
	}

	public void setOrganDonor(boolean isOrganDonor) {
		this.isOrganDonor = isOrganDonor;
	}

	public Date organDonorDate() {
		return organDonorDate;
	}

	public void setOrganDonorDate(Date organDonorDate) {
		this.organDonorDate = organDonorDate;
	}

	public AdministrativeGender getAdministrativeGender() {
		return administrativeGender;
	}

	public void setAdministrativeGender(
			AdministrativeGender administrativeGender) {
		this.administrativeGender = administrativeGender;
	}

	public Map<String, List<Relation<Person, PersonRelation>>> getRelations() {
		return relations;
	}

	public void setRelations(
			Map<String, List<Relation<Person, PersonRelation>>> relations) {
		this.relations = relations;
	}

	public Date getDeceasedDate() {
		return deceasedDate;
	}

	public void setDeceasedDate(Date deceasedDate) {
		this.deceasedDate = deceasedDate;
	}

	public boolean isDeceased() {
		return isDeceased;
	}

	public void setDeceased(boolean isDeceased) {
		this.isDeceased = isDeceased;
	}

	public Quantity getAgeAtDeath() {
		return ageAtDeath;
	}

	public void setAgeAtDeath(Quantity ageAtDeath) {
		this.ageAtDeath = ageAtDeath;
	}

	public Education getEducationLevel() {
		return educationLevel;
	}

	public void setEducationLevel(Education educationLevel) {
		this.educationLevel = educationLevel;
	}

	public String toString() {
		return preferredName + "(" + identifiers + ")" + addresses;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/"
				+ getIdentifiers().get(0).generateDefaultInstanceIdentifier();
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/