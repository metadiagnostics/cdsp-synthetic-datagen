/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model;

import java.io.OutputStream;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationContext;
import org.socraticgrid.clinical.model.serializer.Serializer;

/**
 * Base class for all clinical datatypes including both Core and Complex Datatypes.
 * 
 * 
 * @author Claude Nanjo
 *
 */
public abstract class AbstractClinicalDataType extends AbstractClinicalThing implements ClinicalDataType {
	
	public static final String BASE_IRI = "http://socraticgrid.org/clinical/datatype";
	
	/**
	 * Visitor pattern for serialization.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ClinicalThing, C extends SerializationContext> void serialize(Serializer<T,C> serializer, DocumentFormat format, OutputStream outputStream, C serializationContext) {
		serializer.serialize((T) this, format, outputStream, serializationContext);
	}
	
	/**
	 * Base IRI for all data types
	 * @return
	 */
	public String getBaseIri() {
		return BASE_IRI;
	}
	
	/**
	 * Method returns a unique identifier for the given instance. This identifier
	 * should be the same for all equivalent instances (i.e., instances that are 
	 * equal to one another via the equals() method.
	 * 
	 * @return
	 */
	public abstract String generateDefaultInstanceIdentifier();
	
	/**
	 * Returns the canonical name for the datatype
	 * @return
	 */
	public abstract String getCanonicalName();
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/