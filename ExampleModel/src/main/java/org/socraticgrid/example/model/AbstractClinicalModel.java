/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model;

import java.io.OutputStream;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationContext;
import org.socraticgrid.clinical.model.serializer.Serializer;
import org.socraticgrid.example.model.datatypes.core.Identifier;

/**
 * Base class for all clinical models.
 * 
 * @author Claude Nanjo
 *
 */
public abstract class AbstractClinicalModel extends AbstractClinicalThing implements ClinicalModel {
	
	public static final String BASE_IRI = "http://socraticgrid.org/clinical/model";
	
	private Identifier instanceIdentifier;
	
	/**
	 * Base IRI for all data types
	 * @return
	 */
	public String getBaseIri() {
		return BASE_IRI;
	}
	
	/**
	 * Method returns instance identifier for this model instance
	 */
	public Identifier getInstanceIdentifier() {
		return instanceIdentifier;
	}
	
	/**
	 * Method sets an instance identifier for this model instance
	 */
	public void setInstanceIdentifier(Identifier instanceIdentifier) {
		this.instanceIdentifier = instanceIdentifier;
	}
	
	/**
	 * Visitor pattern for serialization. Allows a class to 'serialize'
	 * itself.
	 */
	@SuppressWarnings("unchecked")
	public <T extends ClinicalThing, C extends SerializationContext> void serialize(Serializer<T,C> serializer, DocumentFormat format, OutputStream outputStream, C serializationContext) {
		serializer.serialize((T) this, format, outputStream, serializationContext);
	}
	
	/**
	 * Method returns a unique identifier for the given instance. This identifier
	 * should be the same for all equivalent instances (i.e., instances that are 
	 * equal to one another via the equals() method.
	 * 
	 * @return
	 */
	public abstract String generateDefaultInstanceIdentifier();
	
	/**
	 * Returns the canonical name for the clinical model
	 * @return
	 */
	public abstract String getCanonicalName();
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/