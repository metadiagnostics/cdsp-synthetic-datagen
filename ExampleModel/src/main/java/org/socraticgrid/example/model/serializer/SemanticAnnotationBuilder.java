/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer;

import java.lang.reflect.Field;
import java.lang.reflect.TypeVariable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.socraticgrid.clinical.model.serializer.SkipSerialization;
import org.socraticgrid.clinical.model.serializer.generic.IncomingObjectProperty;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.RdfModelBuilder;
import org.socraticgrid.common.jena.SemanticException;
import org.socraticgrid.common.jena.SemanticPredicate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.ClinicalThing;
import org.socraticgrid.exception.SocraticGridRuntimeException;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;

/**
 * Helper utility class that returns semantic annotations associated with a
 * given class.
 * 
 * @author Claude Nanjo
 * 
 */
public class SemanticAnnotationBuilder {

	private RdfModelBuilder builder;

	public SemanticAnnotationBuilder() {
	}

	public SemanticAnnotationBuilder(RdfModelBuilder builder) {
		this.builder = builder;
	}

	public RdfModelBuilder getModelBuilder() {
		return builder;
	}

	public void setModelBuilder(RdfModelBuilder builder) {
		this.builder = builder;
	}

	public RdfModelBuilder configureNewBuilder() {
		builder = new RdfModelBuilder();
		return builder;
	}

	/**
	 * Method returns the SemanticResource IRI associated with the object
	 * argument. If no such annotation is defined or if the iri attribute is
	 * undefined, method returns null;
	 * 
	 * @param type
	 * @return
	 */
	public String getSemanticResourceIri(Object type) {
		String resourceIri = null;
		SemanticResource semanticResource = type.getClass().getAnnotation(
				SemanticResource.class);
		if (semanticResource != null) {
			resourceIri = semanticResource.iri();
		}
		return resourceIri;
	}

	/**
	 * Method returns the SemanticResource instance IRI associated with the
	 * object argument. If no such annotation is defined or if the instance iri
	 * attribute is undefined, method returns null;
	 * 
	 * @param type
	 * @return
	 */
	public String getSemanticResourceInstanceIri(Object type) {
		String iri = null;
		SemanticResource annotation = type.getClass().getAnnotation(
				SemanticResource.class);
		if (annotation != null) {
			iri = annotation.instanceIri();
		}
		return iri;
	}

	/**
	 * Method returns the AutoAnnotate base IRI associated with the type object
	 * argument. If no such annotation is defined or if the baseIri attribute is
	 * undefined, method returns null;
	 * 
	 * @param type
	 * @return
	 */
	public String getAutoAnnotateBaseIri(Object type) {
		String iri = null;
		AutoAnnotate annotation = type.getClass().getAnnotation(
				AutoAnnotate.class);
		if (annotation != null) {
			iri = annotation.baseIri();
		}
		return iri;
	}

	/**
	 * Returns the resource's SemanticResource.instanceIri(). If the resource
	 * instance IRI is null, method will return the resource's
	 * AutoAnnotate.baseIri() (which may not be null).
	 * 
	 * If neither are defined, an exception is thrown.
	 * 
	 * @param type
	 * @return
	 */
	public String getAutoAnnotateInstanceIri(Object type) {
		String instanceIri = getAutoAnnotateBaseIri(type);
		if (instanceIri == null) {
			instanceIri = getSemanticResourceInstanceIri(type);
		}
		if (instanceIri == null) {
			throw new SocraticGridRuntimeException(
					"No annotation or instance IRI defined for the resource "
							+ type.getClass().getCanonicalName());
		}
		return instanceIri;
	}

	public String getFieldPredicateIri(Object type, Field field) {
		AutoAnnotate autoAnnotate = type.getClass().getAnnotation(
				AutoAnnotate.class);
		SemanticPredicate semanticPredicate = field
				.getAnnotation(SemanticPredicate.class);
		SemanticResource semanticResource = type.getClass().getAnnotation(
				SemanticResource.class);
		String predicateAnnotation = (semanticPredicate != null) ? semanticPredicate
				.iri() : null;
		String predicateIri = null;
		if (predicateAnnotation != null) {
			predicateIri = predicateAnnotation;
		} else if (autoAnnotate != null && autoAnnotate.value()) {
			if (autoAnnotate.baseIri().trim().length() >= 0) {
				predicateIri = autoAnnotate.baseIri()
						+ autoAnnotate.separator() + field.getName();
			} else if (semanticResource != null
					&& semanticResource.iri() != null) {
				predicateIri = semanticResource.iri()
						+ autoAnnotate.separator() + field.getName();
			} else {
				throw new SemanticException(
						"Neither a resource IRI nor a base IRI has been specified for resource "
								+ type.getClass().getCanonicalName());
			}
		}
		// If AutoAnnotate is null or false, look for explicit annotation or
		// else error out.
		// If AutoAnnotate is present and true, look for explicit annotation. If
		// present, use. If absent, generate predicate from field name and
		// either baseIri, or resource Iri
		return predicateIri;
	}

	/**
	 * Method returns true if field contains the SkipSerialization annotation
	 * 
	 * @param field
	 * @return
	 */
	public static boolean skipField(Field field) {
		SkipSerialization skip = field.getAnnotation(SkipSerialization.class);
		return skip != null;
	}

	/**
	 * Retrieves fields associated with type argument.
	 * 
	 * @param type
	 *            - The type whose fields we are interested in.
	 * @param filterStaticAndTransientFields
	 *            - Flag to filter transient, static, or java-platform
	 *            attributes.
	 * @param includeAncestors
	 *            - Flag to aggregate ancestor fields
	 * 
	 * @return
	 */
	public Map<Field, String> collectSemanticPredicateIris(Object type,
			boolean filterStaticAndTransientFields, boolean includeAncestors) {
		Map<Field, String> predicateMap = new HashMap<Field, String>();
		try {
			List<Field> fieldList = new ArrayList<Field>();
			getFields(type, fieldList, includeAncestors);
			for (Field field : fieldList) {
				if (filterStaticAndTransientFields
						&& isStaticOrTransient(field)) {
					continue;
				}
				SemanticPredicate semanticPredicate = field
						.getAnnotation(SemanticPredicate.class);
				if (semanticPredicate != null) {
					predicateMap.put(field, semanticPredicate.iri());
				} else {
					predicateMap.put(field, null);
				}
			}
		} catch (Exception e) {
			throw new SocraticGridRuntimeException(
					"Error during field introspections", e);
		}
		return predicateMap;
	}

	/**
	 * Method recursively gets fields from given class and , if includeAncestors
	 * == true, from any superclass up to and excluding java.lang.Object.
	 * 
	 * @param type
	 * @param fieldList
	 */
	public static void getFields(Object type, List<Field> fieldList,
			boolean includeAncestors) {
		Field[] fields = null;
		Class<?> parent = null;
		if (type instanceof Class) {
			fields = ((Class<?>) type).getDeclaredFields();
			parent = ((Class<?>) type).getSuperclass();
		} else {
			fields = type.getClass().getDeclaredFields();
			parent = type.getClass().getSuperclass();
		}
		fieldList.addAll(Arrays.asList(fields));
		if (includeAncestors && parent != null
				&& !parent.getCanonicalName().equals("java.lang.Object")) {
			getFields(parent, fieldList, includeAncestors);
		}
	}

	/**
	 * Adds a statement to the Jena Model with the resource's
	 * SemanticResource.instanceIri() + "/" + instanceId as the subject, the
	 * class attribute's SemanticPredicate.iri() as the predicate, and the value
	 * of the attribute as the datatype property value.
	 * 
	 * Method is recursively invoked on object graphs.
	 * 
	 * <p>
	 * Fields with null values will not generate RDF statements.
	 * 
	 * @param model
	 * @param type
	 * @param instanceId
	 */
	public void addDataTypeStatements(ClinicalThing type, RdfContext context) {
		try {
//			if (type instanceof net.fortuna.ical4j.model.Calendar) {
//				System.out.println("here");
//				// doNotSerialize(Calender, null);
//			}
			String subjectIri = type.generateDefaultInstanceIdentifier();
			Model model = builder.getModel();
			String classIri = getSemanticResourceIri(type);

			if (classIri != null) {
				addClassMembershipStatement(model,
						getAutoAnnotateInstanceIri(type) + "/" + subjectIri,
						getSemanticResourceIri(type));
			}

			Map<Field, String> predicateMap = collectSemanticPredicateIris(
					type, true, true);
			Set<Field> fields = predicateMap.keySet();

			buildIncomingEdgesFromContext((ClinicalThing) type, context, subjectIri);

			for (Field field : fields) {
				if (doNotSerialize(type, field)) {
					continue;
				}

				String fieldPredicateIri = getFieldPredicateIri(type, field);
				Object fieldValue = getFieldValue(field, type);

				if (isCoreSerializableType(type, field)) {
					addDataProperty(model, getAutoAnnotateInstanceIri(type)
							+ "/" + subjectIri, fieldPredicateIri,
							fieldValue.toString());
				} else if (fieldValue instanceof List) {
					List<?> list = (List<?>) fieldValue;
//					 System.out.println("*****"+type);
					processList(type, subjectIri, fieldPredicateIri, list);
				} else if (fieldValue instanceof Map) {
					Map map = (Map) fieldValue;
					processMap(type, subjectIri, model, fieldPredicateIri, map);
				} else if (fieldValue instanceof ClinicalThing) {
					convertFieldToRdf(type, subjectIri, fieldPredicateIri,
							(ClinicalThing) fieldValue);
				} else {
					throw new SemanticException(
							"WARNING: No RDF handler found for type: " + type
									+ ", field: " + field.getName() + "("
									+ fieldValue.getClass() + ")");
				}
			}
		} catch (Exception cause) {
			throw new SemanticException("Error parsing semantic annotations",
					cause);
		}
	}

	public boolean doNotSerialize(ClinicalThing type, Field field)
			throws IllegalAccessException {
		return skipField(field) || isStaticOrTransient(field)
				|| getFieldValue(field, type) == null;
	}

	public void processMap(ClinicalThing type, String subjectIri, Model model,
			String fieldIri, Map map) {
		for (Object key : map.keySet()) {
			Object value = map.get(key);
			if (value instanceof List) {
				processList(type, subjectIri, fieldIri, (List) value);
			}
		}
	}

	public void processList(ClinicalThing type, String subjectIri,
			String fieldIri, List list) {
		if (list.size() > 0) {

			 for (ClinicalThing item : (List<ClinicalThing>) list) {
//			 System.out.println(type + "\t" + item);
			 convertFieldToRdf(type, subjectIri, fieldIri, item);
			 }
		}
	}

	public void convertFieldToRdf(ClinicalThing type, String subjectIri,
			String fieldIri, ClinicalThing fieldValue) {
		RdfContext innerContext = new RdfContext();
		// if(fieldValue==null){
		// System.out.println(type);
		// }
		// System.out.println("Canonical Name"+fieldValue.getClass().getCanonicalName()+"instance IRI"+getAutoAnnotateInstanceIri(type));
		innerContext.addIncomingProperty(new IncomingObjectProperty(
				getAutoAnnotateInstanceIri(type) + "/" + subjectIri, fieldIri,
				fieldValue.getClass().getCanonicalName()));
		addDataTypeStatements(fieldValue, innerContext);
	}

	public Resource addDataProperty(Model model, String subjectIri,
			String propertyIri, String value) {
		Property property = model.createProperty(propertyIri);
		return model.createResource(subjectIri).addProperty(property, value);
	}

	public void addClassMembershipStatement(Model model, String subjectIri,
			String classUri) {
		Resource subject = model.createResource(subjectIri);
		Resource objectClass = model.createResource(classUri);
		model.add(subject, RDF.type, objectClass);
	}

	public Object getFieldValue(Field field, Object type)
			throws IllegalAccessException {
		field.setAccessible(true);
		return field.get(type);
	}

	/**
	 * Method returns true if the field is either static or transient.
	 * 
	 * @param field
	 * @return
	 */
	public static boolean isStaticOrTransient(Field field) {
		return java.lang.reflect.Modifier.isStatic(field.getModifiers())
				|| java.lang.reflect.Modifier.isTransient(field.getModifiers())
				|| field.getType().getName()
						.contains("sun.reflect.annotation.AnnotationType");
	}

	protected String autoGenerateInstantIri(Object type,
			String instanceIdentifier) {
		return getAutoAnnotateInstanceIri(type) + "/" + instanceIdentifier;
	}

	protected void buildIncomingEdgesFromContext(ClinicalThing statementObject,
			RdfContext context, String objectIri) {
		List<IncomingObjectProperty> incomingPredicates = context
				.getIncomingPropertiesForClass(statementObject.getClass()
						.getCanonicalName());
		if (incomingPredicates != null) {
			for (IncomingObjectProperty property : incomingPredicates) {
				builder.addObjectProperty(
						property.getSubject(),
						property.getPredicate(),
						getAutoAnnotateInstanceIri(statementObject)
								+ "/"
								+ objectIri);
//								+ statementObject
//										.generateDefaultInstanceIdentifier());
			}
		}
	}

	/**
	 * Convenience method that returns true if the type can is a primitive,
	 * primitive object, String, or an object that can be serialized by invoking
	 * toString(). Primitive objects are serialized using the toString() method.
	 * 
	 * @param parent
	 * @param field
	 * @return
	 */
	protected boolean isCoreSerializableType(Object parent, Field field) {
		boolean isCoreSerializableField = false;
		try {
			Class<?> fieldType = field.getType();

			if (field.getGenericType() instanceof TypeVariable) {// Generic
																	// arguments
																	// appear as
																	// object.
																	// Not
																	// great.
				Object fieldValue = getFieldValue(field, parent);
				fieldType = fieldValue.getClass();
			}

			if (fieldType.isPrimitive()) {
				isCoreSerializableField = true;
			} else if (fieldType.equals(String.class)) {
				isCoreSerializableField = true;
			} else if (fieldType.equals(java.lang.Integer.class)) {
				isCoreSerializableField = true;
			} else if (fieldType.equals(java.lang.Boolean.class)) {
				isCoreSerializableField = true;
			} else if (fieldType.equals(java.lang.Double.class)) {
				isCoreSerializableField = true;
			} else if (fieldType.isEnum()) {
				isCoreSerializableField = true;
			} else if (fieldType.equals(java.util.Date.class)) {
				isCoreSerializableField = true;
			}
			return isCoreSerializableField;
		} catch (Exception e) {
			throw new SocraticGridRuntimeException(
					"Error retrieving field value", e);
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/