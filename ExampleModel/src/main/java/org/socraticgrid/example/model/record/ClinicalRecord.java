/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.record;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.entity.Person;

/**
 * 
 * Class representing a person's clinical record.
 * <p>
 * Notice that here we distinguish between a person
 * and a patient. Patient information may be institution-
 * specific whereas person information spans institutions.
 * A person may seek care at a number of institutions and
 * receive a patient ID in each one.
 * <p>
 * The clinical record may be a container for clinical
 * statements pertaining to one encounter in a single
 * institution or multiple encounters across any number of
 * institutions. The former represents a patient record
 * while the latter represents a personal clinical record.
 * The difference is important as a clinical record may be
 * synthesized by aggregating patient information from any
 * number of sources such as in a care coordination scenario.
 * <p>
 * In a patient record, all clinical statements pertain to
 * a single patient (with a given patient ID issued by a 
 * particular organization).
 * <p>
 * In a personal clinical record, all clinical statements
 * pertain to the evaluated person but may have clinical
 * statements associated with more than one patient ID.
 * 
 * @author Claude Nanjo
 *
 */
public class ClinicalRecord<P extends Person> {

	private Person evaluatedPerson;
	private List<ClinicalStatement> clinicalStatements;
	
	public ClinicalRecord() {
		clinicalStatements = new ArrayList<ClinicalStatement>();
	}
	
	/**
	 * Consolidates the clinical statements of an evaluated person's 
	 * patient record with his/her clinical record.
	 * 
	 */
	public void incorporatePatientRecord(PatientRecord<P> record) {
		for(ClinicalStatement statement : record.getClinicalStatements()) {
			if(statement.getPatient() == null && record.getPatient() != null) {
				statement.setPatient(record.getPatient());
			}
			clinicalStatements.add(statement);
		}
	}

	/***********************************************************
	 * Accessor Methods
	 ***********************************************************/
	
	/**
	 * Returns the evaluated person that is the focus of this patient
	 * record. An evaluated person is the subject of a number of clinical statements.
	 */
	public Person getEvaluatedPerson() {
		return evaluatedPerson;
	}
	
	/**
	 * Sets the evaluated person for this clinical record. This person
	 * MUST be the subject of all clinical statements contained in this record.
	 */
	public void setEvaluatedPerson(Person evaluatedPerson) {
		this.evaluatedPerson = evaluatedPerson;
	}
	
	/**
	 * Returns the set of clinical statements associated with this person.
	 */
	public List<ClinicalStatement> getClinicalStatements() {
		return clinicalStatements;
	}
	
	/**
	 * Sets the clinical statements associated with the person
	 */
	public void setClinicalStatements(List<ClinicalStatement> clinicalStatements) {
		this.clinicalStatements = clinicalStatements;
	}
	
	/**
	 * Adds a clinical statement to this record.
	 */
	public void addStatements(List<ClinicalStatement> statements) {
		clinicalStatements.addAll(statements);
	}
	
	
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/