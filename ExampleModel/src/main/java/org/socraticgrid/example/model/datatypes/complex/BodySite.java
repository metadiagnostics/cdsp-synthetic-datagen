/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.datatypes.complex;

import org.socraticgrid.example.model.datatypes.core.Coding;

public class BodySite {
	
	private Coding bodySiteCode;
	private Coding laterality;
	private double bodySiteArea;

	public BodySite() {
	}

	public Coding getBodySiteCode() {
		return bodySiteCode;
	}

	public void setBodySiteCode(Coding bodySiteCode) {
		this.bodySiteCode = bodySiteCode;
	}

	public Coding getLaterality() {
		return laterality;
	}

	public void setLaterality(Coding laterality) {
		this.laterality = laterality;
	}

	public double getBodySiteArea() {
		return bodySiteArea;
	}

	public void setBodySiteArea(double bodySiteArea) {
		this.bodySiteArea = bodySiteArea;
	}
	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/