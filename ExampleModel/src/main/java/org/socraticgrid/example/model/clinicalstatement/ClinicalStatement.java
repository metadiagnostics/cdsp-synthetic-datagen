/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.clinicalstatement;

import java.util.Date;
import java.util.List;

import org.socraticgrid.example.model.ClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.Comment;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.participation.Patient;

/**
 * This interface represents a clinical statement made about a patient
 * such as whether a patient was ordered a procedure, underwent a given procedure or was
 * diagnosed with a particular problem.
 * 
 * Clinical statements can be grouped using the patient's identifier
 * or the person's identifier depending on the use case.
 * 
 * @author Claude Nanjo
 *
 */
public interface ClinicalStatement extends ClinicalModel{
	public Identifier getStatementId();
	public void setStatementId(Identifier id);
	public Person getEvaluatedPerson();
	public void setEvaluatedPerson(Person evaluatedPersonId);
	public Patient getPatient();
	public void setPatient(Patient patient);
	public Coding getStatementSource();
	public void setStatementSource(Coding statementSource);
	public Date getDocumentationTime();
	public void setDocumentationTime(Date documentationTime);
	public List<Comment> getComments();
	public void setComments(List<Comment> comments);
	public void indexStatement(ClinicalStatementIndexer indexer);
	public ClinicalStatementKey getIndexKey();
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/