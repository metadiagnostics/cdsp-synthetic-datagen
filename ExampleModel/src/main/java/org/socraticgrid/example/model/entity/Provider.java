/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.entity;

import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.HumanName;
import org.socraticgrid.example.model.datatypes.core.Identifier;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Provider")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Provider extends Entity {
	

	public static final String ENTITY_NAME = "Provider";
	private Identifier identifier;
	private HumanName providerName;
	private String providerSpecialty;
	private Identifier facilityIdentifier;

	public Provider() {
		super();
	}
	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public Identifier getProviderIdentifier() {
		return identifier;
	}

	public void setIdentifier(Identifier providerIdentifier) {
		this.identifier = providerIdentifier;
	}

	public HumanName getProviderName() {
		return providerName;
	}

	public void setProviderName(HumanName humanName) {
		this.providerName = humanName;
	}

	public String getProviderSpecialty() {
		return providerSpecialty;
	}

	public void setProviderSpecialty(String providerSpecialty) {
		this.providerSpecialty = providerSpecialty;
	}

}