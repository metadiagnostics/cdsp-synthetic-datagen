/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.enumeration;

import org.socraticgrid.exception.SocraticGridRuntimeException;

public enum StateEnumeration {
	ALABAMA("Alabama", "AL"), 
	ALASKA("Alaska", "AK"), 
	AMERICAN_SAMOA("American Samoa", "AS"), 
	ARIZONA("Arizona", "AZ"), 
	ARKANSAS("Arkansas", "AR"), 
	CALIFORNIA("California", "CA"), 
	COLORADO("Colorado", "CO"), 
	CONNECTICUT("Connecticut", "CT"), 
	DELAWARE("Delaware", "DE"), 
	DC("District of Columbia", "DC"), 
	MICRONESIA("Federated States of Micronesia", "FM"), 
	FLORIDA("Florida", "FL"), 
	GEORGIA("Georgia", "GA"), 
	GUAM("Guam", "GU"), 
	HAWAII("Hawaii", "HI"), 
	IDAHO("Idaho", "ID"), 
	ILLINOIS("Illinois", "IL"), 
	INDIANA("Indiana", "IN"), 
	IOWA("Iowa", "IA"), 
	KANSAS("Kansas", "KS"), 
	KENTUCKY("Kentucky", "KY"), 
	LOUISIANA("Louisiana", "LA"), 
	MAINE("Maine", "ME"), 
	MARSHALL_ISLANDS("Marshall Islands", "MH"), 
	MARYLAND("Maryland", "MD"), 
	MASSACHUSETTS("Massachusetts", "MA"), 
	MICHIGAN("Michigan", "MI"), 
	MINNESOTA("Minnesota", "MN"), 
	MISSISSIPPI("Mississippi", "MS"), 
	MISSOURI("Missouri", "MO"), 
	MONTANA("Montana", "MT"), 
	NEBRASKA("Nebraska", "NE"), 
	NEVADA("Nevada", "NV"), 
	NEW_HAMSHIRE("New Hampshire", "NH"), 
	NEW_JERSEY("New Jersey", "NJ"), 
	NEW_MEXICO("New Mexico", "NM"), 
	NEW_YORK("New York", "NY"), 
	NORTH_CAROLINA("North Carolina", "NC"), 
	NORTH_DAKOTA("North Dakota", "ND"), 
	NORTHERN_MARIANA_ISLANDS("Northern Mariana Islands", "MP"), 
	OHIO("Ohio", "OH"), 
	OKLAHOMA("Oklahoma", "OK"), 
	OREGON("Oregon", "OR"), 
	PALAU("Palau", "PW"), 
	PENNSYLVANIA("Pennsylvania", "PA"), 
	PUERTO_RICO("Puerto Rico", "PR"), 
	RHODE_ISLAND("Rhode Island", "RI"), 
	SOUTH_CAROLINA("South Carolina", "SC"), 
	SOUTH_DAKOTA("South Dakota", "SD"), 
	TENNESSEE("Tennessee", "TN"), 
	TEXAS("Texas", "TX"), 
	UTAH("Utah", "UT"), 
	VERMONT("Vermont", "VT"), 
	VIRGIN_ISLANDS("Virgin Islands", "VI"), 
	VIRGINIA("Virginia", "VA"), 
	WASHINGTON("Washington", "WA"), 
	WEST_VIRGINIA("West Virginia", "WV"), 
	WISCONSIN("Wisconsin", "WI"), 
	WYOMING("Wyoming", "WY");
	
	private String code;
	private String label;
	
	private StateEnumeration(String label, String code) {
		this.code = code;
		this.label = label;
	}

	public String getCode() {
		return code;
	}

	public String getLabel() {
		return label;
	}
	
	public static StateEnumeration getStateEnumerationForState(String state) {
		if(state.equalsIgnoreCase("Alabama") || state.equalsIgnoreCase("AL")) {
			return StateEnumeration.ALABAMA;
		} else if(state.equalsIgnoreCase("Alaska") || state.equalsIgnoreCase("AK")) {
			return StateEnumeration.ALASKA;
		} else if(state.equalsIgnoreCase("American Samoa") || state.equalsIgnoreCase("AS")) {
			return StateEnumeration.AMERICAN_SAMOA;
		} else if(state.equalsIgnoreCase("Arizona") || state.equalsIgnoreCase("AZ")) {
			return StateEnumeration.ARIZONA;
		} else if(state.equalsIgnoreCase("Arkansas") || state.equalsIgnoreCase("AR")) {
			return StateEnumeration.ARKANSAS;
		} else if(state.equalsIgnoreCase("California") || state.equalsIgnoreCase("CA")) {
			return StateEnumeration.CALIFORNIA;
		} else if(state.equalsIgnoreCase("Colorado") || state.equalsIgnoreCase("CO")) {
			return StateEnumeration.COLORADO;
		} else if(state.equalsIgnoreCase("Connecticut") || state.equalsIgnoreCase("CT")) {
			return StateEnumeration.CONNECTICUT;
		} else if(state.equalsIgnoreCase("Delaware") || state.equalsIgnoreCase("DE")) {
			return StateEnumeration.DELAWARE;
		} else if(state.equalsIgnoreCase("District of Columbia") || state.equalsIgnoreCase("DC")) {
			return StateEnumeration.DC;
		} else if(state.equalsIgnoreCase("Federated States of Micronesia") || state.equalsIgnoreCase("FM")) {
			return StateEnumeration.MICRONESIA;
		} else if(state.equalsIgnoreCase("Florida") || state.equalsIgnoreCase("FL")) {
			return StateEnumeration.FLORIDA;
		} else if(state.equalsIgnoreCase("Georgia") || state.equalsIgnoreCase("GA")) {
			return StateEnumeration.GEORGIA;
		} else if(state.equalsIgnoreCase("Guam") || state.equalsIgnoreCase("GU")) {
			return StateEnumeration.GUAM;
		} else if(state.equalsIgnoreCase("Hawaii") || state.equalsIgnoreCase("HI")) {
			return StateEnumeration.HAWAII;
		} else if(state.equalsIgnoreCase("Idaho") || state.equalsIgnoreCase("ID")) {
			return StateEnumeration.IDAHO;
		} else if(state.equalsIgnoreCase("Illinois") || state.equalsIgnoreCase("IL")) {
			return StateEnumeration.ARKANSAS;
		} else if(state.equalsIgnoreCase("Indiana") || state.equalsIgnoreCase("IN")) {
			return StateEnumeration.ILLINOIS;
		} else if(state.equalsIgnoreCase("Iowa") || state.equalsIgnoreCase("IA")) {
			return StateEnumeration.IOWA;
		} else if(state.equalsIgnoreCase("Kansas") || state.equalsIgnoreCase("KS")) {
			return StateEnumeration.KANSAS;
		} else if(state.equalsIgnoreCase("Kentucky") || state.equalsIgnoreCase("KY")) {
			return StateEnumeration.KENTUCKY;
		} else if(state.equalsIgnoreCase("Louisiana") || state.equalsIgnoreCase("LA")) {
			return StateEnumeration.LOUISIANA;
		} else if(state.equalsIgnoreCase("Maine") || state.equalsIgnoreCase("ME")) {
			return StateEnumeration.MAINE;
		} else if(state.equalsIgnoreCase("Marshall Islands") || state.equalsIgnoreCase("MH")) {
			return StateEnumeration.MARSHALL_ISLANDS;
		} else if(state.equalsIgnoreCase("Maryland") || state.equalsIgnoreCase("MD")) {
			return StateEnumeration.MARYLAND;
		} else if(state.equalsIgnoreCase("Massachusetts") || state.equalsIgnoreCase("MA")) {
			return StateEnumeration.MASSACHUSETTS;
		} else if(state.equalsIgnoreCase("Michigan") || state.equalsIgnoreCase("MI")) {
			return StateEnumeration.MICHIGAN;
		} else if(state.equalsIgnoreCase("Minnesota") || state.equalsIgnoreCase("MN")) {
			return StateEnumeration.MINNESOTA;
		} else if(state.equalsIgnoreCase("Mississippi") || state.equalsIgnoreCase("MS")) {
			return StateEnumeration.MISSISSIPPI;
		} else if(state.equalsIgnoreCase("MISSOURI") || state.equalsIgnoreCase("MO")) {
			return StateEnumeration.MISSOURI;
		} else if(state.equalsIgnoreCase("MONTANA") || state.equalsIgnoreCase("MT")) {
			return StateEnumeration.MONTANA;
		} else if(state.equalsIgnoreCase("Nebraska") || state.equalsIgnoreCase("NE")) {
			return StateEnumeration.NEBRASKA;
		} else if(state.equalsIgnoreCase("Nevada") || state.equalsIgnoreCase("NV")) {
			return StateEnumeration.NEVADA;
		} else if(state.equalsIgnoreCase("New Hampshire") || state.equalsIgnoreCase("NH")) {
			return StateEnumeration.NEW_HAMSHIRE;
		} else if(state.equalsIgnoreCase("New Jersey") || state.equalsIgnoreCase("NJ")) {
			return StateEnumeration.NEW_JERSEY;
		} else if(state.equalsIgnoreCase("New Mexico") || state.equalsIgnoreCase("NM")) {
			return StateEnumeration.NEW_MEXICO;
		} else if(state.equalsIgnoreCase("New York") || state.equalsIgnoreCase("NY")) {
			return StateEnumeration.NEW_YORK;
		} else if(state.equalsIgnoreCase("North Carolina") || state.equalsIgnoreCase("NC")) {
			return StateEnumeration.NORTH_CAROLINA;
		} else if(state.equalsIgnoreCase("North Dakota") || state.equalsIgnoreCase("ND")) {
			return StateEnumeration.NORTH_DAKOTA;
		} else if(state.equalsIgnoreCase("Northern Mariana Islands") || state.equalsIgnoreCase("MP")) {
			return StateEnumeration.NORTHERN_MARIANA_ISLANDS;
		} else if(state.equalsIgnoreCase("Ohio") || state.equalsIgnoreCase("OH")) {
			return StateEnumeration.OHIO;
		} else if(state.equalsIgnoreCase("Oklahoma") || state.equalsIgnoreCase("OK")) {
			return StateEnumeration.OKLAHOMA;
		} else if(state.equalsIgnoreCase("Oregon") || state.equalsIgnoreCase("OR")) {
			return StateEnumeration.OREGON;
		} else if(state.equalsIgnoreCase("Palau") || state.equalsIgnoreCase("PW")) {
			return StateEnumeration.PALAU;
		} else if(state.equalsIgnoreCase("Pennsylvania") || state.equalsIgnoreCase("PA")) {
			return StateEnumeration.PENNSYLVANIA;
		} else if(state.equalsIgnoreCase("Puerto Rico") || state.equalsIgnoreCase("PR")) {
			return StateEnumeration.PUERTO_RICO;
		} else if(state.equalsIgnoreCase("Rhode Island") || state.equalsIgnoreCase("RI")) {
			return StateEnumeration.RHODE_ISLAND;
		} else if(state.equalsIgnoreCase("South Carolina") || state.equalsIgnoreCase("SC")) {
			return StateEnumeration.SOUTH_CAROLINA;
		} else if(state.equalsIgnoreCase("South Dakota") || state.equalsIgnoreCase("SD")) {
			return StateEnumeration.SOUTH_DAKOTA;
		} else if(state.equalsIgnoreCase("Tennessee") || state.equalsIgnoreCase("TN")) {
			return StateEnumeration.TENNESSEE;
		} else if(state.equalsIgnoreCase("Texas") || state.equalsIgnoreCase("TX")) {
			return StateEnumeration.TEXAS;
		} else if(state.equalsIgnoreCase("Utah") || state.equalsIgnoreCase("UT")) {
			return StateEnumeration.UTAH;
		} else if(state.equalsIgnoreCase("Vermont") || state.equalsIgnoreCase("VT")) {
			return StateEnumeration.VERMONT;
		} else if(state.equalsIgnoreCase("Virgin Islands") || state.equalsIgnoreCase("VI")) {
			return StateEnumeration.VIRGIN_ISLANDS;
		} else if(state.equalsIgnoreCase("Washington") || state.equalsIgnoreCase("WA")) {
			return StateEnumeration.WASHINGTON;
		} else if(state.equalsIgnoreCase("West Virginia") || state.equalsIgnoreCase("WV")) {
			return StateEnumeration.WEST_VIRGINIA;
		} else if(state.equalsIgnoreCase("Virginia") || state.equalsIgnoreCase("VA")) {
			return StateEnumeration.VIRGINIA;
		} else if(state.equalsIgnoreCase("Wisconsin") || state.equalsIgnoreCase("WI")) {
			return StateEnumeration.WISCONSIN;
		} else if (state.equalsIgnoreCase("Wyoming") || state.equalsIgnoreCase("WY")) {
			return StateEnumeration.WYOMING;
		} else {
			throw new SocraticGridRuntimeException("Invalid State: " + state);
		}
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/