/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.serializer.rdf;

import java.io.OutputStream;
import java.util.UUID;

import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationException;
import org.socraticgrid.clinical.model.serializer.generic.BaseGenericRdfSerializer;
import org.socraticgrid.clinical.model.serializer.generic.IncomingObjectProperty;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.common.jena.RdfModelBuilder;
import org.socraticgrid.example.model.participation.Patient;


public class GenericRdfPatientSerializer extends BaseGenericRdfSerializer<Patient> {
	
	public static final String PATIENT_CONCEPT_IRI = "patient";
	public static final String PREDICATE_PATIENT_IDENTIFIER = BASE_ONTOLOGY_IRI + PATIENT_CONCEPT_IRI + "/" + "identifier";
	public static final String PREDICATE_PATIENT_EVALUATED_PERSON = BASE_ONTOLOGY_IRI + PATIENT_CONCEPT_IRI + "/" + "evaluatedPerson";
	
	public GenericRdfPatientSerializer(RdfModelBuilder model) {}

	public void serialize(Patient patient, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		try {
			if(format == DocumentFormat.GENERIC_RDF) {
				buildRdfGraph(patient, format, outputStream, context);
			} else {
				throw new SerializationException("Unknown output format: " + format);
			}
			
		} catch(Exception cause) {
			throw new SerializationException("Error parsing PatientRecord", cause);
		}
		
	}
	
	protected String buildInstanceIri(String instanceId) {
		return BASE_INSTANCE_IRI + PATIENT_CONCEPT_IRI + "/" + instanceId;
	}
	
	protected String buildInstanceIdentifier(Patient patient) {
		return UUID.randomUUID().toString();
	}
	
	protected void addOutgoingEdgesToContext(Patient patient, RdfContext context, String instanceId) {
		context.addIncomingProperty(new IncomingObjectProperty(buildInstanceIri(instanceId), PREDICATE_PATIENT_EVALUATED_PERSON, patient.getEvaluatedPerson().getClass().getCanonicalName()));
	}
	
	protected void buildOutgoingEdges(Patient patient, String instanceIdentifier, RdfContext context) {
		buildDatatypeStatement(getModel(), buildInstanceIri(instanceIdentifier), PREDICATE_PATIENT_IDENTIFIER, patient.getIdentifier());
		buildIncomingEdgesFromContext(patient, context, instanceIdentifier);
	}

	@Override
	protected void buildRdfGraph(Patient patient, DocumentFormat format, OutputStream outputStream, RdfContext context) {
//		String instanceId = buildInstanceIdentifier(patient);
//		buildOutgoingEdges(patient, instanceId, context);
//		buildIncomingEdgesFromContext(patient, context, instanceId);
//		RdfContext newContext = new RdfContext();
//		addOutgoingEdgesToContext(patient, newContext, instanceId);
//		patient.getEvaluatedPerson().serialize(new GenericRdfPersonSerializer(getModel()), format, outputStream, newContext);
//		SemanticAnnotationBuilder builder = new SemanticAnnotationBuilder(getModel());
//		builder.addDataTypeStatements(patient, context);
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/