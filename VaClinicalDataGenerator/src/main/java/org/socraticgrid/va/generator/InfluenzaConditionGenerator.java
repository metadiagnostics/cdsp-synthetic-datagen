/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.va.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.socraticgrid.example.model.clinicalstatement.ClinicalStatement;
import org.socraticgrid.example.model.clinicalstatement.ClinicalStatementKey;
import org.socraticgrid.example.model.clinicalstatement.Condition;
import org.socraticgrid.example.model.clinicalstatement.ObservationResult;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Period;
import org.socraticgrid.example.model.generator.impl.BaseClinicalStatementGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.UniformDistribution;

public class InfluenzaConditionGenerator  extends BaseClinicalStatementGenerator {
	
	public static final double INFLUENZA_PREVALENCE = 0.13;
	private UniformDistribution<Double> uniformDistribution = new UniformDistribution<Double>();

	public InfluenzaConditionGenerator() {}
	
	public List<ClinicalStatement> generate(Map<String,Object> configurationParameters) {
		List<ClinicalStatement> statements = new ArrayList<ClinicalStatement>();
		if(uniformDistribution.isWithinProbability(INFLUENZA_PREVALENCE)) {
			Condition condition = getCondition();
			statements.add(condition);
			statements.addAll(getRelatedInfluenzaObservations());
			condition.indexStatement(getPatientRecord(configurationParameters).getIndex());
		} else {
			statements.addAll(getBaselineObservations());
		}
		//System.out.println(statements);
		return statements;
	}

	public Condition getCondition() {
		Condition condition = new Condition();
		Coding coding = new Coding();
		coding.setCode("6142004");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Influenza (disorder)");
		coding.setCodeSystemDisplayName("SNOMED CT");
		Period period = new Period();
		TimeCalculation timeCalculation = new TimeCalculation();
		period.setStart(timeCalculation.getTimeStamp());
		period.setEnd(timeCalculation.getCurrentTimeStamp());
		condition.setTimeOfOnset(period);
		condition.setConditionCode(coding);
		
		return condition;
	}
	
	
	public List<ObservationResult<Boolean>> getRelatedInfluenzaObservations() {
		List<ObservationResult<Boolean>> relatedObservations = new ArrayList<ObservationResult<Boolean>>();
		
		if(uniformDistribution.isWithinProbability(0.65)) {
			relatedObservations.add(generateObservation(new Coding("267101005","Runny nose","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.45)) {
			relatedObservations.add(generateObservation(new Coding("386661006","Fever","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.30)) {
			relatedObservations.add(generateObservation(new Coding("49727002","Cough","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.20)) {
			relatedObservations.add(generateObservation(new Coding("162397003","Sore throat","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.15)) {
			relatedObservations.add(generateObservation(new Coding("82991003","Body aches","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.30)) {
			relatedObservations.add(generateObservation(new Coding("51197009","Stomach cramps","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.42)) {
			relatedObservations.add(generateObservation(new Coding("62315008","Diarrhea","http://snomed.info/sct","SNOMED CT")));
		}
		return relatedObservations;
	}
	
	public List<ObservationResult<Boolean>> getBaselineObservations() {
		List<ObservationResult<Boolean>> relatedObservations = new ArrayList<ObservationResult<Boolean>>();
		
		if(uniformDistribution.isWithinProbability(0.16)) {
			relatedObservations.add(generateObservation(new Coding("267101005","Runny nose","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.05)) {
			relatedObservations.add(generateObservation(new Coding("386661006","Fever","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.08)) {
			relatedObservations.add(generateObservation(new Coding("49727002","Cough","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.01)) {
			relatedObservations.add(generateObservation(new Coding("162397003","Sore throat","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.09)) {
			relatedObservations.add(generateObservation(new Coding("82991003","Body aches","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.06)) {
			relatedObservations.add(generateObservation(new Coding("51197009","Stomach cramps","http://snomed.info/sct","SNOMED CT")));
		}
		if(uniformDistribution.isWithinProbability(0.03)) {
			relatedObservations.add(generateObservation(new Coding("62315008","Diarrhea","http://snomed.info/sct","SNOMED CT")));
		}
		return relatedObservations;
	}
	
	public ObservationResult<Boolean> generateObservation(Coding code) {
		ObservationResult<Boolean> observation = new ObservationResult<Boolean>();
		
		observation.setObservationFocus(code);
		observation.setObservationValue(new Boolean(true));
		
		Period periodObservation = new Period();
		TimeCalculation ObservationtimeCalculation = new TimeCalculation();
		periodObservation.setStart(ObservationtimeCalculation.getTimeStamp());
		periodObservation.setEnd(ObservationtimeCalculation
				.getCurrentTimeStamp());
		observation.setObservationTime(periodObservation);
		
		return observation;
	}

	public static ClinicalStatementKey getInfluenzaKey() {
		Coding coding = new Coding();
		coding.setCode("6142004");
		coding.setCodeSystem("http://snomed.info/sct");
		coding.setCodeDisplayName("Influenza (disorder)");
		return new ClinicalStatementKey(Condition.class.getCanonicalName(),
				"conditionCode", coding);
	}

}
