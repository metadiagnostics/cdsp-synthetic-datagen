/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.va.generator;

import java.io.File;

import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.impl.DefaultPersonGenerator;
import org.socraticgrid.example.runner.DefaultRunner;

public class Main {

	public static void main(String[] args) {
		if(args.length != 3) {
			usage();
		} else {
			int count = Integer.parseInt(args[0]);
			String dirPath = args[1];
			String baseFileName = args[2];
			File dir = new File(dirPath);
			if(!dir.isDirectory()) {
				usage();
			}
			
			DefaultRunner<Person> runner = DefaultRunner.getDefaultRunner(new DefaultPersonGenerator());
			runner.getPatientRecordGenerator().registerClinicalStatementGenerator(new InfluenzaConditionGenerator());
			runner.runGenerator(count, dirPath, baseFileName);
		}
	}
	
	public static void usage() {
		System.out.println("Main requires three parameters: an integer count and an output dir path and a base file name without an extension");
		System.out.println("\"Main 5 /users/me/temp/work output\" will generate five patient records and output results in /users/me/temp/work. Each file will have an index appended (e.g., output1.xml)");
	}

}
