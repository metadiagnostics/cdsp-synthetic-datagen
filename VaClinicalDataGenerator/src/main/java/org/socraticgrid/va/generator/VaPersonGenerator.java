/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.va.generator;

import org.socraticgrid.clinical.model.generator.impl.DiscreteGenderDistributionGenerator;
import org.socraticgrid.clinical.model.generator.impl.NormalAgeGenerator;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.impl.DefaultPersonGenerator;


public class VaPersonGenerator extends DefaultPersonGenerator {
	
	private NormalAgeGenerator normalAgeGenerator;
	private DiscreteGenderDistributionGenerator genderDistribution;

	public VaPersonGenerator() {
		super();
		normalAgeGenerator = new NormalAgeGenerator(59,16);
		genderDistribution = new DiscreteGenderDistributionGenerator(0.90);
	}
	
	@Override
	public Person generatePerson() {
		Person person = super.generatePerson();
		generateAge(person);
		generateAdministrativeGender(person);
		return person;
	}
	
	//Let’s use a mean of 59 with a standard deviation of 16 *
	public void generateAge(Person person) {
		person.setDateOfBirth(normalAgeGenerator.generateDateOfBirth());
	}
	
	@Override
	//Also, let’s make the gender balance 90% male 10% female *
	public void generateAdministrativeGender(Person person) {
		person.setAdministrativeGender(genderDistribution.getNextGender());
	}

}
