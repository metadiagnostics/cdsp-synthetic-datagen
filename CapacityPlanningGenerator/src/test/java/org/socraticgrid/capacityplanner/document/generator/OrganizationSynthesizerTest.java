/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacityplanner.document.generator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.capacity.model.entity.Building;
import org.socraticgrid.capacity.model.entity.Facility;
import org.socraticgrid.capacity.model.entity.Floor;
import org.socraticgrid.capacity.model.entity.Organization;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.capacity.model.serializer.rdf.GenericRdfOrganizationSerializer;
import org.socraticgrid.clinical.document.DocumentFormat;



public class OrganizationSynthesizerTest {

	private GenericRdfOrganizationSerializer serializer;
	private Organization organization;

	@Before
	public void setUp() throws Exception {
		serializer = new GenericRdfOrganizationSerializer();
		organization = buildOrganization();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testBasicSerialization() {
		serializer.serialize(organization, DocumentFormat.GENERIC_RDF,
				System.out);
	}

	private Organization buildOrganization() {
		organization = new Organization();
		organization.addFacilities(buildFacility());
		return organization;
	}

	private Facility buildFacility() {
		Facility facility = new Facility();
		facility.setFacilityName("My facility");
		facility.addBuildings(buildBuilding());
		return facility;
	}

	private Building buildBuilding() {
		Building building = new Building();
		building.addFloor(buildFloor());
		return building;
	}

	private Floor buildFloor() {
		Floor floor = new Floor();
		floor.setFloorName("5W");
		floor.addRoom(buildRoom());
		return floor;
	}

	private Room buildRoom() {
		Room room = new Room();
		room.setRoomNumber("504W");
		return room;
	}

}