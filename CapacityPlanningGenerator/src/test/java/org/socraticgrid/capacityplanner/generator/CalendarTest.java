/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacityplanner.generator;

import static org.junit.Assert.assertTrue;

import java.net.SocketException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import net.fortuna.ical4j.filter.Filter;
import net.fortuna.ical4j.filter.PeriodRule;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Period;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.example.model.participation.Practitioner;

public class CalendarTest {
	// private Calender calendar;

	@Before
	public void setUp() throws Exception {
		// cal = new Calender();

		// tz = calendar.setTimeZone("America/Los_Angeles");
		// eventName = "Calendar";
	}

	public Practitioner generatePractitioner() {
		Practitioner practitioner = new Practitioner();
		Identifier instanceIdentifier = null;
		practitioner.setInstanceIdentifier(instanceIdentifier);
		Identifier practitionerId = null;
		practitioner.setPractitionerId(practitionerId);
		return practitioner;
	}

	@Test
	public void checkFutureDate() {
		long offset = Timestamp.valueOf("2014-08-01 00:00:00").getTime();
		Date startDate = new Date(offset);
		for (int i = 0; i < 200; i++) {
			TimeCalculation myTimeCal = new TimeCalculation();
			java.util.Calendar futureTime = myTimeCal.getTimeStampInFuture();
			assertTrue("Check if the start time is correct. ",
					startDate.getTime() <= futureTime.getTime().getTime());
		}
	}

	@Test
	public void checkFutureworkingHours() {
		for (int i = 0; i < 200; i++) {
			TimeCalculation myTimeCal = new TimeCalculation();
			java.util.Calendar futureTime = myTimeCal.getTimeStampInFuture();
			assertTrue("Check if the start time is correct. ", futureTime
					.getTime().getHours() >= 8
					&& futureTime.getTime().getHours() < 17);
		}
	}

	@Test
	public void checkFutureworkingDays() {
		for (int i = 0; i < 200; i++) {
			TimeCalculation myTimeCal = new TimeCalculation();
			java.util.Calendar futureTime = myTimeCal.getTimeStampInFuture();
			assertTrue("Check if the start time is correct. ", futureTime
					.getTime().getDay() >= 1
					&& futureTime.getTime().getDay() <= 5);
		}
	}

	public VEvent createEvent(String eventName, java.util.Calendar startDate,
			java.util.Calendar endDate) {
		eventName = "Progress Meeting";
		DateTime start = new DateTime(startDate.getTime());
		DateTime end = new DateTime(endDate.getTime());
		VEvent meeting = new VEvent(start, end, eventName);
		return meeting;
	}

	@Test
	public void checkCalenderAvailability() throws SocketException {

		// Create a TimeZone
		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance()
				.createRegistry();
		TimeZone timezone = registry.getTimeZone("America/Mexico_City");
		VTimeZone tz = timezone.getVTimeZone();

		java.util.Calendar startDate1 = new GregorianCalendar();
		startDate1.setTimeZone(timezone);
		startDate1.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		startDate1.set(java.util.Calendar.DAY_OF_MONTH, 7);
		startDate1.set(java.util.Calendar.YEAR, 2014);
		startDate1.set(java.util.Calendar.HOUR_OF_DAY, 8);
		startDate1.set(java.util.Calendar.MINUTE, 0);
		startDate1.set(java.util.Calendar.SECOND, 0);

		java.util.Calendar endDate1 = new GregorianCalendar();
		endDate1.setTimeZone(timezone);
		endDate1.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		endDate1.set(java.util.Calendar.DAY_OF_MONTH, 7);
		endDate1.set(java.util.Calendar.YEAR, 2014);
		endDate1.set(java.util.Calendar.HOUR_OF_DAY, 8);
		endDate1.set(java.util.Calendar.MINUTE, 45);
		endDate1.set(java.util.Calendar.SECOND, 0);

		java.util.Calendar startDate2 = new GregorianCalendar();
		startDate2.setTimeZone(timezone);
		startDate2.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		startDate2.set(java.util.Calendar.DAY_OF_MONTH, 7);
		startDate2.set(java.util.Calendar.YEAR, 2014);
		startDate2.set(java.util.Calendar.HOUR_OF_DAY, 8);
		startDate2.set(java.util.Calendar.MINUTE, 0);
		startDate2.set(java.util.Calendar.SECOND, 0);

		java.util.Calendar endDate2 = new GregorianCalendar();
		endDate2.setTimeZone(timezone);
		endDate2.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		endDate2.set(java.util.Calendar.DAY_OF_MONTH, 7);
		endDate2.set(java.util.Calendar.YEAR, 2014);
		endDate2.set(java.util.Calendar.HOUR_OF_DAY, 8);
		endDate2.set(java.util.Calendar.MINUTE, 30);
		endDate2.set(java.util.Calendar.SECOND, 0);

		// Create the event
		VEvent meeting1 = createEvent("Event1", startDate1, endDate1);
		VEvent meeting2 = createEvent("Event1", startDate2, endDate2);

		// Create a calendar
		net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();

		// Add the event and print0
		icsCalendar.getComponents().add(meeting1);
		icsCalendar.getComponents().add(meeting2);

		java.util.Calendar today = java.util.Calendar.getInstance();
		today.set(java.util.Calendar.HOUR_OF_DAY, 8);
		today.clear(java.util.Calendar.MINUTE);
		today.clear(java.util.Calendar.SECOND);

		Period period = new Period(new DateTime(startDate1.getTime()),
				new DateTime(endDate1.getTime()));

		Filter filter = new Filter(new PeriodRule(period));

		Collection<?> eventsToday = filter.filter(icsCalendar
				.getComponents(Component.VEVENT));
		assertTrue("Checking the size", eventsToday.size() >= 1);
		// if (eventsToday.size() == 0) {
		// icsCalendar.getComponents().add(meeting2);
		// System.out.println(icsCalendar);
		// }

		for (Object o : eventsToday) {
			System.out.println(o);
			System.out.println("---");
		}

	}

	@Test
	public void checkAvailabilityTest() {
		Calendar cal = new Calendar();
		// Create a TimeZone
		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance()
				.createRegistry();
		TimeZone timezone = registry.getTimeZone("America/Mexico_City");
		VTimeZone tz = timezone.getVTimeZone();

		java.util.Calendar startDate1 = new GregorianCalendar();
		startDate1.setTimeZone(timezone);
		startDate1.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		startDate1.set(java.util.Calendar.DAY_OF_MONTH, 7);
		startDate1.set(java.util.Calendar.YEAR, 2014);
		startDate1.set(java.util.Calendar.HOUR_OF_DAY, 8);
		startDate1.set(java.util.Calendar.MINUTE, 0);
		startDate1.set(java.util.Calendar.SECOND, 0);

		java.util.Calendar endDate1 = new GregorianCalendar();
		endDate1.setTimeZone(timezone);
		endDate1.set(java.util.Calendar.MONTH, java.util.Calendar.JULY);
		endDate1.set(java.util.Calendar.DAY_OF_MONTH, 7);
		endDate1.set(java.util.Calendar.YEAR, 2014);
		endDate1.set(java.util.Calendar.HOUR_OF_DAY, 8);
		endDate1.set(java.util.Calendar.MINUTE, 45);
		endDate1.set(java.util.Calendar.SECOND, 0);
		// Create the event
		VEvent meeting1 = createEvent("Event1", startDate1, endDate1);

		// Create a calendar
		net.fortuna.ical4j.model.Calendar icsCalendar = new net.fortuna.ical4j.model.Calendar();

		// Add the event and print0
		icsCalendar.getComponents().add(meeting1);

		assertTrue("Time Slot Availability Test: ", cal.checkAvailability(
				startDate1.getTime().getYear(), java.util.Calendar.JULY,
				startDate1.getTime().getDay(), startDate1.getTime().getHours(),
				startDate1.getTime().getMinutes(), 0, 30));
	}

}
