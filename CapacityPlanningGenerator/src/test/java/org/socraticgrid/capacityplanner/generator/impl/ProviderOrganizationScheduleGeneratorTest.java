/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacityplanner.generator.impl;

import java.util.ArrayList;
import java.util.HashMap;

import net.fortuna.ical4j.model.component.VTimeZone;

import org.socraticgrid.capacity.model.entity.Building;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.capacity.model.entity.Facility;
import org.socraticgrid.capacity.model.entity.Floor;
import org.socraticgrid.capacity.model.entity.Organization;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.example.model.entity.Provider;

public class ProviderOrganizationScheduleGeneratorTest {
	public final String DEPARTMENTS_METADATA_FILE = "generatormetadata/Departments.txt";
	private HashMap<String, String> departmentClinicMaps;
	private Organization organization;
	private Calendar roomCalendar;
	private Calendar providerCalendar;
	private VTimeZone tz;
	private String eventName;

	public static int getRandomElement(int numberOfOrganizations) {
		return (int) Math.floor(Math.random() * (numberOfOrganizations - 0));
	}

	public Clinic generateClinic(String clinicName, Room room,
			Provider practioner) {
		Clinic clinic = new Clinic();
		clinic.setClinicName(clinicName);
//		clinic.addRoom(room);
//		clinic.addPractitioner(practioner);
		return clinic;
	}

	public void setUpProviderCalender() {

	}

	public  Room getRandomRoom(ArrayList<Organization> OrganizationList) {
		Organization organization = OrganizationList.get(0);

		Facility facility = organization.getFacilities().get(0);
		Building building = facility.getBuilding().get(0);
		Floor floor = building.getFloors().get(0);
		return floor.getRooms().get(0);

	}

//	@Test
//	public void SetupRooms() throws IOException, ParseException {
//		providerCalendar = new Calender();
//		tz = providerCalendar.setTimeZone("America/Los_Angeles");
//		eventName = "Calendar";
//		OrganizationFactory organizaitons = new OrganizationFactory();
//		ArrayList<Organization> OrganizationList = organizaitons
//				.getGeneratedOrganization();
//		setUpProviderCalender();
//		ProviderScheduleGenerator providerSchedule = new ProviderScheduleGenerator(
//				providerCalendar, tz, eventName);
//
//		DepartmentFactory department = new DepartmentFactory();
//		Organization organization = new Organization();
//
//		for (int i = 0; i <= 4; i++) {
//			Room room = getRandomRoom(OrganizationList);
//			Calender roomCalendar = room.getCalendar();
//			System.out.println(room+"\t"+roomCalendar);
//
//			setUpProviderCalender();
//
//			ArrayList<Appointment> providerAppointments = providerSchedule
//					.generateProviderSchedules();
//			Provider practitioner = providerSchedule.getProvider();
//			String providerSpecialty = providerSchedule.getProviderSpecialty();
//			practitioner.setProviderSpecialty(providerSpecialty);
//			practitioner.addAllAppointments(providerAppointments);
//			// ******************
//
//			Clinic clinic = new Clinic();
//			clinic = generateClinic(
//					department.getDepartmentClinicMaps().get(
//							providerSpecialty.toLowerCase()), room,
//					practitioner);
//
////			organization.addClinic(clinic);
//
//			if (roomCalendar.getComponents().size() == 0) {
//				roomCalendar.bookAllAppointments(roomCalendar, providerAppointments);
//				room.addAllAppointments(providerAppointments);
//			} else {
//				Calender.checkCommonAvailabilityCalender(roomCalendar,
//						providerSchedule.getAvailableCalender());
//
//			}
//
//		}
//	}

//	@Test
//	public void ComputeRequiredRooms() {
//
//	}

}
