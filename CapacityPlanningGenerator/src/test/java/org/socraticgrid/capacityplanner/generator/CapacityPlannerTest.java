/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacityplanner.generator;

import static org.junit.Assert.fail;

import java.io.FileOutputStream;
import java.io.IOException;

import net.fortuna.ical4j.data.CalendarOutputter;
import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VTimeZone;

import org.junit.Before;
import org.junit.Test;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.generator.capacityplanner.CapacityPlanner;


public class CapacityPlannerTest {

	CapacityPlanner capacityPlanner;
	Calendar calendar;
	VTimeZone tz;
	String eventName;

	@Before
	public void setUp() throws Exception {
		calendar = new Calendar();
		tz = calendar.setTimeZone("America/Los_Angeles");
		eventName = "Calendar";
	}

	//@Test
	public void testTimeAllocation() throws IOException, ValidationException {
		capacityPlanner = new CapacityPlanner("", 6, 0, 4, 0, 5, 18, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
		capacityPlanner.practionerTimeAllocation();
		capacityPlanner = new CapacityPlanner("", 3, 2, 1, 2, 5, 18, calendar);
		capacityPlanner
				.generateNextPractionerTimeSlot(0.375, 0.25, 0.125, 0.25);
		capacityPlanner.practionerTimeAllocation();
		capacityPlanner = new CapacityPlanner("", 3, 4, 3, 0, 10, 18, calendar);
		capacityPlanner
				.generateNextPractionerTimeSlot(0.375, 0.25, 0.125, 0.25);
		capacityPlanner.practionerTimeAllocation();
		System.out.println(calendar.getCalender());

		FileOutputStream fout = new FileOutputStream("mycalendar.ics");

		CalendarOutputter outputter = new CalendarOutputter();
		outputter.output(calendar.getCalender(), fout);
	}

	//@Test
	public void testPsychologiestPractionerTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 6, 0, 4, 0, 5, 18, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testBehavioralHealthPractionerTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 3, 2, 1, 2, 5, 18, calendar);
		capacityPlanner
				.generateNextPractionerTimeSlot(0.375, 0.25, 0.125, 0.25);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testPTSDSupportTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 3, 4, 3, 0, 10, 18, calendar);
		capacityPlanner
				.generateNextPractionerTimeSlot(0.375, 0.25, 0.125, 0.25);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testPsychiatryTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 6, 0, 4, 0, 2, 16, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testPrimaryCareTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 12, 4, 0, 0, 10, 16, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.75, 0.25, 0, 0);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testPainClinicTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 12, 4, 0, 0, 5, 18, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.75, 0.25, 0, 0);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}

	//@Test
	public void testInternalMedicineTimeAllocation() {
		capacityPlanner = new CapacityPlanner("", 6, 2, 3, 0, 8, 16, calendar);
		capacityPlanner.generateNextPractionerTimeSlot(0.55, 0.18, 0.27, 0);
		capacityPlanner.practionerTimeAllocation();
		fail("Not yet implemented");
	}
	
	
}
