/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class ProviderToClinicAssingmentFactory {

	public final String ASSIGNMENT_METADATA_FILE = "factorymetadata/AssignmentProviderClinic.txt";
	private List<String> providerToClinicAssignments;

	public ProviderToClinicAssingmentFactory() throws IOException {
		this.setProviderToClinicAssingments(new ArrayList<String>());
		Setup();
	}

	
	private void Setup() throws IOException {
		TextReader reader = new TextReader();

		InputStream assignmentStream = ReaderUtilities
				.loadResourceFromStream(ASSIGNMENT_METADATA_FILE);
		setProviderToClinicAssingments((reader.readFileToList(assignmentStream)));

	}


	public List<String> getProviderToClinicAssignments() {
		return providerToClinicAssignments;
	}


	public void setProviderToClinicAssingments(
			List<String> providerToClinicAssingments) {
		this.providerToClinicAssignments = providerToClinicAssingments;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/