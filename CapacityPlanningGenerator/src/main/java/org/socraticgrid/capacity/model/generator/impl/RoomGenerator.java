/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.impl;

import java.util.Random;

import org.socraticgrid.capacity.model.entity.Building;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.entity.Floor;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.example.model.enumeration.RoomType;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

/**
 * 
 * @author claude
 * 
 */
public class RoomGenerator extends Room{

	private DiscreteProbabilityDistribution<String> streetDistribution;
	private DiscreteProbabilityDistribution<String> cityDistribution;
	private DiscreteProbabilityDistribution<String> stateDistribution;
	private DiscreteProbabilityDistribution<String> zipDistribution;

	private Random random;
	private  Calendar calendar;

	public RoomGenerator() {
		random = new Random();
	}

	public void initialize() {
	}

	public int randomRoomCapacity(int max, int min) {
		Random r = new Random();
		return r.nextInt(max - min) + min;
	}

	public int randomBuilding() {
		Random r = new Random();
		return r.nextInt(9-1) + 1;
	}

	public Room generateRoom() {
		Room room = new Room();
		Building building=new Building();
		building.setBuildingName("Building"+ randomBuilding());
		room.setBuilding(building);
		Floor floor= new Floor();
		floor.setFloorName("Floor"+ randomBuilding());
		room.setFloor(floor);
		room.setRoomCapacity(randomRoomCapacity(4,1));
		int randomRoomType = random.nextInt();
		if (randomRoomType > 0.5) {
			room.setRoomType(RoomType.Clinical);
		} else {
			room.setRoomType(RoomType.Admin);
		}
		return room;
	}
	
	
	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/