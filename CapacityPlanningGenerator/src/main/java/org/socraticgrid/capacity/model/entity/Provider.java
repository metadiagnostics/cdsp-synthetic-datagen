/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Provider")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Provider extends org.socraticgrid.example.model.entity.Provider {
	

	private Calendar calendar;
	private List<Appointment> appointmentsList;

	public Provider() {
		super();
		this.appointmentsList = new ArrayList<Appointment>();
	}
	
	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void setCalendar(Calendar calendar) {
		this.calendar = calendar;
	}

	public void addAllAppointments(Collection<Appointment> appointments) {
		appointmentsList.addAll(appointments);
	}

	public void removeAllAppointments(Collection<Appointment> appointments) {
		appointmentsList.removeAll(appointments);
	}

	public void addAppointments(Appointment appointment) {
		appointmentsList.add(appointment);
	}

	public List<Appointment> getAppointmentsList() {
		return appointmentsList;
	}

	public void setAppointmentsList(List<Appointment> appointmentsList) {
		this.appointmentsList = appointmentsList;
	}

	// public String getFacilityIdentifier() {
	// return facilityIdentifier;
	// }
	//
	// public void setFacilityIdentifier(Identifier facilityIdentifier) {
	// this.facilityIdentifier = facilityIdentifier;
	// }
}