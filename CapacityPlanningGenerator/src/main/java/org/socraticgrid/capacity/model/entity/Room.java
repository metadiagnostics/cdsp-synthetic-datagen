/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;
import org.socraticgrid.example.model.enumeration.RoomType;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Room")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Room extends Entity {
	
	public static final String ENTITY_NAME = "Entity";
	private String roomNumber;
	private Integer roomCapacity;
	private RoomType roomType;
	private Floor floor;
	private Building building;
	private Calendar calendar;
	private ArrayList<Appointment> appointmentsList;
//	private List<RoomToClinicAssignment> clinicsListAssignment;

	public Room() {
		super();
		this.calendar = new Calendar();
		appointmentsList= new ArrayList<Appointment>();
//		clinicsListAssignment= new ArrayList<RoomToClinicAssignment>();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public String getRoomNumber() {
		return roomNumber;
	}

	public void setRoomNumber(String room_Number) {
		this.roomNumber = room_Number;
	}

	public Integer getRoomCapacity() {
		return roomCapacity;
	}

	public void setRoomCapacity(Integer room_Capacity) {
		this.roomCapacity = room_Capacity;
	}

	public RoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(RoomType clinical) {
		this.roomType = clinical;
	}

	public void assignToFloor(Floor floor) {
		floor.addRoom(this);
	}

	public Floor getFloor() {
		return floor;
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	public Building getBuilding() {
		return building;
	}

	public void setBuilding(Building building) {
		this.building = building;
	}

	public Calendar getCalendar() {
		return calendar;
	}

	public void addAppointments(Appointment appointments ){
		appointmentsList.add(appointments);
	}
	
	public void addAllAppointments(Collection<Appointment> appointments){
		appointmentsList.addAll(appointments);
		
	}
//	public void addRoomToClinic(RoomToClinicAssignment roomToClinicAssignment) {
//		clinicsListAssignment.add(roomToClinicAssignment);
//	}
//
//	public void addAllRoomsToClinic(Collection<RoomToClinicAssignment> roomsListAssignments) {
//		clinicsListAssignment.addAll(roomsListAssignments);
//	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/