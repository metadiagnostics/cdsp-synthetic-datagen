/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class OrganizationFactory {

	public final String ORGANIZATION_METADATA_FILE = "factorymetadata/Organizations.txt";
    private String organizationName;
	private ArrayList<String> organizationList;

	public OrganizationFactory() throws IOException {
		setOrganizationList(new ArrayList<String>());
		setUp();
	}


	private void setUp() throws IOException {
		TextReader reader = new TextReader();

		InputStream organizationStream = ReaderUtilities
				.loadResourceFromStream(ORGANIZATION_METADATA_FILE);

		setOrganizationList(reader.readFileToList(organizationStream));
	}


	public String getOrganizationName() {
		return organizationName;
	}


	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}


	public ArrayList<String> getOrganizationList() {
		return organizationList;
	}


	public void setOrganizationList(ArrayList<String> organizationList) {
		this.organizationList = organizationList;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/