/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Floor")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Floor extends Entity {
	public static final String ENTITY_NAME = "Floor";
	private String floorIdentifier;
	private String floorName;
	private Address locationIdentifier;
	private List<Room> rooms;
//	private Building building;

	public Floor() {
		setRooms(new ArrayList<Room>());
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public String getFloorIdentifier() {
		return floorIdentifier;
	}

	public void setFloorIdentifier(String floorIdentifier) {
		this.floorIdentifier = floorIdentifier;
	}

	public String getFloorName() {
		return floorName;
	}

	public void setFloorName(String floorName) {
		this.floorName = floorName;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public void addRoom(Room room) {
		rooms.add(room);
	}

	public void addAllRooms(Collection<Room> rooms) {
		this.rooms.addAll(rooms);
	}

	public Address getLocationIdentifier() {
		return locationIdentifier;
	}

	public void setLocationIdentifier(Address locationIdentifier) {
		this.locationIdentifier = locationIdentifier;
	}

//	public Building getBuilding() {
//		return building;
//	}
//
//	public void setBuilding(Building building) {
//		this.building = building;
//	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/