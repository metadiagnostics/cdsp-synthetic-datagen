/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.socraticgrid.capacity.model.entity.Division;
import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class DepartmentFactory {

	public final String DEPARTMENTS_METADATA_FILE = "factorymetadata/Departments.txt";
	private HashMap<String, String> departmentClinicMaps;
	private List<Division> divisionsList;
	private ArrayList<String> departmentList;

	public DepartmentFactory() {
		try {
			departmentSetup();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void departmentSetup() throws IOException {
		TextReader reader = new TextReader();

		InputStream departmentStream = ReaderUtilities
				.loadResourceFromStream(DEPARTMENTS_METADATA_FILE);
		setDepartmentList(reader.readFileToList(departmentStream));

	}

	public HashMap<String, String> getDepartmentClinicMaps() {
		return departmentClinicMaps;
	}

	public void setDepartmentClinicMaps(
			HashMap<String, String> departmentClinicMaps) {
		this.departmentClinicMaps = departmentClinicMaps;
	}

	public void addAllDivisions(Collection<Division> c) {
		this.divisionsList.addAll(c);
	}

	public void addDivision(Division c) {
		divisionsList.add(c);
	}

	public ArrayList<String> getDepartmentList() {
		return departmentList;
	}

	public void setDepartmentList(ArrayList<String> departmentList) {
		this.departmentList = departmentList;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/