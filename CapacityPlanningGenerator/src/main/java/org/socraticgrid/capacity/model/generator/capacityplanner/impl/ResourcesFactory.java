/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.socraticgrid.capacity.model.entity.Building;
import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.capacity.model.entity.Department;
import org.socraticgrid.capacity.model.entity.Directorate;
import org.socraticgrid.capacity.model.entity.Division;
import org.socraticgrid.capacity.model.entity.Facility;
import org.socraticgrid.capacity.model.entity.Floor;
import org.socraticgrid.capacity.model.entity.Organization;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.datatypes.core.Coding;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.generator.impl.AddressGenerator;
import org.socraticgrid.example.model.participation.Practitioner;
import org.socraticgrid.statistical.distribution.reader.AddressCsvReader;
import org.socraticgrid.statistical.distribution.reader.AddressReader;

/**
 * This Class uses different factories to instantiate the organizational and
 * physical aspects of organization. It builds the graph with two major
 * branches. 1- Organization -> Facilities -> Building -> Floor -> Room 2-
 * Organization -> Directorate -> Department -> Division -> Clinic
 * 
 * @author Neda Alipanah
 * 
 */

public class ResourcesFactory {

	private ArrayList<String> facilitiesList;
	private ArrayList<String> organizationList;
	private ArrayList<Organization> generatedOrganization;

	public ResourcesFactory() throws IOException {
		this.generatedOrganization = new ArrayList<Organization>();
		generatedOrganization = getAllResources();
	}

	public ArrayList<Organization> getGeneratedOrganization() {
		return generatedOrganization;
	}

	public int getRandom(int max) {
		Random rand = new Random();
		return rand.nextInt(max - 1) + 1;
	}

	public Room generateRoom(String roomNumber, Integer roomCapacity,
			String roomType, String floorIdentifier) throws IOException {

		Room room = new Room();
		room.setRoomNumber(roomNumber);
		room.setRoomCapacity(roomCapacity);
		
		return room;

	}


	public Floor generateFloor(String floorIdentifier, String floorName,
			Building building) throws IOException {
		Floor floor = new Floor();
		floor.setFloorIdentifier(floorIdentifier);
		floor.setFloorName(floorName);
		int totalRooms = getRandom(10);
		for (int j = 0; j < totalRooms; j++) {
			Room room = generateRoom("R" + j, 4, "TBD",
					floor.getFloorIdentifier());
			floor.addRoom(room);
		}
		return floor;

	}

	public Building generateBuilding(String buildingIdentifier,
			String buildingName, Address buildingAddress) throws IOException {
		Building building = new Building();
		building.setBuildingName(buildingName);
		building.setBuildingAddress(buildingAddress);
		int totalFloors = getRandom(2);
		for (int i = 0; i < totalFloors; i++) {
			Floor floor = generateFloor(buildingIdentifier + "" + i, "TBD",
					building);
			building.addFloor(floor);
		}
		return building;

	}

	public Facility generateFacility(String facilityName,
			String facilityIdentifier, Coding code, Integer facilityCapacity,
			Integer facilityRegion, Address facilityAddress,
			Integer facilityEnrollement) throws IOException {

		Facility facility = new Facility();
		// facility.setFacilityName(facilityName);
		facility.setFacilityCapacity(facilityCapacity);
		facility.setFacilityRegion(facilityRegion);
		facility.setFacilityAddress(facilityAddress);
		facility.setFacilityEnrollement(facilityEnrollement);
		for (String facilityElem : facilitiesList) {
			Building CurrentBuilding = generateBuilding(facilityElem,
					facilityName, facilityAddress);
			facility.addBuildings(CurrentBuilding);
		}
		return facility;

	}

	public Organization generateOrganization(String organizationName,
			String organizationIdentifier, Coding code) throws IOException {
		Organization organization = new Organization();
		AddressGenerator addressGenerator = new AddressGenerator();
		AddressReader reader = new AddressCsvReader(
				"distribution/addressesUS.txt", '\t', false,
				"distribution/citiesUS.txt", '\t', false,
				"distribution/StatesByPopulationUS.txt", '\t', false,
				"distribution/ZipByPopulationUS.txt", '\t', false);
		reader.setHeaderFlag(true);
		addressGenerator.initialize(reader);

		FacilityFactory facilityfactory = new FacilityFactory();
		facilitiesList = facilityfactory.getFacilityList();
		organization.setOrganizationName(organizationName);
		organization.setOrganizationType(code);
		int facilityIdentifier = 0;
		for (String facilityName : facilitiesList) {
			Address currentAddress = addressGenerator.generateAddress();
			Facility facility = generateFacility(facilityName
					+ facilityIdentifier, facilityIdentifier + "", code,
					new Integer(150), new Integer((int) (10 * Math.random())),
					currentAddress, new Integer(200));
			organization.addFacilities(facility);
			facilityIdentifier++;
		}
		DirectorateFactory directorateGenerator = new DirectorateFactory();
		ArrayList<String> directorateList = directorateGenerator
				.getDirectoratestList();
		for (String directorateName : directorateList) {
			organization.addDirectorate(generateDirectorate(directorateName));
		}

		return organization;
	}

	public Directorate generateDirectorate(String directorateName) {
		Directorate directorate = new Directorate();
		directorate.setDirectorateName(directorateName);
		DepartmentFactory department = new DepartmentFactory();
		ArrayList<String> departmentList = department.getDepartmentList();
		for (String departmentName : departmentList) {
			directorate.addDepartments(generateDepartment(departmentName));
		}
		return directorate;
	}

	private Department generateDepartment(String departmentName) {
		Department department = new Department();
		department.setDepartmentName(departmentName);
		DivisionFactory division = new DivisionFactory();

		for (String divisionName : division.getDivisionList())
			department.addDivision(generateDivision(divisionName));
		return department;
	}

	private Division generateDivision(String divisionName) {
		Division division = new Division();
		division.setDivisionName(divisionName);
		ClinicFactory clinic = new ClinicFactory();
		for (String clinicName : clinic.getClinicList()) {
			division.addClinics(generateClinic(clinicName));
		}
		return division;
	}

	private Clinic generateClinic(String clinicName) {
		Clinic clinic = new Clinic();
		clinic.setClinicName(clinicName.replaceAll("\\s", "_"));

		return clinic;
	}

	public Practitioner generatePractitioner() {
		Practitioner practitioner = new Practitioner();
		Identifier instanceIdentifier = null;
		practitioner.setInstanceIdentifier(instanceIdentifier);
		Identifier practitionerId = null;
		practitioner.setPractitionerId(practitionerId);
		return practitioner;
	}

	public Coding genrateCoding(String code, String codeDisplayName) {
		Coding coding = new Coding();
		coding.setCode(code);
		coding.setCodeDisplayName(codeDisplayName);
		return coding;
	}

	public ArrayList<Organization> generateAllOrganizations(
			ArrayList<String> organizationList) throws IOException {
		ArrayList<Organization> generatedOrganization = new ArrayList<Organization>();
		int orgIdentifier = 0;

		for (String organization : organizationList) {
			Coding Coding = new Coding();
			Coding.setCode(organization);
			Coding.setCodeDisplayName(organization + " Health Center");
			Organization currentOrganization = generateOrganization(
					organization, orgIdentifier + "", Coding);

			generatedOrganization.add(currentOrganization);

		}
		return generatedOrganization;
	}

	public Clinic generateClinic(String clinicName, Integer numberOfProviders) {
		Clinic currentClinic = new Clinic();
		currentClinic.setClinicName(clinicName);
//		currentClinic.setNumberOfProviders(numberOfProviders);
		return currentClinic;
	}

	public Room generateRoom(int roomCapacity, String roomNum) {
		Room currentRoom = new Room();
		currentRoom.setRoomCapacity(roomCapacity);
		currentRoom.setRoomNumber(roomNum);
		return currentRoom;
	}

	public ArrayList<Organization> getAllResources() throws IOException {
		OrganizationFactory organization = new OrganizationFactory();
		organizationList = organization.getOrganizationList();
		return generateAllOrganizations(organizationList);
	}

}
/* **********************************************************************************
 * Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * *****************************************************************
 * ***************
 */