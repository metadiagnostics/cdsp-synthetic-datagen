/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.component.VEvent;

import org.socraticgrid.capacity.model.entity.Appointment;
import org.socraticgrid.capacity.model.entity.Provider;
import org.socraticgrid.capacity.model.generator.DefaultPractitionerGenerator;
import org.socraticgrid.example.model.generator.impl.TimeCalculation;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class CapacityPlanner {
	private String startCalendarTime;
	private int dailyWorkingHours = 8;
	private org.socraticgrid.capacity.model.entity.Calendar calendar;
	private int availabilityInMinutes;
	private int numberOfProviders;
	private int numberOfNinetyMinutesTimeSlots;
	private int numberOfSixtyMinutesTimeSlots;
	private int numberOfThirtyMinutesTimeSlots;
	private int numberOfTwentyMinutesTimeSlots;
	private int numberOfAllocatedNinetyMinutes;
	private int numberOfAllocatedSixtyMinutes;
	private int numberOfAllocatedThirtyMinutes;
	private int numberOfAllocatedTwentyMinutes;
	private DiscreteProbabilityDistribution<String> practionerAvailabilityDistribution;
	private ArrayList<Appointment> appointmentsList;
	private Provider provider;
	private String practitionerSpecialty;

	public CapacityPlanner(String practitionerSpecialty,
			int numberOfTwentyMinutesTimeSlots,
			int numberOfThirtyMinutesTimeSlots,
			int numberOfSixtyMinutesTimeSlots,
			int numberOfNinetyMinutesTimeSlots,
			double percentrageOfAvailability, int workingDaysPerMonth,
			org.socraticgrid.capacity.model.entity.Calendar cal) {

		availabilityInMinutes = (int) ((workingDaysPerMonth
				* percentrageOfAvailability * dailyWorkingHours * 60) / 100);
		this.numberOfTwentyMinutesTimeSlots = numberOfTwentyMinutesTimeSlots;
		this.numberOfThirtyMinutesTimeSlots = numberOfThirtyMinutesTimeSlots;
		this.numberOfSixtyMinutesTimeSlots = numberOfSixtyMinutesTimeSlots;
		this.numberOfNinetyMinutesTimeSlots = numberOfNinetyMinutesTimeSlots;
		numberOfProviders = 0;
		numberOfAllocatedNinetyMinutes = 0;
		numberOfAllocatedSixtyMinutes = 0;
		numberOfAllocatedThirtyMinutes = 0;
		numberOfAllocatedTwentyMinutes = 0;
		appointmentsList = new ArrayList<Appointment>();
		calendar = cal;
		setStartCalendarTime("2014-08-01 00:00:00");
		this.provider=generatePractitioner();
		this.practitionerSpecialty = practitionerSpecialty;
		
	}

	public ArrayList<Appointment> getAllappointments() {
		return appointmentsList;
	}

	public void generateNextPractionerTimeSlot(
			double probabilityOfTwentyMinutes,
			double probabilityOfThirtyMinutes,
			double probabilityOfSixtyMinutes, double probabilityOfNinetyMinutes) {
		List<DiscreteItemProbabilityPair<String>> practionerAvailability = new ArrayList<DiscreteItemProbabilityPair<String>>();
		practionerAvailability.add(new DiscreteItemProbabilityPair<String>(
				"Twenty Minutes", probabilityOfTwentyMinutes));
		practionerAvailability.add(new DiscreteItemProbabilityPair<String>(
				"Thirty Minutes", probabilityOfThirtyMinutes));
		practionerAvailability.add(new DiscreteItemProbabilityPair<String>(
				"Sixty Minutes", probabilityOfSixtyMinutes));
		practionerAvailability.add(new DiscreteItemProbabilityPair<String>(
				"Ninety Minutes", probabilityOfNinetyMinutes));
		practionerAvailabilityDistribution = new DiscreteProbabilityDistribution<String>(
				practionerAvailability);

	}

	private boolean validateTimeBoundery(int hourOfDay, int slotDuration) {
		// Rule (17-hourOfDay)>slotDuration
		boolean validate = true;
		if (((17 - hourOfDay) * 60) < slotDuration) {
			validate = false;
		}
		return validate;

	}

	private double computeWeekOfCalendar(Calendar meetingDate) {
		String offset = getStartCalendarTime();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(Timestamp.valueOf(offset).getTime());
		long diff = meetingDate.getTimeInMillis() - calendar.getTimeInMillis();
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(diff);
		int ordinalDay = cal.get(Calendar.DAY_OF_YEAR);
		int weekDay = meetingDate.get(Calendar.DAY_OF_WEEK) - 1;
		int numberOfWeeks = (ordinalDay - weekDay + 10) / 7;
		return numberOfWeeks;
	}
	

	private void bookTimeSlot(int slotDuration) {
		TimeCalculation timeGenerator = new TimeCalculation();
		Calendar appoinmentSchedule = timeGenerator.getTimeStampInFuture();

		Appointment appointment = new Appointment();

		Calendar startDate = calendar.setMeetingStart(
				appoinmentSchedule.get(Calendar.MONTH),
				appoinmentSchedule.get(Calendar.DAY_OF_MONTH),
				appoinmentSchedule.get(Calendar.YEAR),
				appoinmentSchedule.get(Calendar.HOUR_OF_DAY),
				appoinmentSchedule.get(Calendar.MINUTE), 0);

		while (!validateTimeBoundery(startDate.getTime().getHours(),
				slotDuration)) {
			appoinmentSchedule = timeGenerator.getTimeStampInFuture();
			 startDate = calendar.setMeetingStart(
					appoinmentSchedule.get(Calendar.MONTH),
					appoinmentSchedule.get(Calendar.DAY_OF_MONTH),
					appoinmentSchedule.get(Calendar.YEAR),
					appoinmentSchedule.get(Calendar.HOUR_OF_DAY),
					appoinmentSchedule.get(Calendar.MINUTE), 0);
		}
		
		appointment.setStartDate(startDate.getTime());
		
		startDate.add(Calendar.MINUTE, slotDuration);
		appointment.setDuration(slotDuration);
		Calendar endDate = startDate;
		// System.out.println("Meeting Ends at: " + endDate.getTime());
		appointment.setEndDate(endDate.getTime());
		
		appointment.setPractitioner(provider);
		provider.setProviderSpecialty(practitionerSpecialty);
		// appointment.setPractitionerIdentifier((int) getRandomInt());
		appointment.setWeekOfCalender((int) computeWeekOfCalendar(startDate));
		appointment.setDayOfWeek(startDate.getTime().getDay());
		// appointment.setPractitionerSpecialty(practitionerSpecialty);
		// System.out.println("**************** Week of the Calender	"+
		// appointment.getWeekOfCalender());

		DateTime start = new DateTime(startDate.getTime());
		DateTime end = new DateTime(endDate.getTime());
		VEvent meeting = new VEvent(start, end, "New Appointment");
		// meeting.getProperties().add(tz.getTimeZoneId());
		boolean availabileTimeSlot = calendar.checkAvailability(startDate
				.getTime().getYear(), startDate.getTime().getMonth(), startDate
				.getTime().getDay(), startDate.getTime().getHours(), startDate
				.getTime().getMinutes(), 0, slotDuration);
		if (availabileTimeSlot) {
			calendar.addAppointmentToCalender(meeting);
			appointmentsList.add(appointment);
		}
	}



	private Provider generatePractitioner() {
		DefaultPractitionerGenerator practitioner = new DefaultPractitionerGenerator();
		return (Provider)practitioner.generatePractitioner();
	}

	public double getRandomInt() {
		return Math.floor(Math.random() * (999999 - 100000 + 1) + 100000);
	}

	

	public int convertTimeSlot(String timeSlotDuration) {
		int minutes = 0;
		if (timeSlotDuration.equalsIgnoreCase("Twenty Minutes")) {
			minutes = 20;
		} else if (timeSlotDuration.equalsIgnoreCase("Thirty Minutes")) {
			minutes = 30;
		} else if (timeSlotDuration.equalsIgnoreCase("Sixty Minutes")) {
			minutes = 60;
		} else if (timeSlotDuration.equalsIgnoreCase("Ninety Minutes")) {
			minutes = 90;
		}
		return minutes;
	}

	// ************************************************************/
	public void practionerTimeAllocation() {
		while (availabilityInMinutes > 20) {
			String timeSlotDuration = practionerAvailabilityDistribution
					.getSample();

//			System.out.println("Available Time:	" + availabilityInMinutes
//					+ "\t" + "Allocated Time:	" + timeSlotDuration);
			
			if (availabilityInMinutes < convertTimeSlot(timeSlotDuration)) {
				break;
			}

			if (timeSlotDuration.equalsIgnoreCase("Twenty Minutes")) {
				if (availabilityInMinutes > 20) {
					bookTimeSlot(20);
					availabilityInMinutes = availabilityInMinutes - 20;
					numberOfAllocatedTwentyMinutes++;
				}
			} else if (timeSlotDuration.equalsIgnoreCase("Thirty Minutes")) {
				if (availabilityInMinutes > 30) {
					bookTimeSlot(30);
					availabilityInMinutes = availabilityInMinutes - 30;
					numberOfAllocatedThirtyMinutes++;
				}
			} else if (timeSlotDuration.equalsIgnoreCase("Sixty Minutes")) {
				if (availabilityInMinutes > 60) {
					bookTimeSlot(60);
					availabilityInMinutes = availabilityInMinutes - 60;
					numberOfAllocatedSixtyMinutes++;
				}
			} else if (timeSlotDuration.equalsIgnoreCase("Ninety Minutes")) {
				if (availabilityInMinutes > 90) {
					bookTimeSlot(90);
					numberOfAllocatedNinetyMinutes++;
					availabilityInMinutes = availabilityInMinutes - 90;
				}
			}
		}

	}

	public String getStartCalendarTime() {
		return startCalendarTime;
	}

	public void setStartCalendarTime(String startCalendarTime) {
		this.startCalendarTime = startCalendarTime;
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/
