/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import net.fortuna.ical4j.model.component.VTimeZone;

import org.socraticgrid.capacity.model.entity.Appointment;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.entity.Provider;
import org.socraticgrid.capacity.model.generator.capacityplanner.BehavioralHealthAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.InternalMedicineAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.PTSDSupportAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.PainClinicAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.PrimaryCareAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.PsychiatryAppointmentGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.PyschologyAppointmentGenerator;
import org.socraticgrid.statistical.distribution.DiscreteItemProbabilityPair;
import org.socraticgrid.statistical.distribution.DiscreteProbabilityDistribution;

public class ProviderScheduleGenerator {

	private ArrayList<Appointment> appointmentsList;
	private DiscreteProbabilityDistribution<String> providersDiscreteDistribution;
	private String providerSpecialty;
	private Calendar availableCalender;
	private Calendar calendar;
	private VTimeZone tz;
	private String eventName;
	private Provider provider;

	public ProviderScheduleGenerator(Calendar calendar, VTimeZone tz,
			String eventName) {
		this.calendar=calendar;
		this.tz=tz;
		this.eventName=eventName;
		generateNextPractionerSpecialty();
		generatePractionerCalender(calendar, tz, eventName);
		this.appointmentsList = new ArrayList<Appointment>();
	}

	public void addAllAppointments(ArrayList<Appointment> appointmentList) {
		appointmentsList.addAll(appointmentList);
	}

	public void generateNextPractionerSpecialty() {
		List<DiscreteItemProbabilityPair<String>> practionerSpecialty = new ArrayList<DiscreteItemProbabilityPair<String>>();
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"PSYCHOLOGY", 0.083));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"BEHAVIORAL HEALTH", 0.208));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"PTSD SUPPORT CLINIC", 0.125));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"PSYCHIATRY", 0.083));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"PRIMARY CARE", 0.208));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"PAIN CLINIC", 0.083));
		practionerSpecialty.add(new DiscreteItemProbabilityPair<String>(
				"INTERNAL MEDICINE", 0.208));
		providersDiscreteDistribution = new DiscreteProbabilityDistribution<String>(
				practionerSpecialty);

	}

	public ArrayList<Appointment> generatePractionerAppointmentList() {

		 providerSpecialty = providersDiscreteDistribution.getSample();
		if (providerSpecialty.equalsIgnoreCase("PSYCHOLOGY")) {
			providerSpecialty = "Psychology";
			PyschologyAppointmentGenerator pyschologyAppointment = new PyschologyAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(pyschologyAppointment
					.getNextPyschologyTimeSlot());
//			appointmentsList = pyschologyAppointment
//					.getNextPyschologyTimeSlot();

		} else if (providerSpecialty.equalsIgnoreCase("BEHAVIORAL HEALTH")) {
			providerSpecialty = "Behavioral Health";
			BehavioralHealthAppointmentGenerator behavioralHealthAppointment = new BehavioralHealthAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(behavioralHealthAppointment
					.getNextBehavioralHealthTimeSlot());
//			appointmentsList = behavioralHealthAppointment
//					.getNextBehavioralHealthTimeSlot();

		} else if (providerSpecialty.equalsIgnoreCase("PTSD SUPPORT CLINIC")) {
			providerSpecialty = "PTSD Support Clinic";
			PTSDSupportAppointmentGenerator ptsdSupportAppointment = new PTSDSupportAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(ptsdSupportAppointment
					.getNextPTSDSupportTimeSlot());
//			appointmentsList = ptsdSupportAppointment
//					.getNextPTSDSupportTimeSlot();

		} else if (providerSpecialty.equalsIgnoreCase("PSYCHIATRY")) {
			providerSpecialty = "Psychiatry";
			PsychiatryAppointmentGenerator PsychiatryAppointment = new PsychiatryAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(PsychiatryAppointment
					.getNextPsychiatryTimeSlot());
//			appointmentsList = PsychiatryAppointment
//					.getNextPsychiatryTimeSlot();

		} else if (providerSpecialty.equalsIgnoreCase("PRIMARY CARE")) {
			providerSpecialty = "Primary Care";
			PrimaryCareAppointmentGenerator primaryCareAppointment = new PrimaryCareAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(primaryCareAppointment
					.getNextPainClinicTimeSlot());
//			appointmentsList = primaryCareAppointment
//					.getNextPainClinicTimeSlot();
		} else if (providerSpecialty.equalsIgnoreCase("PAIN CLINIC")) {
			providerSpecialty = "Pain Clinic";
			PainClinicAppointmentGenerator painClinicAppointment = new PainClinicAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(painClinicAppointment
					.getNextPrimaryCareTimeSlot());
//			appointmentsList = painClinicAppointment
//					.getNextPrimaryCareTimeSlot();
		} else if (providerSpecialty.equalsIgnoreCase("INTERNAL MEDICINE")) {
			providerSpecialty = "Internal Medicine";
			InternalMedicineAppointmentGenerator internalMedicineAppointment = new InternalMedicineAppointmentGenerator(
					calendar, tz, eventName);
			addAllAppointments(internalMedicineAppointment
					.getNextInternalMedicineTimeSlot());
//			appointmentsList = internalMedicineAppointment
//					.getNextInternalMedicineTimeSlot();
		}
		return appointmentsList;
	}

	public String getProviderSpecialty() {
		return providerSpecialty;
	}

	public Provider getProvider() {
		return provider;
	}

	public Calendar getAvailableCalender() {
		return availableCalender;
	}

	public void generatePractionerCalender(Calendar calendar, VTimeZone tz,
			String eventName) {

		providerSpecialty = providersDiscreteDistribution.getSample();
		if (providerSpecialty.equalsIgnoreCase("PSYCHOLOGY")) {
			PyschologyAppointmentGenerator pyschologyAppointment = new PyschologyAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = pyschologyAppointment
					.getNextPyschologyCalender();

		} else if (providerSpecialty.equalsIgnoreCase("BEHAVIORAL HEALTH")) {
			BehavioralHealthAppointmentGenerator behavioralHealthAppointment = new BehavioralHealthAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = behavioralHealthAppointment
					.getNextBehavioralHealthCalender();

		} else if (providerSpecialty.equalsIgnoreCase("PTSD SUPPORT CLINIC")) {
			PTSDSupportAppointmentGenerator ptsdSupportAppointment = new PTSDSupportAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = ptsdSupportAppointment
					.getNextPTSDSupportCalender();

		} else if (providerSpecialty.equalsIgnoreCase("PSYCHIATRY")) {
			PsychiatryAppointmentGenerator PsychiatryAppointment = new PsychiatryAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = PsychiatryAppointment
					.getNextPsychiatryCalender();

		} else if (providerSpecialty.equalsIgnoreCase("PRIMARY CARE")) {
			PrimaryCareAppointmentGenerator primaryCareAppointment = new PrimaryCareAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = primaryCareAppointment
					.getNextPainClinicCalender();
		} else if (providerSpecialty.equalsIgnoreCase("PAIN CLINIC")) {
			PainClinicAppointmentGenerator painClinicAppointment = new PainClinicAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = painClinicAppointment
					.getNextPrimaryCareCalender();
		} else if (providerSpecialty.equalsIgnoreCase("INTERNAL MEDICINE")) {
			InternalMedicineAppointmentGenerator internalMedicineAppointment = new InternalMedicineAppointmentGenerator(
					calendar, tz, eventName);
			availableCalender = internalMedicineAppointment
					.getNextInternalMedicineCalender();
		}
		// return filledCalender;
	}

	public ArrayList<Appointment> generateProviderSchedules() {
		ArrayList<Appointment> list = generatePractionerAppointmentList();
		Iterator<Appointment> itr = list.iterator();
		while (itr.hasNext()) {
			Appointment reservedAppoinments = itr.next();
			reservedAppoinments.setPractitionerSpecialty(providerSpecialty);
			 provider = reservedAppoinments.getPractitioner();
//			System.out.println(provider.getProviderIdentifier() + "\t"
//					+ reservedAppoinments.getPractitionerSpecialty() + "\t"
//					+ reservedAppoinments.getStartDate() + "\t"
//					+ reservedAppoinments.getEndDate() + "\t"
//					+ reservedAppoinments.getDuration() + "\t"
//					+ reservedAppoinments.getDayOfWeek() + "	Week   "
//					+ reservedAppoinments.getWeekOfCalender());
		}
		return list;

	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/