/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketException;
import java.net.URI;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.UUID;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.filter.Filter;
import net.fortuna.ical4j.filter.PeriodRule;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.ComponentList;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Dur;
import net.fortuna.ical4j.model.Period;
import net.fortuna.ical4j.model.PeriodList;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.TimeZone;
import net.fortuna.ical4j.model.TimeZoneRegistry;
import net.fortuna.ical4j.model.TimeZoneRegistryFactory;
import net.fortuna.ical4j.model.component.VEvent;
import net.fortuna.ical4j.model.component.VTimeZone;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.parameter.Role;
import net.fortuna.ical4j.model.property.Attendee;
import net.fortuna.ical4j.model.property.CalScale;
import net.fortuna.ical4j.model.property.ProdId;
import net.fortuna.ical4j.model.property.Uid;
import net.fortuna.ical4j.model.property.Version;
import net.fortuna.ical4j.util.UidGenerator;

import org.socraticgrid.clinical.model.serializer.SkipSerialization;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Calender")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Calendar extends Entity {
	public static final String ENTITY_NAME = "Calender";
	@SkipSerialization
	private net.fortuna.ical4j.model.Calendar calendar;
	@SkipSerialization
	private TimeZone timezone;
	@SkipSerialization
	private VTimeZone tz;
	private Integer duration;

	public Calendar() {
		this.calendar = new net.fortuna.ical4j.model.Calendar();
		this.calendar.getProperties().add(
				new ProdId("-//Appointments Calendar//iCal4j 1.0//EN"));
		this.calendar.getProperties().add(Version.VERSION_2_0);
		this.calendar.getProperties().add(CalScale.GREGORIAN);
	}

	public double getRandomInt(int min, int max) {
		return Math.floor(Math.random() * (max - min + 1) + min);
	}

	public Integer getTemplateDurationMinutes() {
		ArrayList<Integer> visitDuration = new ArrayList<Integer>();
		visitDuration.add(20);
		visitDuration.add(30);
		visitDuration.add(60);
		visitDuration.add(90);

		return visitDuration.get((int) getRandomInt(1, 4));
	}

	public VTimeZone setTimeZone(String timeZone) {
		// Create a TimeZone
		TimeZoneRegistry registry = TimeZoneRegistryFactory.getInstance()
				.createRegistry();
		timezone = registry.getTimeZone(timeZone);
		tz = timezone.getVTimeZone();
		return tz;
	}

	public java.util.Calendar setMeetingStart(int startMonth, int startDay,
			int startYear, int startHour, int startMinute, int startSecond) {
		java.util.Calendar startDate = new GregorianCalendar();
		// Calendar startDate = Calendar.getInstance();
		startDate.setTimeZone(timezone);
		startDate.set(java.util.Calendar.MONTH, startMonth);
		startDate.set(java.util.Calendar.DAY_OF_MONTH, startDay);
		startDate.set(java.util.Calendar.YEAR, startYear);
		startDate.set(java.util.Calendar.HOUR_OF_DAY, startHour);
		startDate.set(java.util.Calendar.MINUTE, startMinute);
		startDate.set(java.util.Calendar.SECOND, startSecond);
		return startDate;
	}

	public java.util.Calendar setMeetingEnd(int endMonth, int endDay,
			int endYear, int endHour, int endMinute, int endSecond) {
		java.util.Calendar endDate = new GregorianCalendar();
		endDate.setTimeZone(timezone);
		endDate.set(java.util.Calendar.MONTH, endMonth);
		endDate.set(java.util.Calendar.DAY_OF_MONTH, endDay);
		endDate.set(java.util.Calendar.YEAR, endYear);
		endDate.set(java.util.Calendar.HOUR_OF_DAY, endHour);
		endDate.set(java.util.Calendar.MINUTE, endMinute);
		endDate.set(java.util.Calendar.SECOND, endSecond);
		return endDate;
	}

	public boolean checkAvailability(java.util.Calendar timeSlotStart,
			int durationHour, int durationMinute) {
		boolean availability = false;

//		System.out.println("This is duration " + durationHour
//				+ "  This is duration Minutes  " + durationMinute);
		Period period = new Period(new DateTime(timeSlotStart.getTime()),
				new Dur(0, durationHour, durationMinute, 0));


		@SuppressWarnings("deprecation")
		Filter filter = new Filter(new PeriodRule(period));

		Collection<?> events = filter.filter(calendar
				.getComponents(Component.VEVENT));
		// for (Object o : events) {
		// System.out.println(o);
		// System.out.println("---");
		// }
		if (events.size() == 0) {
			availability = true;
		}
		return availability;
	}

	public boolean checkAvailability(int year, int month, int day,
			int hourOfDay, int minute, int durationHour, int durationMinute) {
		boolean availability = false;
		java.util.Calendar timeSlotStart = java.util.Calendar.getInstance();
		timeSlotStart.set(java.util.Calendar.MONTH, month);
		timeSlotStart.set(java.util.Calendar.DAY_OF_MONTH, day);
		timeSlotStart.set(java.util.Calendar.YEAR, year);
		timeSlotStart.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
		timeSlotStart.set(java.util.Calendar.MINUTE, minute);
		timeSlotStart.clear(java.util.Calendar.SECOND);
		// System.out.println("Availability Start Time"+timeSlotStart.getTime());

		// java.util.Calendar timeSlotEnd = java.util.Calendar.getInstance();
		// timeSlotEnd = timeSlotStart;
		//
		// timeSlotEnd.add(java.util.Calendar.HOUR_OF_DAY, durationHour);
		// timeSlotEnd.add(java.util.Calendar.MINUTE, durationMinute);

//		System.out.println("This is duration " + durationHour
//				+ "  This is duration Minutes  " + durationMinute);
		Period period = new Period(new DateTime(timeSlotStart.getTime()),
				new Dur(0, durationHour, durationMinute, 0));

		// Period period = new Period(new DateTime(timeSlotStart.getTime()),
		// new DateTime(timeSlotEnd.getTime()));

		// Period period = new Period(new DateTime(timeSlotStart.getTime()), new
		// Dur(0, durationHour, durationMinute, 0));

		@SuppressWarnings("deprecation")
		Filter filter = new Filter(new PeriodRule(period));

		Collection<?> events = filter.filter(calendar
				.getComponents(Component.VEVENT));
		// for (Object o : events) {
		// System.out.println(o);
		// System.out.println("---");
		// }
		if (events.size() == 0) {
			availability = true;
		}
		return availability;
	}

	public void checkEvents(int month, int year, int day, int hourOfDay,
			int durationHour, int durationMinute) {
		java.util.Calendar timeSlot = java.util.Calendar.getInstance();
		timeSlot.set(java.util.Calendar.MONTH, month);
		timeSlot.set(java.util.Calendar.DAY_OF_MONTH, day);
		timeSlot.set(java.util.Calendar.YEAR, year);
		timeSlot.set(java.util.Calendar.HOUR_OF_DAY, hourOfDay);
		timeSlot.clear(java.util.Calendar.MINUTE);
		timeSlot.clear(java.util.Calendar.SECOND);

		// create a period starting now with a duration of one (1) day..
		Period period = new Period(new DateTime(timeSlot.getTime()), new Dur(0,
				durationHour, durationMinute, 0));
		for (Object o : this.calendar.getComponents("VEVENT")) {
			Component c = (Component) o;
			PeriodList list = c.calculateRecurrenceSet(period);

			for (Object po : list) {
				System.out.println((Period) po);
			}
		}
	}

	public VEvent meetingIdentifier(VEvent meeting, String meetingID)
			throws SocketException {
		// generate unique identifier..
		UidGenerator ug = new UidGenerator(meetingID);
		Uid uid = ug.generateUid();
		meeting.getProperties().add(uid);

		return meeting;

	}

	public VEvent setAttendee(VEvent meeting, String attendeeRole,
			String attendeeEmail) {
		Attendee attendee = new Attendee(URI.create("mailto:" + attendeeEmail));
		attendee.getParameters().add(Role.REQ_PARTICIPANT);
		attendee.getParameters().add(new Cn(attendeeRole));
		meeting.getProperties().add(attendee);
		return meeting;

	}

	public void addAppointmentToCalender(VEvent meeting) {
		this.calendar.getComponents().add(meeting);

	}

	public net.fortuna.ical4j.model.Calendar getCalender() {
		return calendar;
	}

	// public void setCalender(net.fortuna.ical4j.model.Calendar calendar) {
	// this.calendar = calendar;
	// }

	/**
	 * @return
	 *********************************************/
	public ComponentList getComponents() {

		return calendar.getComponents();
	}

	/***********************************************/
	public void iterateCalendar(Calendar calendar) {

		for (Iterator i = calendar.getComponents().iterator(); i.hasNext();) {
			Component component = (Component) i.next();
			System.out.println("Component [" + component.getName() + "]");

			for (Iterator j = component.getProperties().iterator(); j.hasNext();) {
				Property property = (Property) j.next();
				System.out.println("Property [" + property.getName() + ", "
						+ property.getValue() + "]");
			}
		}

	}

	public static int getYear(String text) {
		return Integer.parseInt(text.substring(0, 4));
	}

	public static int getMonth(String text) {
		return Integer.parseInt(text.substring(4, 6));
	}

	public static int getDay(String text) {
		return Integer.parseInt(text.substring(6, 8));
	}

	public static int getHour(String text) {
		return Integer.parseInt(text.substring(9, 11));
	}

	public static int getMinutes(String text) {
		return Integer.parseInt(text.substring(11, 13));
	}

	public static java.util.Calendar setTimeslotCalendar(Property cal1Property) {
		java.util.Calendar practitinerTimeSlotCalendar = java.util.Calendar
				.getInstance();
		practitinerTimeSlotCalendar.set(java.util.Calendar.MONTH,
				getMonth(cal1Property.getValue()));
		practitinerTimeSlotCalendar.set(java.util.Calendar.DAY_OF_MONTH,
				getDay(cal1Property.getValue()));
		practitinerTimeSlotCalendar.set(java.util.Calendar.YEAR,
				getYear(cal1Property.getValue()));
		practitinerTimeSlotCalendar.set(java.util.Calendar.HOUR_OF_DAY,
				getHour(cal1Property.getValue()));
		practitinerTimeSlotCalendar.set(java.util.Calendar.MINUTE,
				getMinutes(cal1Property.getValue()));
		practitinerTimeSlotCalendar.clear(java.util.Calendar.SECOND);
//		System.out
//				.println("##########" + practitinerTimeSlotCalendar.getTime());
		return practitinerTimeSlotCalendar;
	}

	/**
	 * @throws ParseException
	 *****************************************************/
	public static Calendar checkCommonAvailabilityCalender(
			Calendar practionerCalender, Calendar roomCalender)
			throws ParseException {
		boolean startFlag = false;
		boolean endFlag = false;
		for (Iterator i = practionerCalender.getComponents().iterator(); i
				.hasNext();) {
			Component cal1Component = (Component) i.next();
			if (cal1Component.getName().matches("VEVENT")) {

				Appointment appointment = new Appointment();
				for (Iterator l = cal1Component.getProperties().iterator(); l
						.hasNext();) {
					Property cal1Property = (Property) l.next();
					if (cal1Property.getName().compareTo("DTSTART") == 0) {
						startFlag = true;
						setTimeslotCalendar(cal1Property);

						appointment.setStartDate(setTimeslotCalendar(
								cal1Property).getTime());
//						System.out.println("Start Date:  "+setTimeslotCalendar(cal1Property)
//								.getTime());
					}
					if (cal1Property.getName().compareTo("DTEND") == 0) {
						endFlag = true;
						appointment
								.setEndDate(setTimeslotCalendar(cal1Property)
										.getTime());
//						System.out.println("End Date:  "+setTimeslotCalendar(cal1Property)
//										.getTime());
					}

					if (startFlag && endFlag) {
						startFlag = false;
						endFlag = false;
//						System.out.println(appointment.getStartDate()
//								+ "*************" + appointment.getEndDate());

						int durationHour = convertDatetoCalendar(
								appointment.getEndDate()).get(
								java.util.Calendar.HOUR_OF_DAY)
								- convertDatetoCalendar(
										appointment.getStartDate()).get(
										java.util.Calendar.HOUR_OF_DAY);

						int durationMinute = convertDatetoCalendar(
								appointment.getEndDate()).get(
								java.util.Calendar.MINUTE)
								- convertDatetoCalendar(
										appointment.getStartDate()).get(
										java.util.Calendar.MINUTE);
						if (durationMinute < 0) {
							durationMinute = (durationHour * 60)
									+ durationMinute;
							durationHour = 0;
						}

//						System.out
//								.println(durationHour + "\t" + durationMinute);
						if (roomCalender.checkAvailability(
								convertDatetoCalendar(appointment.getStartDate()), durationHour,
								durationMinute)) {
							bookAppointment(roomCalender, appointment);
						}
					}
				}

			}
		}
		return roomCalender;
	}

	public static java.util.Calendar convertDatetoCalendar(java.util.Date date) {
		java.util.Calendar cal = java.util.Calendar.getInstance();
		cal.setTime(date);
		return cal;
	}

	// *****************************************************
	public Calendar bookAllAppointments(Calendar calendar,
			ArrayList<Appointment> appointments) {
		Iterator<Appointment> itr = appointments.iterator();
		while (itr.hasNext()) {
			Appointment appointment = itr.next();

			Date startDate = appointment.getStartDate();
			calendar.setTimeZone("America/Los_Angeles");

			java.util.Calendar startMeeting = java.util.Calendar.getInstance();
			startMeeting.setTime(startDate);
			Date endDate = appointment.getEndDate();

			java.util.Calendar EndMeeting = java.util.Calendar.getInstance();
			EndMeeting.setTime(endDate);

			DateTime start = new DateTime(startMeeting.getTime());
			DateTime end = new DateTime(EndMeeting.getTime());
			VEvent meeting = new VEvent(start, end, "New Appointment");
			calendar.addAppointmentToCalender(meeting);
		}
		return calendar;
	}

	// *****************************************************

	public static Calendar bookAppointment(Calendar calendar,
			Appointment appointment) {
		Date startDate = appointment.getStartDate();
		calendar.setTimeZone("America/Los_Angeles");

		java.util.Calendar startMeeting = java.util.Calendar.getInstance();
		startMeeting.setTime(startDate);

		Date endDate = appointment.getEndDate();
		java.util.Calendar EndMeeting = java.util.Calendar.getInstance();
		EndMeeting.setTime(endDate);

		// // System.out.println("Meeting Starts at: " + startDate.getTime());
		DateTime start = new DateTime(startMeeting.getTime());
		//
		DateTime end = new DateTime(EndMeeting.getTime());
		VEvent meeting = new VEvent(start, end, "New Appointment");
		calendar.addAppointmentToCalender(meeting);

		return calendar;
	}

	/***************************************************/
	public void generateEventDateFromInputStream() throws ParseException {
		// Reading the file and creating the calendar
		CalendarBuilder builder = new CalendarBuilder();
		net.fortuna.ical4j.model.Calendar cal = null;
		try {
			cal = builder.build(new FileInputStream("my.ics"));
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParserException e) {
			e.printStackTrace();
		}

		// Create the date range which is desired.
		DateTime from = new DateTime("20100101T070000Z");
		DateTime to = new DateTime("20100201T070000Z");
		Period period = new Period(from, to);

		// For each VEVENT in the ICS
		for (Object o : cal.getComponents("VEVENT")) {
			Component c = (Component) o;
			PeriodList list = c.calculateRecurrenceSet(period);

			for (Object po : list) {
				System.out.println((Period) po);
			}
		}
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/