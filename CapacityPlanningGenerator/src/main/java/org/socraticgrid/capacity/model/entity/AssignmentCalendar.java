/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.datatypes.complex.AbstractComplexDataType;

@SemanticResource(iri = AbstractComplexDataType.BASE_IRI
		+ "/AssignmentCalendar")
@AutoAnnotate(value = true, baseIri = AbstractComplexDataType.BASE_IRI, separator = '/')
public class AssignmentCalendar extends AbstractComplexDataType {

	public static final String COMPLEX_DATATYPE_NAME = "Address";
	private Clinic clinic;
	private Room room;
	private Calendar assignmentCalendar;
	private List<TimeSlot> timeSlots;

	public AssignmentCalendar() {
		super();
		this.setTimeSlots(new ArrayList<TimeSlot>());
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getCanonicalName() + "/" + UUID.randomUUID().toString();
	}

	@Override
	public String getCanonicalName() {
		return COMPLEX_DATATYPE_NAME;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

	public Calendar getAssignCalendar() {
		return assignmentCalendar;
	}

	public void setAssignCalendar(Calendar assignCalendar) {
		this.assignmentCalendar = assignCalendar;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<TimeSlot> getTimeSlots() {
		return timeSlots;
	}

	public void setTimeSlots(List<TimeSlot> timeSlots) {
		this.timeSlots = timeSlots;
	}
	
	public void addTimeSlot(TimeSlot t){
		timeSlots.add(t);
	}
	public void addAllTimeSlots(Collection<TimeSlot> t){
		timeSlots.addAll(t);
	}
}
/* **********************************************************************************
 * Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ***********************************************************************************/