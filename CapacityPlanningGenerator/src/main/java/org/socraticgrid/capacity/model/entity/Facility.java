/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.datatypes.complex.Address;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Facility")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Facility extends Entity {

	public static final String ENTITY_NAME = "Facilities";
	private String facilityName;
	private String facilityType;
	private Address facilityAddress;
	private Integer facilityCapacity;
	private Integer facilityEnrollement;
	private Integer facilityRegion;
//	private Organization organization;
	private List<Building> buildings;

	public Facility() {
		setBuilding(new ArrayList<Building>());
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public String getFacilityName() {
		return facilityName;
	}

	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	public String getFacilityType() {
		return facilityType;
	}

	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	public Address getFacilityAddress() {
		return facilityAddress;
	}

	public void setFacilityAddress(Address facilityAddress) {
		this.facilityAddress = facilityAddress;
	}

	public Integer getFacilityCapacity() {
		return facilityCapacity;
	}

	public void setFacilityCapacity(Integer facilityCapacity) {
		this.facilityCapacity = facilityCapacity;
	}

	public Integer getFacilityEnrollement() {
		return facilityEnrollement;
	}

	public void setFacilityEnrollement(Integer facilityEnrollement) {
		this.facilityEnrollement = facilityEnrollement;
	}

	public Integer getFacilityRegion() {
		return facilityRegion;
	}

	public void setFacilityRegion(Integer facilityRegion) {
		this.facilityRegion = facilityRegion;
	}

//	public Organization getOrganization() {
//		return organization;
//	}
//
//	public void assignToOrganization(Organization organization) {
//		this.organization = organization;
//	}

	public List<Building> getBuilding() {
		return buildings;
	}

	public void addBuildings(Building building) {
		this.buildings.add(building);
	}

	public void addAllBuildings(Collection<Building> buildings) {
		this.buildings.addAll(buildings);
	}

	public void setBuilding(List<Building> building) {
		this.buildings = building;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/