/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator;

import java.util.ArrayList;
import java.util.List;

import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.entity.Provider;
import org.socraticgrid.clinical.model.enumeration.AdministrativeGender;
import org.socraticgrid.clinical.model.generator.impl.RandomChoiceGenerator;
import org.socraticgrid.example.model.datatypes.core.Identifier;
import org.socraticgrid.example.model.entity.Person;
import org.socraticgrid.example.model.generator.PersonGenerator;
import org.socraticgrid.example.model.generator.PractitionerGenerator;
import org.socraticgrid.example.model.generator.impl.IdentifierGenerator;
import org.socraticgrid.example.model.generator.impl.PersonNameGenerator;
import org.socraticgrid.exception.SocraticGridRuntimeException;
import org.socraticgrid.statistical.distribution.reader.PersonNameCsvReader;
import org.socraticgrid.statistical.distribution.reader.PersonNameReader;

public class DefaultPractitionerGenerator<P extends Person> implements
		PractitionerGenerator<Person, Provider> {

	private RandomChoiceGenerator randomChoiceGenerator;
	private PersonGenerator<P> personGenerator;
	private IdentifierGenerator identifierGenerator;
	private PersonNameGenerator maleNameGenerator;
	private PersonNameGenerator femaleNameGenerator;
	private String providerSpecialty;

	// TODO Need a configure method - ideally from a file
	public DefaultPractitionerGenerator() {
		randomChoiceGenerator = new RandomChoiceGenerator();
		identifierGenerator = new IdentifierGenerator(
				Identifier.DEFAULT_IDENTIFIER_ROOT,
				"Default Socratic Grid Identifier");
	}

	public DefaultPractitionerGenerator(PersonGenerator<P> personGenerator) {
		this();
		this.personGenerator = personGenerator;
	}

	public void initialize() {
		randomChoiceGenerator = new RandomChoiceGenerator();
		PersonNameReader maleNameReader = new PersonNameCsvReader(
				"distribution/commonMaleNames.txt", '\t', false,
				"distribution/commonMaleNames.txt", '\t', false,
				"distribution/commonLastNames.txt", '\t', false);
		PersonNameReader femaleNameReader = new PersonNameCsvReader(
				"distribution/commonFemaleNames.txt", '\t', false,
				"distribution/commonFemaleNames.txt", '\t', false,
				"distribution/commonLastNames.txt", '\t', false);

		maleNameReader.setHeaderFlag(true);
		femaleNameReader.setHeaderFlag(true);
		maleNameGenerator = new PersonNameGenerator();
		maleNameGenerator.initialize(maleNameReader);
		femaleNameGenerator = new PersonNameGenerator();
		femaleNameGenerator.initialize(femaleNameReader);
		identifierGenerator = new IdentifierGenerator(Identifier.DEFAULT_IDENTIFIER_ROOT, "Socratic Grid Default Integer Identifier");
		personGenerator.initialize();
	}

	// public void registerPersonGenerator(PersonGenerator<P> personGenerator) {
	// this.personGenerator = personGenerator;
	// }
	public Person generatePerson() {
		Person person = new Person();
		generateAdministrativeGender(person);
//		generatePreferredName(person);
//		generateAddress(person);
		generateRandomIdentifier(person);
		return person;
	}
	public void generateRandomIdentifier(Person person) {
		person.addIdentifier(identifierGenerator.getNextIdentifier());
	}
	
	public void generateAdministrativeGender(Person person) {
		person.setAdministrativeGender(randomChoiceGenerator.getEnumerationItem(AdministrativeGender.class));
	}
	
	public void generatePreferredName(Person person) {
		if(person.getAdministrativeGender() == AdministrativeGender.MALE) {
			person.setPreferredName(maleNameGenerator.generateName());
		} else {
			person.setPreferredName(femaleNameGenerator.generateName());
		}
	}

	public Provider generatePractitioner(String providerSpecialty, Calendar Cal) {
		try {
			Provider provider = new Provider();
			provider.setProviderName(generatePerson().getPreferredName());
			provider.setIdentifier(identifierGenerator.getNextIdentifier());
			provider.setProviderSpecialty(providerSpecialty);
//			provider.setCalendar(Cal);
			// P person = personGenerator.generatePerson();
			return provider;
		} catch (Exception cause) {
			throw new SocraticGridRuntimeException(
					"Error generating provider instance", cause);
		}
	}

	
	public List<Provider> generatePractitioner(int count) {
		List<Provider> generatedPatients = new ArrayList<Provider>(count);
		for (int i = 0; i < count; i++) {
			generatedPatients.add(generatePractitioner());
		}
		return generatedPatients;
	}

	public void registerPersonGenerator(PersonGenerator<Person> personGenerator) {
		// TODO Auto-generated method stub

	}

	public Provider generatePractitioner() {
		try {
			Provider provider = new Provider();
			provider.setProviderName(generatePerson().getPreferredName());
			provider.setIdentifier(identifierGenerator.getNextIdentifier());
			provider.setProviderSpecialty(providerSpecialty);
//			provider.setCalendar(Cal);
			// P person = personGenerator.generatePerson();
			return provider;
		} catch (Exception cause) {
			throw new SocraticGridRuntimeException(
					"Error generating provider instance", cause);
		}
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/