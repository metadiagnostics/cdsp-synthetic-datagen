/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner;
import java.util.ArrayList;

import net.fortuna.ical4j.model.component.VTimeZone;

import org.socraticgrid.capacity.model.entity.Appointment;
import org.socraticgrid.capacity.model.entity.Calendar;

public class InternalMedicineAppointmentGenerator {

	private PractitionerTimeSlotGenerator practitionerTimeSlotGenerator;
	private Calendar calendar;
	private VTimeZone tz;
	private String eventName;
	
	public InternalMedicineAppointmentGenerator(
			Calendar calendar, VTimeZone tz, String eventName) {
		super();
		this.calendar = calendar;
		this.tz = tz;
		this.eventName = eventName;
	}


	public ArrayList<Appointment> getNextInternalMedicineTimeSlot(){
		practitionerTimeSlotGenerator = new PractitionerTimeSlotGenerator("Internal Medicine", 6, 2, 3, 0,
				8, 16, calendar);
		practitionerTimeSlotGenerator.generateNextPractionerTimeSlot(0.54, 0.18, 0.27, 0);
		practitionerTimeSlotGenerator.practionerTimeAllocation();
		return practitionerTimeSlotGenerator.getAllappointments();
	}
	
	public Calendar getNextInternalMedicineCalender(){
		practitionerTimeSlotGenerator = new PractitionerTimeSlotGenerator("Internal Medicine", 6, 2, 3, 0,
				8, 16, calendar);
		practitionerTimeSlotGenerator.generateNextPractionerTimeSlot(0.54, 0.18, 0.27, 0);
		practitionerTimeSlotGenerator.practionerTimeAllocation();
		return practitionerTimeSlotGenerator.getCalender();
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/