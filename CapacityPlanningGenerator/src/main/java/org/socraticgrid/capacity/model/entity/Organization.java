/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Capacity/Organization")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Organization extends org.socraticgrid.example.model.entity.Organization {
	
	private List<Facility> facilities;
	private List<Directorate> directorates;
	
	public Organization() {
		super();
		facilities=new ArrayList<Facility>();
		directorates=new ArrayList<Directorate>();
	}
	
	public void addFacilities(Facility facility) {
		facilities.add(facility);
	}

	public void addAllFacilities(Collection<Facility> facilities) {
		this.facilities.addAll(facilities);
	}

	public List<Facility> getFacilities() {
		return facilities;
	}

	public void addAllDirectorate(Collection<Directorate> c) {
		this.directorates.addAll(c);
	}
	
	public void addDirectorate(Directorate c) {
		directorates.add(c);

	}
	public List<Directorate> getAllDirectorates(){
		return directorates;
	}

}
