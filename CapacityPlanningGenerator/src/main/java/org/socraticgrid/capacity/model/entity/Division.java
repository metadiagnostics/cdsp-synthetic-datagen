/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Division")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Division extends Entity {
	public static final String ENTITY_NAME = "Division";
	private String divisionName;
	// private List<Provider> practioners;
	private List<Clinic> clinics;

	public Division() {
		// practioners = new ArrayList<Provider>();
		clinics = new ArrayList<Clinic>();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return divisionName;
	}

	public String getDivisionName() {
		return divisionName;
	}

	public void setDivisionName(String department) {
		this.divisionName = department;
	}

	public List<Clinic> getClinics() {
		return clinics;
	}

	public void setClinics(List<Clinic> clinics) {
		this.clinics = clinics;
	}

	public void addClinics(Clinic clinic) {
		clinics.add(clinic);
	}

	public void addAllClinics(Collection<Clinic> clinic) {
		clinics.addAll(clinic);
	}

	public List<Clinic> getAllClinics() {
		return clinics;
	}

}
/* **********************************************************************************
 * Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * *****************************************************************
 * ***************
 */