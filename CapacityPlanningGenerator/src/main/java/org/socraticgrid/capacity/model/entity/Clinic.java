/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;
import org.socraticgrid.example.model.entity.relations.ProviderToClinicAssignment;
import org.socraticgrid.example.model.entity.relations.RoomToClinicAssignment;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Clinic")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Clinic extends Entity {

	public static final String ENTITY_NAME = "Clinic";
	private String clinicName;
	private Integer numberOfProviders;
	private List<ProviderToClinicAssignment> practionersListAssignment;
	private List<RoomToClinicAssignment> clinicsListAssignment;
	public Clinic() {
		practionersListAssignment=new ArrayList<ProviderToClinicAssignment>();
		clinicsListAssignment= new ArrayList<RoomToClinicAssignment>();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return clinicName;
	}

	public String getClinicName() {
		return clinicName;
	}

	public void setClinicName(String clinicName) {
		this.clinicName = clinicName;
	}

	public Integer getNumberOfProviders() {
		return numberOfProviders;
	}


	
	public void addProvidertoClinic(ProviderToClinicAssignment providerToClinicAssignment) {
		practionersListAssignment.add(providerToClinicAssignment);
	}

	public void addAllProviderstoClinic(Collection<ProviderToClinicAssignment> providerToClinicAssignments) {
		practionersListAssignment.addAll(providerToClinicAssignments);
	}
	public void addRoomToClinic(RoomToClinicAssignment roomToClinicAssignment) {
		clinicsListAssignment.add(roomToClinicAssignment);
	}

	public void addAllRoomsToClinic(Collection<RoomToClinicAssignment> roomsListAssignments) {
		clinicsListAssignment.addAll(roomsListAssignments);
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/