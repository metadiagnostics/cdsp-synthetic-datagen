/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.socraticgrid.capacity.model.entity.AssignmentCalendar;
import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.capacity.model.entity.TimeSlot;
import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class RoomToClinicAssignmentFactory {

	public static final String ENTITY_NAME = "roomToClinicAssignments";
	private Room room;
	// private List<String> assignmentCals;
	public final String ROOMCLINIC_ASSIGNMENT_METADATA_FILE = "factorymetadata/RoomToClinicAssignment.txt";

//	private List<AssignmentCalendar> roomToClinicAssignmentCalendar;
	private AssignmentCalendar roomToClinicAssignmentCalendar;
	
	public RoomToClinicAssignmentFactory() throws IOException {
//		roomToClinicAssignmentCalendar = new ArrayList<AssignmentCalendar>();
		setup();
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public void setup() throws IOException {
		TextReader reader = new TextReader();

		InputStream roomtoClinicStream = ReaderUtilities
				.loadResourceFromStream(ROOMCLINIC_ASSIGNMENT_METADATA_FILE);

		 ArrayList<String> schedule = reader.readFileToList(roomtoClinicStream);
		setAssignmentCalendar(schedule);

	}

	private void setAssignmentCalendar(ArrayList<String> schedule) {
		AssignmentCalendar assignmentCalendar = new AssignmentCalendar();
		Iterator<String> itr = schedule.iterator();
		while (itr.hasNext()) {
			String element = itr.next();
			String[] weeklyschedule = element.split("\\#");
			String weekDay = weeklyschedule[0];
			String period = weeklyschedule[1];
			String[] rangePeriod = period.split("\\-");
			String start = rangePeriod[0];
			String end = rangePeriod[1];
			assignmentCalendar.addTimeSlot(setTimeSlot(checkDay(weekDay), start, end));
//			assignmentCalendar.setClinic(buildClinic(clinic));
		}
		roomToClinicAssignmentCalendar=assignmentCalendar;
	}

	public AssignmentCalendar getAssignmentCalendars() {
		return roomToClinicAssignmentCalendar;
	}

	public TimeSlot setTimeSlot(int i, String startTime, String endTime) {
		TimeSlot timeSlot = new TimeSlot();
		timeSlot.setStartTime(startTime);
		timeSlot.setEndTime(endTime);
		timeSlot.setDayOfWeek(i);
		return timeSlot;
	}

	public Clinic buildClinic(String clinicName) {
		Clinic clinic = new Clinic();
		clinic.setClinicName(clinicName.replaceAll("\\s", "_"));
		return clinic;
	}

	public int checkDay(String day) {
		HashMap<String, Integer> mp = new HashMap<String, Integer>();

		mp.put("Sunday", 1);
		mp.put("Monday", 2);
		mp.put("Tuesday", 3);
		mp.put("Wednesday", 4);
		mp.put("Thursday", 5);
		mp.put("Friday", 6);
		mp.put("Saturday", 7);

		return mp.get(day).intValue();
	}
}
/* **********************************************************************************
 * Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ***********************************************************************************
 */