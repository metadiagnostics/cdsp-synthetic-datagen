/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import org.socraticgrid.capacity.model.generator.DefaultPractitionerGenerator;
import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.example.model.entity.Provider;
import org.socraticgrid.example.model.serializer.rdf.GenericRdfProviderSerializer;


public class ProviderFactory {

	private static int totalProviders = 40;


	public static void generateProviders() {
		for (int i = 0; i < totalProviders; i++) {
			GenericRdfProviderSerializer serializer = new GenericRdfProviderSerializer();
			DefaultPractitionerGenerator practitioner = new DefaultPractitionerGenerator();
			Provider currentProvider = practitioner.generatePractitioner();
			serializer.serialize(currentProvider, DocumentFormat.GENERIC_RDF,
					System.out);
		}
	}

	public static void main(String[] args) {
		generateProviders();
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/