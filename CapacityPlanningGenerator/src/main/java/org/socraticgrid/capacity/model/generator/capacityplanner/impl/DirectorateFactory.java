/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class DirectorateFactory {
	
	public final String DERECTORATES_METADATA_FILE = "factorymetadata/Directorates.txt";
	private HashMap<String, String> departmentClinicMaps;
	private List<Clinic> departmentsList;
	private ArrayList<String> directoratesList;
	
	public DirectorateFactory() {
		try {
			directorateSetup();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	private void directorateSetup() throws IOException {
		TextReader reader = new TextReader();

		InputStream directorateStream = ReaderUtilities.loadResourceFromStream(DERECTORATES_METADATA_FILE);
		 setDirectoratesList(reader.readFileToList(directorateStream));

	
	}


	public ArrayList<String> getDirectoratestList() {
		return directoratesList;
	}


	private void setDirectoratesList(ArrayList<String> directoratesList) {
		this.directoratesList = directoratesList;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/