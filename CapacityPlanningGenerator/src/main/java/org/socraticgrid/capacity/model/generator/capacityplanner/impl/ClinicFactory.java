/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.socraticgrid.common.reader.ReaderUtilities;
import org.socraticgrid.common.reader.TextReader;

public class ClinicFactory {

	public final String CLINICS_METADATA_FILE = "factorymetadata/Clinics.txt";
	private ArrayList<String> clinicsList;

	public ClinicFactory() {
		try {
			clinicSetup();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void clinicSetup() throws IOException {
		TextReader reader = new TextReader();

		InputStream clinicStream = ReaderUtilities
				.loadResourceFromStream(CLINICS_METADATA_FILE);
		setClinicList(reader.readFileToList(clinicStream));

	}

	public ArrayList<String> getClinicList() {
		return clinicsList;
	}

	public void setClinicList(ArrayList<String> clinicList) {
		this.clinicsList = clinicList;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/