/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.serializer.rdf;

import java.io.OutputStream;
import java.util.UUID;

import org.socraticgrid.capacity.model.entity.Department;
import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.clinical.model.serializer.SerializationException;
import org.socraticgrid.clinical.model.serializer.generic.BaseGenericRdfSerializer;
import org.socraticgrid.clinical.model.serializer.generic.RdfContext;
import org.socraticgrid.example.model.serializer.SemanticAnnotationBuilder;

public class GenericRdfDepartmentSerializer extends BaseGenericRdfSerializer<Department>{
	public static final String DEPARTMENT_CONCEPT_IRI = "department";

	@Override
	public void serialize(Department department, DocumentFormat format, OutputStream outputStream) {
		try {
			if(format == DocumentFormat.GENERIC_RDF) {
				RdfContext context = new RdfContext();
				buildRdfGraph(department, format, outputStream, context);
				getModel().writeasRdfXml(outputStream);
			} else {
				throw new SerializationException("Unknown output format: " + format);
			}
		} catch(Exception cause) {
			throw new SerializationException("Error parsing organization", cause);
		}
	}
	
	protected void buildRdfGraph(Department department, DocumentFormat format, OutputStream outputStream, RdfContext context) {
		SemanticAnnotationBuilder builder = new SemanticAnnotationBuilder(getModel());
		builder.addDataTypeStatements(department, context);
	}
	
	protected String buildInstanceIri(String instanceId) {
		return BASE_INSTANCE_IRI + DEPARTMENT_CONCEPT_IRI+ "/" + instanceId;
	}

	@Override
	protected String buildInstanceIdentifier(Department department) {
		return UUID.randomUUID().toString();
	}
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/