/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.generator.capacityplanner.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import net.fortuna.ical4j.model.ValidationException;
import net.fortuna.ical4j.model.component.VTimeZone;

import org.socraticgrid.capacity.model.entity.Appointment;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.generator.capacityplanner.CapacityPlanner;
import org.socraticgrid.example.model.entity.Provider;


public class ProviderTimeSlotGenerator {

	private static Calendar calendar;
	private static VTimeZone tz;
	private static String eventName;
	private static ArrayList<Appointment> appointmentsList;

	public static void addAllAppointments(
			ArrayList<Appointment> appointmentList) {
		appointmentsList.addAll(appointmentList);
	}

	public static void main(String[] args) throws IOException,
			ValidationException {
		calendar = new Calendar();
		CapacityPlanner capacityPlanner;
		tz = calendar.setTimeZone("America/Los_Angeles");
		eventName = "Calendar";
		appointmentsList = new ArrayList<Appointment>();
		for (int i = 0; i < 4; i++) {
			capacityPlanner = new CapacityPlanner("Psychology", 6, 0, 4, 0, 5,
					18, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}
		// **********************
		for (int i = 0; i < 4; i++) {
			capacityPlanner = new CapacityPlanner("Behavioral Health", 3, 2, 1,
					2, 5, 18, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.375, 0.25, 0.125,
					0.25);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());

		}
		// **********************
		for (int i = 0; i < 3; i++) {
			capacityPlanner = new CapacityPlanner("PTSD Support Clinic", 3, 4,
					3, 0, 10, 18, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.375, 0.25, 0.125,
					0.25);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}
		// **********************
		for (int i = 0; i < 5; i++) {
			capacityPlanner = new CapacityPlanner("Psychiatry", 6, 0, 4, 0, 2,
					16, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}

		// **********************
		for (int i = 0; i < 2; i++) {
			capacityPlanner = new CapacityPlanner("Primary Care", 12, 4, 0, 0,
					10, 16, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.75, 0.25, 0, 0);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}
		// **********************
		for (int i = 0; i < 3; i++) {
			capacityPlanner = new CapacityPlanner("Pain Clinic", 12, 4, 0, 0,
					5, 18, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.6, 0, 0.4, 0);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}
		// **********************
		for (int i = 0; i < 4; i++) {
			capacityPlanner = new CapacityPlanner("Internal Medicine", 6, 2, 3,
					0, 8, 16, calendar);
			capacityPlanner.generateNextPractionerTimeSlot(0.54, 0.18, 0.27, 0);
			capacityPlanner.practionerTimeAllocation();
			addAllAppointments(capacityPlanner.getAllappointments());
		}
		Iterator<Appointment> itr = appointmentsList.iterator();
		while (itr.hasNext()) {
			Appointment reservedAppoinments = itr.next();
			Provider provider = reservedAppoinments.getPractitioner();
//			System.out.println(provider.getProviderIdentifier() + "\t"
//					+ provider.getProviderSpecialty() + "\t"
//					+ reservedAppoinments.getStartDate() + "\t"
//					+ reservedAppoinments.getEndDate() + "\t"
//					+ reservedAppoinments.getDuration() + "\t"
//					+ reservedAppoinments.getDayOfWeek() + "	Week   "
//					+ reservedAppoinments.getWeekOfCalender());
		}

	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/