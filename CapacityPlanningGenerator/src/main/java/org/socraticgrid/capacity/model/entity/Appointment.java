/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.model.entity;

import java.util.Date;
import java.util.UUID;

import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI + "/Appointments")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class Appointment extends Entity implements java.io.Serializable{
	/**
	 * 
	 */
	public static final String ENTITY_NAME = "Appointment";
	private static final long serialVersionUID = 1L;
	private Date startDate;
	private Date endDate;
	private int weekOfCalender;
	private String practitionerSpecialty;
	private Provider practitioner;
	private int dayOfWeek;
	private int duration;
	private String appointmentType;
	

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date date) {
		this.startDate = date;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getWeekOfCalender() {
		return weekOfCalender;
	}

	public void setWeekOfCalender(int weekOfCalender) {
		this.weekOfCalender = weekOfCalender;
	}

	public String getPractitionerSpecialty() {
		return practitionerSpecialty;
	}

	public void setPractitionerSpecialty(String practitioner) {
		this.practitionerSpecialty = practitioner;
	}



	public int getDayOfWeek() {
		return dayOfWeek;
	}

	public void setDayOfWeek(int dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}


	public Provider getPractitioner() {
		return practitioner;
	}

	public void setPractitioner(Provider practitioner) {
		this.practitioner = practitioner;
	}

	public String getAppointmentType() {
		return appointmentType;
	}

	public void setAppointmentType(String appointmentType) {
		this.appointmentType = appointmentType;
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}


	
}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/