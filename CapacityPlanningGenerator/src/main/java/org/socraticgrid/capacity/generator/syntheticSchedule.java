/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.capacity.generator;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import net.fortuna.ical4j.model.component.VTimeZone;

import org.socraticgrid.capacity.model.entity.Appointment;
import org.socraticgrid.capacity.model.entity.AssignmentCalendar;
import org.socraticgrid.capacity.model.entity.Building;
import org.socraticgrid.capacity.model.entity.Calendar;
import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.capacity.model.entity.Department;
import org.socraticgrid.capacity.model.entity.Directorate;
import org.socraticgrid.capacity.model.entity.Division;
import org.socraticgrid.capacity.model.entity.Facility;
import org.socraticgrid.capacity.model.entity.Floor;
import org.socraticgrid.capacity.model.entity.Organization;
import org.socraticgrid.capacity.model.entity.Provider;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.capacity.model.entity.TimeSlot;
import org.socraticgrid.capacity.model.generator.capacityplanner.impl.ProviderScheduleGenerator;
import org.socraticgrid.capacity.model.generator.capacityplanner.impl.ProviderToClinicAssingmentFactory;
import org.socraticgrid.capacity.model.generator.capacityplanner.impl.ResourcesFactory;
import org.socraticgrid.capacity.model.generator.capacityplanner.impl.RoomToClinicAssignmentFactory;
import org.socraticgrid.capacity.model.serializer.rdf.GenericRdfOrganizationSerializer;
import org.socraticgrid.clinical.document.DocumentFormat;
import org.socraticgrid.example.model.entity.relations.ProviderToClinicAssignment;
import org.socraticgrid.example.model.entity.relations.RoomToClinicAssignment;



public class syntheticSchedule {

	public static Integer NUMBER_OF_PRACTITIONER = 1;
	private static Calendar providerCalender;
	private static VTimeZone tz;
	private static String eventName;
	private Department currentDepartment;

	public syntheticSchedule() {
		// TODO Auto-generated constructor stub
	}

	public static void setUpProviderCalender() {
		providerCalender = new Calendar();
		tz = providerCalender.setTimeZone("America/Los_Angeles");
		eventName = "Calendar";
	}

	public static int getRandomElement(int numberOfOrganizations) {
		return (int) Math.floor(Math.random() * (numberOfOrganizations - 0));
	}

	public static Room getRandomRoom(ArrayList<Organization> OrganizationList) {
		Organization organization = OrganizationList
				.get(getRandomElement(OrganizationList.size()));

		Facility facility = organization.getFacilities().get(
				getRandomElement(organization.getFacilities().size()));
		Building building = facility.getBuilding().get(
				getRandomElement(facility.getBuilding().size()));
		Floor floor = building.getFloors().get(
				getRandomElement(building.getFloors().size()));
		return floor.getRooms().get(getRandomElement(floor.getRooms().size()));

	}

	public Provider generateProvider(ProviderScheduleGenerator providerSchedule) {
		setUpProviderCalender();
		ArrayList<Appointment> providerAppointments = providerSchedule
				.generateProviderSchedules();
		Provider practitioner = providerSchedule.getProvider();
		String providerSpecialty = providerSchedule.getProviderSpecialty();
		practitioner.setProviderSpecialty(providerSpecialty);
		practitioner
				.addAllAppointments(cloneAppointmentList(providerAppointments));
		return practitioner;
	}

	public static Organization addAllFacilities(
			ArrayList<Organization> OrganizationList,
			Organization newOrganization) {

		Iterator<Organization> organizationitr = OrganizationList.iterator();
		Organization organization;
		while (organizationitr.hasNext()) {
			organization = organizationitr.next();
			List<Facility> facilitiesList = organization.getFacilities();
			newOrganization.addAllFacilities(facilitiesList);

		}
		return newOrganization;

	}

	// public static Clinic assignToClinic(String clinicName, Room room,
	// Provider practioner) {
	// Clinic clinic = new Clinic();
	// clinic.setClinicName(clinicName.replaceAll("\\s", "_"));
	// clinic.addRoom(room);
	// // clinic.addPractitioner(practioner);
	//
	// return clinic;
	// }

	public static Appointment cloneAppointment(Appointment appointment) {
		Appointment appointmentCopy = new Appointment();
		appointmentCopy.setStartDate(appointment.getStartDate());
		appointmentCopy.setEndDate(appointment.getEndDate());
		appointmentCopy.setDuration(appointment.getDuration());
		appointmentCopy.setDayOfWeek(appointment.getDayOfWeek());
		appointmentCopy.setWeekOfCalender(appointment.getWeekOfCalender());
		return appointmentCopy;
	}

	public String getClinicForPractitionarAssignment(String clinicAssignment,
			int i) {
		String[] orgHierarchy = clinicAssignment.split("\t");
		return orgHierarchy[i];
	}

	public void BFSTraversal(ArrayList<Organization> OrganizationList,
			String organization, String directorate, String dep,
			String division, String clinic) {
		Queue queue = new LinkedList();
		for (Organization org : OrganizationList) {
			if (org.getOrganizationName().compareTo(organization) == 0) {
				queue.add(org);
			}
		}
		while (!queue.isEmpty()) {
			Object currentNode = queue.remove();
			List<Object> child = new ArrayList<Object>();
			if (currentNode instanceof Organization) {
				child.addAll(((Organization) currentNode).getAllDirectorates());
			} else if (currentNode instanceof Directorate) {
				child.addAll(((Directorate) currentNode).getDepartment());
			} else if (currentNode instanceof Department) {
				child.addAll(((Department) currentNode).getDivisions());
			} else if (currentNode instanceof Division) {
				child.addAll(((Division) currentNode).getClinics());
			}
			queue.addAll(child);
			// while (child != null) {
			// queue.add(child);
			// }
		}
	}

	public Clinic traversingOrganizationGetClinic(
			ArrayList<Organization> OrganizationList, String organization,
			String directorate, String dep, String division, String clinic) {
		Clinic currentClinic = null;

		for (Organization org : OrganizationList) {
			if (org.getOrganizationName().compareTo(organization) == 0) {
				for (Directorate directName : org.getAllDirectorates()) {
					if (directName.getDirectorateName().compareTo(directorate) == 0) {
						for (Department departmentName : directName
								.getDepartment()) {
							if (departmentName.getDepartmentName().compareTo(
									dep) == 0) {
								currentDepartment = departmentName;
								for (Division div : departmentName
										.getDivisions()) {
									if (div.getDivisionName().compareTo(
											division) == 0) {
										for (Clinic clinicObj : div
												.getAllClinics()) {
											if (clinicObj.getClinicName()
													.compareTo(clinic) == 0) {
												currentClinic = clinicObj;
											}
											break;
										}
									}
									break;
								}
							}
							break;
						}
					}
				}
			}
		}
		return currentClinic;
	}

	public RoomToClinicAssignment generateRoomtoClinicAssignment(Room room,
			AssignmentCalendar assignCal, Clinic clinic) throws IOException {
		RoomToClinicAssignment assignmentCal = new RoomToClinicAssignment();
		assignmentCal.setRoom(room);
		for (TimeSlot timeSlot : assignCal.getTimeSlots()) {
			assignmentCal.setTimeSlot(timeSlot.getDayOfWeek(),
					timeSlot.getStartTime(), timeSlot.getEndTime());
		}
		return assignmentCal;
	}

	public void generateRoomSettings(Room room, Clinic clinic)
			throws IOException {
		RoomToClinicAssignmentFactory RCAssignment = new RoomToClinicAssignmentFactory();
		RoomToClinicAssignment roomToClinicAssignment = generateRoomtoClinicAssignment(
				room, RCAssignment.getAssignmentCalendars(), clinic);
	
		clinic.addRoomToClinic(roomToClinicAssignment);
	}

	public static ArrayList<Appointment> cloneAppointmentList(
			ArrayList<Appointment> appointmentsList) {

		ArrayList<Appointment> newAppointments = new ArrayList<Appointment>();
		Iterator<Appointment> itr = appointmentsList.iterator();
		while (itr.hasNext()) {
			Appointment appointment = itr.next();
			newAppointments.add(cloneAppointment(appointment));
		}
		return newAppointments;
	}

	public static void main(String[] args) throws IOException, ParseException {
		if (args.length != 3) {
			usage();
		} else {
			NUMBER_OF_PRACTITIONER = Integer.parseInt(args[0]);
			String dirPath = args[1];
			String baseFileName = args[2];
			File dir = new File(dirPath);
			if (!dir.isDirectory()) {
				usage();
			}
			syntheticSchedule main = new syntheticSchedule();
			main.runGenerator(dirPath, baseFileName);
		}
	}

	public static void usage() {
		System.out
				.println("Main requires three parameters: an integer count, an output dir path and  a base file name without an extension");
	}

	public void runGenerator(String dirPath, String baseFileName)
			throws IOException, ParseException {

		// *** 1- Instantiate All the Resources

		ResourcesFactory resources = new ResourcesFactory();
		ArrayList<Organization> OrganizationList = resources.getAllResources();
		for (Organization organization : OrganizationList) {

			// *** 2- Initialize Practitioner Schedule

			setUpProviderCalender();
			ProviderScheduleGenerator providerSchedule = new ProviderScheduleGenerator(
					providerCalender, tz, eventName);
			GenericRdfOrganizationSerializer serializer = new GenericRdfOrganizationSerializer();

			for (int i = 0; i < NUMBER_OF_PRACTITIONER; i++) {

				// *** 3- Generate Practitioner with his/her Schedule
				Provider practitioner = generateProvider(providerSchedule);

				Room room = getRandomRoom(OrganizationList);
				Calendar roomCalendar = room.getCalendar();

				// **** 4- Assign the practitioner to Clinic
				ProviderToClinicAssingmentFactory providerToClinicAssignmentName = new ProviderToClinicAssingmentFactory();
				Clinic currentClinic = null;
				for (String clinicAssignment : providerToClinicAssignmentName
						.getProviderToClinicAssignments()) {
					ProviderToClinicAssignment providerToClinicAssignment = new ProviderToClinicAssignment();

					// **** 5- Retrieve the Clinic from the MetaData
					currentClinic = traversingOrganizationGetClinic(
							OrganizationList,
							getClinicForPractitionarAssignment(
									clinicAssignment, 0),
							getClinicForPractitionarAssignment(
									clinicAssignment, 1),
							getClinicForPractitionarAssignment(
									clinicAssignment, 2),
							getClinicForPractitionarAssignment(
									clinicAssignment, 3),
							getClinicForPractitionarAssignment(
									clinicAssignment, 4));
					currentDepartment.addPractitioner(practitioner);
					providerToClinicAssignment.setProvider(practitioner);
					providerToClinicAssignment.addClinic(currentClinic);
					if (currentClinic!=null) generateRoomSettings(room, currentClinic);
				}

				if (roomCalendar.getComponents().size() == 0) {
					roomCalendar.bookAllAppointments(roomCalendar,
							(ArrayList<Appointment>) practitioner
									.getAppointmentsList());
					room.addAllAppointments(cloneAppointmentList((ArrayList<Appointment>) practitioner
							.getAppointmentsList()));
				} else {
					Calendar.checkCommonAvailabilityCalender(roomCalendar,
							providerSchedule.getAvailableCalender());
				}
			}
			File file = new File(dirPath
					+ organization.getOrganizationName().replaceAll("\\s", "_")
					+ baseFileName);
			OutputStream output = new FileOutputStream(file);
			serializer.serialize(organization, DocumentFormat.GENERIC_RDF,
					output);
		}
	}
}
/* **********************************************************************************
 * Copyright (C) 2013 by Cognitive Medical Systems, Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * ***********************************************************************************
 */