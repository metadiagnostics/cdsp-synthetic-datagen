/* 
 * Copyright 2015 Cognitive Medical Systems, Inc (http://www.cognitivemedicine.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.socraticgrid.example.model.entity.relations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import org.socraticgrid.capacity.model.entity.AssignmentCalendar;
import org.socraticgrid.capacity.model.entity.Clinic;
import org.socraticgrid.capacity.model.entity.Room;
import org.socraticgrid.capacity.model.entity.TimeSlot;
import org.socraticgrid.common.jena.AutoAnnotate;
import org.socraticgrid.common.jena.SemanticResource;
import org.socraticgrid.example.model.AbstractClinicalModel;
import org.socraticgrid.example.model.entity.Entity;

@SemanticResource(iri = AbstractClinicalModel.BASE_IRI
		+ "/RoomToClinicAssignment")
@AutoAnnotate(value = true, baseIri = AbstractClinicalModel.BASE_IRI, separator = '/')
public class RoomToClinicAssignment extends Entity {

	public static final String ENTITY_NAME = "RoomToClinic";
	private Room room;
	private Clinic clinic;
	private List<AssignmentCalendar> assignmentCals;
	private List<TimeSlot> timeSlots;

	
	public RoomToClinicAssignment() {
		super();
		this.timeSlots = new ArrayList<TimeSlot>();
	}

	@Override
	public String generateDefaultInstanceIdentifier() {
		return getBaseIri() + "/" + getCanonicalName() + "/"
				+ UUID.randomUUID().toString(); // TODO Fix
	}

	@Override
	public String getCanonicalName() {
		return ENTITY_NAME;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public void addAssingmentCalendar(AssignmentCalendar e) {
		assignmentCals.add(e);
	}

	
	public void addAllAssignmentCalendar(Collection<AssignmentCalendar> c) {
		assignmentCals.addAll(c);
	}

	public List<AssignmentCalendar> getAssignmentCals() {
		return assignmentCals;
	}

	
	public void setAssignmentCals(List<AssignmentCalendar> assignmentCals) {
		this.assignmentCals = assignmentCals;
	}

	public List<TimeSlot> getAllTimeslot(){
		return timeSlots;
	}
	public TimeSlot setTimeSlot(int i, String startTime, String endTime) {
		TimeSlot timeSlot = new TimeSlot();
		timeSlot.setStartTime(startTime);
		timeSlot.setEndTime(endTime);
		timeSlot.setDayOfWeek(i);
		timeSlots.add(timeSlot);
		return timeSlot;
	}

	public Clinic getClinic() {
		return clinic;
	}

	public void setClinic(Clinic clinic) {
		this.clinic = clinic;
	}

}
/* **********************************************************************************
Copyright (C) 2013 by Cognitive Medical Systems, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
* *********************************************************************************/